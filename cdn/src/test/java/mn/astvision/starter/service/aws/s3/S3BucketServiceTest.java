package mn.astvision.starter.service.aws.s3;

import com.amazonaws.AmazonServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author MethoD
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class S3BucketServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(S3BucketServiceTest.class);

    @Autowired
    private S3BucketService s3BucketService;

    @Test
    public void testPut() {
        try {
            LOGGER.info("Testing s3 put request...");
            LOGGER.info("Uploaded to: " + s3BucketService.upload("test-image2", "bla2.png", "bla 2".getBytes(), null));
        } catch (AmazonServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
