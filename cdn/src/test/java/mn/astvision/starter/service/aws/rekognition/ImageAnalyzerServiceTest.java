package mn.astvision.starter.service.aws.rekognition;

import com.amazonaws.services.rekognition.model.FaceDetail;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author MethoD
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ImageAnalyzerServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageAnalyzerServiceTest.class);

    @Autowired
    private ImageAnalyzerService imageAnalyzerService;

    private final ObjectMapper om = new ObjectMapper();

    private final String zassanPath = "E:/Dropbox/Work Files/UBEG/passport/passport photos example/zassan";
    private final String zasaaguiPath = "E:/Dropbox/Work Files/UBEG/passport/passport photos example/zasaagui";

    @Ignore
    @Test
    public void testWrong() {
        try {
            LOGGER.info("wrong 1 ->");
            FaceDetail faceDetail = imageAnalyzerService.analyze("D:/test-gp/wrong_1.jpg");
            LOGGER.info(om.writeValueAsString(faceDetail));

            LOGGER.info("wrong 2 ->");
            faceDetail = imageAnalyzerService.analyze("D:/test-gp/wrong_2.jpg");
            LOGGER.info(om.writeValueAsString(faceDetail));

            LOGGER.info("wrong 3 ->");
            faceDetail = imageAnalyzerService.analyze("D:/test-gp/wrong_3.jpg");
            LOGGER.info(om.writeValueAsString(faceDetail));
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Ignore
    @Test
    public void testZassan() {
        try {
            String[] files = new String[] {
                "ни84032926",
                "по72121005",
                "уч90111615",
                "фб73111965",
                "хб70101415",
                "чо62012664",
                "чс81111005",
                "щк14211004",
                "щш17302803",
                "щш17311004"};

            for (String file : files) {
                LOGGER.info(file + " ->");
                FaceDetail faceDetail = imageAnalyzerService.analyze(zassanPath + "/" + file + ".jpg");
                LOGGER.info(om.writeValueAsString(faceDetail));
                LOGGER.info("===================================================================");
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Ignore
    @Test
    public void testZasaagui() {
        try {
            String[] files = new String[] {
                "46936573_2233407070024681_5973848116263124992_n",
                "IMG_2513",
                "чс81111005",
                "glass"};

            for (String file : files) {
                LOGGER.info(file + " ->");
                FaceDetail faceDetail = imageAnalyzerService.analyze(zasaaguiPath + "/" + file + ".jpg");
                LOGGER.info(om.writeValueAsString(faceDetail));
                LOGGER.info("===================================================================");
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
