package mn.astvision.starter.repository;

import mn.astvision.starter.model.AstFile;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author digz6
 */
@Repository
public interface AstFileRepository extends MongoRepository<AstFile, String> {
}
