package mn.astvision.starter.repository;

import mn.astvision.starter.model.AstImage;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author digz6
 */
@Repository
public interface AstImageRepository extends MongoRepository<AstImage, String> {

    public AstImage findOneByIdAndCreatedBy(String id, String createdBy);
}
