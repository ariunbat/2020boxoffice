package mn.astvision.starter.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author MethoD
 */
@Data
@AllArgsConstructor
public class ImageCropData {

    private Integer imgW; // image width
    private Integer imgH; // image height
    private int canW; // canvas width
    private int canH; // canvas height

    // crop data
    private Integer x;
    private Integer y;
    private Integer w; // crop width
    private Integer h; // crop height
}
