package mn.astvision.starter.model;

import com.amazonaws.services.rekognition.model.BoundingBox;
import com.amazonaws.services.rekognition.model.Eyeglasses;
import com.amazonaws.services.rekognition.model.Sunglasses;
import lombok.Data;
import org.springframework.data.annotation.Transient;

/**
 *
 * @author MethoD
 */
@Data
public class ImageProcessResult {

    private String imageId;
    @Transient
    private String url;

    private BoundingBox boundingBox;
    private Eyeglasses eyeglasses;
    private Sunglasses sunglasses;
}
