package mn.astvision.starter.model;

import com.amazonaws.services.rekognition.model.FaceDetail;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author digz6
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AstImage extends BaseEntityWithUser {

    private String entity;
    private String entityId;

    private String fileId;
    private String fileExtension;
    private String fileName;
    private long fileSize;
    private String fileContentType;
    private String ipAddress;

    private String path;
    private String url;
    private FaceDetail faceDetail; // rekognition result
    //private ImageProcessResult imageProcessResult;

    private ImageCropData imageCropData;
    // cropped face url
    private String cropPath;
    private String cropUrl;
}
