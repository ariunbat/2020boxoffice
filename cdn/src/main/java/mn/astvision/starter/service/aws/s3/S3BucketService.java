package mn.astvision.starter.service.aws.s3;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectResult;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author MethoD
 */
@Service
public class S3BucketService {

    private static final Logger LOGGER = LoggerFactory.getLogger(S3BucketService.class);

    @Value("${aws.s3.accessKey}")
    private String accessKey;

    @Value("${aws.s3.secretKey}")
    private String secretKey;

    @Value("${aws.s3.bucket}")
    private String bucketName;

    private AmazonS3 s3Client;

    @PostConstruct
    private void init() {
        LOGGER.info("Initializing s3 bucket service with: " + accessKey + " - " + secretKey);
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);

        s3Client = AmazonS3ClientBuilder.standard()
                .withRegion(Regions.AP_SOUTHEAST_1)
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .build();
    }

    public String upload(String fileKey, String name, byte[] file, Map<String, String> metaData) throws AmazonServiceException {
        LOGGER.debug(fileKey + " -> uploading file + " + name + " to bucket: " + bucketName);
        InputStream fis = new ByteArrayInputStream(file);

        ObjectMetadata omd = new ObjectMetadata();
        if (metaData != null) {
            for (String key : metaData.keySet()) {
                omd.addUserMetadata(key, metaData.get(key));
            }
        }

        // TODO append file extension
        PutObjectResult putResult = s3Client.putObject(bucketName, fileKey, fis, omd);
        LOGGER.debug(fileKey + " -> file uploaded with etag: " + putResult.getETag());

        // set public read permission
        s3Client.setObjectAcl(bucketName, fileKey, CannedAccessControlList.PublicRead);

        return s3Client.getUrl(bucketName, fileKey).toString();
    }
}
