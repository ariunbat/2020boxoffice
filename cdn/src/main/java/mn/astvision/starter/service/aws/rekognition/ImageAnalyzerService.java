package mn.astvision.starter.service.aws.rekognition;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.AmazonRekognitionException;
import com.amazonaws.services.rekognition.model.Attribute;
import com.amazonaws.services.rekognition.model.DetectFacesRequest;
import com.amazonaws.services.rekognition.model.DetectFacesResult;
import com.amazonaws.services.rekognition.model.FaceDetail;
import com.amazonaws.services.rekognition.model.Image;
import com.amazonaws.util.IOUtils;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author digz6
 */
@Service
public class ImageAnalyzerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageAnalyzerService.class);

    @Value("${aws.rekognition.accessKey}")
    private String accessKey;

    @Value("${aws.rekognition.secretKey}")
    private String secretKey;

    private AmazonRekognition rekognitionClient;

    @PostConstruct
    private void init() {
        LOGGER.info("Initializing image analyzer service with: " + accessKey + " - " + secretKey);
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(accessKey, secretKey);

        rekognitionClient = AmazonRekognitionClientBuilder.standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .build();
    }

    public FaceDetail analyze(String localPath) throws FileNotFoundException, IOException {
        //File file = new File(localPath);
        FileInputStream stream = new FileInputStream(localPath);
        return analyze(IOUtils.toByteArray(stream));
    }

    public FaceDetail analyze(byte[] bytes) throws IOException {
        return analyze(ByteBuffer.wrap(bytes));
    }

    public FaceDetail analyze(ByteBuffer bytes) throws AmazonRekognitionException {
        DetectFacesRequest request = new DetectFacesRequest().withImage(new Image().withBytes(bytes)).withAttributes(Attribute.ALL);

        try {
            DetectFacesResult result = rekognitionClient.detectFaces(request);
            List<FaceDetail> faceDetails = result.getFaceDetails();

            if (faceDetails.size() == 1) {
                return faceDetails.get(0);
            } else {
                // TODO multiple face, raise an error
            }
        } catch (AmazonRekognitionException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return null;
    }
}
