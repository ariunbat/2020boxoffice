package mn.astvision.starter.api.aws;

import com.amazonaws.AmazonServiceException;
import java.security.Principal;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import mn.astvision.starter.api.request.FileUploadRequest;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.service.aws.s3.S3BucketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MethoD
 */
@Secured("ROLE_MANAGE_DATA")
@RestController
@RequestMapping("/api/s3")
public class S3BucketApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(S3BucketApi.class);

    @Autowired
    private S3BucketService s3BucketService;

    @RequestMapping(value = "upload", method = RequestMethod.POST)
    public BaseResponse uploadImage(@RequestBody FileUploadRequest fileUploadRequest,
            Principal principal, Locale locale, HttpServletRequest servletRequest) {
        BaseResponse response = new BaseResponse();

        try {
            response.setData(s3BucketService.upload(
                    fileUploadRequest.getKey(),
                    fileUploadRequest.getName(),
                    fileUploadRequest.getFile(),
                    fileUploadRequest.getMetaData()));
            response.setResult(true);
        } catch (AmazonServiceException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(e.getMessage());
        }

        return response;
    }
}
