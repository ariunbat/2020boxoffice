package mn.astvision.starter.api;

import com.amazonaws.services.rekognition.model.FaceDetail;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Optional;
import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.model.AstImage;
import mn.astvision.starter.model.ImageCropData;
import mn.astvision.starter.model.ImageProcessResult;
import mn.astvision.starter.repository.AstImageRepository;
import mn.astvision.starter.service.aws.rekognition.ImageAnalyzerService;
import mn.astvision.starter.util.FileUtil;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author MethoD
 */
@Secured("ROLE_DEFAULT")
@RestController
@RequestMapping("/api/image")
public class ImageApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageApi.class);

    @Value("${fileUploadRootPath}")
    private String fileUploadRootPath;

    @Value("${fileUploadPath}")
    private String fileUploadPath;

    @Value("${fileServePath}")
    private String fileServePath;

    @Autowired
    private AstImageRepository astImageRepository;

    @Autowired
    private ImageAnalyzerService imageAnalyzerService;

    @Autowired
    private MessageSource messageSource;

    @PostConstruct
    public void setup() {
        if (StringUtils.isEmpty(fileUploadRootPath)) {
            fileUploadRootPath = System.getProperty("user.home");
        }
    }

    @RequestMapping(value = "upload", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public BaseResponse upload(
            @RequestParam("entity") String entity,
            @RequestParam(name = "entityId", required = false) String entityId,
            @RequestPart("file") MultipartFile file,
            Locale locale,
            Principal principal,
            HttpServletRequest servletRequest) {

        BaseResponse baseResponse = new BaseResponse();

        try {
            LOGGER.debug("Image upload and process request: {entity: " + entity + ", entityId: " + entityId + "}");

            if (!StringUtils.isEmpty(entity) && !file.isEmpty()) {
                ImageProcessResult imageProcessResponse = new ImageProcessResult();

                LOGGER.debug("File details: {name: " + file.getOriginalFilename() + ", contentType:" + file.getContentType() + ", size: " + file.getSize() + "}");
                String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename()).toLowerCase();
                LOGGER.debug("File extension: " + fileExtension);

                AstImage astImage = new AstImage();
                astImage.setEntity(entity);
                astImage.setEntityId(entityId);
                //astFile.setFileId(fileId);
                astImage.setFileName(file.getOriginalFilename());
                astImage.setFileExtension(fileExtension);
                astImage.setFileSize(file.getSize());
                astImage.setFileContentType(file.getContentType());
                astImage.setIpAddress(servletRequest.getRemoteAddr());
                astImage.setCreatedBy(principal.getName());
                astImage.setCreatedDate(LocalDateTime.now());
                astImage = astImageRepository.save(astImage);

                // do upload
                String outputPath = entity + "/";
                if (!StringUtils.isEmpty(entityId)) {
                    outputPath += entityId + "/";
                } else {
                    outputPath += LocalDate.now().toString() + "/";
                }
                String fileName = astImage.getId() + "." + fileExtension;
                String fullPath = fileUploadRootPath + "/" + fileUploadPath + "/" + outputPath + fileName;
                LOGGER.debug("Creating file at: " + fullPath);
                FileUtil.createFile(fileUploadRootPath, fileUploadPath + "/" + outputPath, fileName, file.getBytes());

                // set serve url
                astImage.setPath(outputPath + "/" + fileName);
                astImage.setUrl(fileServePath + "/" + outputPath + fileName);

                // set crop url
                String cropFileName = astImage.getId() + "_crop." + fileExtension;
                astImage.setCropPath(outputPath + cropFileName);
                astImage.setCropUrl(fileServePath + "/" + outputPath + cropFileName);

                FaceDetail faceDetail = imageAnalyzerService.analyze(fullPath);
                astImage.setFaceDetail(faceDetail);
                astImage = astImageRepository.save(astImage);

                if (faceDetail != null) {
                    imageProcessResponse.setBoundingBox(faceDetail.getBoundingBox());
                    imageProcessResponse.setEyeglasses(faceDetail.getEyeglasses());
                    imageProcessResponse.setSunglasses(faceDetail.getSunglasses());
                }
                imageProcessResponse.setUrl(astImage.getUrl());
                //astImage.setImageProcessResult(imageProcessResponse);

                imageProcessResponse.setImageId(astImage.getId());
                baseResponse.setData(imageProcessResponse);
                baseResponse.setResult(true);
            } else {
                baseResponse.setMessage(messageSource.getMessage("error.invalidRequest", null, locale));
            }
        } catch (IOException | NoSuchMessageException e) {
            LOGGER.error(e.getMessage(), e);
            baseResponse.setResult(false);
            baseResponse.setMessage(e.getMessage());
        }

        return baseResponse;
    }

    @RequestMapping(value = "crop", method = RequestMethod.POST)
    public BaseResponse crop(
            @RequestParam("imageId") String imageId,
            @RequestParam("img_w") Float img_w,
            @RequestParam("img_h") Float img_h,
            @RequestParam("can_w") Float can_w,
            @RequestParam("can_h") Float can_h,
            @RequestParam("x") Float x,
            @RequestParam("y") Float y,
            @RequestParam("w") Float w,
            @RequestParam("h") Float h,
            Locale locale,
            Principal principal,
            HttpServletRequest servletRequest) {

        BaseResponse baseResponse = new BaseResponse();

        try {
            LOGGER.debug("Image crop request: {imageId: " + imageId + ", img_w: " + img_w + ", img_h: " + img_h
                    + ", can_w: " + can_w + ", can_h: " + can_h
                    + ", x: " + x + ", y: " + y + ", w: " + w + ", h: " + h + "}");

            if (!StringUtils.isEmpty(imageId)
                    && img_w != null && img_h != null
                    && can_w != null && can_h != null
                    && x != null && y != null && w != null && h != null) {
                Optional<AstImage> astImageOpt = astImageRepository.findById(imageId);
                if (astImageOpt.isPresent()) {
                    AstImage astImage = astImageOpt.get();
                    astImage.setImageCropData(new ImageCropData(
                            img_w.intValue(),
                            img_h.intValue(),
                            can_w.intValue(),
                            can_h.intValue(),
                            x.intValue(),
                            y.intValue(),
                            w.intValue(),
                            h.intValue()));
                    astImageRepository.save(astImage);

                    float ratio = Math.max(img_w / can_w, img_h / can_h);
                    //LOGGER.debug("ratio: " + ratio);
                    int cropX = new Float(x * ratio).intValue();
                    int cropY = new Float(y * ratio).intValue();
                    int cropW = new Float(w * ratio).intValue();
                    if (cropW > img_w.intValue() - cropX) {
                        cropW = img_w.intValue() - cropX;
                    }
                    int cropH = new Float(h * ratio).intValue();
                    if (cropH > img_h.intValue() - cropY) {
                        cropH = img_h.intValue() - cropY;
                    }
                    LOGGER.debug("Image crop size -> cropX: " + cropX + ", cropY: " + cropY + ", cropW: " + cropW + ", cropH: " + cropH);

                    //LOGGER.debug("Opening file: " + fileUploadRootPath + "/" + fileUploadPath + "/" + astImage.getPath());
                    BufferedImage croppedImage = ImageIO.read(
                            new File(fileUploadRootPath + "/" + fileUploadPath + "/" + astImage.getPath()))
                            .getSubimage(cropX, cropY, cropW, cropH);
                    ImageIO.write(croppedImage, "JPG", new File(fileUploadRootPath + "/" + fileUploadPath + "/" + astImage.getCropPath()));

                    baseResponse.setData(astImage.getCropUrl()); // crop path
                    baseResponse.setResult(true);
                } else {
                    baseResponse.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                baseResponse.setMessage(messageSource.getMessage("error.invalidRequest", null, locale));
            }
        } catch (IOException | NoSuchMessageException e) {
            LOGGER.error(e.getMessage(), e);
            baseResponse.setResult(false);
            baseResponse.setMessage(e.getMessage());
        }

        return baseResponse;
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    public BaseResponse delete(
            @RequestParam("id") String imageId,
            Principal principal,
            HttpServletRequest servletRequest) {

        LOGGER.debug("Delete file: {ip: " + servletRequest.getRemoteAddr() + ", entity: " + imageId + ", regnum:" + principal.getName() + "}");
        BaseResponse baseResponse = new BaseResponse();

        try {
            AstImage astImage = astImageRepository.findOneByIdAndCreatedBy(imageId, principal.getName());
            if (astImage != null) {
                if (StringUtils.isEmpty(fileUploadRootPath)) {
                    fileUploadRootPath = System.getProperty("user.home");
                }
                String fullPath = fileUploadRootPath + astImage.getPath();
                LOGGER.debug("Delete file: " + fullPath + ", result: " + FileUtil.deleteFile(fullPath)); // try to delete file

                astImage.setDeleted(true);
                astImageRepository.save(astImage);

                baseResponse.setResult(true);
            } else {
                baseResponse.setMessage("File not found!");
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            baseResponse.setResult(false);
            baseResponse.setMessage(e.getMessage());
        }

        return baseResponse;
    }
}
