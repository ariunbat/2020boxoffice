package mn.astvision.starter.api.aws;

import java.io.IOException;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.service.aws.rekognition.ImageAnalyzerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author MethoD
 */
@RestController
@RequestMapping("/api/image-analyze")
@Secured("ROLE_DEFAULT")
public class ImageAnalyzerApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageAnalyzerApi.class);

    @Value("${fileUploadRootPath}")
    private String fileUploadRootPath;

    @Value("${fileUploadPath}")
    private String fileUploadPath;

    @Autowired
    private ImageAnalyzerService imageAnalyzerService;

    @PostMapping("analyze-local")
    public BaseResponse analyze(String file) {
        BaseResponse response = new BaseResponse();

        try {
            response.setData(imageAnalyzerService.analyze(fileUploadRootPath + "/" + fileUploadPath + "/" + file));
            response.setResult(true);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(e.getMessage());
        }

        return response;
    }

    @PostMapping("analyze")
    public BaseResponse analyze(@RequestPart("file") MultipartFile file) {
        BaseResponse response = new BaseResponse();

        try {
            response.setData(imageAnalyzerService.analyze(file.getBytes()));
            response.setResult(true);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(e.getMessage());
        }

        return response;
    }
}
