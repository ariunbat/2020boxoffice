package mn.astvision.starter.api;

import mn.astvision.starter.api.response.BaseResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author digz6
 */
@RestController
@RequestMapping("/api/test")
public class TestApi {

    @GetMapping("")
    public BaseResponse get() {
        BaseResponse response = new BaseResponse();
        response.setMessage("Working");
        response.setResult(true);

        return response;
    }
}
