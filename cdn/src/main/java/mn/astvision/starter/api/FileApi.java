package mn.astvision.starter.api;

import java.io.IOException;
import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.model.AstFile;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.repository.AstFileRepository;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.util.FileUtil;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author MethoD
 */
@Secured("ROLE_DEFAULT")
@RestController
@RequestMapping("/api/file")
public class FileApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileApi.class);

    @Value("${fileUploadRootPath}")
    private String fileUploadRootPath;

    @Value("${fileUploadPath}")
    private String fileUploadPath;

    @Value("${fileServePath}")
    private String fileServePath;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AstFileRepository astFileRepository;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "upload", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public BaseResponse uploadImage(
            @RequestParam("entity") String entity,
            @RequestParam(name = "entityId", required = false) String entityId,
            @RequestPart("file") MultipartFile file,
            Principal principal, Locale locale,
            HttpServletRequest servletRequest) {

        BaseResponse baseResponse = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                LOGGER.debug("File upload request: {entity: " + entity + ", entityId: " + entityId + "}");

                if (!StringUtils.isEmpty(entity) && !file.isEmpty()) {
                    LOGGER.debug("File details: {name: " + file.getOriginalFilename() + ", contentType:" + file.getContentType() + ", size: " + file.getSize() + "}");
                    String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename()).toLowerCase();

                    AstFile astFile = new AstFile();
                    astFile.setEntity(entity);
                    astFile.setEntityId(entityId);
                    //astFile.setFileId(fileId);
                    astFile.setFileName(file.getOriginalFilename());
                    astFile.setFileExtension(fileExtension);
                    astFile.setFileSize(file.getSize());
                    astFile.setFileContentType(file.getContentType());

                    astFile.setIpAddress(servletRequest.getRemoteAddr());
                    astFile.setCreatedBy(user.getId());
                    astFile.setCreatedDate(LocalDateTime.now());
                    astFile = astFileRepository.save(astFile);

                    // do upload
                    if (StringUtils.isEmpty(fileUploadRootPath)) {
                        fileUploadRootPath = System.getProperty("user.home");
                    }

                    String outputPath = entity + "/";
                    if (!StringUtils.isEmpty(entityId)) {
                        outputPath += entityId + "/";
                    } else {
                        outputPath += LocalDate.now().toString() + "/";
                    }
                    String fileName = astFile.getId() + "." + fileExtension;
                    FileUtil.createFile(fileUploadRootPath, fileUploadPath + "/" + outputPath, fileName, file.getBytes());

                    // set serve url
                    astFile.setUrl(fileServePath + "/" + outputPath + fileName);
                    astFileRepository.save(astFile);

                    baseResponse.setData(astFile);
                    baseResponse.setResult(true);
                } else {
                    baseResponse.setMessage("Incomplete request or empty file!");
                }
            } else {
                baseResponse.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (IOException | NoSuchMessageException e) {
            LOGGER.error(e.getMessage(), e);
            baseResponse.setResult(false);
            baseResponse.setMessage(e.getMessage());
        }

        return baseResponse;
    }

    /*@RequestMapping(value = "delete", method = RequestMethod.POST)
    public BaseResponse deleteImage(
            @RequestParam("entity") String entity,
            @RequestParam("entityId") String entityId,
            @RequestParam("fileUrl") String fileUrl,
            HttpServletRequest servletRequest) {

        LOGGER.debug("Delete file: {ip: " + servletRequest.getRemoteAddr() + ", entity: " + entity + ", entityId:" + entityId + ", fileUrl: " + fileUrl + "}");
        BaseResponse baseResponse = new BaseResponse();

        try {
            AstFile astFile = astFileRepository.get(entity, entityId, fileUrl, userId);
            if (astFile != null) {
                String folderPath = System.getProperty("user.home") + "/" + fileUploadPath;
                String filePath = entity + "/" + entityId + "/" + astFile.getFileId() + "." + astFile.getFileExtension();
                LOGGER.debug("Delete file: " + filePath + ", result: " + FileUtil.deleteFile(folderPath + "/" + filePath)); // try to delete file

                astFileRepository.delete(astFile); // delete file record?
                baseResponse.setResult(true);
            } else {
                baseResponse.setMessage("File not found!");
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            baseResponse.setResult(false);
            baseResponse.setMessage(e.getMessage());
        }

        return baseResponse;
    }*/
}
