Module path and ports on dev machine:

public backend      -> :8080/
back-office backend -> :8081/manage
Email API           -> :8082/email-api
Analytics API       -> :8083/analytics-api
Notification API    -> :8084/notification-api
CDN                 -> :9090/file-server
