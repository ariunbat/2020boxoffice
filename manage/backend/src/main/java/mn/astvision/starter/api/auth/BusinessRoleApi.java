package mn.astvision.starter.api.auth;

import com.mongodb.MongoException;
import java.util.Locale;
import java.util.Optional;
import mn.astvision.starter.api.request.MultiDeleteRequest;
import mn.astvision.starter.api.request.antd.AntdPagination;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.api.response.antd.AntdTableDataList;
import mn.astvision.starter.dao.BusinessRoleDAO;
import mn.astvision.starter.model.auth.BusinessRole;
import mn.astvision.starter.model.enums.ApplicationRole;
import mn.astvision.starter.repository.auth.BusinessRoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MethoD
 */
@RestController
@RequestMapping("/api/business-role")
@Secured("ROLE_MANAGE_BUSINESS_ROLE")
public class BusinessRoleApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessRoleApi.class);

    @Autowired
    private BusinessRoleRepository businessRoleRepository;

    @Autowired
    private BusinessRoleDAO businessRoleDAO;

    @Autowired
    private MessageSource messageSource;

    @GetMapping
    public BaseResponse list(String role, ApplicationRole applicationRoles, AntdPagination pagination, Locale locale) {
        LOGGER.debug("List business role > role: " + role + ", applicationRoles: " + applicationRoles + ", pagination: " + pagination);
        BaseResponse response = new BaseResponse();

        try {
            AntdTableDataList<BusinessRole> listData = new AntdTableDataList<>();

            pagination.setTotal(businessRoleRepository.count());
            listData.setPagination(pagination);
            listData.setList(businessRoleDAO.find(role, applicationRoles,
                    PageRequest.of(pagination.getCurrentPage(), pagination.getPageSize())));

            LOGGER.debug("Total business roles: " + pagination.getTotal());
            response.setData(listData);
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("create")
    public BaseResponse create(
            @RequestBody BusinessRole businessRole,
            Locale locale) {

        LOGGER.debug("Create business role: " + businessRole.getRole());
        BaseResponse response = new BaseResponse();

        try {
            if (!businessRoleRepository.existsById(businessRole.getRole())) {
                businessRole = businessRoleRepository.save(businessRole);

                response.setData(businessRole.getRole());
                response.setResult(true);
            } else {
                response.setMessage(messageSource.getMessage("data.exists", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("update")
    public BaseResponse update(
            @RequestBody BusinessRole businessRole,
            Locale locale) {

        LOGGER.debug("Update business role: " + businessRole);
        BaseResponse response = new BaseResponse();

        try {
            Optional<BusinessRole> businessRoleOpt = businessRoleRepository.findById(businessRole.getKey());
            if (businessRoleOpt.isPresent()) {
                BusinessRole oldBusinessRole = businessRoleOpt.get();
                oldBusinessRole.setApplicationRoles(businessRole.getApplicationRoles());

                businessRoleRepository.save(oldBusinessRole);
                response.setResult(true);
            } else {
                response.setMessage(messageSource.getMessage("data.notFound", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("delete-multi")
    public BaseResponse deleteMulti(
            @RequestBody MultiDeleteRequest multiDeleteRequest,
            Locale locale) {
        LOGGER.debug("Deleting business roles: " + multiDeleteRequest);
        BaseResponse response = new BaseResponse();

        try {
            if (multiDeleteRequest.getIds() != null) {
                for (String role : multiDeleteRequest.getIds()) {
                    businessRoleRepository.deleteById(role);
                }

                response.setResult(true);
            } else {
                response.setMessage(messageSource.getMessage("error.invalidRequest", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }
}
