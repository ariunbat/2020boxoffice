package mn.astvision.starter.api.citizen;

import com.mongodb.MongoException;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.model.citizen.ColleagueType;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.repository.citizen.ColleagueTypeRepository;
import mn.astvision.starter.service.citizen.ColleagueTypeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Optional;

/**
 * @author Ariuka
 */
@RestController
@RequestMapping("/api/colleague-type")
public class ColleagueTypeApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(ColleagueTypeApi.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ColleagueTypeRepository colleagueTypeRepository;

    @Autowired
    private ColleagueTypeService colleagueTypeService;

    @GetMapping
    public BaseResponse list(Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("ColleagueType list");

        try {
            response.setData(colleagueTypeService.list(locale));
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }


    @PostMapping("create")
    @Secured("ROLE_MANAGE_CITIZEN")
    public BaseResponse create(
            @RequestBody ColleagueType createRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("create colleagueType request: " + createRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                ColleagueType colleagueType = new ColleagueType();
                colleagueType.setName(createRequest.getName());
                colleagueType.setCreatedBy(user.getId());
                colleagueType.setCreatedDate(LocalDateTime.now());
                colleagueType = colleagueTypeRepository.save(colleagueType);

                response.setData(colleagueType.getId());
                response.setResult(true);
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("update")
    @Secured("ROLE_MANAGE_CITIZEN")
    public BaseResponse update(
            @RequestBody ColleagueType updateRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("update colleagueType request" + updateRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Optional<ColleagueType> colleagueTypeOptional = colleagueTypeRepository.findById(updateRequest.getId());
                if (colleagueTypeOptional.isPresent()) {
                    ColleagueType colleagueType = colleagueTypeOptional.get();
                    colleagueType.setName(updateRequest.getName());
                    colleagueType.setModifiedBy(user.getId());
                    colleagueType.setModifiedDate(LocalDateTime.now());
                    colleagueType = colleagueTypeRepository.save(colleagueType);
                    response.setData(colleagueType.getId());
                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("delete")
    public BaseResponse delete(
            @RequestParam String id,
            Locale locale, Principal principal) {
        LOGGER.debug("Deleting colleagueType: " + id);
        BaseResponse response = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {

                Optional<ColleagueType> colOpt = colleagueTypeRepository.findById(id);
                if (colOpt.isPresent()) {
                    ColleagueType colleagueType = colOpt.get();
                    colleagueType.setDeleted(true);
                    colleagueType.setModifiedDate(LocalDateTime.now());
                    colleagueType.setModifiedBy(user.getId());
                    colleagueTypeRepository.save(colleagueType);

                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }
}
