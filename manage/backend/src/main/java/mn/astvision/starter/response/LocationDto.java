package mn.astvision.starter.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author enkhd on !13/05/2019
 */
@Data
@Builder
@AllArgsConstructor
public class LocationDto {
  private int locationId;
  private int parentId;
  @JsonProperty("name")
  private String nameMn;
  private boolean isLeaf;
}
