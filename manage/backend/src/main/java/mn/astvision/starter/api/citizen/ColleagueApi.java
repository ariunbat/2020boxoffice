package mn.astvision.starter.api.citizen;

import com.mongodb.MongoException;
import mn.astvision.starter.api.request.ColleagueRequest;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.model.citizen.Colleague;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.repository.citizen.ColleagueRepository;
import mn.astvision.starter.service.citizen.ColleagueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Optional;

/**
 * @author Ariuka
 */
@RestController
@RequestMapping("/api/colleague")
public class ColleagueApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(ColleagueApi.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ColleagueRepository colleagueRepository;

    @Autowired
    private ColleagueService colleagueService;

    @GetMapping
    public BaseResponse list(String citizenId, Boolean deleted, Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("Colleague list: citizenId: " + citizenId + " deleted: " + deleted);

        try {
            response.setData(colleagueService.list(citizenId, deleted, locale));
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @GetMapping("select-list")
    public BaseResponse selectList(String type, String name, Locale locale) {
        BaseResponse response = new BaseResponse();

        try {
            response.setData(colleagueService.selectList(type, name).size() > 0 ? colleagueService.selectList(type, name).get(0) : new ArrayList<>());
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }


    @PostMapping("create")
    @Secured("ROLE_MANAGE_CITIZEN")
    public BaseResponse create(
            @RequestBody ColleagueRequest createRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("create colleague request: " + createRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Colleague colleague = new Colleague();
                colleague.setCitizenId(createRequest.getCitizenId());
                colleague.setColleagueTypeId(createRequest.getColleagueTypeId());
                colleague.setName(createRequest.getName());
                colleague.setDescription(createRequest.getDescription());
                colleague.setStartDate(createRequest.getStartDate());
                colleague.setEndDate(createRequest.getEndDate());
                colleague.setParticipation(createRequest.getParticipation());
                colleague.setCreatedBy(user.getId());
                colleague.setCreatedDate(LocalDateTime.now());
                colleague = colleagueRepository.save(colleague);

                response.setData(colleague.getId());
                response.setResult(true);
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("update")
    @Secured("ROLE_MANAGE_CITIZEN")
    public BaseResponse update(
            @RequestBody ColleagueRequest updateRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("update colleague request" + updateRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Optional<Colleague> colleagueOptional = colleagueRepository.findById(updateRequest.getId());
                if (colleagueOptional.isPresent()) {
                    Colleague colleague = colleagueOptional.get();
                    colleague.setCitizenId(updateRequest.getCitizenId());
                    colleague.setColleagueTypeId(updateRequest.getColleagueTypeId());
                    colleague.setName(updateRequest.getName());
                    colleague.setDescription(updateRequest.getDescription());
                    colleague.setStartDate(updateRequest.getStartDate());
                    colleague.setEndDate(updateRequest.getEndDate());
                    colleague.setParticipation(updateRequest.getParticipation());
                    colleague.setModifiedBy(user.getId());
                    colleague.setModifiedDate(LocalDateTime.now());
                    colleague = colleagueRepository.save(colleague);
                    response.setData(colleague.getId());
                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("delete")
    public BaseResponse delete(
            @RequestParam String id,
            Locale locale, Principal principal) {
        LOGGER.debug("Deleting colleague: " + id);
        BaseResponse response = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {

                Optional<Colleague> colOpt = colleagueRepository.findById(id);
                if (colOpt.isPresent()) {
                    Colleague colleague = colOpt.get();
                    colleague.setDeleted(true);
                    colleague.setModifiedDate(LocalDateTime.now());
                    colleague.setModifiedBy(user.getId());
                    colleagueRepository.save(colleague);

                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }
}
