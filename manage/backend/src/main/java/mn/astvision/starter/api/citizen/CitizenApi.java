package mn.astvision.starter.api.citizen;

import com.mongodb.MongoException;
import mn.astvision.starter.api.request.antd.AntdPagination;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.api.response.antd.AntdTableDataList;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.model.citizen.Citizen;
import mn.astvision.starter.model.citizen.CitizenDto;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.repository.citizen.CitizenRepository;
import mn.astvision.starter.service.citizen.CitizenService;
import mn.astvision.starter.util.BirthDateGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author Ariuka
 */
@RestController
@RequestMapping("/api/citizen")
public class CitizenApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(CitizenApi.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private CitizenService citizenService;

    @Autowired
    private CitizenRepository citizenRepository;

    @Autowired
    private BirthDateGenerator birthDateGenerator;

    @GetMapping
    public BaseResponse list(String registerNumber, String firstName, String lastName, String familyName, Integer[] locationCode,
                             String partyId, String boardId,
                             @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam(required = false) LocalDate birthStart,
                             @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam(required = false) LocalDate birthEnd,
                             Boolean deleted, AntdPagination pagination, Principal principal, Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("Citizen list: registerNumber: " + registerNumber + " firstName: " + firstName + " lastName: " + lastName + " familyName: " + familyName +
                " locationCode: " + Arrays.toString(locationCode) + " boardId: " + boardId
                + " birthStart: " + birthStart + " birthEnd: " + birthEnd + " deleted: " + deleted);

        User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());

        List<Integer> locationCodeList = new ArrayList<>();
        if (locationCode != null && locationCode.length != 0) {
            locationCodeList = Arrays.asList(locationCode[locationCode.length -1 ]);
        } else {
            if (user != null) {
                locationCodeList = user.getLocationCodes();
            }
        }

        try {
            AntdTableDataList<CitizenDto> listData = new AntdTableDataList<>();

            pagination.setTotal(citizenService.count(registerNumber, firstName, lastName, familyName, locationCodeList, boardId, birthStart, birthEnd, deleted));
            listData.setPagination(pagination);
            listData.setList(citizenService.list(registerNumber, firstName, lastName, familyName, locationCodeList, boardId, birthStart, birthEnd, deleted,
                    PageRequest.of(pagination.getCurrentPage(), pagination.getPageSize()), locale));

            response.setData(listData);
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @GetMapping("member-list")
    public BaseResponse memberList(String registerNumber, String firstName, String lastName, String familyName, Integer[] locationCode,
                                   String partyId, String boardId,
                                   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam(required = false) LocalDate birthStart,
                                   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @RequestParam(required = false) LocalDate birthEnd,
                                   Boolean deleted, AntdPagination pagination, Principal principal, Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("Member list: registerNumber: " + registerNumber + " firstName: " + firstName + " lastName: " + lastName + " familyName: " + familyName +
                " locationCode: " + Arrays.toString(locationCode) + " partyId: " + partyId + " boardId: " + boardId
                + " birthStart: " + birthStart + " birthEnd: " + birthEnd + " deleted: " + deleted);

        User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());

        List<Integer> locationCodeList = new ArrayList<>();
        if (locationCode != null && locationCode.length != 0) {
            locationCodeList = Arrays.asList(locationCode[locationCode.length -1 ]);
        } else {
            if (user != null) {
                locationCodeList = user.getLocationCodes();
            }
        }

        try {
            AntdTableDataList<CitizenDto> listData = new AntdTableDataList<>();

            pagination.setTotal(citizenService.memberCount(registerNumber, firstName, lastName, familyName, locationCodeList, boardId, partyId, birthStart, birthEnd, deleted));
            listData.setPagination(pagination);
            listData.setList(citizenService.memberList(registerNumber, firstName, lastName, familyName, locationCodeList, partyId, boardId, birthStart, birthEnd, deleted,
                    PageRequest.of(pagination.getCurrentPage(), pagination.getPageSize()), locale));

            response.setData(listData);
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @GetMapping("{id}")
    public BaseResponse get(@PathVariable String id, Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("Get citizen: " + id);

        try {
            Citizen citizen = citizenService.get(id, locale);
            if (citizen != null) {
                response.setData(citizen);
                response.setResult(true);
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }
        return response;
    }

    @PostMapping("create-main-info")
    @Secured("ROLE_MANAGE_CITIZEN")
    public BaseResponse createMainInfo(
            @RequestBody Citizen createRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("create main info request" + createRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Optional<Citizen> citizenOptional = citizenRepository.findById(createRequest.getId());
                if (citizenOptional.isPresent()) {
                    Citizen citizen = citizenOptional.get();
                    citizen.setPhoto(createRequest.getPhoto());
                    citizen.setPhone(createRequest.getPhone());
                    citizen.setEmail(createRequest.getEmail());
                    citizen.setSocialAccount(createRequest.getSocialAccount());
                    citizen.setJob(createRequest.getJob());
                    citizen.setBirthPlaceCode(createRequest.getBirthPlaceCode());
                    citizen.setBirthPlaceAddress(createRequest.getBirthPlaceAddress());
                    citizen.setOfficialAddressCode(createRequest.getOfficialAddressCode());
//                    citizen.setOfficialAddressAttitude(createRequest.getOfficialAddressAttitude());
                    citizen = citizenRepository.save(citizen);
                    response.setData(citizen.getId());
                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("create")
    @Secured("ROLE_MANAGE_CITIZEN")
    public BaseResponse create(
            @RequestBody Citizen createRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("create request" + createRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Citizen citizen = new Citizen();
                citizen.setBoardId(createRequest.getBoardId());
                citizen.setPhoto(createRequest.getPhoto());
                citizen.setRegisterNumber(createRequest.getRegisterNumber());
                citizen.setFamilyName(createRequest.getFamilyName());
                citizen.setLastName(createRequest.getLastName());
                citizen.setFirstName(createRequest.getFirstName());
                citizen.setGender(createRequest.getGender());
                citizen.setBirthDate(birthDateGenerator.getBirthDateFromRegNum(createRequest.getRegisterNumber()));
                citizen.setLocationCode(createRequest.getLocationCode());
                citizen.setZipCode(createRequest.getLocationCode().get(createRequest.getLocationCode().size() - 1));
                citizen.setAddressType(createRequest.getAddressType());
                citizen.setTown(createRequest.getTown());
                citizen.setApartment(createRequest.getApartment());
                citizen.setApartmentAddress(createRequest.getApartmentAddress());
                citizen.setStreet(createRequest.getStreet());
                citizen.setGerAddress(createRequest.getGerAddress());
                citizen.setCountrySideAddress(createRequest.getCountrySideAddress());
                citizen.setCreatedBy(user.getId());
                citizen.setCreatedDate(LocalDateTime.now());
                citizen = citizenRepository.save(citizen);

                response.setData(citizen.getId());
                response.setResult(true);
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("update")
    @Secured("ROLE_MANAGE_CITIZEN")
    public BaseResponse update(
            @RequestBody Citizen updateRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("update request" + updateRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Optional<Citizen> citizenOptional = citizenRepository.findById(updateRequest.getId());
                if (citizenOptional.isPresent()) {
                    Citizen citizen = citizenOptional.get();
                    citizen.setPhoto(updateRequest.getPhoto());
                    citizen.setRegisterNumber(updateRequest.getRegisterNumber());
                    citizen.setFamilyName(updateRequest.getFamilyName());
                    citizen.setLastName(updateRequest.getLastName());
                    citizen.setFirstName(updateRequest.getFirstName());
                    citizen.setGender(updateRequest.getGender());
                    citizen.setBirthDate(birthDateGenerator.getBirthDateFromRegNum(updateRequest.getRegisterNumber()));
                    citizen.setLocationCode(updateRequest.getLocationCode());
                    citizen.setZipCode(updateRequest.getLocationCode().get(updateRequest.getLocationCode().size() - 1));
                    citizen.setAddressType(updateRequest.getAddressType());
                    citizen.setTown(updateRequest.getTown());
                    citizen.setApartment(updateRequest.getApartment());
                    citizen.setApartmentAddress(updateRequest.getApartmentAddress());
                    citizen.setStreet(updateRequest.getStreet());
                    citizen.setGerAddress(updateRequest.getGerAddress());
                    citizen.setCountrySideAddress(updateRequest.getCountrySideAddress());
                    citizen.setModifiedBy(user.getId());
                    citizen.setModifiedDate(LocalDateTime.now());
                    citizen = citizenRepository.save(citizen);

                    response.setData(citizen.getId());
                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("create-member")
    @Secured("ROLE_MANAGE_MEMBER")
    public BaseResponse createMember(
            @RequestBody Citizen createRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("create member request" + createRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Citizen citizen = new Citizen();
                Citizen citizenExisting = citizenRepository.findByRegisterNumber(createRequest.getRegisterNumber());
                if (citizenExisting != null) {
                    citizen = citizenExisting;
                } else {
                    citizen.setPhoto(createRequest.getPhoto());
                    citizen.setRegisterNumber(createRequest.getRegisterNumber());
                    citizen.setFamilyName(createRequest.getFamilyName());
                    citizen.setLastName(createRequest.getLastName());
                    citizen.setFirstName(createRequest.getFirstName());
                    citizen.setGender(createRequest.getGender());
                    citizen.setBirthDate(birthDateGenerator.getBirthDateFromRegNum(createRequest.getRegisterNumber()));
                    citizen.setCreatedBy(user.getId());
                    citizen.setCreatedDate(LocalDateTime.now());
                }
                citizen.setPartyId(createRequest.getPartyId());
                citizen.setPosition(createRequest.getPosition());
                citizen.setDescription(createRequest.getDescription());
                citizen = citizenRepository.save(citizen);

                response.setData(citizen.getId());
                response.setResult(true);
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("update-member")
    @Secured("ROLE_MANAGE_MEMBER")
    public BaseResponse updateMember(
            @RequestBody Citizen updateRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("update member request" + updateRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Optional<Citizen> citizenOptional = citizenRepository.findById(updateRequest.getId());
                if (citizenOptional.isPresent()) {
                    Citizen citizen = citizenOptional.get();
                    citizen.setPartyId(updateRequest.getPartyId());
                    citizen.setPosition(updateRequest.getPosition());
                    citizen.setDescription(updateRequest.getDescription());
                    citizen.setModifiedBy(user.getId());
                    citizen.setModifiedDate(LocalDateTime.now());
                    citizen = citizenRepository.save(citizen);

                    response.setData(citizen.getId());
                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("check-citizen-exists")
    public BaseResponse checkCitizenExists(@RequestBody Citizen request, Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("Check if citizen exists register number: " + request.getRegisterNumber());

        if (request.getRegisterNumber() != null) {
            Boolean exists = citizenRepository.existsByRegisterNumberAndDeletedFalse(request.getRegisterNumber());
            response.setResult(true);
            response.setData(exists);
        }
        return response;
    }

    @PostMapping("check-citizen")
    public BaseResponse checkCitizen(@RequestBody Citizen request, Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("check citizen exists: " + request.getRegisterNumber());

        try {
            Citizen citizen = citizenRepository.findByRegisterNumber(request.getRegisterNumber());
            if (citizen != null) {
                response.setData(citizen);
            }
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }
        return response;
    }
}
