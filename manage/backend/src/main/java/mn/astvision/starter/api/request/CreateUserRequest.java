package mn.astvision.starter.api.request;

import lombok.Data;
import mn.astvision.starter.model.FileData;

import java.util.List;

/**
 *
 * @author MethoD
 */
@Data
public class CreateUserRequest {

    private String id;
    private String username;
    private String password;
    private String lastName;
    private String firstName;
    private String familyName;
    private String businessRole;
    private String registerNumber;
    private String email;
    private List<Integer> phone;
    private String description;
    private FileData photo;
    private String boardId;
    private List<Integer> locationCodes;
}
