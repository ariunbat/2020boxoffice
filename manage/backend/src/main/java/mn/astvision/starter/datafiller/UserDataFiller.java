package mn.astvision.starter.datafiller;

import javax.annotation.PostConstruct;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.repository.auth.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 *
 * @author MethoD
 */
@Component
public class UserDataFiller {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void fill() {
        if (userRepository.count() == 0) {
            User userDev = new User("dev", passwordEncoder.encode("dev123"));
            userDev.setBusinessRole("DEVELOPER");
            userDev.setActive(true);
            userDev.setLastName("Astvision");
            userDev.setFirstName("Developer");
            userRepository.save(userDev);

            User userAdmin = new User("admin", passwordEncoder.encode("admin123"));
            userAdmin.setBusinessRole("ADMIN");
            userAdmin.setActive(true);
            userAdmin.setLastName("Astvision");
            userAdmin.setFirstName("Admin");
            userRepository.save(userAdmin);

            User user = new User("user", passwordEncoder.encode("user123"));
            user.setBusinessRole("USER");
            user.setActive(true);
            user.setLastName("Astvision");
            user.setFirstName("User");
            userRepository.save(user);
        }
    }
}
