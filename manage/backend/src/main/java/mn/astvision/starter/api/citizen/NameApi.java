package mn.astvision.starter.api.citizen;

import com.mongodb.MongoException;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.service.citizen.NameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Locale;

/**
 * @author Ariuka
 */
@RestController
@RequestMapping("/api/name")
public class NameApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(CitizenApi.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private NameService nameService;

    @GetMapping
    public BaseResponse list(String type, String name, Locale locale) {
        BaseResponse response = new BaseResponse();

        try {
                response.setData(nameService.list(type, name).size() > 0 ? nameService.list(type, name).get(0) : new ArrayList<>());
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }
}
