package mn.astvision.starter.api.auth;

import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.model.enums.ApplicationRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MethoD
 */
@RestController
@RequestMapping("/api/application-role")
@Secured("ROLE_MANAGE_BUSINESS_ROLE")
public class ApplicationRoleApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationRoleApi.class);

    @GetMapping
    public BaseResponse list() {
        LOGGER.info("List application role");
        BaseResponse response = new BaseResponse();

        response.setData(ApplicationRole.values());
        response.setResult(true);

        return response;
    }
}
