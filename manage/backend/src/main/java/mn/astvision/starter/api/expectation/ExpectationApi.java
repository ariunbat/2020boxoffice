package mn.astvision.starter.api.expectation;

import com.mongodb.MongoException;
import mn.astvision.starter.api.request.antd.AntdPagination;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.api.response.antd.AntdTableDataList;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.model.citizen.Citizen;
import mn.astvision.starter.model.expectation.Expectation;
import mn.astvision.starter.model.expectation.ExpectationDto;
import mn.astvision.starter.model.party.Party;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.repository.citizen.CitizenRepository;
import mn.astvision.starter.repository.expectation.ExpectationRepository;
import mn.astvision.starter.repository.party.PartyRepository;
import mn.astvision.starter.service.expectation.ExpectationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Locale;
import java.util.Optional;

/**
 * @author Ariuka
 */
@RestController
@RequestMapping("/api/expectation")
@Secured("ROLE_MANAGE_EXPECTATION")
public class ExpectationApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExpectationApi.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ExpectationRepository expectationRepository;

    @Autowired
    private ExpectationService expectationService;

    @Autowired
    private PartyRepository partyRepository;

    @Autowired
    private CitizenRepository citizenRepository;

    @GetMapping
    public BaseResponse list(Boolean deleted, AntdPagination pagination, Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("Expectation list deleted: " + deleted + " pagination: " + pagination);

        try {
            AntdTableDataList<ExpectationDto> listData = new AntdTableDataList<>();

            pagination.setTotal(expectationService.count(deleted, locale));
            listData.setPagination(pagination);
            listData.setList(expectationService.list(deleted, PageRequest.of(pagination.getCurrentPage(), pagination.getPageSize()), locale));

            response.setData(listData);
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("create")
    public BaseResponse create(
            @RequestBody Expectation createRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("create expectation request: " + createRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Expectation expectation = new Expectation();
                expectation.setCitizenId(createRequest.getCitizenId());
                expectation.setPartyId(createRequest.getPartyId());

                Optional<Party> partyOptional = partyRepository.findById(createRequest.getPartyId());
                if (partyOptional.isPresent()) {
                    if(partyOptional.get().getName().equals("Ардчилсан нам") || partyOptional.get().getName().equals("Монгол ардын хувьсгалт нам")) {
                        expectation.setColor(partyOptional.get().getColor());
                    } else {
                        expectation.setColor("#da00ff");
                    }
                }

                Optional<Citizen> citizenOptional = citizenRepository.findById(createRequest.getCitizenId());
                if (citizenOptional.isPresent()) {
                    Citizen citizen = citizenOptional.get();
                    expectation.setAge(Period.between( citizen.getBirthDate(), LocalDate.now()).getYears());
                    expectation.setGender(citizen.getGender());
                    expectation.setLocationCode(citizen.getLocationCode());
                }

                expectation.setPercent(createRequest.getPercent());
                expectation.setReason(createRequest.getReason());
                expectation.setNeedCalculation(createRequest.isNeedCalculation());
                expectation.setCreatedBy(user.getId());
                expectation.setCreatedDate(LocalDateTime.now());
                expectation = expectationRepository.save(expectation);

                response.setData(expectation.getId());
                response.setResult(true);
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }


    @PostMapping("delete")
    public BaseResponse delete(
            @RequestParam String id,
            Locale locale, Principal principal) {
        LOGGER.debug("Deleting expectation: " + id);
        BaseResponse response = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {

                Optional<Expectation> contOpt = expectationRepository.findById(id);
                if (contOpt.isPresent()) {
                    Expectation expectation = contOpt.get();
                    expectation.setDeleted(true);
                    expectation.setModifiedDate(LocalDateTime.now());
                    expectation.setModifiedBy(user.getId());
                    expectationRepository.save(expectation);

                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }
}
