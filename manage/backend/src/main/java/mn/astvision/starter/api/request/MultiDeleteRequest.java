package mn.astvision.starter.api.request;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author MethoD
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MultiDeleteRequest {

    private List<String> ids;
}
