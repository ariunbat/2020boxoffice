package mn.astvision.starter.api.board;

import com.mongodb.MongoException;
import mn.astvision.starter.api.request.antd.AntdPagination;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.api.response.antd.AntdTableDataList;
import mn.astvision.starter.model.Board;
import mn.astvision.starter.model.Location;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.repository.LocationRepository;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.repository.board.BoardRepository;
import mn.astvision.starter.service.board.BoardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Optional;

/**
 * @author Ariuka
 */
@RestController
@RequestMapping("/api/board")
public class BoardApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(BoardApi.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private BoardService boardService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private LocationRepository locationRepository;

    @GetMapping
    public BaseResponse list(String name, Integer code, AntdPagination pagination, Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("Board list name: " + name + " code: " + code);

        try {
            AntdTableDataList<Board> listData = new AntdTableDataList<>();

            pagination.setTotal(boardService.count(name, code, locale));
            listData.setPagination(pagination);
            listData.setList(boardService.list(name, code, PageRequest.of(pagination.getCurrentPage(), pagination.getPageSize()), locale));

            response.setData(listData);
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("create")
    @Secured("ROLE_MANAGE_DATA")
    public BaseResponse create(
            @RequestBody Board createRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("create board request: " + createRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Board board = new Board();
                board.setName(createRequest.getName());
                if (createRequest.getLocationCodes() != null) {
                    board.setLocationCodes(createRequest.getLocationCodes());

                    for (Integer code : createRequest.getLocationCodes()) {
                        Location location = locationRepository.findByLocationId(code);
                        if (location != null) {
                            location.setUsed(true);
                            locationRepository.save(location);
                        }
                    }
                }

                board.setCreatedBy(user.getId());
                board.setCreatedDate(LocalDateTime.now());
                board = boardRepository.save(board);

                response.setData(board.getId());
                response.setResult(true);
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("update")
    @Secured("ROLE_MANAGE_DATA")
    public BaseResponse update(
            @RequestBody Board updateRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("update board request" + updateRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Optional<Board> boardOptional = boardRepository.findById(updateRequest.getId());
                if (boardOptional.isPresent()) {
                    Board board = boardOptional.get();
                    board.setName(updateRequest.getName());
                    board.setLocationCodes(updateRequest.getLocationCodes());
                    board.setModifiedBy(user.getId());
                    board.setModifiedDate(LocalDateTime.now());
                    board = boardRepository.save(board);
                    response.setData(board.getId());
                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

}
