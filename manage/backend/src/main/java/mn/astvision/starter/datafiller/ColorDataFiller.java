package mn.astvision.starter.datafiller;

import mn.astvision.starter.model.party.Color;
import mn.astvision.starter.repository.party.ColorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;

/**
 * @author Ariuka
 */
@Component
public class ColorDataFiller {

    @Autowired
    private ColorRepository colorRepository;

    public List<Color> getColor() {
        return Arrays.asList(
                new Color("#14469f", false),
                new Color("#ae0717", false),
                new Color("#ff001b", false),
                new Color("#005f32", false),
                new Color("#169eaf", false),
                new Color("#1666d5", false),
                new Color("#64ade0", false),
                new Color("#ff3dd6", false),
                new Color("#fecb00", false),
                new Color("#457d84", false),
                new Color("#6a2c8f", false),
                new Color("#ded512", false),
                new Color("#7fe5f0", false),
                new Color("#008080", false),
                new Color("#00ced1", false),
                new Color("#bada55", false),
                new Color("#a569bd", false),
                new Color("#99a3a4", false),
                new Color("#4dd0e1", false),
                new Color("#283593", false),
                new Color("#61f02a", false),
                new Color("#536cf0", false),
                new Color("#f08856", false),
                new Color("#2df024", false),
                new Color("#514b51", false),
                new Color("#f0ea00", false),
                new Color("#05f097", false),
                new Color("#b000f0", false),
                new Color("#7a8ff0", false),
                new Color("#87f001", false),
                new Color("#7f406e", false),
                new Color("#458554", false),
                new Color("#5e0cff", false),
                new Color("#00c6f0", false),
                new Color("#f05b5e", false),
                new Color("#4a1213", false),
                new Color("#f0572f", false),
                new Color("#f06292", false)
        );
    }

    @PostConstruct
    public void fill() {

        if (colorRepository.count() == 0) {
            colorRepository.saveAll(this.getColor());
        }
    }

}
