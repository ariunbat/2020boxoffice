package mn.astvision.starter.api.auth;

import com.mongodb.MongoException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import mn.astvision.starter.api.request.CreateUserRequest;
import mn.astvision.starter.api.request.antd.AntdPagination;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.api.response.antd.AntdTableDataList;
import mn.astvision.starter.dao.UserDAO;
import mn.astvision.starter.model.Board;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.repository.board.BoardRepository;
import mn.astvision.starter.service.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MethoD
 */
@RestController
@RequestMapping("/api/user")
public class UserApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserApi.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserService userService;

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MessageSource messageSource;

    @GetMapping
    @Secured("ROLE_MANAGE_USER")
    public BaseResponse list(String businessRole, String search, String boardId, Boolean deleted, AntdPagination pagination, Locale locale) {
        LOGGER.debug("List user > businessRole: " + businessRole + ", search: " + search + " boardId: " + boardId + ", deleted: " + deleted + ", pagination: " + pagination);
        BaseResponse response = new BaseResponse();

        try {
            AntdTableDataList<User> listData = new AntdTableDataList<>();

            pagination.setTotal(userRepository.count());
            listData.setPagination(pagination);
            listData.setList(userService.list(businessRole, search, boardId, deleted,
                    PageRequest.of(pagination.getCurrentPage(), pagination.getPageSize())));

            LOGGER.debug("Total users: " + pagination.getTotal());
            response.setData(listData);
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("create")
    @Secured("ROLE_MANAGE_USER")
    public BaseResponse create(
            @RequestBody CreateUserRequest createUserRequest,
            Locale locale, Principal principal) {

        LOGGER.debug("Create user: " + createUserRequest);
        BaseResponse response = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                if (!userRepository.existsByUsernameAndDeletedFalse(createUserRequest.getUsername())) {
                    User userToCreate = new User(createUserRequest.getUsername(), passwordEncoder.encode(createUserRequest.getPassword()));
                    userToCreate.setBusinessRole(createUserRequest.getBusinessRole());
                    userToCreate.setBoardId(createUserRequest.getBoardId());

                    if(createUserRequest.getBusinessRole().equals("ADMIN")) {
                        Optional<Board> boardOptional = boardRepository.findById(createUserRequest.getBoardId());
                        if (boardOptional.isPresent()) {
                            userToCreate.setLocationCodes(boardOptional.get().getLocationCodes());
                        }
                    }

                    if(createUserRequest.getBusinessRole().equals("USER")) {
                        userToCreate.setLocationCodes(createUserRequest.getLocationCodes());
                    }

                    userToCreate.setLastName(createUserRequest.getLastName());
                    userToCreate.setFirstName(createUserRequest.getFirstName());
                    userToCreate.setRegisterNumber(createUserRequest.getRegisterNumber());
                    userToCreate.setEmail(createUserRequest.getEmail());
                    userToCreate.setFamilyName(createUserRequest.getFamilyName());
                    userToCreate.setPhone(createUserRequest.getPhone());
                    userToCreate.setDescription(createUserRequest.getDescription());
                    userToCreate.setPhoto(createUserRequest.getPhoto());
                    userToCreate.setActive(true);
                    userToCreate.setCreatedDate(LocalDateTime.now());
                    userToCreate.setCreatedBy(user.getId());
                    userToCreate = userRepository.save(userToCreate);

                    response.setData(userToCreate.getId());
                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.exists", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("update")
    @Secured("ROLE_MANAGE_USER")
    public BaseResponse update(
            @RequestBody CreateUserRequest updateUserRequest,
            Locale locale, Principal principal) {

        LOGGER.debug("Update user: " + updateUserRequest);
        BaseResponse response = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Optional<User> userOpt = userRepository.findById(updateUserRequest.getId());
                if (!userRepository.existsByUsernameAndIdNotAndDeletedFalse(updateUserRequest.getUsername(), updateUserRequest.getId())) {
                    if (userOpt.isPresent()) {
                        User userToUpdate = userOpt.get();
                        userToUpdate.setUsername(updateUserRequest.getUsername());
                        if (!StringUtils.isEmpty(updateUserRequest.getPassword())) {
                            userToUpdate.setPassword(passwordEncoder.encode(updateUserRequest.getPassword()));
                            userToUpdate.setPasswordChangeDate(new Date());
                        }
                        userToUpdate.setLastName(updateUserRequest.getLastName());
                        userToUpdate.setFirstName(updateUserRequest.getFirstName());
                        userToUpdate.setRegisterNumber(updateUserRequest.getRegisterNumber());
                        userToUpdate.setEmail(updateUserRequest.getEmail());
                        userToUpdate.setFamilyName(updateUserRequest.getFamilyName());
                        userToUpdate.setPhone(updateUserRequest.getPhone());
                        userToUpdate.setDescription(updateUserRequest.getDescription());
                        userToUpdate.setPhoto(updateUserRequest.getPhoto());
                        userToUpdate.setModifiedDate(LocalDateTime.now());
                        userToUpdate.setModifiedBy(user.getId());

                        userRepository.save(userToUpdate);
                        response.setResult(true);
                    } else {
                        response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                    }
                } else {
                    response.setMessage(messageSource.getMessage("username.exists", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("delete")
    @Secured("ROLE_MANAGE_USER")
    public BaseResponse delete(
            @RequestParam String id,
            Locale locale, Principal principal) {
        LOGGER.debug("Deleting user: " + id);
        BaseResponse response = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Optional<User> userOpt = userRepository.findById(id);
                if (userOpt.isPresent()) {
                    User userToUpdate = userOpt.get();
                    userToUpdate.setDeleted(true);
                    userToUpdate.setModifiedDate(LocalDateTime.now());
                    userToUpdate.setModifiedBy(user.getId());
                    userRepository.save(userToUpdate);

                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }
}
