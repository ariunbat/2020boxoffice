package mn.astvision.starter.datafiller;

import mn.astvision.starter.model.AgeCalcDate;
import mn.astvision.starter.repository.AgeCalcDateRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

/**
 * @author Ariuka
 */
public class AgeCalcDateDataFiller {

    @Autowired
    private AgeCalcDateRepository ageCalcDateRepository;

    @PostConstruct
    public void fill(){
        if(ageCalcDateRepository.count() == 0) {
            AgeCalcDate ageCalcDate = new AgeCalcDate();
            ageCalcDate.setCheckDate(LocalDateTime.now());
        }
    }
}
