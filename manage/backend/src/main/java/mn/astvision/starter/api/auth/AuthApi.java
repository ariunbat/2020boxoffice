package mn.astvision.starter.api.auth;

import com.blueconic.browscap.Capabilities;
import com.mongodb.MongoException;
import java.util.Locale;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.api.response.auth.AuthResponse;
import mn.astvision.starter.model.enums.DeviceType;
import mn.astvision.starter.auth.TokenUtil;
import mn.astvision.starter.model.auth.BusinessRole;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.repository.auth.BusinessRoleRepository;
import mn.astvision.starter.repository.auth.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author digz6
 */
@RestController
@RequestMapping("/api/auth")
public class AuthApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthApi.class);

    @Value("${token.header}")
    private String tokenHeader;

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BusinessRoleRepository businessRoleRepository;

    @PostMapping("login")
    //@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:8000"})
    public BaseResponse login(
            @RequestParam String username,
            @RequestParam String password,
            Capabilities userAgent,
            Locale locale) {
        LOGGER.debug("Login in with: " + username + " - " + password);
        //LOGGER.debug("UserAgent: " + userAgent);
        BaseResponse response = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(username);
            if (user != null && passwordEncoder.matches(password, user.getPassword())) {
                if (user.isActive()) {
                    AuthResponse authResponse = new AuthResponse();
                    authResponse.setToken(tokenUtil.generateToken(user.getUsername(), DeviceType.fromString(userAgent != null ? userAgent.getDeviceType() : "")));
                    authResponse.setUserId(user.getId());
                    authResponse.setBoardId(user.getBoardId());

                    Optional<BusinessRole> businessRoleOpt = businessRoleRepository.findById(user.getBusinessRole());
                    if (businessRoleOpt.isPresent()) {
                        authResponse.setBusinessRole(businessRoleOpt.get());
                    }

                    response.setData(authResponse);
                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("login.accountDisabled", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("login.usernameOrPasswordWrong", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        } catch (NullPointerException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.app", null, locale));
        }

        return response;
    }

    @GetMapping("refresh")
    public BaseResponse refreshToken(HttpServletRequest request) {
        BaseResponse response = new BaseResponse();

        String token = request.getHeader(tokenHeader);
        String username = tokenUtil.getUsernameFromToken(token);
        User user = userRepository.findByUsernameAndDeletedFalse(username);
        if (user != null && user.isActive() && tokenUtil.canTokenBeRefreshed(token, user.getPasswordChangeDate())) {
            AuthResponse authResponse = new AuthResponse();
            authResponse.setToken(tokenUtil.refreshToken(token));

            Optional<BusinessRole> businessRoleOpt = businessRoleRepository.findById(user.getBusinessRole());
            if (businessRoleOpt.isPresent()) {
                authResponse.setBusinessRole(businessRoleOpt.get());
            }

            response.setData(authResponse);
            response.setResult(true);
        }

        return response;
    }
}
