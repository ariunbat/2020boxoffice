package mn.astvision.starter.api.auth;

import com.blueconic.browscap.Capabilities;
import com.mongodb.MongoException;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.api.response.auth.AuthResponse;
import mn.astvision.starter.auth.TokenUtil;
import mn.astvision.starter.model.auth.BusinessRole;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.model.enums.DeviceType;
import mn.astvision.starter.repository.auth.BusinessRoleRepository;
import mn.astvision.starter.repository.auth.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author digz6
 */
@RestController
@RequestMapping("/api/account")
@Secured("ROLE_DEFAULT")
public class AccountApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountApi.class);

    @Value("${token.header}")
    private String tokenHeader;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BusinessRoleRepository businessRoleRepository;

    @GetMapping("profile")
    public BaseResponse getProfile(HttpServletRequest request, Locale locale) {
        BaseResponse response = new BaseResponse();

        try {
            String token = request.getHeader(tokenHeader);
            String username = tokenUtil.getUsernameFromToken(token);
            User user = userRepository.findByUsernameAndDeletedFalse(username);

            if (user != null && user.isActive()) {
                response.setData(user);
                response.setResult(true);
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("profile")
    public BaseResponse updateProfile(
            @RequestParam String lastName,
            @RequestParam String firstName,
            HttpServletRequest request, Locale locale) {
        BaseResponse response = new BaseResponse();

        try {
            String token = request.getHeader(tokenHeader);
            String username = tokenUtil.getUsernameFromToken(token);
            User user = userRepository.findByUsernameAndDeletedFalse(username);

            if (user != null && user.isActive()) {
                user.setLastName(lastName);
                user.setFirstName(firstName);
                userRepository.save(user);

                response.setResult(true);
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("change-password")
    public BaseResponse changePassword(
            @RequestParam String oldPassword,
            @RequestParam String newPassword,
            Capabilities userAgent, HttpServletRequest request, Locale locale) {
        BaseResponse response = new BaseResponse();

        try {
            String token = request.getHeader(tokenHeader);
            String username = tokenUtil.getUsernameFromToken(token);
            User user = userRepository.findByUsernameAndDeletedFalse(username);

            if (user != null && user.isActive()) {
                if (passwordEncoder.matches(oldPassword, user.getPassword())) {
                    user.setPassword(passwordEncoder.encode(newPassword));
                    user.setPasswordChangeDate(new Date());
                    userRepository.save(user);

                    AuthResponse authResponse = new AuthResponse();
                    authResponse.setToken(tokenUtil.generateToken(user.getUsername(), DeviceType.fromString(userAgent != null ? userAgent.getDeviceType() : "")));

                    Optional<BusinessRole> businessRoleOpt = businessRoleRepository.findById(user.getBusinessRole());
                    if (businessRoleOpt.isPresent()) {
                        authResponse.setBusinessRole(businessRoleOpt.get());
                    }

                    response.setData(authResponse);
                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("profile.oldPasswordWrong", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }
}
