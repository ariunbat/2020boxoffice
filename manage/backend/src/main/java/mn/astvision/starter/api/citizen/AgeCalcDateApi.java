package mn.astvision.starter.api.citizen;

import com.mongodb.MongoException;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.api.response.antd.AntdTableDataList;
import mn.astvision.starter.model.AgeCalcDate;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.repository.AgeCalcDateRepository;
import mn.astvision.starter.repository.auth.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Locale;

/**
 * @author Ariuka
 */
@RestController
@RequestMapping("/api/age-calc-date")
public class AgeCalcDateApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(AgeCalcDateApi.class);

    @Autowired
    private AgeCalcDateRepository ageCalcDateRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private UserRepository userRepository;

    @GetMapping
    public BaseResponse get() {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("Age calc date");
        try {
            AntdTableDataList<AgeCalcDate> listData = new AntdTableDataList<>();

            response.setData(ageCalcDateRepository.findFirstByDeletedFalse().getCheckDate());
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, null));
        }
        return response;
    }

    @PostMapping("update")
    @Secured("ROLE_MANAGE_DATA")
    public BaseResponse update(
            @RequestBody AgeCalcDate updateRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("Update Age calc date request" + updateRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                AgeCalcDate date = ageCalcDateRepository.findFirstByDeletedFalse();
                date.setCheckDate(updateRequest.getCheckDate());
                date.setModifiedBy(user.getId());
                date.setModifiedDate(LocalDateTime.now());
                date = ageCalcDateRepository.save(date);
                response.setData(date.getId());
                response.setResult(true);
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }
}
