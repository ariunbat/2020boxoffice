package mn.astvision.starter.api;

import com.mongodb.MongoException;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.api.response.auth.BaseController;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.model.LocationDto;
import mn.astvision.starter.repository.LocationRepository;
import mn.astvision.starter.service.location.LocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static java.lang.Math.toIntExact;

@RestController
@RequestMapping("/api/location")
public class LocationApi extends BaseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocationApi.class);

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private LocationService locationService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping()
    public BaseResponse data(Integer parentId, Locale locale) {
        LOGGER.debug("List Location > parentId: " + parentId);
        try {
            List<mn.astvision.starter.response.LocationDto> locations = new ArrayList();
            locationRepository.findByParentId(parentId).forEach((location -> {
                locations.add(
                        mn.astvision.starter.response.LocationDto.builder()
                                .locationId(location.getLocationId())
                                .parentId(location.getParentId())
                                .nameMn(location.getNameMn())
                                .isLeaf(location.isLeaf())
                                .build()
                );
            }));
            return responseData(locations);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            return responseError(locale, "error.database");
        }
    }

    @GetMapping("list")
    public BaseResponse list(Boolean used, Locale locale) {
        LOGGER.debug("all location list used: " + used);
        try {
            if(used != null) {
                return responseData(locationRepository.findByUsed(used));
            }
            return responseData(locationRepository.findAll());
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            return responseError(locale, "error.database");
        }
    }

    @GetMapping("treeData")
    public BaseResponse treeData(Principal principal, Locale locale) {
        BaseResponse response = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                List<LocationDto> locationDto = locationService.treeSelectData(user.getLocationCodes());
                response.setData(locationDto);
                response.setResult(true);
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            return responseError(locale, "error.database");
        }
        return response;
    }

}
