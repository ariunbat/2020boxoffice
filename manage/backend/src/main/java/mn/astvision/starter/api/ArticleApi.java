package mn.astvision.starter.api;

import com.mongodb.MongoException;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Optional;
import mn.astvision.starter.api.request.antd.AntdPagination;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.api.response.antd.AntdTableDataList;
import mn.astvision.starter.model.Article;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.model.enums.ArticleType;
import mn.astvision.starter.repository.ArticleRepository;
import mn.astvision.starter.repository.auth.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author digz6
 */
@RestController
@RequestMapping("/api/article")
public class ArticleApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArticleApi.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private MessageSource messageSource;

    @GetMapping
    public BaseResponse list(ArticleType articleType, AntdPagination pagination, Locale locale) {
        BaseResponse response = new BaseResponse();

        try {
            AntdTableDataList<Article> listData = new AntdTableDataList<>();

            pagination.setTotal(articleRepository.countByArticleType(articleType));
            listData.setPagination(pagination);
            listData.setList(articleRepository.findByArticleType(articleType,
                    PageRequest.of(pagination.getCurrentPage(), pagination.getPageSize())));

            response.setData(listData);
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("create")
    @Secured("ROLE_MANAGE_ARTICLE")
    public BaseResponse create(
            @RequestParam ArticleType articleType,
            @RequestParam String title,
            @RequestParam String content,
            @RequestParam String coverImgUrl,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());

            Article article = new Article(articleType, title, content, coverImgUrl);
            if (user != null) {
                article.setCreatedBy(user.getId());
            }
            article.setCreatedDate(LocalDateTime.now());
            article = articleRepository.save(article);

            response.setData(article.getId());
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("delete")
    @Secured("ROLE_MANAGE_ARTICLE")
    public BaseResponse delete(
            @RequestParam String id,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());

            Optional<Article> articleOpt = articleRepository.findById(id);
            if (articleOpt.isPresent()) {
                Article article = articleOpt.get();

                article.setDeleted(true);
                if (user != null) {
                    article.setModifiedBy(user.getId());
                }
                article.setModifiedDate(LocalDateTime.now());
                articleRepository.save(article);

                response.setResult(true);
            } else {
                response.setMessage(messageSource.getMessage("data.notFound", null, locale));
            }

        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }
}
