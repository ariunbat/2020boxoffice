package mn.astvision.starter.datafiller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import mn.astvision.starter.model.auth.BusinessRole;
import mn.astvision.starter.model.enums.ApplicationRole;
import mn.astvision.starter.repository.auth.BusinessRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author MethoD
 */
@Component
public class BusinessRoleDataFiller {

    @Autowired
    private BusinessRoleRepository businessRoleRepository;

    @PostConstruct
    private void fill() {
        if (businessRoleRepository.count() == 0) {
            BusinessRole businessRoleDeveloper = new BusinessRole("DEVELOPER");
            businessRoleDeveloper.setApplicationRoles(Arrays.asList(ApplicationRole.values()));
            businessRoleRepository.save(businessRoleDeveloper);

            BusinessRole businessRoleAdmin = new BusinessRole("ADMIN");
            List<ApplicationRole> appRoles = new ArrayList<>();
            appRoles.addAll(Arrays.asList(ApplicationRole.getDefaultRoles()));
            appRoles.add(ApplicationRole.ROLE_MANAGE_USER);
            appRoles.add(ApplicationRole.ROLE_MANAGE_CITIZEN);
            appRoles.add(ApplicationRole.ROLE_MANAGE_MEMBER);
            appRoles.add(ApplicationRole.ROLE_MANAGE_EXPECTATION);
            businessRoleAdmin.setApplicationRoles(appRoles);
            businessRoleRepository.save(businessRoleAdmin);

            BusinessRole businessRoleUser = new BusinessRole("USER");
            appRoles = new ArrayList<>();
            appRoles.addAll(Arrays.asList(ApplicationRole.getDefaultRoles()));
            appRoles.add(ApplicationRole.ROLE_MANAGE_CITIZEN);
            appRoles.add(ApplicationRole.ROLE_MANAGE_MEMBER);
            appRoles.add(ApplicationRole.ROLE_MANAGE_EXPECTATION);
            businessRoleUser.setApplicationRoles(appRoles);
            businessRoleRepository.save(businessRoleUser);
        }
    }
}
