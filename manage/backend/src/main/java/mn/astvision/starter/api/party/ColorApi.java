package mn.astvision.starter.api.party;

import com.mongodb.MongoException;
import mn.astvision.starter.api.citizen.ColleagueApi;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.repository.party.ColorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

/**
 * @author Ariuka
 */
@RestController
@RequestMapping("/api/color")
public class ColorApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(ColorApi.class);

    @Autowired
    private ColorRepository colorRepository;

    @Autowired
    private MessageSource messageSource;

    @GetMapping
    public BaseResponse list(Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("Color list");

        try {
            response.setData(colorRepository.findByUsedFalse());
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }
}
