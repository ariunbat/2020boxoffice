package mn.astvision.starter.api.citizen;

import com.mongodb.MongoException;
import mn.astvision.starter.api.request.antd.AntdPagination;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.api.response.antd.AntdTableDataList;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.model.citizen.Content;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.repository.citizen.ContentRepository;
import mn.astvision.starter.service.citizen.ContentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Optional;

/**
 * @author Ariuka
 */
@RestController
@RequestMapping("/api/content")
public class ContentApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContentApi.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ContentRepository contentRepository;

    @Autowired
    private ContentService contentService;

    @GetMapping
    public BaseResponse list(String citizenId, Boolean deleted, AntdPagination pagination, Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("Content list citizenId: " + citizenId + " deleted: " + deleted + " pagination: " + pagination);

        try {
            AntdTableDataList<Content> listData = new AntdTableDataList<>();

            pagination.setTotal(contentService.count(citizenId, deleted, locale));
            listData.setPagination(pagination);
            listData.setList(contentService.list(citizenId, deleted, PageRequest.of(pagination.getCurrentPage(), pagination.getPageSize()), locale));

            response.setData(listData);
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }


    @PostMapping("create")
    @Secured("ROLE_MANAGE_CITIZEN")
    public BaseResponse create(
            @RequestBody Content createRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("create content request: " + createRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Content content = new Content();
                content.setCitizenId(createRequest.getCitizenId());
                content.setType(createRequest.getType());
                content.setText(createRequest.getText());
                content.setImages(createRequest.getImages());
                content.setVideo(createRequest.getVideo());
                content.setUrl(createRequest.getUrl());
                content.setDescription(createRequest.getDescription());
                content.setCreatedBy(user.getId());
                content.setCreatedDate(LocalDateTime.now());
                content = contentRepository.save(content);

                response.setData(content.getId());
                response.setResult(true);
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("update")
    @Secured("ROLE_MANAGE_CITIZEN")
    public BaseResponse update(
            @RequestBody Content updateRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("update content request" + updateRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Optional<Content> contentOptional = contentRepository.findById(updateRequest.getId());
                if (contentOptional.isPresent()) {
                    Content content = contentOptional.get();
                    content.setType(updateRequest.getType());
                    content.setText(updateRequest.getText());
                    content.setImages(updateRequest.getImages());
                    content.setVideo(updateRequest.getVideo());
                    content.setUrl(updateRequest.getUrl());
                    content.setDescription(updateRequest.getDescription());
                    content.setModifiedBy(user.getId());
                    content.setModifiedDate(LocalDateTime.now());
                    content = contentRepository.save(content);
                    response.setData(content.getId());
                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("delete")
    public BaseResponse delete(
            @RequestParam String id,
            Locale locale, Principal principal) {
        LOGGER.debug("Deleting content: " + id);
        BaseResponse response = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {

                Optional<Content> contOpt = contentRepository.findById(id);
                if (contOpt.isPresent()) {
                    Content content = contOpt.get();
                    content.setDeleted(true);
                    content.setModifiedDate(LocalDateTime.now());
                    content.setModifiedBy(user.getId());
                    contentRepository.save(content);

                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }
}
