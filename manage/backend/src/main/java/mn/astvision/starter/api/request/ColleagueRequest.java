package mn.astvision.starter.api.request;

import lombok.Data;
import org.bson.types.ObjectId;

import java.time.LocalDateTime;

/**
 * @author Ariuka
 */
@Data
public class ColleagueRequest {
    private String id;
    private String citizenId;
    private String colleagueTypeId;
    private String name;
    private String description;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private String participation;
}
