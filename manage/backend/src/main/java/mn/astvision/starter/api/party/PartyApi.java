package mn.astvision.starter.api.party;

import com.mongodb.MongoException;
import mn.astvision.starter.api.request.antd.AntdPagination;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.api.response.antd.AntdTableDataList;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.model.party.Color;
import mn.astvision.starter.model.party.Party;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.repository.party.ColorRepository;
import mn.astvision.starter.repository.party.PartyRepository;
import mn.astvision.starter.service.party.PartyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

/**
 * @author Ariuka
 */
@RestController
@RequestMapping("/api/party")
public class PartyApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(PartyApi.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private PartyRepository partyRepository;

    @Autowired
    private PartyService partyService;

    @Autowired
    private ColorRepository colorRepository;

    @GetMapping
    public BaseResponse list(String name, Boolean deleted, AntdPagination pagination, Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("Party list name: " + name + " deleted: " + deleted + " pagination: " + pagination);

        try {
            AntdTableDataList<Party> listData = new AntdTableDataList<>();

            pagination.setTotal(partyService.count(name, deleted, locale));
            listData.setPagination(pagination);
            listData.setList(partyService.list(name, deleted, PageRequest.of(pagination.getCurrentPage(), pagination.getPageSize()), locale));

            response.setData(listData);
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @GetMapping("select")
    public BaseResponse selectList(Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("Party select list");

        try {
            AntdTableDataList<Party> listData = new AntdTableDataList<>();

            listData.setList(partyService.selectList());

            response.setData(listData);
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }


    @PostMapping("create")
    @Secured("ROLE_MANAGE_CITIZEN")
    public BaseResponse create(
            @RequestBody Party createRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("create party request: " + createRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Party existsData = partyRepository.findByOrderAndDeletedFalse(createRequest.getOrder());
                if (existsData != null) {
                    List<Party> parties = partyRepository.findAllByDeletedFalseOrderByOrder(Sort.by(Sort.Direction.ASC, "Order"));
                    boolean checkNext = true;
                    if (parties != null) {
                        for (Party party : parties) {
                            if (!party.isDeleted() && party.getOrder() >= createRequest.getOrder() && checkNext) {
                                Integer order = party.getOrder();
                                order++;
                                party.setOrder(order);
                                Party checkReferenceData = partyRepository.findByOrderAndDeletedFalse(order);
                                if (checkReferenceData != null) {
                                    checkNext = false;
                                }
                                partyRepository.save(party);
                            }
                        }
                    }

                }
                Party party = new Party();
                party.setName(createRequest.getName());
                party.setDescription(createRequest.getDescription());
                party.setOrder(createRequest.getOrder()); // TODO calculate
                party.setIcon(createRequest.getIcon());
                party.setColor(createRequest.getColor());

                Color color = colorRepository.findByCode(createRequest.getColor());
                color.setUsed(true);
                colorRepository.save(color);

                party.setVisible(createRequest.isVisible());
                party.setCreatedBy(user.getId());
                party.setCreatedDate(LocalDateTime.now());
                party = partyRepository.save(party);

                response.setData(party.getId());
                response.setResult(true);
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("update")
    @Secured("ROLE_MANAGE_CITIZEN")
    public BaseResponse update(
            @RequestBody Party updateRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("update party request" + updateRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Optional<Party> partyOptional = partyRepository.findById(updateRequest.getId());
                if (partyOptional.isPresent()) {
                    Party existsData = partyRepository.findByOrderAndDeletedFalse(updateRequest.getOrder());
                    if (existsData != null) {
                        List<Party> parties = partyRepository.findAllByDeletedFalseOrderByOrder(Sort.by(Sort.Direction.ASC, "Order"));
                        boolean checkNext = true;
                        if (parties != null) {
                            for (Party party : parties) {
                                if (!party.isDeleted() && party.getOrder() >= updateRequest.getOrder() && checkNext) {
                                    Integer order = party.getOrder();
                                    order++;
                                    party.setOrder(order);
                                    Party checkReferenceData = partyRepository.findByOrderAndDeletedFalse(order);
                                    if (checkReferenceData != null) {
                                        checkNext = false;
                                    }
                                    partyRepository.save(party);
                                }
                            }
                        }

                    }
                    Party party = partyOptional.get();
                    party.setName(updateRequest.getName());
                    party.setDescription(updateRequest.getDescription());
                    party.setOrder(updateRequest.getOrder()); // TODO calculate
                    party.setIcon(updateRequest.getIcon());

                    Color oldColor = colorRepository.findByCode(party.getColor());
                    oldColor.setUsed(false);
                    colorRepository.save(oldColor);

                    Color color = colorRepository.findByCode(updateRequest.getColor());
                    color.setUsed(true);
                    colorRepository.save(color);

                    party.setColor(updateRequest.getColor());
                    party.setVisible(updateRequest.isVisible());
                    party.setModifiedBy(user.getId());
                    party.setModifiedDate(LocalDateTime.now());
                    party = partyRepository.save(party);
                    response.setData(party.getId());
                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("delete")
    public BaseResponse delete(
            @RequestParam String id,
            Locale locale, Principal principal) {
        LOGGER.debug("Deleting party: " + id);
        BaseResponse response = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {

                Optional<Party> contOpt = partyRepository.findById(id);
                if (contOpt.isPresent()) {
                    Party party = contOpt.get();

                    Color color = colorRepository.findByCode(party.getColor());
                    color.setUsed(false);
                    colorRepository.save(color);

                    party.setDeleted(true);
                    party.setModifiedDate(LocalDateTime.now());
                    party.setModifiedBy(user.getId());
                    partyRepository.save(party);

                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }
}
