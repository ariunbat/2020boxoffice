package mn.astvision.starter.api.citizen;

import com.mongodb.MongoException;
import mn.astvision.starter.api.request.antd.AntdPagination;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.model.citizen.Citizen;
import mn.astvision.starter.model.citizen.FamilyMember;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.repository.citizen.CitizenRepository;
import mn.astvision.starter.repository.citizen.FamilyMemberRepository;
import mn.astvision.starter.service.citizen.FamilyMemberService;
import mn.astvision.starter.util.BirthDateGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Optional;

/**
 * @author Ariuka
 */
@RestController
@RequestMapping("/api/family-member")
public class FamilyMemberApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(FamilyMemberApi.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private FamilyMemberService familyMemberService;

    @Autowired
    private FamilyMemberRepository familyMemberRepository;

    @Autowired
    private BirthDateGenerator birthDateGenerator;

    @Autowired
    private CitizenRepository citizenRepository;

    @GetMapping
    public BaseResponse list(String citizenId, Boolean deleted, AntdPagination pagination, Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("Family member list: citizenId: " + citizenId + " deleted: " + deleted);

        try {
            response.setData(familyMemberService.list(citizenId, deleted, locale));
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }


    @PostMapping("create")
    @Secured("ROLE_MANAGE_CITIZEN")
    public BaseResponse create(
            @RequestBody FamilyMember createRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("create family member request: " + createRequest);
        if (!familyMemberRepository.existsByRegisterNumberAndCitizenIdAndDeletedFalse(createRequest.getRegisterNumber(), createRequest.getCitizenId())) {
            try {
                User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
                if (user != null) {
                    FamilyMember familyMember = new FamilyMember();
                    familyMember.setCitizenId(createRequest.getCitizenId());
                    familyMember.setRelationship(createRequest.getRelationship());
                    familyMember.setRegisterNumber(createRequest.getRegisterNumber());
                    familyMember.setFamilyName(createRequest.getFamilyName());
                    familyMember.setLastName(createRequest.getLastName());
                    familyMember.setFirstName(createRequest.getFirstName());
                    if (createRequest.getRegisterNumber() != null) {
                        familyMember.setBirthDate(birthDateGenerator.getBirthDateFromRegNum(createRequest.getRegisterNumber()));
                    }
                    familyMember.setPhone(createRequest.getPhone());
                    familyMember.setEmail(createRequest.getEmail());
                    familyMember.setJob(createRequest.getJob());
                    familyMember.setSocialAccount(createRequest.getSocialAccount());
                    familyMember.setDescription(createRequest.getDescription());
                    familyMember.setCreatedBy(user.getId());
                    familyMember.setCreatedDate(LocalDateTime.now());
                    familyMember = familyMemberRepository.save(familyMember);

                    response.setData(familyMember.getId());
                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("error.permission", null, locale));
                }
            } catch (MongoException | DataAccessException e) {
                LOGGER.error(e.getMessage(), e);
                response.setMessage(messageSource.getMessage("error.database", null, locale));
            }
        } else {
            response.setResult(true);
            response.setMessage("Гэр бүлийн гишүүн бүртгэлтэй байна");
        }

        return response;
    }

    @PostMapping("update")
    @Secured("ROLE_MANAGE_CITIZEN")
    public BaseResponse update(
            @RequestBody FamilyMember updateRequest,
            Principal principal,
            Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("update family member request: " + updateRequest);

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Optional<FamilyMember> familyMemberOptional = familyMemberRepository.findById(updateRequest.getId());
                if (familyMemberOptional.isPresent()) {
                    FamilyMember familyMember = familyMemberOptional.get();
                    familyMember.setRelationship(updateRequest.getRelationship());
                    familyMember.setRegisterNumber(updateRequest.getRegisterNumber());
                    familyMember.setFamilyName(updateRequest.getFamilyName());
                    familyMember.setLastName(updateRequest.getLastName());
                    familyMember.setFirstName(updateRequest.getFirstName());
                    if (updateRequest.getRegisterNumber() != null) {
                        familyMember.setBirthDate(birthDateGenerator.getBirthDateFromRegNum(updateRequest.getRegisterNumber()));
                    }
                    familyMember.setPhone(updateRequest.getPhone());
                    familyMember.setEmail(updateRequest.getEmail());
                    familyMember.setJob(updateRequest.getJob());
                    familyMember.setSocialAccount(updateRequest.getSocialAccount());
                    familyMember.setDescription(updateRequest.getDescription());
                    familyMember.setModifiedBy(user.getId());
                    familyMember.setModifiedDate(LocalDateTime.now());
                    familyMember = familyMemberRepository.save(familyMember);
                    response.setData(familyMember.getId());
                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("delete")
    public BaseResponse delete(
            @RequestParam String id,
            Locale locale, Principal principal) {
        LOGGER.debug("Deleting family member: " + id);
        BaseResponse response = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                Optional<FamilyMember> famOpt = familyMemberRepository.findById(id);
                if (famOpt.isPresent()) {
                    FamilyMember famDelete = famOpt.get();
                    famDelete.setDeleted(true);
                    famDelete.setModifiedDate(LocalDateTime.now());
                    famDelete.setModifiedBy(user.getId());
                    familyMemberRepository.save(famDelete);

                    response.setResult(true);
                } else {
                    response.setMessage(messageSource.getMessage("data.notFound", null, locale));
                }
            } else {
                response.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }

        return response;
    }

    @PostMapping("check-family-member")
    public BaseResponse checkFamilyMember(@RequestBody Citizen request, Locale locale) {
        BaseResponse response = new BaseResponse();
        LOGGER.debug("check family member: " + request.getRegisterNumber());

        try {
            Citizen citizen = citizenRepository.findByRegisterNumber(request.getRegisterNumber());
            if (citizen != null) {
                response.setData(citizen);
            }
            response.setResult(true);
        } catch (MongoException | DataAccessException e) {
            LOGGER.error(e.getMessage(), e);
            response.setMessage(messageSource.getMessage("error.database", null, locale));
        }
        return response;
    }
}
