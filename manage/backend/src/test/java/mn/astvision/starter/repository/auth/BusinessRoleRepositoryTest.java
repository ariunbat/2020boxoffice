package mn.astvision.starter.repository.auth;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author MethoD
 */
@RunWith(SpringRunner.class)
@DataMongoTest
public class BusinessRoleRepositoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessRoleRepositoryTest.class);

    @Autowired
    private BusinessRoleRepository businessRoleRepository;

    @Test
    public void testCount() {
        LOGGER.info("All business roles: " + businessRoleRepository.count());
    }
}
