package mn.astvision.starter.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.auth.TokenUtil;
import mn.astvision.starter.model.Article;
import mn.astvision.starter.model.enums.ArticleType;
import mn.astvision.starter.model.enums.DeviceType;
import mn.astvision.starter.repository.ArticleRepository;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author MethoD
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ArticleApiTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ArticleApiTest.class);

    private final String baseUrl = "/api/article";

    @Value("${token.header}")
    private String tokenHeader;

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private ArticleRepository articleRepository;

    private MockMvc mockMvc;
    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(springSecurity()).build();
    }

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void testList() throws Exception {
        LOGGER.info("Running article api test - list");

        Article article = new Article(ArticleType.NEWS, "Test news", "Test content", "");
        article = articleRepository.save(article);

        MvcResult mvcResult = mockMvc.perform(get(baseUrl)
                .param("articleType", ArticleType.NEWS.name())
                .param("pageNumber", "1")
                .param("pageSize", "10")
        )
                .andExpect(status().is(200))
                .andExpect(jsonPath("result", is(true)))
                .andExpect(jsonPath("data.pagination.total", notNullValue())).andReturn();

        LOGGER.info("Article api list response: " + mvcResult.getResponse().getContentAsString());

        articleRepository.delete(article);
    }

    //@Ignore
    @Test
    public void testCreate() throws Exception {
        LOGGER.info("Running article api test - create");

        MvcResult mvcResult = mockMvc.perform(post(baseUrl + "/create")
                .header(tokenHeader, tokenUtil.generateToken("dev", DeviceType.DESKTOP))
                .param("articleType", ArticleType.NEWS.name())
                .param("title", "Test news API")
                .param("content", "Test content API")
                .param("coverImgUrl", "")
        )
                .andExpect(status().is(200))
                .andExpect(jsonPath("result", is(true)))
                .andExpect(jsonPath("data", notNullValue())).andReturn();

        String rawResponse = mvcResult.getResponse().getContentAsString();
        LOGGER.info("Article api create response: " + rawResponse);

        BaseResponse baseResponse = objectMapper.readValue(rawResponse, BaseResponse.class);
        articleRepository.deleteById((String) baseResponse.getData());
    }

    //@Ignore
    @Test
    public void testDelete() throws Exception {
        LOGGER.info("Running article api test - delete");

        Article article = new Article(ArticleType.NEWS, "Test news", "Test content", "");
        article = articleRepository.save(article);

        MvcResult mvcResult = mockMvc.perform(post(baseUrl + "/delete")
                .header(tokenHeader, tokenUtil.generateToken("dev", DeviceType.DESKTOP))
                .param("id", article.getId())
        )
                .andExpect(status().is(200))
                .andExpect(jsonPath("result", is(true)))
                .andReturn();

        LOGGER.info("Article api delete response: " + mvcResult.getResponse().getContentAsString());

        articleRepository.delete(article);
    }
}
