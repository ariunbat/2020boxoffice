package mn.astvision.starter.api.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import mn.astvision.starter.api.request.CreateUserRequest;
import mn.astvision.starter.auth.TokenUtil;
import mn.astvision.starter.model.FileData;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.model.enums.DeviceType;
import mn.astvision.starter.repository.auth.BusinessRoleRepository;
import mn.astvision.starter.repository.auth.UserRepository;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author MethoD
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserApiTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserApiTest.class);

    private final String baseUrl = "/api/user";

    @Value("${token.header}")
    private String tokenHeader;

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BusinessRoleRepository businessRoleRepository;

    private MockMvc mockMvc;
    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(springSecurity()).build();
    }

    private final ObjectMapper objectMapper = new ObjectMapper();

    //@Ignore
    @Test
    public void testList() throws Exception {
        LOGGER.info("Running user api test - list");

        MvcResult mvcResult = mockMvc.perform(get(baseUrl)
                .header(tokenHeader, tokenUtil.generateToken("dev", DeviceType.DESKTOP))
                .param("businessRole", "")
                .param("search", "")
                .param("currentPage", "1")
                .param("pageSize", "10")
        )
                .andExpect(status().is(200))
                .andExpect(jsonPath("result", is(true)))
                .andExpect(jsonPath("data.pagination.total", notNullValue())).andReturn();

        LOGGER.info("User api list response: " + mvcResult.getResponse().getContentAsString());
    }

    //@Ignore
    @Test
    public void testCreate() throws Exception {
        LOGGER.info("Running user api test - create");

        String username = "test-user";

        userRepository.deleteByUsername(username);

        CreateUserRequest createUserRequest = new CreateUserRequest();
        createUserRequest.setUsername(username);
        createUserRequest.setPassword("pass");
        createUserRequest.setLastName("Last name");
        createUserRequest.setFirstName("First name");
        createUserRequest.setBoardId("test");
        createUserRequest.setDescription("test");
        createUserRequest.setEmail("test@test.com");
        createUserRequest.setFamilyName("test");
        List<Integer> codes = new ArrayList<>();
        codes.add(123);
        createUserRequest.setLocationCodes(codes);
        List<Integer> phone = new ArrayList<>();
        phone.add(99112211);
        createUserRequest.setPhone(phone);
createUserRequest.setRegisterNumber("!23");
        FileData file = new FileData();
        file.setName("asd");
        file.setStatus("done");
        file.setUid("123");
        file.setUrl("asd");
        createUserRequest.setPhoto(file);
//createUserRequest.setPhoto(File);

        createUserRequest.setBusinessRole("test");

        MvcResult mvcResult = mockMvc.perform(post(baseUrl + "/create")
                .header(tokenHeader, tokenUtil.generateToken("dev", DeviceType.DESKTOP))
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createUserRequest))
        )
                .andExpect(status().is(200))
                .andExpect(jsonPath("result", is(true)))
                .andExpect(jsonPath("data", notNullValue())).andReturn();

        String rawResponse = mvcResult.getResponse().getContentAsString();
        LOGGER.info("User api create response: " + rawResponse);

        userRepository.deleteByUsername(username);
    }

    //@Ignore
    @Test
    public void testDelete() throws Exception {
        LOGGER.info("Running user api test - delete");

        String username = "test-user";

        userRepository.deleteByUsername(username);

        User user = new User(username, "test");
        user.setActive(true);

        user = userRepository.save(user);

        MvcResult mvcResult = mockMvc.perform(post(baseUrl + "/delete")
                .header(tokenHeader, tokenUtil.generateToken("dev", DeviceType.DESKTOP))
                .param("id", user.getId())
        )
                .andExpect(status().is(200))
                .andExpect(jsonPath("result", is(true)))
                .andReturn();

        LOGGER.info("User delete response: " + mvcResult.getResponse().getContentAsString());
    }
}
