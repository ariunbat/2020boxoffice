package mn.astvision.starter.repository.auth;

import mn.astvision.starter.model.auth.User;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author MethoD
 */
@RunWith(SpringRunner.class)
@DataMongoTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testCreateUser() {
        String username = "test-user";
        String password = "pass";

        userRepository.deleteByUsername(username);

        User user = new User(username, password);
        userRepository.save(user);

        user = userRepository.findByUsernameAndDeletedFalse(username);
        assertNotNull(user);

        userRepository.delete(user);
    }
}
