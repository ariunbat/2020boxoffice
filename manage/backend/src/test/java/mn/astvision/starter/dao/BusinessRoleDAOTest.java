package mn.astvision.starter.dao;

import mn.astvision.starter.model.enums.ApplicationRole;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author MethoD
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BusinessRoleDAOTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessRoleDAOTest.class);

    @Autowired
    private BusinessRoleDAO businessRoleDAO;

    @Test
    public void testCount() {
        long count = businessRoleDAO.count("ADMIN", ApplicationRole.ROLE_MANAGE_USER);
        LOGGER.info("Found business roles: " + count);
    }
}
