package mn.astvision.starter.api.auth;

import java.util.Arrays;
import mn.astvision.starter.model.auth.BusinessRole;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.model.enums.ApplicationRole;
import mn.astvision.starter.repository.auth.BusinessRoleRepository;
import mn.astvision.starter.repository.auth.UserRepository;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 *
 * @author MethoD
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthApiTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthApiTest.class);

    private final String baseUrl = "/api/auth";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private BusinessRoleRepository businessRoleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    //@Ignore
    @Test
    public void testLogin() throws Exception {
        LOGGER.info("Running auth api test - login");
        String username = "test-user";
        String password = "pass";

        BusinessRole businessRole = new BusinessRole("Test");
        businessRole.setApplicationRoles(Arrays.asList(ApplicationRole.getDefaultRoles()));
        businessRole = businessRoleRepository.save(businessRole);

        userRepository.deleteByUsername(username);

        User user = new User(username, passwordEncoder.encode(password));
        user.setActive(true);
        user.setBusinessRole(businessRole.getRole());

        user = userRepository.save(user);

        MvcResult mvcResult = mockMvc.perform(post(baseUrl + "/login")
                .param("username", username)
                .param("password", password)
        )
                .andExpect(status().is(200))
                .andExpect(jsonPath("result", is(true)))
                .andExpect(jsonPath("data.token", notNullValue())).andReturn();

        LOGGER.info("Auth api test response: " + mvcResult.getResponse().getContentAsString());

        userRepository.delete(user);
        businessRoleRepository.delete(businessRole);
    }
}
