package mn.astvision.starter.api.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;
import mn.astvision.starter.api.request.MultiDeleteRequest;
import mn.astvision.starter.auth.TokenUtil;
import mn.astvision.starter.model.auth.BusinessRole;
import mn.astvision.starter.model.enums.ApplicationRole;
import mn.astvision.starter.model.enums.DeviceType;
import mn.astvision.starter.repository.auth.BusinessRoleRepository;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author MethoD
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BusinessRoleApiTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessRoleApiTest.class);

    private final String baseUrl = "/api/business-role";

    @Value("${token.header}")
    private String tokenHeader;

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private BusinessRoleRepository businessRoleRepository;

    private MockMvc mockMvc;
    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(springSecurity()).build();
    }

    private final ObjectMapper objectMapper = new ObjectMapper();

    //@Ignore
    @Test
    public void testList() throws Exception {
        LOGGER.info("Running business role api test - list");

        MvcResult mvcResult = mockMvc.perform(get(baseUrl)
                .header(tokenHeader, tokenUtil.generateToken("dev", DeviceType.DESKTOP))
                .param("role", "")
                .param("applicationRoles", ApplicationRole.ROLE_DEFAULT.name())
                .param("currentPage", "1")
                .param("pageSize", "10")
        )
                .andExpect(status().is(200))
                .andExpect(jsonPath("result", is(true)))
                .andExpect(jsonPath("data.pagination.total", notNullValue())).andReturn();

        LOGGER.info("Business role api list response: " + mvcResult.getResponse().getContentAsString());
    }

    //@Ignore
    @Test
    public void testCreate() throws Exception {
        LOGGER.info("Running business role api test - create");

        String role = "Test role";
        businessRoleRepository.deleteById(role);

        BusinessRole businessRole = new BusinessRole(role);
        businessRole.setApplicationRoles(Arrays.asList(ApplicationRole.getDefaultRoles()));

        MvcResult mvcResult = mockMvc.perform(post(baseUrl + "/create")
                .header(tokenHeader, tokenUtil.generateToken("dev", DeviceType.DESKTOP))
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(businessRole))
        )
                .andExpect(status().is(200))
                .andExpect(jsonPath("result", is(true)))
                .andExpect(jsonPath("data", notNullValue())).andReturn();

        String rawResponse = mvcResult.getResponse().getContentAsString();
        LOGGER.info("Business role api create response: " + rawResponse);

        businessRoleRepository.deleteById(role);
    }

    @Ignore
    @Test
    public void testDelete() throws Exception {
        LOGGER.info("Running business role api test - delete");

        String role = "Test role";
        BusinessRole businessRole = new BusinessRole(role);
        businessRole = businessRoleRepository.save(businessRole);

        MvcResult mvcResult = mockMvc.perform(post(baseUrl + "/delete-multi")
                .header(tokenHeader, tokenUtil.generateToken("dev", DeviceType.DESKTOP))
                .header("Content-Type", MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new MultiDeleteRequest(Arrays.asList(new String[]{role}))))
        )
                .andExpect(status().is(200))
                .andExpect(jsonPath("result", is(true)))
                .andReturn();

        LOGGER.info("Business role api delete response: " + mvcResult.getResponse().getContentAsString());

        businessRoleRepository.delete(businessRole);
    }
}
