## Development info
1. JDK 1.8
2. MongoDB 3.6+, 4.0
3. Maven 3+

### Libraries
1. Spring Framework - https://spring.io/projects/spring-framework
2. Spring Boot - https://spring.io/projects/spring-boot
3. Spring Security - https://spring.io/projects/spring-security
4. JSON web token - https://github.com/jwtk/jjwt
5. Spring Data MongoDB - https://projects.spring.io/spring-data-mongodb/
6. QueryDSL - http://querydsl.com/
7. Browscap - https://github.com/blueconic/browscap-java

## Getting started

### Generate querydsl classess
```
mvn generate-resources
```

### Start on embedded tomcat
```
mvn spring-boot:run
```

### Build war file
```
mvn clean install
```
