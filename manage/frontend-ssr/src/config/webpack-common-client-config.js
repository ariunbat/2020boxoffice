const path = require('path');

// const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');

module.exports = {
  target: 'web', // default is web
  entry: {
    'index.js': path.resolve(__dirname, '../client/index.js')
  },
  plugins: [
    //new CleanWebpackPlugin(),
    /*new HtmlWebpackPlugin({
      inject: true,
      template: paths.appHtml,
      title: 'Astvision Starter',
      favicon: './src/assets/favicon.png'
    }),*/
    new CopyPlugin([
      { from: 'src/client/assets/tinymce', to: '../client/tinymce' }, // copy tinymce
      { from: 'src/serviceWorker', to: '../client' }, // copy service workers
    ]),
    new WebpackPwaManifest({
      filename: 'assets/manifest.json',
      name: 'Astvision Starter',
      short_name: 'AstStarter',
      description: 'Astvision Starter',
      // crossorigin: 'use-credentials', //can be null, use-credentials or anonymous
      icons: [
        {
          src: path.resolve('src/client/assets/favicon.png'),
          sizes: [16, 24, 32, 64], // multiple sizes
          destination: 'assets'
        }
        /*,{
          src: path.resolve('src/assets/large-icon.png'),
          size: '1024x1024' // you can also use the specifications pattern
        }*/
      ],
      start_url: '.',
      display: 'standalone',
      theme_color: '#000000',
      background_color: '#ffffff'
    })
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/react'],
            // plugins: [['import', { libraryName: 'antd', style: 'css' }]]
            plugins: [['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }]]
          }
        }
      },
      {
        test: /\.(png|jpg|gif|svg|ico)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'assets',
              //name: '[hash].[ext]',
              publicPath: '/static/assets'
            },
          }
        ]
      }
    ]
  },
  resolve: {
    // File extensions. Add others and needed (e.g. scss, json)
    extensions: ['.js', '.jsx'],
    modules: ['node_modules']
  }
};
