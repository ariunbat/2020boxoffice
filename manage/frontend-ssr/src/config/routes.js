import React from 'react';
import { Redirect } from 'react-router-dom';

import { checkAuth } from '../common/utils/auth';

import BaseRoute from '../client/layout/baseRoute';
import BaseLayout from '../client/layout/baseLayout';
import PublicLayout from '../client/layout/publicLayout';
import LoginLayout from '../client/layout/loginLayout';
import Exception403 from '../client/pages/Exception/403';

import Page404 from '../client/pages/exception/404';
import Login from '../client/pages/auth/login';
import Logout from '../client/pages/auth/logout';
import Forgot from '../client/pages/auth/forgot';
import Dashboard from '../client/pages/dashboard';
import CitizenList from '../client/pages/citizen/List';
import CitizenCreate from '../client/pages/citizen/Create';
import CitizenDetail from '../client/pages/citizen/Detail';
import CitizenEdit from '../client/pages/citizen/Edit';
import User from '../client/pages/user/List';
import AdminUser from '../client/pages/adminUser/List';
import BusinessRole from '../client/pages/businessRole/List';
import ColleagueType from '../client/pages/colleagueType/List';
import Party from '../client/pages/party/List';
import Member from '../client/pages/member/List';
import AgeCalcDate from '../client/pages/settings/ageCalcDate';
import Board from '../client/pages/board/List';


const Routes = [
  {
    component: BaseRoute,
    routes: [
      {
        component: LoginLayout,
        path: '/auth',
        routes: [
          {
            path: '/auth/login',
            exact: true,
            component: Login
          },
          {
            path: '/auth/logout',
            exact: true,
            component: Logout
          },
          {
            path: '/auth/forgot',
            exact: true,
            component: Forgot
          }
        ]
      },
      {
        path: '/',
        component: BaseLayout,
        routes: [
          {
            path: '/',
            exact: true,
            render: () => <Redirect to="/dashboard" />
          },
          {
            path: '/dashboard',
            exact: true,
            // authority: ['ROLE_DEFAULT'],
            render: (props) => checkAuth('ROLE_DEFAULT') ? <Dashboard {...props} /> : <Redirect to="/auth/login" />
          },
          {
            path: '/citizen',
            exact: true,
            // authority: ['ROLE_DEFAULT'],
            render: (props) => checkAuth('ROLE_DEFAULT') ? <CitizenList {...props} /> : <Redirect to="/auth/login" />
          },
          {
            path: '/citizen/create',
            exact: true,
            // authority: ['ROLE_DEFAULT'],
            render: (props) => checkAuth('ROLE_DEFAULT') ? <CitizenCreate {...props} /> : <Redirect to="/auth/login" />
          },
          {
            path: '/citizen/edit/:id',
            exact: true,
            // authority: ['ROLE_DEFAULT'],
            render: (props) => checkAuth('ROLE_DEFAULT') ? <CitizenEdit {...props} /> : <Redirect to="/auth/login" />
          },
          {
            path: '/citizen/:id',
            exact: true,
            // authority: ['ROLE_DEFAULT'],
            render: (props) => checkAuth('ROLE_DEFAULT') ? <CitizenDetail {...props} /> : <Redirect to="/auth/login" />
          },
          {
            path: '/member',
            exact: true,
            // authority: ['ROLE_DEFAULT'],
            render: (props) => checkAuth('ROLE_DEFAULT') ? <Member {...props} /> : <Redirect to="/auth/login" />
          },
          {
            path: '/categories/colleagueType',
            exact: true,
            render: (props) => checkAuth('ROLE_MANAGE_DATA') ? <ColleagueType {...props} /> : <Redirect to="/auth/login" />
          },
          {
            path: '/categories/party',
            exact: true,
            render: (props) => checkAuth('ROLE_MANAGE_DATA') ? <Party {...props} /> : <Redirect to="/auth/login" />
          },
          {
            path: '/settings/ageCalcDate',
            exact: true,
            render: (props) => checkAuth('ROLE_MANAGE_DATA') ? <AgeCalcDate {...props} /> : <Redirect to="/auth/login" />
          },
          {
            path: '/settings/board',
            exact: true,
            render: (props) => checkAuth('ROLE_MANAGE_DATA') ? <Board {...props} /> : <Redirect to="/auth/login" />
          },
          {
            path: '/settings/adminUser',
            exact: true,
            render: (props) => {
              if (!checkAuth('ROLE_DEFAULT')) {
                return <Redirect to="/auth/login" />
              }
              if (checkAuth('ROLE_DEFAULT') && checkAuth('ROLE_MANAGE_ADMIN')) {
                return <AdminUser {...props} />
              } else {
                return <Exception403 />
              }
            }
          },
          {
            path: '/user',
            exact: true,
            render: (props) => {
              if (!checkAuth('ROLE_DEFAULT')) {
                return <Redirect to="/auth/login" />
              }
              if (checkAuth('ROLE_DEFAULT') && checkAuth('ROLE_MANAGE_USER')) {
                return <User {...props} />
              } else {
                return <Exception403 />
              }
            }
          },
          {
            path: '/businessRole',
            exact: true,
            render: (props) => {
              if (!checkAuth('ROLE_DEFAULT')) {
                return <Redirect to="/auth/login" />
              } else {
                if (checkAuth('ROLE_DEFAULT') && checkAuth('ROLE_MANAGE_BUSINESS_ROLE')) {
                  return <BusinessRole {...props} />
                } else {
                  return <Exception403 />
                }
              }
            }
          },
          {
            path: '*',
            exact: false,
            component: Page404
          },
        ]
      }
    ]
  }
];

export default Routes;
