const path = require('path');
const fs  = require('fs');

const webpack = require('webpack');
const merge = require('webpack-merge');
const lessToJS = require('less-vars-to-js');

const CopyPlugin = require('copy-webpack-plugin');
// const TerserJSPlugin = require('terser-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
// const ExtractTextPlugin = require('extract-text-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

// import common webpack config
const commonServerConfig = require('./webpack-common-server-config.js');
const commonClientConfig = require('./webpack-common-client-config.js');

const themeVariables = lessToJS(
  fs.readFileSync(path.join(__dirname, '../../src/client/ant-theme-vars.less'), 'utf8')
);

const serverConfig = {
  mode: 'production',
  plugins: [
    // Set process.env.NODE_ENV to production
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    // Extract text/(s)css from a bundle, or bundles, into a separate file.
    new MiniCssExtractPlugin({
      filename: 'assets/[name].css',
      chunkFilename: 'assets/[name].css'
    })
  ],
  module: {
    rules: [
      {
        test: /\.less$/,
        use: [
          //'isomorphic-style-loader',
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              //import: true,
              importLoaders: 1,
              getLocalIdent: (loaderContext, localIdentName, localName, options) => {
                if (loaderContext.resourcePath.includes('node_modules')) {
                  return localName;
                } else {
                  const fileName = path.basename(loaderContext.resourcePath);
                  const name = fileName.replace(/\.[^/.]+$/, '');
                  if (name === 'global') {
                    return localName;
                  }
                  return `${name}__${localName}`;
                }
              }
            }
          },
          {
            loader: 'less-loader',
            options: {
              javascriptEnabled: true,
              modifyVars: themeVariables
            }
          }
        ]
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, '../../dist/server'),
    filename: '[name]'
  }
};

const clientConfig = {
  mode: 'production',
  plugins: [
    new CopyPlugin([
      { from: './src/client/assets/og_image.jpg', to: 'assets/og_image.jpg' },
      { from: './src/client/assets/favicon.png', to: 'assets/favicon.png' },
      { from: './src/client/assets/sitemap.xml', to: 'assets/sitemap.xml' }
    ]),
    //new CleanWebpackPlugin(),
    new UglifyJSPlugin({
      uglifyOptions: {
        output: {
          // beautify: true,
          comments: false
        }
      }
    }),
    // Set process.env.NODE_ENV to production
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    // Extract text/(s)css from a bundle, or bundles, into a separate file.
    new MiniCssExtractPlugin({
      filename: 'assets/[name].css',
      chunkFilename: 'assets/[name].css'
    })
  ],
  module: {
    rules: [
      {
        test: /\.less$/,
        use: [
          //'isomorphic-style-loader',
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              //import: true,
              importLoaders: 1,
              getLocalIdent: (loaderContext, localIdentName, localName, options) => {
                if (loaderContext.resourcePath.includes('node_modules')) {
                  return localName;
                } else {
                  const fileName = path.basename(loaderContext.resourcePath);
                  const name = fileName.replace(/\.[^/.]+$/, '');
                  if (name === 'global') {
                    return localName;
                  }
                  return `${name}__${localName}`;
                }
              }
              //sourceMap: true // slows build
            }
          },
          {
            loader: 'less-loader',
            options: {
              javascriptEnabled: true,
              modifyVars: themeVariables
              //sourceMap: true // slows build
            }
          }
        ]
      },
      /*
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              sourceMap: true,
              modules: true
            }
          }
        ]
      }
      */
    ]
  },
  optimization: {
    // minimizer: [new TerserJSPlugin({}), new OptimizeCSSAssetsPlugin({})],
    minimizer: [
      new OptimizeCssAssetsPlugin({
        cssProcessorOptions: {
          // safe: true,
          discardComments: {
            removeAll: true
          }
        }
      })
    ],
    splitChunks: {
      chunks: 'all'
    }
  },
  output: {
    path: path.resolve(__dirname, '../../dist/client'),
    filename: '[name]',
    publicPath: '/'
  }
};

module.exports = [merge(commonServerConfig, serverConfig), merge(commonClientConfig, clientConfig)];
