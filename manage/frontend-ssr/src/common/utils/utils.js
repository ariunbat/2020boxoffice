import moment from 'moment';
import React from 'react';
// import nzh from 'nzh/cn';
import { parse, stringify } from 'qs';

export function objectToCommaSeparatedString(obj) {
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');
}

export function fixedZero(val) {
  return val * 1 < 10 ? `0${val}` : val;
}

export function getTimeDistance(type) {
  const now = new Date();
  const oneDay = 1000 * 60 * 60 * 24;

  if (type === 'today') {
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);
    return [moment(now), moment(now.getTime() + (oneDay - 1000))];
  }

  if (type === 'week') {
    let day = now.getDay();
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);

    if (day === 0) {
      day = 6;
    } else {
      day -= 1;
    }

    const beginTime = now.getTime() - day * oneDay;

    return [moment(beginTime), moment(beginTime + (7 * oneDay - 1000))];
  }

  if (type === 'month') {
    const year = now.getFullYear();
    const month = now.getMonth();
    const nextDate = moment(now).add(1, 'months');
    const nextYear = nextDate.year();
    const nextMonth = nextDate.month();

    return [
      moment(`${year}-${fixedZero(month + 1)}-01 00:00:00`),
      moment(moment(`${nextYear}-${fixedZero(nextMonth + 1)}-01 00:00:00`).valueOf() - 1000),
    ];
  }

  const year = now.getFullYear();
  return [moment(`${year}-01-01 00:00:00`), moment(`${year}-12-31 23:59:59`)];
}

export function getPlainNode(nodeList, parentPath = '') {
  const arr = [];
  nodeList.forEach(node => {
    const item = node;
    item.path = `${parentPath}/${item.path || ''}`.replace(/\/+/g, '/');
    item.exact = true;
    if (item.children && !item.component) {
      arr.push(...getPlainNode(item.children, item.path));
    } else {
      if (item.children && item.component) {
        item.exact = false;
      }
      arr.push(item);
    }
  });
  return arr;
}

// export function digitUppercase(n) {
//   return nzh.toMoney(n);
// }

function getRelation(str1, str2) {
  if (str1 === str2) {
    console.warn('Two path are equal!'); // eslint-disable-line
  }
  const arr1 = str1.split('/');
  const arr2 = str2.split('/');
  if (arr2.every((item, index) => item === arr1[index])) {
    return 1;
  }
  if (arr1.every((item, index) => item === arr2[index])) {
    return 2;
  }
  return 3;
}

function getRenderArr(routes) {
  let renderArr = [];
  renderArr.push(routes[0]);
  for (let i = 1; i < routes.length; i += 1) {
    // 去重
    renderArr = renderArr.filter(item => getRelation(item, routes[i]) !== 1);
    // 是否包含
    const isAdd = renderArr.every(item => getRelation(item, routes[i]) === 3);
    if (isAdd) {
      renderArr.push(routes[i]);
    }
  }
  return renderArr;
}

/**
 * Get router routing configuration
 * { path:{name,...param}}=>Array<{name,path ...param}>
 * @param {string} path
 * @param {routerData} routerData
 */
export function getRoutes(path, routerData) {
  let routes = Object.keys(routerData).filter(
    routePath => routePath.indexOf(path) === 0 && routePath !== path
  );
  // Replace path to '' eg. path='user' /user/name => name
  routes = routes.map(item => item.replace(path, ''));
  // Get the route to be rendered to remove the deep rendering
  const renderArr = getRenderArr(routes);
  // Conversion and stitching parameters
  const renderRoutes = renderArr.map(item => {
    const exact = !routes.some(route => route !== item && getRelation(route, item) === 1);
    return {
      exact,
      ...routerData[`${path}${item}`],
      key: `${path}${item}`,
      path: `${path}${item}`,
    };
  });
  return renderRoutes;
}

export function getPageQuery() {
  return parse(window.location.href.split('?')[1]);
}

export function getQueryPath(path = '', query = {}) {
  const search = stringify(query);
  if (search.length) {
    return `${path}?${search}`;
  }
  return path;
}

/* eslint no-useless-escape:0 */
// eslint-disable-next-line max-len
const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;

export function isUrl(path) {
  return reg.test(path);
}

export function formatWan(val) {
  const v = val * 1;
  if (!v || Number.isNaN(v)) return '';

  let result = val;
  if (val > 10000) {
    result = Math.floor(val / 10000);
    result = (
      <span>
        {result}
        <span
          styles={{
            position: 'relative',
            top: -2,
            fontSize: 14,
            fontStyle: 'normal',
            lineHeight: 20,
            marginLeft: 2,
          }}
        >
          万
        </span>
      </span>
    );
  }
  return result;
}

export function isAntdPro() {
  return window.location.hostname === 'preview.pro.ant.design';
}

export function checkPhoneNumber(rule, value, callback) {
  if(value) {
    if (value.length !== 8) {
      callback('phone number is not valid');
    }
    callback();
  }
  callback();
}

export function checkWorkPhoneNumber(rule, value, callback) {
  if(value) {
    if (value.length !== 6 && value.length !== 8) {
      callback('phone number is not valid');
    }
    callback();
  }
  callback();
}

export function checkRegistry(rule, control, callback, isVisibleLegalEntity) {
  if (control) {
    const value = control.trim().toUpperCase();
    if (value.length === 10 && !isVisibleLegalEntity) {
      const regNumLetterScope = 'ЧАБВДГЕЖЗРИЙКЛМНОПСТУФЦХЩЫЬЭЯЮШ';
      const regNumNumberScope = '24010203050406070817091011121314151618192021232226272829313025';
      let checkInt = 0;
      let k;
      let s1 = 0;
      let j;
      let s = 0;
      const a = '5678987';
      if (regNumLetterScope.indexOf(value[0]) !== -1 && regNumLetterScope.indexOf(value[1]) !== -1) {
        for (let numIndex = 2; numIndex < 10; numIndex += 1) {
          if ('1234567890'.indexOf(value[numIndex]) !== -1) {
            numIndex = 10;
            const day = value[6].toString() + value[7].toString();
            const dayInt = parseInt(day, 10);
            if (dayInt < 32 && dayInt > 0) {
              checkInt = 1;
            } else {
              return callback('Регистр-н дугаар буруу байна!');
            }
          }
        }
        if (checkInt === 1) {
          for (let ii = 0; ii < 7; ii += 1) {
            s += parseInt(value[ii + 2].toString(), 10) * parseInt(a[ii].toString(), 10);
          }
          for (let ii = 0; ii < 2; ii += 1) {
            j = regNumLetterScope.indexOf(value[ii]);
            if (ii === 0) {
              s1 = parseInt(regNumNumberScope.substr(j * 2, 1), 10) +
                2 * parseInt(regNumNumberScope.substr(j * 2 + 1, 1), 10);
            } else {
              s1 = s1 + 3 * parseInt(regNumNumberScope.substr(j * 2, 1), 10) +
                4 * parseInt(regNumNumberScope.substr(j * 2 + 1, 1), 10);
            }
          }
          k = s1 % 11 + s % 11;
          if (k === 10) {
            k = 1;
          }
          if (k > 10) {
            k -= 11;
          }
          if (parseInt(value[9].toString(), 10) !== k) {
            return callback('Регистр-н дугаар буруу байна!');
          }
          return callback();
        }
        return callback('Регистр-н дугаар буруу байна!');

      }
      return callback('Регистр-н дугаар буруу байна!');

    }
    if (value.length === 0) {
      return callback();
    }
    return callback('Регистр-н дугаарын орон буруу байна!');
  }
  return callback();
};
