export function getLocationArrayByParent(location, parentId, hasChildren) {
  const locationData = [];

  if (location) {
    const location1 = location;
    for (let j = 0; j < location1.length; j += 1) {
      if (location1[j].parentId === parentId) {
        locationData.push(
          Object.assign(
            {
              key: location1[j].key,
              value: location1[j].locationId,
              label: location1[j].nameMn,
              disabled: location1[j].used,
              dmsCode: location1[j].dmsCode,
            },
            hasChildren && hasChildren === true ? { children: [] } : {}
          )
        );
      }
    }
  }
  return locationData;
}

export function getLocationByAll(location) {
  let locationData = [];

  if (location) {
    locationData = getLocationArrayByParent(location, 1, true);
    for (let j = 0; j < locationData.length; j += 1) {
      const district = getLocationArrayByParent(location, locationData[j].value, true);
      for (let k = 0; k < district.length; k += 1) {
        const sum = getLocationArrayByParent(location, district[k].value);
        if (sum.length === 0) {
          district[k].disabled = true;
        }
        district[k].children = sum;
      }
      locationData[j].children = district;
    }
  }

  return locationData;
}


export function getLocationWithCityAndDistrict(location) {
  let locationData = [];

  if (location) {
    locationData = getLocationArrayByParent(location, 1, true);
    for (let j = 0; j < locationData.length; j += 1) {
      locationData[j].children = getLocationArrayByParent(location, locationData[j].value);
    }
  }

  return locationData;
}

export function getLocationWithCity(location) {
  let locationData = [];

  if (location) {
    locationData = getLocationArrayByParent(location, 1, false);
  }

  return locationData;
}

export function getDmsCodeByLocationId(location, locationId) {
  if (locationId)
    for (let j = 0; j < location.length; j += 1)
      if (location[j].locationId === locationId) return location[j].dmsCode;

  return -1;
}
