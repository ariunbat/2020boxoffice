export const apiFormatWithTime = 'YYYY-MM-DDThh:mm:ss';
export const apiFormat = 'YYYY-MM-DD';
export const displayFormat = 'YYYY/MM/DD';
export const displayFormatWithTime = 'YYYY/MM/DD hh:mm:ss';
