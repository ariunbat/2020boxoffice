import { apiRequest } from './util';

const API_BASE_URL = 'https://smartcar.mn:8443/analytics-api/api/session-count';

export default async function getCount() {
  return apiRequest(`${API_BASE_URL}`, 'GET');
}
