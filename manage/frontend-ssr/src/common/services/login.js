import { stringify } from 'qs';
import { getBaseUrl } from './base';
import { apiRequest } from './util';

const login = (username, password) => {
  return apiRequest(getBaseUrl() + '/auth/login', 'POST', stringify({ username, password }));
};

export default login;
