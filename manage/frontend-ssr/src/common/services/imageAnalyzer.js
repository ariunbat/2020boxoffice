let IMAGE_ANALYZER_URL = 'http://localhost:9090/fileserver/api/image-analyze/analyze';
if (process.env.NODE_ENV === 'production') {
  IMAGE_ANALYZER_URL = 'http://starter.astvision.mn:8080/cdn/api/image-analyze/analyze';
}

export default function getImageAnalyzerUrl() {
  return IMAGE_ANALYZER_URL;
}
