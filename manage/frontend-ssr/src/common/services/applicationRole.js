import { apiRequestWithToken } from './util';
import { getAuthData } from '../utils/auth';
import { getBaseUrl } from './base';

export default async function list(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(getBaseUrl() + '/application-role', 'GET', token);
}
