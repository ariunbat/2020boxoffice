import { apiRequestWithToken } from './util';
import { getAuthData } from '../utils/auth';

let API_BASE_URL = 'http://localhost:8084/notification-api/api/notification';
if (process.env.NODE_ENV === 'production') {
  API_BASE_URL = 'http://starter.astvision.mn:8080/notification-api/api/notification';
}

export async function getLatest() {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(`${API_BASE_URL}`, 'GET', token);
}

export async function markRead(id) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(`${API_BASE_URL}/mark-read/${id}`, 'GET', token);
}

export async function markReadAll() {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(`${API_BASE_URL}/mark-read-all`, 'GET', token);
}

export function resolveIcon(notification) {
  if (notification.type === 'DATA_UPDATE') {
    notification.icon = 'edit';
  }
}
