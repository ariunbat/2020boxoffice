let SOCKET_URL = 'http://localhost:8084/notification-api/ws';
if (process.env.NODE_ENV === 'production') {
  SOCKET_URL = 'http://starter.astvision.mn:8080/notification-api/ws';
}

export function getSocketUrl() {
  return SOCKET_URL;
}
