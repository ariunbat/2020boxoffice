let API_BASE_URL = 'http://localhost:8081/manage/api';
if (process.env.NODE_ENV === 'production') {
  API_BASE_URL = 'http://starter.astvision.mn:8080/manage/api';
}

export function getBaseUrl() {
  return API_BASE_URL;
}

export function getLocale() {
  // TODO stores and impl later
  return 'mn';
}
