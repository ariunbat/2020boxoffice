import { getLocale } from './base';

let IMAGE_SERVER_URL = 'http://localhost:9090/fileserver/api/file';
if (process.env.NODE_ENV === 'production') {
  IMAGE_SERVER_URL = 'http://starter.astvision.mn:8080/cdn/api/file';
}

export function getImageServerUrl() {
  return IMAGE_SERVER_URL;
}

// export function getImageUploadUrl() {
//   return IMAGE_SERVER_URL + '/upload';
// }
//
// export function getImageCropUrl() {
//   return IMAGE_SERVER_URL + '/crop';
// }
