import { stringify } from 'qs';
import { apiRequestWithToken, jsonRequestWithToken } from './util';
import { getAuthData } from '../utils/auth';
import { getBaseUrl } from './base';

const baseUrl = '/expectation';

export async function list(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(getBaseUrl() + `${baseUrl}?${stringify(params)}`, 'GET', token);
}

export async function create(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return jsonRequestWithToken(getBaseUrl() + `${baseUrl}/create`, 'POST', token, JSON.stringify(params));
}

export async function deleteOne(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(getBaseUrl() + `${baseUrl}/delete`, 'POST', token, stringify(params));
}