import { stringify } from 'qs';
import { apiRequestWithToken, jsonRequestWithToken } from './util';
import { getAuthData } from '../utils/auth';
import { getBaseUrl } from './base';

const baseUrl = '/business-role';

/**
 *
 * @param params (paging, sort etc)
 * currentPage
 * pageSize
 * total
 * @returns {Promise<Object>}
 */
export async function list(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(getBaseUrl() + `${baseUrl}?${stringify(params)}`, 'GET', token);
}

export async function create(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return jsonRequestWithToken(getBaseUrl() + `${baseUrl}/create`, 'POST', token, JSON.stringify(params));
}

export async function update(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return jsonRequestWithToken(getBaseUrl() + `${baseUrl}/update`, 'POST', token, JSON.stringify(params));
}
export async function deleteMulti(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(getBaseUrl() + `${baseUrl}/delete-multi`, 'POST', token, stringify(params));
}
