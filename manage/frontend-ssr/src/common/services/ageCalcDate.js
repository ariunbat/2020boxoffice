import { apiRequestWithToken, jsonRequestWithToken } from './util';
import { getAuthData } from '../utils/auth';
import { getBaseUrl } from './base';

const baseUrl = '/age-calc-date';

export async function get(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(getBaseUrl() + `${baseUrl}`, 'GET', token);
}

export async function update(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return jsonRequestWithToken(getBaseUrl() + `${baseUrl}/update`, 'POST', token, JSON.stringify(params));
}