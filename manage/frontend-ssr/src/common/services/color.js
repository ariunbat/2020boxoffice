import { stringify } from 'qs';
import { apiRequestWithToken } from './util';
import { getAuthData } from '../utils/auth';
import { getBaseUrl } from './base';

const baseUrl = '/color';

export async function list(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(getBaseUrl() + `${baseUrl}`, 'GET', token);
}
