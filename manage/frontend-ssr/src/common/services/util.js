import { getLocale } from './base';

function processResponse(promise) {
  return promise
    .then(response => response.json().then(json => json))
    .catch(e => ({ result: false, message: e.message }));
}

function buildFetch(url, method, headers, body) {
  const requestOptions = Object.assign(
    {},
    { headers },
    {
      method,
      body,
    }
  );
  return fetch(url, requestOptions);
}

export async function apiRequest(url, method, body) {
  const headers = new Headers({
    'Content-Type': 'application/x-www-form-urlencoded', // application/json
    'Accept-Language': getLocale(),
  });

  const promise = buildFetch(url, method, headers, body);
  return processResponse(promise);
}

export async function apiRequestWithToken(url, method, token, body) {
  const headers = new Headers({
    'Content-Type': 'application/x-www-form-urlencoded', // application/json
    'Accept-Language': getLocale(),
    'X-Auth-Token': token,
  });

  const promise = buildFetch(url, method, headers, body);
  return processResponse(promise);
}

export async function jsonRequest(url, method, body) {
  const headers = new Headers({
    'Content-Type': 'application/json',
    'Accept-Language': getLocale()
  });

  const promise = buildFetch(url, method, headers, body);
  return processResponse(promise);
}

export async function jsonRequestWithToken(url, method, token, body) {
  const headers = new Headers({
    'Content-Type': 'application/json',
    'Accept-Language': getLocale(),
    'X-Auth-Token': token,
  });

  const promise = buildFetch(url, method, headers, body);
  return processResponse(promise);
}

