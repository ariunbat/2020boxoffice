import { stringify } from 'qs';
import { apiRequestWithToken, jsonRequestWithToken } from './util';
import { getAuthData } from '../utils/auth';
import { getBaseUrl } from './base';

const baseUrl = '/citizen';

export async function list(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(getBaseUrl() + `${baseUrl}?${stringify(params)}`, 'GET', token);
}

export async function memberList(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(getBaseUrl() + `${baseUrl}/member-list?${stringify(params)}`, 'GET', token);
}

export async function get(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(getBaseUrl() + `${baseUrl}/${params}`, 'GET', token);
}

export async function createMainInfo(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return jsonRequestWithToken(getBaseUrl() + `${baseUrl}/create-main-info`, 'POST', token, JSON.stringify(params));
}

export async function create(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return jsonRequestWithToken(getBaseUrl() + `${baseUrl}/create`, 'POST', token, JSON.stringify(params));
}

export async function update(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return jsonRequestWithToken(getBaseUrl() + `${baseUrl}/update`, 'POST', token, JSON.stringify(params));
}

export async function createMember(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return jsonRequestWithToken(getBaseUrl() + `${baseUrl}/create-member`, 'POST', token, JSON.stringify(params));
}

export async function updateMember(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return jsonRequestWithToken(getBaseUrl() + `${baseUrl}/update-member`, 'POST', token, JSON.stringify(params));
}

export async function deleteOne(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(getBaseUrl() + `${baseUrl}/delete`, 'POST', token, stringify(params));
}

export async function checkExists(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return jsonRequestWithToken(getBaseUrl() + `${baseUrl}/check-citizen-exists`, 'POST', token, JSON.stringify(params));
}

export async function checkCitizen(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return jsonRequestWithToken(getBaseUrl() + `${baseUrl}/check-citizen`, 'POST', token, JSON.stringify(params));
}