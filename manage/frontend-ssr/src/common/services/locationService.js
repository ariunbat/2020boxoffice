import { stringify } from 'qs';
import { apiRequestWithToken } from './util';
import { getAuthData } from '../utils/auth';
import { getBaseUrl } from './base';

const baseUrl = '/location';

export async function data(params) {
    const token = getAuthData() != null ? getAuthData().token : null;
    return apiRequestWithToken(getBaseUrl() + `${baseUrl}?${stringify(params)}`, 'GET', token);
}

export async function list(params) {
    const token = getAuthData() != null ? getAuthData().token : null;
    return apiRequestWithToken(getBaseUrl() + `${baseUrl}/list?${stringify(params)}`, 'GET', token);
}


export async function treeData(params) {
    const token = getAuthData() != null ? getAuthData().token : null;
    return apiRequestWithToken(getBaseUrl() + `${baseUrl}/treeData`, 'GET', token);
}
