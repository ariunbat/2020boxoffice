/**
 * Enum API client
 */
import { apiRequestWithToken } from './util';
import { getAuthData } from '../utils/auth';

let API_BASE_URL = 'http://localhost:8084/reference-api/api/enum';
if (process.env.NODE_ENV === 'production') {
    API_BASE_URL = 'http://starter.astvision.mn:8080/reference-api/api/enum';
}

export async function list() {
    const token = getAuthData() != null ? getAuthData().token : null;
    return apiRequestWithToken(`${API_BASE_URL}`, 'GET', token);
}
