import { stringify } from 'qs';
import { apiRequestWithToken } from './util';
import { getAuthData } from '../utils/auth';
import { getBaseUrl } from './base';

const baseUrl = '/name';

export async function list(params) {
  const token = getAuthData() != null ? getAuthData().token : null;
  return apiRequestWithToken(getBaseUrl() + `${baseUrl}?${stringify(params)}`, 'GET', token);
}