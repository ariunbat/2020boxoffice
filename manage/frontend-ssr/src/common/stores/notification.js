import { observable, action } from 'mobx';
import { getLatest, markRead, markReadAll, resolveIcon } from '../services/notification';

class NotificationStore {
  @observable unreadCount = 0;
  @observable totalCount = 0;
  @observable latestList = [];

  @action
  fetchLatest() {
    getLatest().then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        this.latestList = apiResult.data.list;
        this.totalCount = apiResult.data.totalCount;
        this.unreadCount = apiResult.data.unreadCount;

        // set icons based on notification type
        this.latestList.map((item, i) => {
          resolveIcon(item);
        });
      } else {
        this.latestList = [
          {id: 'n1', content: 'Notif 1', read: true, type: 'GENERAL', createdDateText:'2019/08/20 16:51'},
          {id: 'n2', content: 'Notif 2', read: false, type: 'DATA_UPDATE', createdDateText:'2019/08/20 16:52'},
          {id: 'n3', content: 'Notif 2', read: false, type: 'GENERAL', createdDateText:'2019/08/20 16:53'},
        ];
        this.unreadCount = 3;
        this.totalCount = 3;
      }
    });
  }

  @action
  markRead(id) {
    markRead(id).then(apiResult => {
      if (apiResult.result === true) {
        this.fetchLatest();
      } else {
        // TODO show fail message
      }
    });
  }

  @action
  markReadAll() {
    markReadAll().then(apiResult => {
      if (apiResult.result === true) {
        this.fetchLatest();
      } else {
        // TODO show fail message
      }
    });
  }
}

export default NotificationStore;
