import { observable, action } from 'mobx';
import login from '../services/login';
import { getAuthData, setAuthData } from '../../common/utils/auth';

class AuthStore {
  @observable values = {
    status: false,
    userId: null,
    token: null,
    businessRole: 'ANONYMOUS',
    applicationRoles: ['ROLE_ANONYMOUS'],
    boardId: null,
    photo: null,
    fullName: null
  };

  // TODO remove initialState
  constructor(initialState) {
    try {
      const authData = getAuthData();
      //console.log('initial auth data ->');
      //console.log(authData);
      if (authData) {
        // TODO check token with server
        // TODO if token => valid set in values
        // this.values.status = authData.status;
        this.updateAuthData(authData);
      } else {
        this.reset();
      }
    } catch (e) {
      //console.log(e);
    }
  }

  @action
  reset() {
    // console.log('resetting');
    setAuthData({});

    this.values.status = false;
    this.values.userId = null;
    this.values.token = null;
    this.values.businessRole = 'ANONYMOUS';
    this.values.applicationRoles = ['ROLE_ANONYMOUS'];
    this.values.photo = null;
    this.values.fullName = null;
    this.values.boardId = null;
  }

  @action
  updateAuthData(authData) {
    setAuthData(authData);

    this.values.status = authData.status;
    this.values.userId = authData.userId;
    this.values.token = authData.token;
    this.values.boardId = authData.boardId;
    if (authData.businessRole) {
      this.values.businessRole = authData.businessRole.role;
      this.values.applicationRoles = authData.businessRole.applicationRoles;
    }
    // TODO
    //this.values.photo = null;
    //this.values.fullName = null;
    //console.log('st: ' + this.values.status);
  }

  @action
  login(username, password) {
    const response = login(username, password);
    response.then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        apiResult.data.status = apiResult.result;

        this.updateAuthData(apiResult.data);
      } else {
        this.reset();
      }
    });

    return response;
  }

  toJson() {
    return {
      status: this.values.status,
      userId: this.values.userId,
      token: this.values.token,
      businessRole: this.values.businessRole,
      applicationRoles: this.values.applicationRoles,
      photo: this.values.photo,
      fullName: this.values.fullName,
      boardId: this.values.boardId
    };
  }
}

export default AuthStore;
