import { observable, action, runInAction } from 'mobx';
import { list, create, deleteOne } from '../services/expectation';

class ExpectationStore {
  @observable data = [];
  @observable loading = true;

  constructor(initialState) {
    this.setList(initialState);
  }

  @action
  setList(data) {
    this.data = data;
  }

  @action
  list(payload) {
    this.loading = true;
    const newPayload = Object.assign({ deleted: false }, payload);
    list(newPayload).then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        runInAction(() => {
          this.setList(apiResult.data);
        });
      }
      this.loading = false;
    });
  }

  @action
  create(payload) {
    return create(payload);
  }

  @action
  delete(payload) {
    return deleteOne(payload);
  }
}

export default ExpectationStore;
