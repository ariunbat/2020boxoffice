import { observable, action, runInAction } from 'mobx';
import {
  list, get, create, createMainInfo, update, deleteOne, checkExists,
  memberList, createMember, updateMember, checkCitizen,
} from '../services/citizen';

class CitizenStore {
  @observable data = {
    list: [],
    pagination: [],
  };
  @observable current = {};
  @observable loading = true;

  constructor(initialState) {
    this.setList(initialState);
  }

  @action
  setList(data) {
    this.data = Object.assign(this.data, data);
  }

  @action
  setCurrent(current) {
    this.current = Object.assign(this.current, current);
  }

  @action
  clearCurrent() {
    this.current = {};
  }

  @action
  fetchList(payload) {
    this.loading = true;
    const newPayload = Object.assign({ deleted: false }, payload);
    list(newPayload).then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        runInAction(() => {
          this.setList(apiResult.data);
        })
      }
      this.loading = false;
    });
  }

  @action
  memberList(payload) {
    this.loading = true;
    const newPayload = Object.assign({ deleted: false }, payload);
    memberList(newPayload).then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        runInAction(() => {
          this.setList(apiResult.data);
        })
      }
      this.loading = false;
    });
  }

  @action
  get(payload) {
    this.loading = true;
    get(payload).then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        runInAction(() => {
          this.setCurrent(apiResult.data);
          this.loading = false;
        })
      }
      this.loading = false;
    });
  }

  @action
  create(payload) {
    return create(payload);
  }

  @action
  createMainInfo(payload) {
    return createMainInfo(payload);
  }

  @action
  update(payload) {
    return update(payload);
  }

  @action
  createMember(payload) {
    return createMember(payload);
  }

  @action
  updateMember(payload) {
    return updateMember(payload);
  }

  @action
  delete(payload) {
    return deleteOne(payload);
  }

  @action
  checkExists(payload) {
    return checkExists(payload);
  }

  @action
  checkCitizen(payload) {
    return checkCitizen(payload);
  }
}

export default CitizenStore;
