import { observable, action, runInAction } from 'mobx';
import { list, create, update, deleteOne, selectList } from '../services/colleague';

class ColleagueStore {
  @observable data = [];
  @observable loading = true;

  constructor(initialState) {
    this.setList(initialState);
  }

  @action
  setList(data) {
    this.data = data;
  }

  @action
  list(payload) {
    this.loading = true;
    const newPayload = Object.assign({ deleted: false }, payload);
    list(newPayload).then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        runInAction(() => {
          this.setList(apiResult.data);
        });
      }
      this.loading = false;
    });
  }

  @action
  selectList(payload) {
    this.loading = true;
    return selectList(payload);
  }

  @action
  create(payload) {
    return create(payload);
  }

  @action
  update(payload) {
    return update(payload);
  }

  @action
  delete(payload) {
    return deleteOne(payload);
  }
}

export default ColleagueStore;
