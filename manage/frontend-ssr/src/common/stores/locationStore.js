import { observable, action, runInAction } from 'mobx';
import { data, list, treeData } from '../services/locationService';


class LocationStore {
  @observable data = [];
  @observable sum = [];
  @observable bag = [];
  @observable treeData = [];

  @observable allLocation = [];
  @observable current = {};
  @observable loading = true;

  @action
  setList(data, parentId) {
    if (parentId === 1) {
      let newData = [];
      data.map(option => {
        if (!this.data.find((l) => l.value === option.parentId)) {
          newData.push({
            value: option.locationId,
            parent: option.parentId,
            label: option.name,
            isLeaf: option.leaf
          })
        }
      });
      this.data = newData;
    }
  }

  @action
  setTreeData(data) {
    this.treeData = data;
  }

  @action
  setAllLocation(data) {
    this.allLocation = data;
  }

  @action
  setCurrent(current) {
    this.current = current;
  }

  @action
  list(payload) {
    this.loading = true;
    const promise = data(payload);
    promise.then(response => {
      if (response.result === true && response.data) {
        runInAction(() => {
          this.setList(response.data, payload.parentId);
        });
      }
      this.loading = false;
    });
    return promise;
  }

  @action
  tree(payload) {
    this.loading = true;
    const promise = treeData();
    promise.then(response => {
      if (response.result === true && response.data) {
        runInAction(() => {
          this.setTreeData(response.data);
        });
      }
      this.loading = false;
    });
    return promise;
  }

  @action
  allList(payload) {
    this.loading = true;
    const promise = list(payload);
    promise.then(response => {
      if (response.result === true && response.data) {
        runInAction(() => {
          this.setAllLocation(response.data);
        });
      }
      this.loading = false;
    });
    return promise;
  }
}

export default LocationStore;
