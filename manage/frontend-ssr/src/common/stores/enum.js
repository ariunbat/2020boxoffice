import { observable, action } from 'mobx';
import { list } from '../services/enum';

/**
 * Enum store
 */
class EnumStore {
    @observable enumList = [
        {value: 'ENTITY_TYPE', name: 'Төрөл'},
        {value: 'SERVICE_TYPE', name: 'Үйлчилгээний төрөл'},
    ];

    @action
    fetchList() {
        list().then(apiResult => {
            if (apiResult.result === true && apiResult.data) {
                this.enumList = apiResult.data.list;
            }
        });
    }
}

export default EnumStore;
