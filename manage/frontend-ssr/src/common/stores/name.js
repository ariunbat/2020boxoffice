import { observable, action, runInAction } from 'mobx';
import { list } from '../services/name';

class NameStore {


  @action
  list(payload) {
    this.loading = true;
    return list(payload);
  }

}

export default NameStore;
