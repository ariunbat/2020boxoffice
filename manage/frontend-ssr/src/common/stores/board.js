import { observable, action, runInAction } from 'mobx';
import { list, create, update, deleteOne } from '../services/board';

class BoardStore {
  @observable data = [];
  @observable loading = true;

  constructor(initialState) {
    this.setList(initialState);
  }

  @action
  setList(data) {
    this.data = data;
  }

  @action
  list(payload) {
    this.loading = true;
    const newPayload = Object.assign({ deleted: false }, payload);
    list(newPayload).then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        runInAction(() => {
          this.setList(apiResult.data);
        });
      }
      this.loading = false;
    });
  }

  @action
  create(payload) {
    return create(payload);
  }

  @action
  update(payload) {
    return update(payload);
  }

  @action
  delete(payload) {
    return deleteOne(payload);
  }
}

export default BoardStore;
