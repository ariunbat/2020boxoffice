import { observable, action, runInAction } from 'mobx';
import list from '../services/applicationRole';

class ApplicationRoleStore {
  @observable list = [];
  @observable loading = true;

  constructor(initialState) {
    if (initialState) {
      this.setList(initialState);
    }
  }

  @action
  setList(data) {
    this.list = Object.assign(this.list, data);
  }

  @action
  fetchRoles(payload) {
    this.loading = true;
    list(payload).then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        runInAction(() => {
          this.setList(apiResult.data);
          this.loading = false;
        })
      }
    });
  }
}

export default ApplicationRoleStore;
