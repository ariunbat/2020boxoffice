import { observable, action } from 'mobx';

class LangStore {
  @observable locale = 'en';

  @action
  setLocale(locale) {
    this.locale = locale;
  }

  constructor(locale) {
    this.locale = locale;
  }
}

export default LangStore;
