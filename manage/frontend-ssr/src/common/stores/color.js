import { observable, action, runInAction } from 'mobx';
import { list } from '../services/color';

class ColorStore {
  @observable data = [];
  @observable loading = true;

  constructor(initialState) {
    this.setList(initialState);
  }

  @action
  setList(data) {
    this.data = data;
  }

  @action
  list(payload) {
    this.loading = true;
    const newPayload = Object.assign({ deleted: false }, payload);
    return list(newPayload).then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        runInAction(() => {
          this.setList(apiResult.data);
        });
      }
      this.loading = false;
    });
  }
}

export default ColorStore;
