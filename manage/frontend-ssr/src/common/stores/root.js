import AuthStore from './auth';
import LangStore from './lang';
import NotificationStore from './notification';
import ReferenceStore from './reference';
import EnumStore from './enum';
import ServiceFormStore from './serviceForm';
import LocationStore from './locationStore';
import CitizenStore from './citizen';
import UserStore from './user';
import BusinessRoleStore from './businessRoleStore';
import ApplicationRoleStore from './applicationRole';
import AgeCalcDate from './ageCalcDate';
import FamilyMemberStore from './familyMember';
import ColleagueStore from './colleague';
import ColleagueTypeStore from './colleagueType';
import ContentStore from './content';
import NameStore from './name';
import ColorStore from './color';
import PartyStore from './party';
import ExpectationStore from './expectation';
import BoardStore from './board';

class RootStore {
  authStore = null;
  langStore = null;
  notificationStore = null;
  referenceStore = null;
  enumStore = null;
  serviceFormStore = null;
  locationStore = null;
  citizenStore = null;
  userStore = null;
  businessRoleStore = null;
  applicationRoleStore = null;
  ageCalcDate = null;
  familyMemberStore = null;
  colleagueStore = null;
  colleagueTypeStore = null;
  contentStore = null;
  nameStore = null;
  colorStore = null;
  partyStore = null;
  expectationStore = null;
  boardStore = null;

  constructor(initialStates) {
    this.authStore = new AuthStore(initialStates.authStore);
    // other stores
    this.langStore = new LangStore();
    this.notificationStore = new NotificationStore();
    this.referenceStore = new ReferenceStore();
    this.enumStore = new EnumStore();
    this.serviceFormStore = new ServiceFormStore();
    this.locationStore = new LocationStore();
    this.citizenStore = new CitizenStore();
    this.userStore = new UserStore();
    this.businessRoleStore = new BusinessRoleStore();
    this.applicationRoleStore = new ApplicationRoleStore();
    this.ageCalcDate = new AgeCalcDate();
    this.familyMemberStore = new FamilyMemberStore();
    this.colleagueStore = new ColleagueStore();
    this.colleagueTypeStore = new ColleagueTypeStore();
    this.contentStore = new ContentStore();
    this.nameStore = new NameStore();
    this.colorStore = new ColorStore();
    this.partyStore = new PartyStore();
    this.expectationStore = new ExpectationStore();
    this.boardStore = new BoardStore();
  }

  getStores() {
    return {
      authStore: this.authStore,
      langStore: this.langStore,
      notificationStore: this.notificationStore,
      referenceStore: this.referenceStore,
      enumStore: this.enumStore,
      serviceFormStore: this.serviceFormStore,
      locationStore: this.locationStore,
      citizenStore: this.citizenStore,
      userStore: this.userStore,
      businessRoleStore: this.businessRoleStore,
      applicationRoleStore: this.applicationRoleStore,
      ageCalcDate : this.ageCalcDate,
      familyMemberStore: this.familyMemberStore,
      colleagueStore: this.colleagueStore,
      colleagueTypeStore: this.colleagueTypeStore,
      contentStore: this.contentStore,
      nameStore: this.nameStore,
      colorStore: this.colorStore,
      partyStore: this.partyStore,
      expectationStore: this.expectationStore,
      boardStore: this.boardStore,
    };
  }
}

export default RootStore;
