import { observable, action, runInAction } from 'mobx';
import { list, create, update, deleteOne } from '../services/user';

class UserStore {
  @observable data = {
    list: [],
    pagination: [],
  };
  @observable selectListData = [];
  @observable current = {};
  @observable loading = true;

  constructor(initialState) {
    this.setList(initialState);
  }

  @action
  setList(data) {
    this.data = Object.assign(this.data, data);
  }

  @action
  setSelectListData(selectListData) {
    this.selectListData = Object.assign(this.selectListData, selectListData);
  }

  @action
  setCurrent(current) {
    this.current = Object.assign(this.current, current);
  }

  @action
  list(payload) {
    this.loading = true;
    const newPayload = Object.assign({ deleted: false }, payload);
    list(newPayload).then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        runInAction(() => {
          this.setList(apiResult.data);
          this.loading = false;
        })
      }
    });
  }

  @action
  selectList(payload) {
    this.loading = true;
    const newPayload = Object.assign({ deleted: false }, payload);
    list(newPayload).then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        runInAction(() => {
          this.setSelectListData(apiResult.data.list);
          this.loading = false;
        })
      }
    });
  }

  @action
  get(payload) {
    this.loading = true;
    get(payload).then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        runInAction(() => {
          this.setCurrent(apiResult.data);
          this.loading = false;
        })
      }
    });
  }

  @action
  create(payload) {
    return create(payload);
  }

  @action
  update(payload) {
    return update(payload);
  }

  @action
  delete(payload) {
    return deleteOne(payload);
  }
}

export default UserStore;
