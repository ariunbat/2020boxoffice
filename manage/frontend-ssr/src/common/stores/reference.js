import { observable, action } from 'mobx';
import { list } from '../services/reference';

/**
 * Лавлах сангийн store
 */
class ReferenceStore {
    @observable referenceList = [
        {id: 1, name: 'Салбар'},
        {id: 2, name: 'Баримтын төрөл'},
    ];

    @action
    fetchList() {
        list().then(apiResult => {
            if (apiResult.result === true && apiResult.data) {
                this.referenceList = apiResult.data.list;
            }
        });
    }
}

export default ReferenceStore;
