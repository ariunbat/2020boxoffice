import { observable, action, runInAction } from 'mobx';
import { list, create, update, deleteMulti } from '../services/businessRole';

class BusinessRoleStore {
  @observable data = {
    list: [],
    pagination: [],
  };
  @observable current = {};
  @observable loading = true;

  constructor(initialState) {
    this.setList(initialState);
  }

  @action
  setList(data) {
    this.data = Object.assign(this.data, data);
  }

  @action
  setCurrent(current) {
    this.current = Object.assign(this.current, current);
  }

  @action
  list(payload) {
    this.loading = true;
    list(payload).then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        runInAction(() => {
          this.setList(apiResult.data);
          this.loading = false;
        })
      }
    });
  }

  @action
  get(payload) {
    this.loading = true;
    get(payload).then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        runInAction(() => {
          this.setCurrent(apiResult.data);
          this.loading = false;
        })
      }
    });
  }

  @action
  create(payload) {
    return create(payload);
  }

  @action
  update(payload) {
    return update(payload);
  }

  @action
  deleteMulti(payload) {
    return deleteMulti(payload);
  }
}

export default BusinessRoleStore;
