import { observable, action, runInAction } from 'mobx';
import { get, update } from '../services/ageCalcDate';

class AgeCalcDate {
  @observable current = {};
  @observable loading = true;

  @action
  setCurrent(current) {
    this.current = current;
  }

  @action
  get(payload) {
    this.loading = true;
    get(payload).then(apiResult => {
      if (apiResult.result === true && apiResult.data) {
        runInAction(() => {
          this.setCurrent(apiResult.data);
          this.loading = false;
        })
      }
    });
  }

  @action
  update(payload) {
    return update(payload);
  }
}

export default AgeCalcDate;
