import { observable, action } from 'mobx';

class ServiceFormStore {
  @observable steps = {};
  @observable stepCount = 0;

  constructor(initialState) {
    if (initialState) {
      this.setSteps(initialState.steps);
      this.setStepCount(initialState.stepCount);
    }
  }

  @action
  setSteps(steps) {
    this.steps = steps;
  }

  @action
  setStepCount(stepCount) {
    this.stepCount = stepCount;
  }

  @action
  addStep(stepData) {
    this.steps[stepData.key] = {
      key: stepData.key,
      name: stepData.name,
      formItems: [],
    };
    this.stepCount += 1;
  }

  getStepArray() {
    let stepArray = [];
    let _steps = this.steps;
    Object.keys(_steps).forEach(function(key) {
      stepArray.push(_steps[key]);
    });
    return stepArray;
  }
}

export default ServiceFormStore;
