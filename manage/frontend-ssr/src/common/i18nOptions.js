// import en from '../common/locales/en.json';
// import mn from '../common/locales/mn.json';

const i18nOptions = {
  // debug: false,
  load: 'languageOnly', // 2 character locale (en, mn) instead of 5 (en-US)
  fallbackLng: 'mn',
  ns: ['translations'],
  defaultNS: 'translations',
  caches: ['localStorage', 'cookie'],
  order: ['localStorage', 'cookie'],
};

export const i18nOptionsServer = {
  debug: false,
  detection: Object.assign(i18nOptions, {
    preload: ['mn', 'en'],
  }),
  // resources: {
  //   en: en,
  //   mn: mn
  // }
};

export const i18nOptionsClient = Object.assign(i18nOptions, {
  wait: process && !process.release
});
