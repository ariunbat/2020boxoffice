import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Layout, Menu, Icon } from 'antd';
import { inject, observer } from 'mobx-react/index';
import withStyles from 'isomorphic-style-loader/withStyles';
import classNames from 'classnames';
import { Trans } from 'react-i18next';

import styles from './sideMenu.less';
import logo from '../assets/logo_header.svg';
import { checkAuth } from '../../common/utils/auth';

let firstMount = true;
const navTheme = 'light';
const { SubMenu } = Menu;

@inject('authStore')
@observer
class SideMenu extends Component {
  constructor(props) {
    super(props);
    firstMount = false;
  }

  render() {
    const { authStore, collapsed, location } = this.props;

    const menus = [
      {
        key: '/dashboard',
        link: '/dashboard',
        icon: 'dashboard',
        title: 'menu.dashboard'
      },
      {
        key: '/citizen',
        link: '/citizen',
        icon: 'idcard',
        title: 'menu.citizen',
        role: 'ROLE_MANAGE_CITIZEN',
      },
      {
        key: '/member',
        link: '/member',
        icon: 'solution',
        title: 'menu.member',
        role: 'ROLE_MANAGE_MEMBER',
      },
      {
        key: '/categories',
        link: '/categories',
        icon: 'apartment',
        title: 'menu.categories.index',
        role: 'ROLE_MANAGE_DATA',
        subMenu: true,
        child: [
          {
            key: '/categories/colleagueType',
            link: '/categories/colleagueType',
            title: 'menu.categories.colleagueType',
            role: 'ROLE_MANAGE_DATA',
          },
          {
            key: '/categories/party',
            link: '/categories/party',
            title: 'menu.categories.party',
            role: 'ROLE_MANAGE_DATA',
          },
        ]
      },
      {
        key: '/settings',
        link: '/settings',
        icon: 'setting',
        title: 'menu.settings.index',
        role: 'ROLE_MANAGE_DATA',
        subMenu: true,
        child: [
          {
            key: '/settings/ageCalcDate',
            link: '/settings/ageCalcDate',
            icon: 'calendar',
            title: 'menu.settings.ageCalcDate',
            role: 'ROLE_MANAGE_DATA',
          },
          {
            key: '/settings/board',
            link: '/settings/board',
            icon: 'flag',
            title: 'menu.settings.board',
            role: 'ROLE_MANAGE_DATA',
          },
          {
            key: '/settings/adminUser',
            link: '/settings/adminUser',
            icon: 'user',
            title: 'menu.settings.admin',
            role: 'ROLE_MANAGE_ADMIN',
          },
        ]
      },
      {
        key: '/user',
        link: '/user',
        icon: 'user',
        title: 'menu.user',
        role: 'ROLE_MANAGE_USER',
      },
      {
        key: '/businessRole',
        link: '/businessRole',
        icon: 'solution',
        title: 'menu.businessRole',
        role: 'ROLE_MANAGE_BUSINESS_ROLE',
      },
    ];

    const renderchild = item => {
    if (checkAuth(item.role)) {
      return (
        <Menu.Item key={item.key}>
          <Link to={item.link}>
            <Icon type={item.icon} />
            <span>
              <Trans>{item.title}</Trans>
            </span>
          </Link>
        </Menu.Item>)
    }
    return null;
  };

    return (
      <Layout.Sider
        trigger={null}
        collapsible
        collapsed={collapsed}
        breakpoint="lg"
        width={256}
        theme={navTheme}
        className={classNames(styles.sider, styles.light)}
      >
        <div className={styles.logo}>
          <Link to="/">
            <Icon type='dropbox' />
            <h1>2020 Box Office</h1>
          </Link>
        </div>
        <Menu
          mode="inline"
          theme={navTheme}
          // defaultSelectedKeys={['1']}
          // defaultOpenKeys={['sub1']}
          selectedKeys={[this.currentPath]}
          style={{ borderRight: 0 }}
        >
          {menus.map((item, idx) => {
            if (item.subMenu) {
              if (checkAuth(item.role)) {
                return (
                  <SubMenu
                    key={item.key}
                    title={
                      <span>
                        <Icon type={item.icon} />
                        <Trans>{item.title}</Trans>
                      </span>
                    }>
                    {item.child && item.child.length !== 0 && item.child.map(renderchild)}
                  </SubMenu>
                )
              }
            } else {
              if (checkAuth(item.role)) {
                return (
                  <Menu.Item key={item.key}>
                    <Link to={item.link}>
                      <Icon type={item.icon} />
                      <span>
                        <Trans>{item.title}</Trans>
                      </span>
                    </Link>
                  </Menu.Item>
                )
              }
            }
          })}

        </Menu>
      </Layout.Sider>
    );
  }
}

export default withRouter(withStyles(styles)(SideMenu));
