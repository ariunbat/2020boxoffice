import React, { Component } from 'react';
import { inject, observer } from 'mobx-react/index';
import withStyles from 'isomorphic-style-loader/withStyles';
import { Link } from 'react-router-dom';
import { Layout, Icon, Button } from 'antd';
import { withTranslation, Trans } from 'react-i18next';

import i18n from '../../common/i18n';
import TopMenu from './topMenu';
import styles from './publicHeader.less';
import logo from '../assets/logo_header.svg';

@withTranslation()
@inject('langStore')
@observer
class PublicHeader extends Component {

  setLocale(locale) {
    const { langStore } = this.props;

    const newLocale = locale === 'en' ? 'mn' : 'en';
    langStore.setLocale(newLocale);
    // i18n.changeLanguage(newLocale);
  }

  render() {
    const { collapsed, toggle, isMobile, history, t } = this.props;

    return (
      <Layout.Header style={{ padding: 0, height: 'auto' }}>
        <div className={styles.header}>
          {isMobile ?
            <div className={styles.mobile}>
              <span className={styles.trigger} onClick={toggle}>
                <Icon type={collapsed ? 'menu-unfold' : 'menu-fold'} />
              </span>
              <Link to="/" className={styles.logo} key="logo">
                <img src={logo} alt="logo" />
              </Link>
              <div className={styles.lang}>
                <Button
                  className=""
                  onClick={() => this.setLocale(i18n.language)}
                >
                  <Trans>navbar.lang</Trans>
                </Button>
              </div>
            </div>
            : null
          }
          <TopMenu history={history} isMobile={isMobile} />
        </div>
      </Layout.Header>
    );
  }
}

export default withStyles(styles)(PublicHeader);
