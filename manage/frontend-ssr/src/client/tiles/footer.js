import React from 'react';
import { Layout } from 'antd';

const Footer = () => (
  <Layout.Footer style={{ textAlign: 'center' }}>©2019 Astvision LLC</Layout.Footer>
);

export default Footer;
