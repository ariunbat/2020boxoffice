import React, { Component } from 'react';
import { Icon, Badge, notification } from 'antd';
import withStyles from 'isomorphic-style-loader/withStyles';
import classNames from 'classnames';
import { inject, observer } from 'mobx-react/index';
import SockJsClient from 'react-stomp';

import HeaderDropdown from '../components/HeaderDropdown/index';
import NotificationList from '../components/Notification/NotificationList';
import { getSocketUrl } from '../../common/services/notificationSocket';
import { getAuthData } from '../../common/utils/auth';
import { resolveIcon } from '../../common/services/notification';

import styles from './notificationIcon.less';

@inject('notificationStore')
@observer
class NotificationIcon extends Component {

  state = {
    visible: false,
  };

  componentDidMount() {
    const { notificationStore } = this.props;
    notificationStore.fetchLatest();
  }

  getNotificationList() {
    const { notificationStore } = this.props;

    // const notifications = notificationStore.latestList;
    return (
      <NotificationList
        data={notificationStore.latestList}
        notificationClick={this.notificationClick}
        markReadAll={this.markReadAll}
      />
    );
  };

  notificationClick = (notification) => {
    const { history } = this.props;
    this.markRead(notification.id);
    if (notification.type === 'DATA_UPDATE') {
      // navigate to detail page
      switch(notification.entity) {
        case 'article':
          history.push(`/article/${notification.entityId}`);
          break;
        case 'service':
          history.push(`/service/${notification.entityId}`);
          break;
      }
    }
  };

  markRead = id => {
    const { notificationStore } = this.props;
    notificationStore.markRead(id);
  };

  markReadAll = () => {
    const { notificationStore } = this.props;
    notificationStore.markReadAll();
  };

  handleVisibleChange = visible => {
    //const { onPopupVisibleChange } = this.props;
    this.setState({ visible });
    //onPopupVisibleChange(visible);
  };

  onSocketMessage = (socketMessage) => {
    const { notificationStore } = this.props;
    notificationStore.fetchLatest();

    // show popup
    resolveIcon(socketMessage);
    notification.open({
      message: 'Сонордуулга',
      description: socketMessage.content,
      icon: <Icon type={socketMessage.icon || 'notification'} />,
    });
  };

  render() {
    const { notificationStore } = this.props;
    const { visible } = this.state;

    const notificationList = this.getNotificationList();
    const authData = getAuthData();

    return (
      <HeaderDropdown
        placement="bottomRight"
        overlay={notificationList}
        overlayClassName={styles.popover}
        trigger={['click']}
        visible={visible}
        onVisibleChange={this.handleVisibleChange}
      >
        <span>
          <span className={classNames(styles.noticeButton, { opened: visible })}>
            <Badge count={notificationStore.unreadCount} style={{ boxShadow: 'none' }} className={styles.badge}>
              <Icon type="bell" />
            </Badge>
          </span>
          {
            authData && authData.token && authData.status === true ?
              <SockJsClient
                url={getSocketUrl()}
                headers={{'X-Auth-Token': authData.token}}
                topics={[`/topic/notification/${authData.userId}`]}
                onMessage={this.onSocketMessage}
                // onConnect={this.onConnect}
                // onDisconnect={this.onDisconnect}
                ref={client => {
                  this.clientRef = client;
                }}
              /> : null
          }
        </span>
      </HeaderDropdown>
    );
  }
}

export default withStyles(styles)(NotificationIcon);
