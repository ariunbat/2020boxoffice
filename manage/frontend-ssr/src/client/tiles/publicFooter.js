import React, { PureComponent } from 'react';
import { Row, Col, Divider, List, Icon } from 'antd';
import withStyles from 'isomorphic-style-loader/withStyles';
import { Link } from 'react-router-dom';
import getCount from '../../common/services/analytics';
import { withTranslation, Trans } from 'react-i18next';
import classnames from 'classnames';

import Ast from '../assets/logo/logo-dev.svg';
import styles from './publicFooter.less';
import Google from '../assets/logo/logo-ga.svg';

@withTranslation()
class Footer extends PureComponent {
  state = {
    countData: null
  };

  componentDidMount() {
    const promise = getCount();

    promise.then(response => {
      if (response.result) {
        this.setState({ countData: response.data });
      }
    });

  }

  render() {
    const { countData } = this.state;
    const { t } = this.props;

    const contactList = [
      {
        title: t('footer.contact.address.title'),
        content: t('footer.contact.address.content'),
      }, {
        title: t('footer.contact.mailbox.title'),
        content: t('footer.contact.mailbox.content'),
      }, {
        title: t('footer.contact.phone.title'),
        content: t('footer.contact.phone.content'),
      }, {
        title: t('footer.contact.fax.title'),
        content: t('footer.contact.fax.content'),
      }, {
        title: t('footer.contact.email.title'),
        content: t('footer.contact.email.content'),
      },
    ];

    const socialListData = [
      {
        link: 'https://www.facebook.com/AstvisionLLC/',
        icon: 'facebook',
        title: 'Facebook',
      },
      {
        link: '',
        icon: 'twitter',
        title: 'Twitter',
      },
      {
        link: '',
        icon: 'youtube',
        title: 'Youtube',
      },
    ];

    const googleAnalytic = [
      {
        link: '',
        title: 'Өнөөдөр',
        data: countData && countData.today ? countData.today : '-'
      },
      {
        link: '',
        title: 'Энэ 7 хоног',
        data: countData && countData.thisWeek ? countData.thisWeek : '-'
      },
      {
        link: '',
        title: 'Энэ сар',
        data: countData && countData.thisMonth ? countData.thisMonth : '-'
      },
      {
        link: '',
        title: 'Нийт',
        data: countData && countData.total ? countData.total : '-'
      },
    ];

    return (
      <div className={styles.main}>
        <div className={styles.header}>
          <div className="container">
            <Link to="/home">
              <img src={Ast} alt="" />
            </Link>
            <Divider className="autorentDivider" />
          </div>
        </div>
        <div className={styles.content}>
          <div className="container">
            <Row gutter={25}>
              <Col xs={24} sm={24} md={14} lg={14} xl={14}>
                <List
                  className={styles.list}
                  size="small"
                  header={
                    <div className={styles.listHeader}>
                      <Trans>footer.contact.contact</Trans>
                    </div>
                  }
                  dataSource={contactList}
                  renderItem={item => (
                    <List.Item className={classnames(styles.listItem, styles.contact)}>
                      <p>
                        <span>{item.title}:</span> {item.content}
                      </p>
                    </List.Item>
                  )}
                />
              </Col>
              <Col xs={24} sm={24} md={6} lg={6} xl={6}>
                <List
                  className={styles.list}
                  size="small"
                  header={
                    <div className={styles.listHeader}>
                      <Trans>footer.social.title</Trans>
                    </div>
                  }
                  dataSource={socialListData}
                  renderItem={item => (
                    <List.Item className={styles.listItem}>
                      <a
                        href={item.link}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        <Icon type={item.icon} /> {item.title}
                      </a>
                    </List.Item>
                  )}
                />
              </Col>
              <Col xs={24} sm={24} md={4} lg={4} xl={4}>
                <List
                  className={styles.list}
                  size="small"
                  header={
                    <div className={styles.listHeader}>
                      <Trans>footer.analytics.title</Trans>
                    </div>
                  }
                  dataSource={googleAnalytic}
                  renderItem={item => (
                    <List.Item className={styles.listItem}>
                      <Link to={item.link}>{item.title}: {item.data}</Link>
                    </List.Item>
                  )}
                />
                <img src={Google} alt="" className={styles.google} />
              </Col>
            </Row>
          </div>
        </div>
        <div className={styles.footer}>
          <div className="container">
            <Row gutter={25}>
              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <a
                  href="http://astvision.mn"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  ©2019 Astvision LLC
                </a>
              </Col>
              <Col xs={24} sm={24} md={12} lg={12} xl={12}>
                <a
                  className={styles.ast}
                  href="http://astvision.mn"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <img className={styles.astvisionLogo} src={Ast} alt="" />
                </a>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    )
  }
}


export default withStyles(styles)(Footer);
