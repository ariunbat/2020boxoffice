import React, { Component } from 'react';
import withStyles from 'isomorphic-style-loader/withStyles';
import { Link } from 'react-router-dom';
import { Layout, Icon } from 'antd';

import TopRightMenu from './topRightMenu';
import styles from './header.less';

class Header extends Component {
  render() {
    const { collapsed, toggle, isMobile, history } = this.props;

    return (
      <Layout.Header style={{ background: '#fff', padding: 0 }}>
        <div className={styles.header}>
          {isMobile ?
            <Link to="/" className={styles.logo} key="logo">
              <Icon type='dropbox' />
            </Link>
            : null
          }
          <TopRightMenu history={history} />
          <span className={styles.trigger} onClick={toggle}>
            <Icon type={collapsed ? 'menu-unfold' : 'menu-fold'} />
          </span>
        </div>
      </Layout.Header>
    );
  }
}

export default withStyles(styles)(Header);
