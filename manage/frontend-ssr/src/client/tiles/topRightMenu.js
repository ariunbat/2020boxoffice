import React, { Component } from 'react';
import { inject, observer } from 'mobx-react/index';
import withStyles from 'isomorphic-style-loader/withStyles';
// import { Link } from 'react-router-dom';
import { Tooltip, Icon, Menu, Avatar } from 'antd';

import HeaderDropdown from '../components/HeaderDropdown';
import NotificationIcon from './notificationIcon';
import styles from './topRightMenu.less';

@inject('authStore')
@observer
class TopRightMenu extends Component {
  handleMenuClick = ({ key }) => {
    const { authStore, history } = this.props;

    if (key === 'logout') {
      // do logout
      authStore.reset();
      history.push('/auth/login');
    }
  };

  render() {
    const { authStore, history } = this.props;

    const menu = (
      <Menu className={styles.menu} selectedKeys={[]} onClick={this.handleMenuClick}>
        <Menu.Item key="logout">
          <Icon type="logout" />
          &nbsp;Гарах
        </Menu.Item>
      </Menu>
    );

    return (
      <div className={styles.right}>
        <HeaderDropdown overlay={menu}>
          <span className={`${styles.action} ${styles.account}`}>
            {authStore.values.avatar ?
              <Avatar
                size="small"
                className={styles.avatar}
                src={authStore.values.avatar}
                alt="avatar"
              />
              :
              <Avatar
                size="small"
                className={styles.avatar}
                icon="user"
                alt="avatar"
              />}
            <span className={styles.name}>{authStore.values.fullName || 'Хэрэглэгч'}</span>
          </span>
        </HeaderDropdown>
      </div>
    );
  }
}

export default withStyles(styles)(TopRightMenu);
