import React, { Component } from 'react';
import { Transformer } from 'react-konva';

class KonvaTransformer extends Component {
  componentDidMount() {
    this.checkNode();
  }
  componentDidUpdate() {
    this.checkNode();
  }
  checkNode() {
    // here we need to manually attach or detach Transformer node
    const { selectedShapeName, handleResize } = this.props;
    const stage = this.transformer.getStage();

    const selectedNode = stage.findOne('.' + selectedShapeName);
    // do nothing if selected node is already attached
    if (selectedNode === this.transformer.node()) {
      return;
    }

    if (selectedNode) {
      // attach to another node
      this.transformer.attachTo(selectedNode);
    } else {
      // remove transformer
      this.transformer.detach();
    }
    this.transformer.getLayer().batchDraw();
  }

  boundBoxFunc = (oldBox, newBox) => {
    const { minWidth, minHeight, maxWidth, maxHeight, handleResize } = this.props;
    //console.log({w: newBox.width, h: newBox.height, mw: maxWidth, mh: maxHeight});
    //console.log({c1: newBox.width > maxWidth, c2: newBox.height > maxHeight});
    if (newBox.x < 0 || newBox.y < 0
      || newBox.x > maxWidth || newBox.y > maxHeight
      || newBox.width > maxWidth || newBox.height > maxHeight
      || newBox.width < minWidth || newBox.height < minHeight
    ) {
      return oldBox;
    }

    handleResize(newBox);
    return newBox;
  };

  render() {
    return (
      <Transformer rotateEnabled={false}
                   keepRatio={true}
                   enabledAnchors={['top-left', 'top-right', 'bottom-left', 'bottom-right']}
                   boundBoxFunc={this.boundBoxFunc}
                   ref={node => {
                     this.transformer = node;
                   }}
      />
    );
  }
}

export default KonvaTransformer;
