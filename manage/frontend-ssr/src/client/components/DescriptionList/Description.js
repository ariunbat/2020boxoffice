import React from 'react';
import PropTypes from 'prop-types';
import { Col } from 'antd';
import styles from './index.less';
import responsive from './responsive';
import withStyles from 'isomorphic-style-loader/withStyles';


const Description = ({ term, column, verticalTerm, children, ...restProps }) => (
  <Col {...responsive[column]} {...restProps}>
    {term && <div className={styles.term}>{term}</div>}
    {verticalTerm && <div className={styles.verticalTerm}>{verticalTerm}</div>}
    {children !== null && children !== undefined && <div className={styles.detail}>{children}</div>}
  </Col>
);

Description.defaultProps = {
  term: '',
};

Description.propTypes = {
  term: PropTypes.node,
};

export default withStyles(styles)(Description);
