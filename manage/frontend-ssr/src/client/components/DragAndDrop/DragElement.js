import React from 'react';
import { useDrag } from 'react-dnd-cjs';

const DragElement = ({ itemType, element }) => {
    const [{ isDragging }, drag] = useDrag({
        item: { type: itemType },
        canDrag: true,
        collect: monitor => ({
            isDragging: monitor.isDragging(),
        }),
    });

    return (
        <div ref={drag}>
            {element}
        </div>
    )
};

export default DragElement;
