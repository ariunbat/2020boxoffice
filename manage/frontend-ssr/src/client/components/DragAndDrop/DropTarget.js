import React from 'react';
import { useDrop } from 'react-dnd-cjs';

const DropTarget = (props) => {
    const [{ isOver, canDrop }, drop] = useDrop({
        accept: props.accept, // ['TEXT', 'INPUT']
        drop(item) {
            props.onDrop(item.type);
            return undefined;
        },
        collect: monitor => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
        }),
    });
    const opacity = isOver ? 1 : 0.7;
    return (
        <div ref={drop} style={{ opacity }}>
            {props.children}
        </div>
    )
};

export default DropTarget;
