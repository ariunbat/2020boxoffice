import React from 'react';
import { Avatar, List, Skeleton, Row, Col } from 'antd';
import withStyles from 'isomorphic-style-loader/withStyles';
import classNames from 'classnames';

import styles from './NotificationList.less';

function NotificationList(
  {
    data = [],
    notificationClick,
    markReadAll,
  })
{
  return (
    <div>
      <List className={styles.list}>
        {
          data.map((item, i) => {
            const itemCls = classNames(styles.item, {
              [styles.read]: item.read,
            });

            return <List.Item
              key={item.id}
              className={itemCls}
              onClick={() => notificationClick(item)}
            >
              <Skeleton avatar title={false} active loading={false}>
                <List.Item.Meta
                  className={styles.meta}
                  avatar={<Avatar icon={ item.icon || 'notification' } />}
                  title={
                    <div className={styles.title}>
                      {item.content}
                      {/*<div className={styles.extra}>{item.extra}</div>*/}
                    </div>
                  }
                  description={
                    <div>
                      {/*<div className={styles.description} title={item.description}>*/}
                        {/*{item.description}*/}
                      {/*</div>*/}
                      <div className={styles.datetime}>{item.createdDateText}</div>
                    </div>
                  }
                />
              </Skeleton>
            </List.Item>
          })
        }
      </List>
      <div className={styles.actionContainer}>
        <Row>
          <Col span={12} className={styles.actionItem}>
            <a onClick={markReadAll}>Бүгдийг уншсан</a>
          </Col>
          <Col span={12} className={styles.actionItem}>
            <a>Бүгдийг харах</a>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default withStyles(styles)(NotificationList);
