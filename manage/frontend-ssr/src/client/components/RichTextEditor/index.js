import React, { Component } from 'react';
import { Editor } from '@tinymce/tinymce-react';

class RichTextEditor extends Component {
  // TODO хэрэв plugin нэмсэн бол JS файлаа татаж аваад \src\client\assets\tinymce folder дотор хуулах шаардлагатай

  render() {
    const { onEditorChange, value, init, disabled } = this.props;

    const defaultInit = {
      toolbar: 'bold italic underline | alignleft aligncenter alignright | forecolor',
    };

    return (
      <Editor
        apiKey="2576zajtql8smg94vmlcenzxxfazm3p932utnmmhuaagzj61"
        init={init || defaultInit}
        value={value && value != null ? value : ''}
        onEditorChange={onEditorChange}
        disabled={disabled}
      />
    )
  }
}

export default (RichTextEditor);
