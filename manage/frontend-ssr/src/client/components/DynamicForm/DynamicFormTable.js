import React, {Component} from 'react';
import { Button } from 'antd';
import withStyles from 'isomorphic-style-loader/withStyles';

import styles from './DynamicFormTable.less';

class DynamicFormTable extends Component {

    state = {
        cols: {
            'col-1': {
                key: 'col-1',
                header: 'Толгой 1',
                field: 'field1',
            },
            'col-2': {
                key: 'col-2',
                header: 'Толгой 2',
                field: 'field2',
            }
        },
        colCounter: 2
    };

    handleAdd = () => {
        const { cols, colCounter } = this.state;

        const colKey = `col-${colCounter + 1}`;
        cols[colKey] = {
            key: colKey,
            header: `Толгой ${colCounter + 1}`,
            field: `field${colCounter + 1}`,
        };
        this.setState({ cols, colCounter: colCounter + 1 });
    };

    handleRemove = (colKey) => {
        const { cols } = this.state;
        delete cols[colKey];
        this.setState({ cols });
    };

    handleFieldChange = (e) => {
        const { cols } = this.state;
        let col = cols[e.target.name];
        if (col) {
            col.field = e.target.value;
            this.setState({ cols });
        }
    };

    render() {
        const { cols } = this.state;

        let colArray = [];
        Object.keys(cols).forEach(function(key) {
            colArray.push(cols[key]);
        });

        return (
            <div className='ant-table ant-table-default ant-table-scroll-position-left'>
                <div className='ant-table-body'>
                    <table style={{ border: 1 }}>
                        <thead className='ant-table-thead'>
                        <tr>
                            {
                                colArray.map((col, i) => {
                                    return <td key={ `td-header-${i}` }>
                                        <input placeholder={ col.header } key={ `header-${i}` } />
                                        <Button
                                            type='danger'
                                            icon='minus'
                                            size='small'
                                            onClick={ () => this.handleRemove(col.key) }
                                        />
                                    </td>
                                })
                            }
                            <td style={{ width: 50}}>
                                <Button
                                    type='primary'
                                    icon='plus'
                                    size='small'
                                    onClick={ this.handleAdd }
                                />
                            </td>
                        </tr>
                        </thead>
                        <tbody className='ant-table-tbody'>
                        <tr>
                            {
                                colArray.map((col, i) => {
                                    return <td key={ `td-header-${i}` }>
                                        <input key={ col.key } name={ col.key } value={ col.field } onChange={ this.handleFieldChange } />
                                    </td>
                                })
                            }
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

export default withStyles(styles)(DynamicFormTable);
