import React from 'react';
import {Button} from 'antd';
import {DndProvider} from 'react-dnd-cjs';
import HTML5Backend from 'react-dnd-html5-backend-cjs';
import withStyles from 'isomorphic-style-loader/withStyles';

import DragElement from '../DragAndDrop/DragElement';

import styles from './DynamicFormElements.less';

const DynamicFormElements = () => {
    const itemTypes = [
        {type: 'TEXT', icon: 'font-size', name: 'Text'},
        {type: 'INPUT', icon: 'line', name: 'Input'},
        {type: 'SELECT', icon: 'down-circle', name: 'Select'},
        {type: 'RADIO_BUTTON', icon: 'check-circle', name: 'Radio Button'},
        {type: 'CHECKBOX', icon: 'check-circle', name: 'Checkbox'},
        {type: 'TEXTAREA', icon: 'pic-center', name: 'Text Area'},
        {type: 'FILE_UPLOAD', icon: 'file-add', name: 'File Upload'},
        {type: 'DATE_PICKER', icon: 'calendar', name: 'Date Picker'},
        {type: 'TIME_PICKER', icon: 'calendar', name: 'Time Picker'},
        {type: 'DATE_TIME_PICKER', icon: 'calendar', name: 'Date Time Picker'},
        {type: 'TABLE', icon: 'table', name: 'Table'},
    ];

    return (
        <div>
            <p>Форм элемэнтүүд</p>
            <DndProvider backend={HTML5Backend}>
                {
                    itemTypes.map((formItem, i) => {
                        return <DragElement
                            key={`drag-element-${formItem.type}`}
                            itemType={formItem.type}
                            element={
                                <Button
                                    key={`button-${formItem.type}`}
                                    type='default'
                                    icon={formItem.icon}
                                    size='large'
                                    className={styles.formElementButton}
                                >
                                    {formItem.name}
                                </Button>
                            } />
                    })
                }
            </DndProvider>
        </div>
    );
};

export default withStyles(styles)(DynamicFormElements);
