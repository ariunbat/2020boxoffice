import React, {Component} from 'react';
import { Col, Row, Button, Input, Checkbox, Select } from 'antd';
import withStyles from 'isomorphic-style-loader/withStyles';
import { inject, observer } from 'mobx-react';

import styles from './DynamicFormRadioButton.less';

const Option = Select.Option;

@inject('referenceStore', 'enumStore')
@observer
class DynamicFormRadioButton extends Component {

  componentDidMount() {
    const {referenceTypeStore} = this.props;
    referenceTypeStore.fetchList({});
  }

  handleItemChange = (e) => {
    const { formItem } = this.props;
    switch (e.target.name) {
      case 'label':
        formItem.label = e.target.value;
        break;
      case 'field':
        formItem.field = e.target.value;
        break;
      case 'required':
        formItem.required = e.target.checked;
        break;
      case 'multiple':
        formItem.multiple = e.target.checked;
        break;

    }
  };

  handleSelectChange = (select, value) => {
    const { formItem } = this.props;
    switch (select) {
      case 'selectValueType':
        formItem.selectValueType = value;
        formItem.selectValueName = null; // clear value
        break;
      case 'selectValueName':
        formItem.selectValueName = value;
        break;
    }
  };

  render() {
    //const { onDragStart } = this.props;
    const { formItem, handleRemove, referenceStore, enumStore } = this.props;

    return <div>
      <p>Radio Button</p>
      <Row
        gutter={ 16 }
        className={ styles.inputGroup }
        //draggable={ true }
        //onDragStart={ (event) => onDragStart(event, 'input-group', '') }
      >
        <Col span={ 4 }>
          <Input name='label' value={ formItem.label } placeholder='Гарчиг' title={ 'Гарчиг' } onChange={ this.handleItemChange } />
        </Col>
        <Col span={ 4 }>
          <Input
            name='field'
            value={ formItem.field }
            placeholder='Хувьсагчийн нэр'
            title={ 'Хувьсагчийн нэр' }
            onChange={ this.handleItemChange }
          />
        </Col>
        <Col span={ 4 }>
          <Select
            value={ formItem.selectValueType }
            onSelect={ (value) => this.handleSelectChange('selectValueType', value) }
            className={ styles.select }
          >
            <Option value={ 'REFERENCE' }>Лавлах сан</Option>
            <Option value={ 'ENUM' }>Системийн утга</Option>
          </Select>
        </Col>
        <Col span={ 4 }>
          <Select
            value={ formItem.selectValueName }
            onSelect={ (value) => this.handleSelectChange('selectValueName', value) }
            className={ styles.select }
          >
            {
              formItem.selectValueType === 'REFERENCE' &&
              referenceStore.referenceList.map((referenceData, i) => {
                return <Option key={ `option-${referenceData.id}` } value={ referenceData.id }>{ referenceData.name }</Option>
              })
            }
            {/*{*/}
            {/*    formItem.selectValueType === 'ENUM' &&*/}
            {/*    enumStore.enumList.map((enumData, i) => {*/}
            {/*        return <Option key={ `option-${enumData.value}` } value={ enumData.value }>{ enumData.name }</Option>*/}
            {/*    })*/}
            {/*}*/}
          </Select>
        </Col>
        <Col span={ 4 } />
        <Col span={ 3 }>
          <Checkbox name='required' checked={ formItem.required } onChange={ this.handleItemChange }>Заавал бөглөх</Checkbox>
        </Col>
        <Col span={ 1 }>
          <Button
            type='danger'
            icon='minus'
            size='small'
            onClick={ handleRemove }
          />
        </Col>
      </Row>
    </div>
  }
}

export default withStyles(styles)(DynamicFormRadioButton);
