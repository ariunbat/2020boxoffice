import React, {Component} from 'react';
import { Col, Row, Button, Input, Checkbox, Select } from 'antd';
import withStyles from 'isomorphic-style-loader/withStyles';
import { observer } from 'mobx-react';

import styles from './DynamicFormFileUpload.less';

const Option = Select.Option;

@observer
class DynamicFormFileUpload extends Component {

    handleItemChange = (e) => {
        const { formItem } = this.props;
        switch (e.target.name) {
            case 'label':
                formItem.label = e.target.value;
                break;
            case 'field':
                formItem.field = e.target.value;
                break;
            case 'required':
                formItem.required = e.target.checked;
                break;
            case 'maxSize':
                const min = parseInt(e.target.min);
                const max = parseInt(e.target.max);
                if (min <= e.target.value && e.target.value <= max) {
                    formItem.maxSize = e.target.value;
                } else if (e.target.value > max) {
                    formItem.maxSize = max;
                }
                break;
        }
    };

    handleSelectChange = (select, value) => {
        const { formItem } = this.props;
        switch (select) {
            case 'extensions':
                formItem.extensions = value;
                break;
            case '':
                break;
        }
    };

    render() {
        //const { onDragStart } = this.props;
        const { formItem, handleRemove } = this.props;

        return <div>
            <p>File upload</p>
            <Row
                gutter={ 16 }
                className={ styles.inputGroup }
                //draggable={ true }
                //onDragStart={ (event) => onDragStart(event, 'input-group', '') }
            >
                <Col span={ 4 }>
                    <Input name='label' value={ formItem.label } placeholder='Гарчиг' title={ 'Гарчиг' } onChange={ this.handleItemChange } />
                </Col>
                <Col span={ 4 }>
                    <Input
                        name='field'
                        value={ formItem.field }
                        placeholder='Талбарын нэр'
                        title={ 'Талбарын нэр' }
                        onChange={ this.handleItemChange }
                    />
                </Col>
                <Col span={ 2 }>
                    <Input
                        name='maxSize'
                        value={ formItem.maxSize }
                        placeholder='Дээд хэмжээ /MB/'
                        title={ 'Дээд хэмжээ /MB/' }
                        suffix='MB'
                        type={ 'number' }
                        min={ 1 }
                        max={ 100 }
                        onChange={ this.handleItemChange }
                    />
                </Col>
                <Col span={ 6 }>
                    <Select
                        mode='multiple'
                        value={ formItem.extensions }
                        // onSelect={ (value) => this.handleSelectChange('extensions', value) }
                        onChange={ (value) => this.handleSelectChange('extensions', value) }
                        className={ styles.select }
                    >
                        <Option value={ 'DOC' }>DOC</Option>
                        <Option value={ 'PDF' }>PDF</Option>
                        <Option value={ 'PIC' }>Зураг</Option>
                    </Select>
                </Col>
                <Col span={ 4 } />
                <Col span={ 3 }>
                    <Checkbox name='required' checked={ formItem.required } onChange={ this.handleItemChange }>Заавал бөглөх</Checkbox>
                </Col>
                <Col span={ 1 }>
                    <Button
                        type='danger'
                        icon='minus'
                        size='small'
                        onClick={ handleRemove }
                    />
                </Col>
            </Row>
        </div>
    }
}

export default withStyles(styles)(DynamicFormFileUpload);
