import React, {Component} from 'react';
import { Col, Row, Button, Input, Checkbox } from 'antd';
import withStyles from 'isomorphic-style-loader/withStyles';
import { observer } from 'mobx-react';

import styles from './DynamicFormInput.less';

@observer
class DynamicFormInput extends Component {

    handleItemChange = (e) => {
        const { formItem } = this.props;
        switch (e.target.name) {
            case 'label':
                formItem.label = e.target.value;
                break;
            case 'field':
                formItem.field = e.target.value;
                break;
            case 'required':
                formItem.required = e.target.checked;
                break;
        }
    };

    render() {
        //const { onDragStart } = this.props;
        const { formItem, handleRemove } = this.props;

        return <div>
            <p>Input</p>
            <Row
                gutter={ 16 }
                className={ styles.inputGroup }
                //draggable={ true }
                //onDragStart={ (event) => onDragStart(event, 'input-group', '') }
            >
                <Col span={ 4 }>
                    <Input name='label' value={ formItem.label } placeholder='Гарчиг' title={ 'Гарчиг' } onChange={ this.handleItemChange } />
                </Col>
                <Col span={ 4 }>
                    <Input
                        name='field'
                        value={ formItem.field }
                        placeholder='Талбарын нэр'
                        title={ 'Талбарын нэр' }
                        onChange={ this.handleItemChange }
                    />
                </Col>
                <Col span={ 12 } />
                <Col span={ 3 }>
                    <Checkbox name='required' checked={ formItem.required } onChange={ this.handleItemChange }>Заавал бөглөх</Checkbox>
                </Col>
                <Col span={ 1 }>
                    <Button
                        type='danger'
                        icon='minus'
                        size='small'
                        onClick={ handleRemove }
                    />
                </Col>
            </Row>
        </div>
    }
}

export default withStyles(styles)(DynamicFormInput);
