
import withStyles from 'isomorphic-style-loader/withStyles';
import { inject, observer } from 'mobx-react/index';
import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import { renderRoutes } from 'react-router-config';

import Logo from '../assets/logo/logo-astvision.svg';
import styles from './loginLayout.less';

@withTranslation()
@inject('langStore')
@observer
class LoginLayout extends Component {
  state = {
    collapsed: false
  };

  toggle = (collapse) => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  getContext() {
    const { location } = this.props;
    return {
      location
    };
  }

  render() {
    const { route } = this.props;

    return (
      <div className={styles.container}>
        <div className={styles.content}>
          <div className={styles.top}>
            <div className={styles.header}>
            </div>
          </div>
          {route != null ? renderRoutes(route.routes) : null}
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(LoginLayout);