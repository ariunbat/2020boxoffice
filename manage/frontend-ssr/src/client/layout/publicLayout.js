import React, { Component } from 'react';
import { renderRoutes } from 'react-router-config';
import { Layout } from 'antd';
import withStyles from 'isomorphic-style-loader/withStyles';
import { ContainerQuery } from 'react-container-query';
import classNames from 'classnames';

import { withTranslation } from 'react-i18next';
import Context from '../tiles/menuContext';
import SideMenuWrapper from '../tiles/sideMenuWrapper';
import Header from '../tiles/publicHeader';
import Footer from '../tiles/publicFooter';

import styles from './publicLayout.less';

const query = {
  'screen-xs': {
    maxWidth: 575
  },
  'screen-sm': {
    minWidth: 576,
    maxWidth: 767
  },
  'screen-md': {
    minWidth: 768,
    maxWidth: 991
  },
  'screen-lg': {
    minWidth: 992,
    maxWidth: 1199
  },
  'screen-xl': {
    minWidth: 1200,
    maxWidth: 1599
  },
  'screen-xxl': {
    minWidth: 1600
  },
  isMobile: {
    maxWidth: 599
  }
};

@withTranslation()
class PublicLayout extends Component {
  state = {
    collapsed: false
  };

  toggle = (collapse) => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  getContext() {
    const { location } = this.props;
    return {
      location
    };
  }

  render() {
    const { route, history } = this.props;
    const { collapsed } = this.state;

    return (
      <ContainerQuery query={query}>
        {params => (
          <Context.Provider value={this.getContext()}>
            <div className={classNames(params, styles.fullHeight)}>
              <Layout className={styles.fullHeight}>
                {!params.isMobile ? null : (
                  <SideMenuWrapper
                    isMobile={params.isMobile || false}
                    collapsed={collapsed}
                    toggle={this.toggle}
                  />
                )}
                <Layout className={styles.layout}>
                  <Header history={history}
                    isMobile={params.isMobile || false}
                    collapsed={collapsed}
                    toggle={this.toggle} />
                  <Layout.Content
                    style={{
                      background: '#fff',
                      minHeight: 'auto',
                    }}
                  >
                    <div className="container">
                      {route != null ? renderRoutes(route.routes) : null}
                    </div>
                  </Layout.Content>
                  <Footer />
                </Layout>
              </Layout>
            </div>
          </Context.Provider>
        )}
      </ContainerQuery>
    );
  }
}

export default withStyles(styles)(PublicLayout);
