import React, { Component } from 'react';
import { renderRoutes } from 'react-router-config';
import withStyles from 'isomorphic-style-loader/withStyles';

import styles from './baseRoute.less';

class BaseRoute extends Component {
  render() {
    const { route } = this.props;
    return (
      <div className={styles.fullHeight}>
        {route != null ? renderRoutes(route.routes) : null}
      </div>
    );
  }
}

/*const BaseRoute = ({ route }) => (
  <div>
    <h1>BaseRoute Route</h1>
    {route != null ? renderRoutes(route.routes) : null}
  </div>
);*/

export default withStyles(styles)(BaseRoute);
