import React, { Component } from 'react';
import { Modal, Form, Input, DatePicker } from 'antd';
import { inject, observer } from 'mobx-react/index';

const FormItem = Form.Item;
const { confirm } = Modal;

@Form.create()
@inject('colleagueTypeStore')
@observer
class ColleagueTypeEditModal extends Component {

  submitHandle = () => {
    const { form, handleUpdate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  backHandle = () => {
    const { handleModalVisible, form } = this.props;
    form.resetFields();
    handleModalVisible(false);
  };

  showConfirm = () => {
    const { form } = this.props;
    const parentMethods = { backHandle: this.backHandle };
    if (form.isFieldsTouched()) {
      confirm({
        title: 'Та гарахдаа итгэлтэй байна уу?',
        content: '',
        okText: 'Тийм',
        okType: 'danger',
        cancelText: 'Үгүй',
        onOk() {
          parentMethods.backHandle()
        },
        onCancel() {
        },
      });
    } else {
      parentMethods.backHandle()
    }
  };

  render() {
    const { form, modalVisible, editFormValues } = this.props;
    const { getFieldDecorator } = form;

    const formItemLayout = { labelCol: { span: 10 }, wrapperCol: { span: 14 } };

    return (
      <Modal
        title="Коллегийн төрөл засварлах"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={this.showConfirm}
        okText='Хадгалах'
        cancelText='Болих'
      >
        <Form>
          <FormItem {...formItemLayout} label="Коллегийн төрлийн нэр">
            {getFieldDecorator('name', {
              initialValue: editFormValues.name,
              rules: [{ required: true, message: 'Коллегийн төрлийн нэр бичнэ үү' }],
            })(
              <Input placeholder="Коллегийн төрлийн нэр" />)
            }
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default ColleagueTypeEditModal;
