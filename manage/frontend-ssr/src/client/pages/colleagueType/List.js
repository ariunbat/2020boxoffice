import React, { Component, Fragment } from 'react';
import {
  Card,
  Form,
  Icon,
  Button,
  Modal,
  Divider,
  message,
  Table,
} from 'antd';
import { Trans } from 'react-i18next';
import { objectToCommaSeparatedString } from '../../../common/utils/utils';
import ColleagueTypeCreateModal from './CreateModal';
import ColleagueTypeEditModal from './EditModal';
import { inject, observer } from 'mobx-react/index';
import withStyles from 'isomorphic-style-loader/withStyles';
import styles from './List.less';
import PageHeaderWrapper from '../../components/PageHeaderWrapper';

const FormItem = Form.Item;
const Confirm = Modal.confirm;

@Form.create()
@inject('colleagueTypeStore')
@observer
class ColleagueTypeList extends Component {
  state = {
    createModalVisible: false,
    editModalVisible: false,
    editFormValues: [],
    searchFormValues: {},
  };

  componentDidMount() {
    this.refreshTable();
  }

  refreshTable = () => {
    const { colleagueTypeStore } = this.props;
    colleagueTypeStore.list();
  };

  handleTableChange = () => {
    this.refreshTable();
  };

  showDeleteConfirm = clickedId => {
    const parentMethods = { handleDelete: this.handleDelete };

    Confirm({
      title: 'Анхааруулга',
      content: 'Сонгосон коллегиийн төрөл устгах уу?',
      okText: 'Тийм',
      okType: 'danger',
      cancelText: 'Үгүй',
      onOk() {
        parentMethods.handleDelete(clickedId);
      },
    });
  };

  handleDelete = clickedId => {
    const { colleagueTypeStore } = this.props;

    const payload = { id: clickedId };

    colleagueTypeStore.delete(payload).then(response => {
      if (!response.result) {
        message.error(`Коллегийн төрөл устгахад алдаа гарлаа: ${response.message}`);
      }
      this.refreshTable();
    })
      .catch(e => {
        message.error(`Коллегийн төрөл устгахад алдаа гарлаа: ${e.message}`);
      });
  };

  handleCreateModalVisible = flag => {
    this.setState({
      createModalVisible: !!flag,
    });
  };

  handleCreate = fields => {
    const { colleagueTypeStore } = this.props;

    colleagueTypeStore.create(fields).then(response => {
      if (response.result) {
        message.success('Коллегийн төрөл амжилттай бүртгэлээ');
        this.setState({
          createModalVisible: false,
        });

        this.refreshTable();
      } else {
        message.error(`Коллегийн төрөл бүртгэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Хэрэглэгч бүртгэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  showEditForm = record => {
    this.setState({
      editFormValues: record,
    });
    this.handleEditModalVisible(true);
  };

  handleEditModalVisible = flag => {
    this.setState({
      editModalVisible: !!flag,
    });
  };

  handleEdit = fields => {
    const { colleagueTypeStore } = this.props;
    const { editFormValues } = this.state;

    const payload = Object.assign(editFormValues, fields);

    colleagueTypeStore.update(payload).then(response => {
      if (response.result) {
        message.success('Коллегийн төрөл амжилттай шинэчиллээ');
        this.setState({
          editModalVisible: false,
        });

        this.refreshTable();
      } else {
        message.error(`Коллегийн төрөл шинэчлэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Коллегийн төрөл шинэчлэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  renderCreateForm() {
    const { createModalVisible } = this.state;
    const { ageCalcDate } = this.props;

    return (
      <ColleagueTypeCreateModal
        ageCalcDate={ageCalcDate}
        handleCreate={this.handleCreate}
        handleModalVisible={this.handleCreateModalVisible}
        modalVisible={createModalVisible}
      />
    );
  }

  renderEditForm() {
    const { editModalVisible, editFormValues, } = this.state;
    const { ageCalcDate } = this.props;

    return (
      <ColleagueTypeEditModal
        ageCalcDate={ageCalcDate}
        handleUpdate={this.handleEdit}
        handleModalVisible={this.handleEditModalVisible}
        modalVisible={editModalVisible}
        editFormValues={editFormValues}
      />
    );
  }

  render() {
    const { colleagueTypeStore, colleagueTypeStore: { loading } } = this.props;

    const columns = [
      {
        title: 'Коллегийн төрлийн нэр',
        dataIndex: 'name',
      },
      {
        title: 'Үйлдэл',
        render: (text, record) => (
          <Fragment>
            <a key="edit" onClick={() => this.showEditForm(record)}>
              <Icon type="edit" />
            </a>
            <Divider type="vertical" />
            <a key="delete" onClick={() => this.showDeleteConfirm(record.key)}>
              <Icon type="delete" />
            </a>
          </Fragment>
        ),
      },
    ];

    const action = (
      <Button icon='plus' type='primary' onClick={() => this.handleCreateModalVisible(true)} style={{ float: 'right' }}>
        Нэмэх
      </Button>
    );

    return (
      <div>
        <PageHeaderWrapper title="Коллегийн төрөл" loading={loading} action={action}>
          <Card bordered={false}>
            <div className={styles.tableList}>
              <Table
                rowKey="key"
                loading={loading}
                columns={columns}
                dataSource={colleagueTypeStore && colleagueTypeStore.data != null ? colleagueTypeStore.data : []}
                onChange={this.handleTableChange}
              />
            </div>
          </Card>
        </PageHeaderWrapper>
        {this.renderCreateForm()}
        {this.renderEditForm()}
      </div>
    );
  }
}

export default withStyles(styles)(ColleagueTypeList);
