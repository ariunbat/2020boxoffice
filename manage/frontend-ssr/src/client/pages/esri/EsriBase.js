import { Component } from 'react';
import { loadModules, loadCss } from 'esri-loader';
import { Map } from '@esri/react-arcgis';
import { message } from 'antd/lib/index';

export default class EsriBase extends Component {

  static esri = {
    Graphic: null,
    Viewpoint: null,

    Extent: null,
    Polygon: null,
    Point: null,
    projection: null,
    SpatialReference: null,

    GraphicsLayer: null,
    FeatureLayer: null,

    SimpleFillSymbol: null,
    SimpleLineSymbol: null,

    ScaleBar: null,
    BasemapToggle: null,
    Home: null,

    QueryTask: null,
    Query: null,
    ProjectParameters: null,
    IdentityManager: null,
    geometryService: null,
  };

  static esriInstances = {
    geometryServiceInstance: null,
    projectionPromise: null,
  };

  static defaultProps = {
    css: true,
    SR: 102100,
    SR_WGS_84: 4326,
    defaultExtent: {
      xmin: 11900907.813675856,
      ymin: 6092785.321210225,
      xmax: 11903180.620938582,
      ymax: 6093829.164377657,
      spatialReference: {
        wkid: 102100,
      },
    },
    // geometryServiceUrl: 'https://sampleserver6.arcgisonline.com/arcgis/rest/services/Utilities/Geometry/GeometryServer',
  };

  constructor(props) {
    super(props);

    this.handleLoad = this.handleLoad.bind(this);
  }

  componentDidMount() {
    this.handleLoad();
  }

  project = (geometryList, wkId) => {
    const projectParams = new this.esri.ProjectParameters({
      geometries: geometryList,
      outSpatialReference: new this.esri.SpatialReference({ wkid: wkId })
    });
    // return this.esri.projection.project(projectParams);
    return this.geometryService.project(projectParams);
  };

  handleLoad(moduleLoadedCallback) {
    // const { geometryServiceUrl } = this.props;

    loadCss();

    loadModules([
      'esri/Graphic',
      'esri/Viewpoint',

      'esri/geometry/Extent',
      'esri/geometry/Polygon',
      'esri/geometry/Point',
      'esri/geometry/projection',
      'esri/geometry/SpatialReference',

      'esri/layers/GraphicsLayer',
      'esri/layers/FeatureLayer',
      'esri/layers/MapImageLayer',

      'esri/symbols/SimpleLineSymbol',
      'esri/symbols/SimpleFillSymbol',

      'esri/widgets/ScaleBar',
      'esri/widgets/BasemapToggle',
      'esri/widgets/Home',

      'esri/tasks/QueryTask',
      'esri/tasks/support/Query',
      'esri/tasks/support/ProjectParameters',
      'esri/identity/IdentityManager',
      'esri/tasks/GeometryService',
    ]).then((
      [
        Graphic,
        Viewpoint,

        Extent,
        Polygon,
        Point,
        projection,
        SpatialReference,

        GraphicsLayer,
        FeatureLayer,
        MapImageLayer,

        SimpleFillSymbol,
        SimpleLineSymbol,

        ScaleBar,
        BasemapToggle,
        Home,

        QueryTask,
        Query,
        ProjectParameters,
        IdentityManager,
        GeometryService,
      ]) => {

      this.esri = {
        Graphic,
        Viewpoint,

        Extent,
        Polygon,
        Point,
        projection,
        SpatialReference,

        GraphicsLayer,
        FeatureLayer,
        MapImageLayer,

        SimpleFillSymbol,
        SimpleLineSymbol,

        ScaleBar,
        BasemapToggle,
        Home,

        QueryTask,
        Query,
        ProjectParameters,
        IdentityManager,
        GeometryService,
      };

      this.esriInstances = {
        projectionPromise: projection.load(), // load projection module
      };

      this.geometryService = new GeometryService({
        url: 'https://geoportal.nsdi.gov.mn/alagac/rest/services/Utilities/Geometry/GeometryServer',
      });

      if(moduleLoadedCallback) {
        moduleLoadedCallback();
      }
    }).catch((err) => message.error(`Газрын зургын алдаа: ${err.message}`));
  }

  registerToken(s, t, expires) {
    this.esri.IdentityManager.registerToken({
      server: s,
      token: t,
      expires,
      ssl: true,
      userId: 'hulemj',
      scope: "server"
    });
  }

  render() {
    return null;
  }
}
