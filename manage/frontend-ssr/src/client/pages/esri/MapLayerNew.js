import React from 'react';
import { Map } from '@esri/react-arcgis';
import { Form, message } from 'antd'
import EsriBase from './EsriBase';

import Pin from '../../assets/pin.png';
import { inject, observer } from 'mobx-react';

const fillColor = [22, 22, 22, 0.2];
const fillSelectColor = [255, 182, 0, 0.7];
const outlineColor = [255, 182, 0, 1];
const symbolType = 'solid';
const symbolStyleDash = 'dash';
const symbolStyleLine = 'line';
const outlineWidth = 1;
const outSR = 32648;

class EsriMap extends EsriBase {
  constructor(props) {
    super(props);

    this.handleMapLoad = this.handleMapLoad.bind(this);
  }

  state = {
    map: null,
    mapView: null,
    currentPolygon: null,
    layer: null,
    currentType: null,
    currentId: -1,
    currentLocationType: null,
    polygons: [],
  };

  clearPoint = (force) => {
    const { mapView } = this.state;

    mapView.graphics.removeAll();
    if (force) {
      this.setState({ currentPolygon: null });
    }
  };

  onClickEvent = (evt) => {
    const { disableClick } = this.props;

    if (disableClick && disableClick === true)
      return;

    this.clearPoint(true);
    const { mapView } = this.state;

    const point = mapView.toMap(evt);
    this.addPoint(point, true);
  };

  addPoint = (point, isClicked) => {
    const { SR, onChangeCoordinate } = this.props;
    const { mapView } = this.state;

    const newPolygon = new this.esri.Polygon({
      spatialReference: { wkid: SR },
    });

    this.setState({
      currentPolygon: newPolygon,
    });

    newPolygon.addRing([point, point]);

    const polygonGraphic = new this.esri.Graphic({
      geometry: newPolygon,
      symbol: {
        type: 'picture-marker',
        url: Pin,
        width: '40px',
        height: '40px'
      }
    });
    mapView.graphics.add(polygonGraphic);

    if (onChangeCoordinate && isClicked === true) {
      // const data = Object.assign({
      //   inSR: SR,
      //   outSR,
      //   geometries: JSON.stringify(
      //     {
      //       "geometryType": "esriGeometryPoint",
      //       "geometries": [{
      //         "x": point.x,
      //         "y": point.y,
      //       }],
      //     }
      //   ),
      //   f: "pjson",
      //   transformForward: true,
      //   vertical: false,
      // });
      // const promise = project(data);
      // promise.then(responseArcgis => {
      //   if (responseArcgis.error) {
      //     message.warning("Байршлыг дахин тодорхойлно уу!");
      //   } else if (responseArcgis.geometries && responseArcgis.geometries.length > 0) {
      onChangeCoordinate(point.x, point.y, point.longitude, point.latitude);
      //   }
      // });
    }
  };

  handleMapLoad(map, mapView) {
    this.setState({ map, mapView });
    this.initMap();
  }

  initMap() {
    const { mapView, map } = this.state;
    const { defaultExtent, initialCoordinate, SR } = this.props;

    const scaleBar = new this.esri.ScaleBar({
      view: mapView,
      style: 'ruler',
      unit: 'metric',
    });
    mapView.ui.add(scaleBar, {
      position: 'bottom-left',
    });

    const basemapToggle = new this.esri.BasemapToggle({
      view: mapView,
      nextBasemap: 'streets',
    });
    mapView.ui.add(basemapToggle, {
      position: 'top-right',
    });

    const extent = new this.esri.Extent(defaultExtent);
    mapView.extent = extent;

    const homeWidget = new this.esri.Home({
      view: mapView,
      viewpoint: new this.esri.Viewpoint({
        targetGeometry: extent,
      }),
    });

    mapView.ui.add(homeWidget, {
      position: 'top-left',
    });

    if (initialCoordinate && initialCoordinate.x && initialCoordinate.y) {
      const point = new this.esri.Point({
        x: initialCoordinate.x,
        y: initialCoordinate.y,
        spatialReference: SR,
      });
      this.addPoint(point);
      mapView.center = point;
    }

    mapView.on('pointer-down', (evt) => {
      evt.stopPropagation();
      this.onClickEvent(evt);
    });
    const layer = new this.esri.GraphicsLayer({});
    map.add(layer);
    this.setState({ layer });
  }

  stopLoading = () => {
    const { setLoading } = this.props;
    setLoading('initial', false);
    setLoading('spin', false);
  };

  setResultQuery = (type1, result) => {
    const { setResultQuery, id, locationType } = this.props;
    this.setState({ currentType: type1, currentId: id, currentLocationType: locationType });
    setResultQuery(type1, result);
  };

  render() {
    const { initialCoordinate, disableMap } = this.props;
    return (
      <div>
        <div style={{ width: '100%', height: '500px' }}>
          <Map onLoad={this.handleMapLoad} mapProperties={{ basemap: 'satellite' }} />
        </div>
        {initialCoordinate && initialCoordinate.longitude && initialCoordinate.latitude &&
        <span
          style={{
          padding: '10px',
          border: '1px solid rgb(79, 95, 108)',
          borderRadius: '9px',
          position: 'absolute',
          right: '40px',
          bottom: '100px',
          background: 'rgb(255, 255, 255, 0.5)'
        }}
        >
          {/* eslint-disable-next-line max-len */}
          <span style={{
            color: 'black',
            fontWeight: '700'
          }}>{`Уртраг : ${initialCoordinate.longitude.toFixed(4)}`}, {`Өргөрөг : ${initialCoordinate.latitude.toFixed(4)}`}</span>
        </span>
        }
      </div>
    );
  }
}

export default EsriMap;
