import React, { Component } from 'react';
import { observe, observable } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';
import { Card, Form, Input, Checkbox, Alert, Button, Divider } from 'antd';

import styles from './index.less';
import withStyles from 'isomorphic-style-loader/withStyles';

@Form.create()
@inject('authStore')
@observer
class Forgot extends Component {
  // @observable value = '';

  state = {
    submitting: false
  };

  componentDidMount() {
    const { authStore, history } = this.props;

    const disposer = observe(authStore.values, change => {
      /*
      console.log(
        change.type,
        change.name,
        'from',
        change.oldValue,
        'to',
        change.object[change.name]
      );
      */
      if (change.name === 'status') {
        const loginResult = change.object[change.name];
        if (loginResult === true) {
          // redirect
          history.push('/');
        }
      }
    });

    /*
    if (authStore.values.status === true) {
      // logged in
      history.push('/');
    }
    */
  }

  handleSubmit = e => {
    e.preventDefault();
    const { authStore, form } = this.props;

    this.setState({ submitting: true });
    form.validateFields({ force: true }, (err, values) => {
      if (!err) {
        // console.log('yey');
        this.setState({ submitting: false });
        console.log("submitting...");
      } else {
        // console.log('oh noo');
      }
    });
  };

  render() {
    const { form, history } = this.props;
    const { submitting } = this.state;
    const { getFieldDecorator } = form;

    return (
      <div className={styles.main}>
        <Card>
          <h2>Нууц үг сэргээх</h2>
          <p>Хэрэглэгч та бүртгэлтэй имэйл хаягаа оруулан нууц үгээ сэргээнэ үү.</p>
          <Divider />
          <Form onSubmit={this.handleSubmit}>
            <Form.Item>
              {getFieldDecorator('email', {
                rules: [
                  {
                    required: true,
                    message: 'Имэйл хаягаа оруулна уу'
                  }
                ]
              })(<Input size="large" placeholder="Имэйл хаяг" />)}
            </Form.Item>
            <Form.Item>
              <Button size="large" loading={submitting} type="primary" htmlType="submit" block>
                Үргэлжлүүлэх
              </Button>
            </Form.Item>
            <Button size="large" block onClick={() => history.push('/auth/login')}>
              Буцах
            </Button>
          </Form>
        </Card>
      </div>
    );
  }
}

export default withStyles(styles)(Forgot);
