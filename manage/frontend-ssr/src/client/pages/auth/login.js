import React, { Component } from 'react';
import { observe, observable } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';
import { Card, Form, Input, Checkbox, Alert, Button, Divider, message } from 'antd';

import styles from './index.less';
import withStyles from 'isomorphic-style-loader/withStyles';

let firstmount = true;

@Form.create()
@inject('authStore')
@observer
class Login extends Component {

  state = {
    submitting: false
  };

  componentDidMount() {
    const { authStore, history } = this.props;

    const disposer = observe(authStore.values, change => {
      if (change.name === 'status') {
        const loginResult = change.object[change.name];
        if (loginResult === true && firstmount) {
          firstmount = false;
          // redirect
          history.push('/');
        }
      }
    });
  }

  componentWillMount() {
    firstmount = true;
  }

  handleSubmit = e => {
    e.preventDefault();
    const { authStore, form, history } = this.props;

    form.validateFields({ force: true }, (err, values) => {
      if (!err) {
        this.setState({ submitting: true });
        authStore.login(values.username, values.password)
          .then(response => {
            if (response && !response.result) {
              this.setState({ submitting: false });
              message.error(response.message);
            }
            if (response && response.result) {
              history.push('/dashboard');
            }
          })
      }
    });
  };

  render() {
    const { form } = this.props;
    const { submitting } = this.state;
    const { getFieldDecorator } = form;

    return (
      <div className={styles.main}>
        <Card>
          <h2>Системд нэврэх</h2>
          <p>Хэрэглэгч та нэвтрэх нэр болон нууц үгээ ашиглан системд нэвтэрнэ үү.</p>
          <Divider />
          <Form onSubmit={this.handleSubmit}>
            <Form.Item>
              {getFieldDecorator('username', {
                rules: [
                  {
                    required: true,
                    message: 'Нэвтрэх нэрээ оруулна уу'
                  }
                ]
              })(<Input size="large" placeholder="Нэвтрэх нэр" />)}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('password', {
                rules: [
                  {
                    required: true,
                    message: 'Нууц үгээ оруулна уу'
                  }
                ]
              })(<Input size="large" type="password" placeholder="Нууц үг" />)}
            </Form.Item>
            <Form.Item>
              {getFieldDecorator('rememberMe', {
                valuePropName: 'checked',
                initialValue: false
              })(<Checkbox>Намайг сана</Checkbox>)}
              <Link to="/auth/forgot">Нууц үгээ мартсан?</Link>
            </Form.Item>
            <Button size="large" loading={submitting} type="primary" htmlType="submit" block>
              Нэвтрэх
            </Button>
          </Form>
        </Card>
      </div>
    );
  }
}

export default withStyles(styles)(Login);
