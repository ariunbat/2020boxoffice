import React, { Component } from 'react';
import { observe, observable } from 'mobx';
import { observer, inject } from 'mobx-react';
import { Link } from 'react-router-dom';
import { Form, Input, Checkbox, Alert, Button } from 'antd';

@inject('authStore')
@observer
class Logout extends Component {
  // @observable value = '';

  componentDidMount() {
    const { authStore, history } = this.props;

    const disposer = observe(authStore.values, change => {
      /*
      console.log(
        change.type,
        change.name,
        'from',
        change.oldValue,
        'to',
        change.object[change.name]
      );*/
      if (change.name === 'status') {
        const authStatus = change.object[change.name];
        if (authStatus === false) {
          // redirect
          history.push('/');
        }
      }
    });
    authStore.reset();

    /*
    if (authStore.values.status === true) {
      // logged in
      history.push('/');
    }
    */
  }

  render() {
    return (
      <div>
        <h2>Logging out ...</h2>
      </div>
    );
  }
}

export default Logout;
