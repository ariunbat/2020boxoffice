import React, { Component } from 'react';
import { inject, observer } from 'mobx-react/index';
import withStyles from 'isomorphic-style-loader/withStyles';
import { Row, Col, Button, Icon, Upload, message } from 'antd';
import { Stage, Layer, Image, Rect, Transformer } from 'react-konva';
import { stringify } from 'qs';

import { apiRequestWithToken } from '../../../common/services/util';
import { getImageServerUrl } from '../../../common/services/cdn';
import { applyRatio, calculateSize } from '../../../common/utils/image';
import KonvaTransformer from '../../components/konva/KonvaTransformer';

import styles from './index.less';

const canvasSize = {
  width: 160, height: 205.714, paddingPercent: 0.7
};

@inject('authStore')
@observer
class AwsRekognitionTest extends Component {
  state = {
    image: null, // uploaded image file
    imageDetail: null, // image size and info
    faceAttributes: {
      eyeglasses: { value: true, confidence: 80 },
      sunglasses: { value: true, confidence: 81 }
    }, // has glasses etc
    faceDetailOriginal: null, // original face bounding box
    faceDetail: null, // calculated face bounding box

    // for canvas
    selectedShapeName: null, // used for canvas shape selection
    faceDetailNode: null
  };

  componentDidMount() {
    //
  }

  onFileUploadChange = resultData => {
    if (resultData.fileList && resultData.file.status === 'uploading') {
      const appThis = this;
      if (resultData.file.originFileObj) {
        const reader = new FileReader();
        reader.onload = (e) => {
          const image = new window.Image();
          image.onload = () => {
            appThis.setState({
              //imageDetail: {width: image.width, height: image.height}
              imageDetail:
                calculateSize(image.width, image.height, canvasSize.width, canvasSize.height)
            });
          };

          image.src = e.target.result;
          appThis.setState({
            image: image
          });
        };
        reader.readAsDataURL(resultData.file.originFileObj);
      }
    }

    if (resultData.fileList
      && resultData.file.status === 'done'
      && resultData.file.response.result) {

      const { imageDetail } = this.state;
      const faceResult = resultData.file.response.data;
      this.setState({ faceAttributes: faceResult });

      if (faceResult.boundingBox) {
        this.setState({
          faceDetail: {
            x: Math.max(0,
              imageDetail.width * faceResult.boundingBox.left
              - imageDetail.width * faceResult.boundingBox.width * canvasSize.paddingPercent / 2
            ),
            y: Math.max(0,
              imageDetail.height * faceResult.boundingBox.top
              - imageDetail.height * faceResult.boundingBox.height * canvasSize.paddingPercent / 2
            ),
            width: Math.min(imageDetail.width,
              imageDetail.width * faceResult.boundingBox.width * (1 + canvasSize.paddingPercent)
            ),
            height: Math.min(imageDetail.height,
              imageDetail.height * faceResult.boundingBox.height * (1 + canvasSize.paddingPercent)
            )
          }
        });
      } else {
        message.error('Царай олдсонгүй');
      }
    }

    if (resultData.file.status === 'error') {
      message.error('Зураг шалгах сервис дээр алдаа гарлаа');
    }

    // TODO remove later
    if (resultData.fileList && resultData.file.status === 'error') {
      const { imageDetail } = this.state;
      const faceResult = {
        //boundingBox: {width: 0.5915949, height:0.576682, left:0.19300775, top:0.17259426},
        //boundingBox: {width: 0.592247, height: 0.38691252, left: 0.21227604, top: 0.29050332},
        //boundingBox: {width: 0.26932615, height: 0.5538911, left: 0.36608586, top: 0.22128169},
        //boundingBox: {width: 0.158009, height: 0.33085617, left: 0.4214094, top: 0.29695633},
        boundingBox: {width: 0.21397425, height: 0.514388, left: 0.36981973, top: 0.21351711},
        eyeglasses: {value: true, confidence: 99.91707},
        sunglasses: {value: false, confidence: 99.99626}
      };
      this.setState({ faceAttributes: faceResult });

      if (faceResult.boundingBox) {
        this.setState({
          faceDetailOriginal: {
            x: Math.max(0, imageDetail.width * faceResult.boundingBox.left),
            y: Math.max(0, imageDetail.height * faceResult.boundingBox.top),
            width: Math.min(imageDetail.width, imageDetail.width * faceResult.boundingBox.width),
            height: Math.min(imageDetail.height, imageDetail.height * faceResult.boundingBox.height)
          }
        });

        let faceWidth = imageDetail.width * faceResult.boundingBox.width
          * (1 + canvasSize.paddingPercent);
        let faceHeight = imageDetail.height * faceResult.boundingBox.height
          * (1 + canvasSize.paddingPercent);
        let hPadding = imageDetail.width * faceResult.boundingBox.width
          * canvasSize.paddingPercent / 2;
        let vPadding = imageDetail.height * faceResult.boundingBox.height
          * canvasSize.paddingPercent / 2;

        // apply ratio to face crop
        const ratio = canvasSize.width / canvasSize.height;
        if (ratio < 1) { // width < height
          hPadding = hPadding + (faceWidth / ratio - faceWidth) / 2;
          faceWidth = faceWidth / ratio; // increase width
        } else { // width > height
          vPadding = vPadding + (faceHeight * ratio - faceHeight) / 2;
          faceHeight = faceHeight * ratio; // increase height
        }

        this.setState({
          faceDetail: {
            x: Math.max(0,
              imageDetail.width * faceResult.boundingBox.left - hPadding
            ),
            y: Math.max(0,
              imageDetail.height * faceResult.boundingBox.top - vPadding
            ),
            width: Math.min(imageDetail.width, faceWidth),
            height: Math.min(imageDetail.height, faceHeight)
          }
        });
      } else {
        message.error('Царай олдсонгүй');
      }
    }
  };

  handleStageMouseDown = e => {
    // clicked on stage - clear selection
    if (e.target === e.target.getStage()) {
      this.setState({
        selectedShapeName: ''
      });
      return;
    }

    // clicked on transformer - do nothing
    if (e.target.getParent().className === 'Transformer') {
      return;
    }

    if (e.target.name() === 'faceDetail') {
      this.setState({
        faceDetailNode: e.target,
        selectedShapeName: e.target.name()
      });
    }
  };

  dragBoundFunc = (pos) => {
    let x = pos.x;
    let y = pos.y;

    if (pos.x < 0) {
      x = 0;
    }
    if (pos.y < 0) {
      y = 0;
    }

    const { imageDetail, faceDetailNode, faceDetail } = this.state;
    if (faceDetailNode) {
      if (pos.x + faceDetailNode.width() > imageDetail.width) {
        x = imageDetail.width - faceDetailNode.width();
      }
      if (pos.y + faceDetailNode.height() > imageDetail.height) {
        y = imageDetail.height - faceDetailNode.height();
      }
    }

    faceDetail.x = x;
    faceDetail.y = y;
    this.setState({
      faceDetail: faceDetail
    });

    return { x: x, y: y };
  };

  onResize = (boundingBox) => {
    const { faceDetail } = this.state;

    faceDetail.width = boundingBox.width;
    faceDetail.height = boundingBox.height;
    this.setState({
      faceDetail: faceDetail
    });
  };

  onCrop = () => {
    const { authStore } = this.props;
    const { image, faceDetail, faceAttributes } = this.state;

    const promise = apiRequestWithToken(getImageServerUrl() + '/crop', 'POST', authStore.values.token, stringify(
      {
        imageId: faceAttributes.imageId,
        img_w: image.width,
        img_h: image.height,
        can_w: canvasSize.width,
        can_h: canvasSize.height,
        x: faceDetail.x,
        y: faceDetail.y,
        w: faceDetail.width,
        h: faceDetail.height
      }));

    promise
      .then((result) => {
        console.log(result);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  render() {
    const { authStore } = this.props;
    const { image, imageDetail, faceAttributes,
      faceDetail, faceDetailOriginal, selectedShapeName } = this.state;

    const beforeUpload = function (file) {
      const isLt1M = file.size / 1024 / 1024 < 3;
      if (!isLt1M) {
        message.error('3MB аас бага хэмжээтэй зураг оруулна уу');
      }
      return isLt1M;
    };

    return (
      <div>
        <h2>AWS Rekognition Test</h2>
        <Row gutter={16}>
          <Col span={12}>
            <Stage width={canvasSize.width}
                   height={canvasSize.height}
                   onMouseDown={this.handleStageMouseDown}>
              <Layer>
                {imageDetail ?
                  <Image x={0}
                         y={0}
                         image={image}
                         width={imageDetail.width}
                         height={imageDetail.height}
                         // ref={node => {
                         //   this.imageNode = node;
                         //   }}
                  />
                  : null}
                {faceDetail ?
                  <Rect name='faceDetail'
                        x={faceDetail.x}
                        y={faceDetail.y}
                        width={faceDetail.width}
                        height={faceDetail.height}
                        stroke={'red'}
                        draggable
                        dragBoundFunc={this.dragBoundFunc}
                        // ref={node => {
                        //   this.setState({ faceDetailNode: node });
                        //   // this.faceDetailOriginal = node;
                        // }}
                  />
                  : null}
                {faceDetailOriginal ?
                  <Rect name='faceDetailOriginal'
                        x={faceDetailOriginal.x}
                        y={faceDetailOriginal.y}
                        width={faceDetailOriginal.width}
                        height={faceDetailOriginal.height}
                        stroke={'yellow'}
                  />
                  : null}
                {faceDetail ?
                  <KonvaTransformer selectedShapeName={selectedShapeName}
                                    minWidth={faceDetail.width}
                                    minHeight={faceDetail.height}
                                    maxWidth={canvasSize.width}
                                    maxHeight={canvasSize.height}
                                    handleResize={this.onResize}
                  />
                  : null}
              </Layer>
            </Stage>
          </Col>
          <Col span={12}>
            <div style={{marginBottom: 16}}>
              {faceAttributes.sunglasses.confidence >= 80
              && faceAttributes.sunglasses.value === true ?
                <Icon type="close-circle" className={styles.failIcon} />
                :
                <Icon type="check-circle" className={styles.successIcon} />
              }
              <span>Нарны шил</span>
            </div>
            <div>
              {faceAttributes.eyeglasses.confidence >= 80
              && faceAttributes.eyeglasses.value === true ?
                <Icon type="close-circle" className={styles.failIcon} />
                :
                <Icon type="check-circle" className={styles.successIcon} />
              }
              <span>Харааны шил</span>
            </div>
          </Col>
        </Row>
        <Row gutter={16}>
          <Col span={24}>
            <Upload
              name="file"
              accept="image/*"
              beforeUpload={beforeUpload}
              onChange={this.onFileUploadChange}
              headers={{'X-Auth-Token': authStore.values.token}}
              data={{'entity': 'rekognition', 'entityId': Math.random().toString(36).substring(2)}}
              action={getImageServerUrl() + '/upload'}
              showUploadList={false}
            >
              <Button block>
                <Icon type="upload" /> Энд дарж зургаа оруулна уу
              </Button>
            </Upload>
            <Button block onClick={this.onCrop}>
              <Icon type="upload" /> Crop
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}

export default withStyles(styles)(AwsRekognitionTest);
