import React, { Component } from 'react';
import { withTranslation, Trans } from 'react-i18next';

@withTranslation()
class Home extends Component {

  render() {
    const { t } = this.props;
    return (
      <div style={{ border: 'solid 1px', height: '900px' }}>
        <h2>Home</h2>
        <h1>
          {t('common.test')} <br />
          <Trans>common.test</Trans>
        </h1>
      </div>
    )
  }
}

export default (Home);
