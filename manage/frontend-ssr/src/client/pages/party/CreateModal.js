import React, { Component } from 'react';
import { Modal, Form, Input, InputNumber, Upload, Icon, Radio } from 'antd';
import { inject, observer } from 'mobx-react/index';
import { CirclePicker } from 'react-color';
import { getImageServerUrl } from '../../../common/services/cdn';

const FormItem = Form.Item;
const { confirm } = Modal;

@Form.create()
@inject('authStore')
@observer
class ColleagueTypeCreateModal extends Component {

  state = {
    previewVisible: false,
    preview: '',
    color: '',
  };

  submitHandle = () => {
    const { form, handleCreate } = this.props;

    form.validateFields((err, fieldsValue) => {
      console.log(fieldsValue);
      if (err) return;

      form.resetFields();
      handleCreate(fieldsValue);
    });
  };

  backHandle = () => {
    const { handleModalVisible, form } = this.props;
    form.resetFields();
    handleModalVisible(false);
  };

  showConfirm = () => {
    const { form } = this.props;
    const parentMethods = { backHandle: this.backHandle };
    if (form.isFieldsTouched()) {
      confirm({
        title: 'Та гарахдаа итгэлтэй байна уу?',
        content: '',
        okText: 'Тийм',
        okType: 'danger',
        cancelText: 'Үгүй',
        onOk() {
          parentMethods.backHandle();
        },
        onCancel() {
        },
      });
    } else {
      parentMethods.backHandle()
    }
  };

  normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  handlePreview = (file) => {
    console.log(file);
    this.setState({
      preview: file.response && file.response.data && file.response.data.url,
      previewVisible: true,
    });
  };

  handleCancelPreview = () => this.setState({ previewVisible: false });

  handleColorChange = (e) => {
    if (e && e.hex) {
      this.setState({
        color: e.hex,
      });
      return e.hex;
    }
    return e;
  };

  render() {
    const { form, colorStore, modalVisible, authStore } = this.props;
    console.log('create', colorStore);
    const { getFieldDecorator } = form;
    const { previewVisible, preview, color } = this.state;
    const formItemLayout = { labelCol: { span: 8 }, wrapperCol: { span: 14 } };
    const colors = [];

    if (colorStore && colorStore.data) {
      colorStore.data.forEach(color => {
        colors.push(color.code);
      })
    }

    const uploadButton = (
      <Icon type="upload" />
    );

    return (
      <Modal
        title="Коллегийн төрөл нэмэх"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={this.showConfirm}
      >
        <Form>
          <FormItem {...formItemLayout} label="Нэр">
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Нэр бичнэ үү' }],
            })(<Input placeholder='Нэр' />
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Тайлбар">
            {getFieldDecorator('description')(<Input.TextArea placeholder='Тайлбар' rows={4} />
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Эрэмбэ">
            {getFieldDecorator('order', {
              rules: [{ required: true, message: 'Эрэмбэ оруулна уу' }],
            })(<InputNumber placeholder='Эрэмбэ' min={1} />
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Лого">
            {getFieldDecorator('file', {
              valuePropName: 'fileList',
              getValueFromEvent: this.normFile,
            })(
              <Upload
                name="file"
                accept="image/*"
                listType="picture-card"
                headers={{ 'X-Auth-Token': authStore.values.token }}
                data={{ 'entity': 'partyLogo', 'entityId': Math.random().toString(36).substring(2) }}
                action={getImageServerUrl() + '/upload'}
                onPreview={this.handlePreview}
              >
                {form.getFieldValue('file') && form.getFieldValue('file').length !== 0 ? null : uploadButton}
              </Upload>)}
          </FormItem>
          <FormItem {...formItemLayout} label="Өнгө">
            {getFieldDecorator('color', {
              valuePropName: 'hex',
              getValueFromEvent: this.handleColorChange,
              rules: [{ required: true, message: 'Өнгө сонгоно уу' }],
            })(
              <CirclePicker
                colors={colors}
                color={color}
                circleSize={18}
                circleSpacing={8}
              />
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Ашиглах эсэх:">
            {getFieldDecorator('visible', {
              initialValue: true,
            })(
              <Radio.Group>
                <Radio value={true}>Тийм</Radio>
                <Radio value={false}>Үгүй</Radio>
              </Radio.Group>
            )}
          </FormItem>
        </Form>
        <Modal visible={previewVisible} footer={null} onCancel={this.handleCancelPreview}>
          <img src={preview} width='100%' />
        </Modal>
      </Modal>
    );
  }
}

export default ColleagueTypeCreateModal;