import React, { Component, Fragment } from 'react';
import {
  Card,
  Form,
  Input,
  Icon,
  Button,
  Modal,
  Divider,
  message,
  Table,
} from 'antd';
import { Trans } from 'react-i18next';
import { objectToCommaSeparatedString } from '../../../common/utils/utils';
import PartyCreateModal from './CreateModal';
import PartyEditModal from './EditModal';
import { inject, observer } from 'mobx-react';
import withStyles from 'isomorphic-style-loader/withStyles';
import styles from './List.less';
import PageHeaderWrapper from '../../components/PageHeaderWrapper';
import LocationField from '../Location';
import { displayFormat } from '../../../common/utils/dateFormat';
import PartyDefaultLogo from '../../assets/party.png';

const FormItem = Form.Item;
const Confirm = Modal.confirm;

@Form.create()
@inject('partyStore', 'colorStore')
@observer
class PartyList extends Component {
  state = {
    createModalVisible: false,
    editModalVisible: false,
    editFormValues: [],
    searchFormValues: {},
  };

  componentDidMount() {
    this.refreshTable();
  }


  refreshTable = () => {
    const { partyStore, colorStore } = this.props;
    partyStore.list();
    colorStore.list();
  };

  handleTableChange = () => {
    this.refreshTable();
  };

  handleSearch = e => {
    e.preventDefault();
    const { form } = this.props;

    this.setState({
      searchFormValues: form.getFieldsValue(),
    });

    this.refreshTable(form.getFieldsValue());
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      searchFormValues: {},
    });

    this.refreshList();
  };

  showDeleteConfirm = clickedId => {
    const parentMethods = { handleDelete: this.handleDelete };

    Confirm({
      title: 'Анхааруулга',
      content: 'Сонгосон нам устгах уу?',
      okText: 'Тийм',
      okType: 'danger',
      cancelText: 'Үгүй',
      onOk() {
        parentMethods.handleDelete(clickedId);
      },
    });
  };

  handleDelete = clickedId => {
    const { partyStore } = this.props;

    const payload = { id: clickedId };

    partyStore.delete(payload).then(response => {
      if (!response.result) {
        message.error(`Нам устгахад алдаа гарлаа: ${response.message}`);
      }
      this.refreshTable();
    })
      .catch(e => {
        message.error(`Нам устгахад алдаа гарлаа: ${e.message}`);
      });
  };

  handleCreateModalVisible = flag => {
    this.setState({
      createModalVisible: !!flag,
    });
  };

  handleCreate = fields => {
    const { partyStore } = this.props;

    const { file, ...rest } = fields;
    let fileData;

    if (file && file.length !== 0) {
      file.map(item => {
        if (item.response && item.response.result) {
          fileData = {
            uid: item.uid,
            name: item.name,
            url: item.response.data.url
          }
        }
      })
    }

    const payload = Object.assign(rest, { icon: fileData });

    partyStore.create(payload).then(response => {
      if (response.result) {
        message.success('Нам амжилттай бүртгэлээ');
        this.setState({
          createModalVisible: false,
        });
        window.location.reload();
        // this.refreshTable();
      } else {
        message.error(`Нам бүртгэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Хэрэглэгч бүртгэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  showEditForm = record => {
    this.setState({
      editFormValues: record,
    });
    this.handleEditModalVisible(true);
  };

  handleEditModalVisible = flag => {
    this.setState({
      editModalVisible: !!flag,
    });
  };

  handleEdit = fields => {
    const { partyStore } = this.props;
    const { editFormValues } = this.state;

    const { file, ...rest } = fields;
    let fileData;

    if (file && file.length !== 0) {
      file.map(item => {
        if (item.response && item.response.result) {
          fileData = {
            uid: item.uid,
            name: item.name,
            url: item.response.data.url
          }
        } else {
          fileData = item;
        }
      })
    }

    const editedValues = Object.assign(fields, { icon: fileData });
    const payload = Object.assign(editFormValues, editedValues);

    partyStore.update(payload).then(response => {
      if (response.result) {
        message.success('Нам амжилттай шинэчиллээ');
        this.setState({
          editModalVisible: false,
        });
        window.location.reload();
        // this.refreshTable();
      } else {
        message.error(`Нам шинэчлэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Нам шинэчлэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  renderCreateForm() {
    const { createModalVisible } = this.state;
    const { colorStore } = this.props;

    return (
      <PartyCreateModal
        handleCreate={this.handleCreate}
        handleModalVisible={this.handleCreateModalVisible}
        modalVisible={createModalVisible}
        colorStore={colorStore}
      />
    );
  }

  renderEditForm() {
    const { editModalVisible, editFormValues } = this.state;
    const { colorStore } = this.props;

    return (
      <PartyEditModal
        handleUpdate={this.handleEdit}
        handleModalVisible={this.handleEditModalVisible}
        modalVisible={editModalVisible}
        editFormValues={editFormValues}
        colorStore={colorStore}
      />
    );
  }

  renderFilterForm = () => {
    const { form } = this.props;
    const { getFieldDecorator } = form;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <FormItem>
          {getFieldDecorator('name')(<Input placeholder="Нэр" />)}
        </FormItem>
        <FormItem>
          <Button type="primary" htmlType="submit">
            Хайх
          </Button>
          <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
            Цэвэрлэх
          </Button>
        </FormItem>
      </Form>
    );
  }

  render() {
    const { partyStore, partyStore: { loading } } = this.props;

    const columns = [
      {
        title: 'Лого',
        dataIndex: 'icon',
        render: text => <img src={text ? text.url : PartyDefaultLogo} alt='logo' width='40px' />,
        width: 40,
      },
      {
        title: 'Өнгө',
        dataIndex: 'color',
        render: text => <div style={{ height: 25, width: 40, backgroundColor: text,
          clipPath: 'circle(12px at center)' }} />,
      },
      {
        title: 'Нэр',
        dataIndex: 'name',
      },
      {
        title: 'Эрэмбэ',
        dataIndex: 'order',
      },
      {
        title: 'Тайлбар',
        dataIndex: 'description',
      },
      {
        title: 'Үйлдэл',
        render: (text, record) => (
          <Fragment>
            <a key="edit" onClick={() => this.showEditForm(record)}>
              <Icon type="edit" />
            </a>
            <Divider type="vertical" />
            <a key="delete" onClick={() => this.showDeleteConfirm(record.key)}>
              <Icon type="delete" />
            </a>
          </Fragment>
        ),
      },
    ];

    const action = (
      <Button icon='plus' type='primary' onClick={() => this.handleCreateModalVisible(true)} style={{ float: 'right' }}>
        Нэмэх
      </Button>
    );

    return (
      <div>
        <PageHeaderWrapper title="Нам" loading={loading} action={action}>
          <Card bordered={false}>
            <div className={styles.tableList}>
              <Table
                rowKey="key"
                loading={loading}
                columns={columns}
                dataSource={partyStore && partyStore.data && partyStore.data.list ? partyStore.data.list : []}
                pagination={partyStore && partyStore.data && partyStore.data.pagination
                  ? partyStore.data.pagination : []}
                onChange={this.handleTableChange}
              />
            </div>
          </Card>
        </PageHeaderWrapper>
        {this.renderCreateForm()}
        {this.renderEditForm()}
      </div>
    );
  }
}

export default withStyles(styles)(PartyList);
