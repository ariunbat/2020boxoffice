import React, { Component } from 'react';
import { Modal, Form, Input, InputNumber, Upload, Radio, Icon } from 'antd';
import { inject, observer } from 'mobx-react/index';
import { getImageServerUrl } from '../../../common/services/cdn';
import { CirclePicker } from 'react-color';

const FormItem = Form.Item;
const { confirm } = Modal;

@Form.create()
@inject('authStore')
@observer
class ColleagueTypeEditModal extends Component {

  constructor(props) {
    super();
    this.state = {
      previewVisible: false,
      preview: '',
      color: null,
    };
  }

  submitHandle = () => {
    const { form, handleUpdate, colorStore } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      this.setState({
        color: null
      });
      handleUpdate(fieldsValue);
    });
  };

  backHandle = () => {
    const { handleModalVisible, form } = this.props;
    form.resetFields();
    this.setState({
      color: null
    });
    handleModalVisible(false);
  };

  showConfirm = () => {
    const { form } = this.props;
    const parentMethods = { backHandle: this.backHandle };
    if (form.isFieldsTouched()) {
      confirm({
        title: 'Та гарахдаа итгэлтэй байна уу?',
        content: '',
        okText: 'Тийм',
        okType: 'danger',
        cancelText: 'Үгүй',
        onOk() {
          parentMethods.backHandle()
        },
        onCancel() {
        },
      });
    } else {
      parentMethods.backHandle()
    }
  };

  normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  handlePreview = (file) => {
    console.log(file);
    this.setState({
      preview: file.response && file.response.data && file.response.data.url,
      previewVisible: true,
    });
  };

  handleCancelPreview = () => this.setState({ previewVisible: false });

  handleColorChange = (e) => {
    if (e && e.hex) {
      this.setState({
        color: e.hex
      });
      return e.hex;
    }
    return e;
  };

  render() {
    const { form, colorStore, modalVisible, authStore, editFormValues } = this.props;
    const { getFieldDecorator } = form;
    const { previewVisible, preview, color } = this.state;
    const formItemLayout = { labelCol: { span: 8 }, wrapperCol: { span: 14 } };
    const colors = [];

    if (colorStore && colorStore.data) {
      colorStore.data.forEach(color => {
        colors.push(color.code);
      })
    }

    if (editFormValues) {
      colors.push(editFormValues.color);
      console.log(colors);
    }

    const uploadButton = (
      <Icon type="upload" />
    );

    return (
      <Modal
        title="Коллегийн төрөл засварлах"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={this.showConfirm}
        okText='Хадгалах'
        cancelText='Болих'
      >
        <Form>
          <FormItem {...formItemLayout} label="Нэр">
            {getFieldDecorator('name', {
              initialValue: editFormValues.name,
              rules: [{ required: true, message: 'Нэр бичнэ үү' }],
            })(<Input placeholder='Нэр' />
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Тайлбар">
            {getFieldDecorator('description', {
              initialValue: editFormValues.description,
            })(<Input.TextArea placeholder='Тайлбар' rows={4} />
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Эрэмбэ">
            {getFieldDecorator('order', {
              initialValue: editFormValues.order,
              rules: [{ required: true, message: 'Эрэмбэ оруулна уу' }],
            })(<InputNumber placeholder='Эрэмбэ' min={1} />
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Лого">
            {getFieldDecorator('file', {
              valuePropName: 'fileList',
              getValueFromEvent: this.normFile,
              initialValue: editFormValues.icon && [editFormValues.icon] || [],
            })(
              <Upload
                name="file"
                accept="image/*"
                listType="picture-card"
                headers={{ 'X-Auth-Token': authStore.values.token }}
                data={{ 'entity': 'partyLogo', 'entityId': Math.random().toString(36).substring(2) }}
                action={getImageServerUrl() + '/upload'}
                onPreview={this.handlePreview}
              >
                {form.getFieldValue('file') && form.getFieldValue('file').length !== 0 ? null : uploadButton}
              </Upload>)}
          </FormItem>
          <FormItem {...formItemLayout} label="Өнгө">
            {getFieldDecorator('color', {
              initialValue: editFormValues.color,
              valuePropName: 'hex',
              getValueFromEvent: this.handleColorChange,
              rules: [{ required: true, message: 'Өнгө сонгоно уу' }],
            })(
              <CirclePicker
                colors={colors}
                color={color || editFormValues.color}
                circleSize={18}
                circleSpacing={8}
              />
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Ашиглах эсэх:">
            {getFieldDecorator('visible', {
              initialValue: editFormValues.visible,
            })(
              <Radio.Group>
                <Radio value={true}>Тийм</Radio>
                <Radio value={false}>Үгүй</Radio>
              </Radio.Group>
            )}
          </FormItem>
        </Form>
        <Modal visible={previewVisible} footer={null} onCancel={this.handleCancelPreview}>
          <img src={preview} width='100%' />
        </Modal>
      </Modal>
    );
  }
}

export default ColleagueTypeEditModal;
