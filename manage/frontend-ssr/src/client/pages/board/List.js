import React, { Component } from 'react';
import { Button, Card, Cascader, Col, Form, Input, message, Modal, Row, Select, Table, Tag, } from 'antd';
import { Trans } from 'react-i18next';
import PageHeaderWrapper from '../../components/PageHeaderWrapper';
import { objectToCommaSeparatedString } from '../../../common/utils/utils';
import BoardCreateModal from './CreateModal';
import BoardEditModal from './EditModal';

import { inject, observer } from 'mobx-react/index';
import withStyles from 'isomorphic-style-loader/withStyles';
import styles from './List.less';
import { observable } from 'mobx';
import { getLocationWithCity } from '../../../common/utils/location';

const FormItem = Form.Item;
const { Option } = Select;
const Confirm = Modal.confirm;

@Form.create()
@inject('boardStore', 'locationStore')
@observer
class BoardList extends Component {

  @observable cityData = [];

  state = {
    createModalVisible: false,
    editModalVisible: false,
    editFormValues: [],
    searchFormValues: {},
  };

  componentDidMount() {
    const { locationStore } = this.props;
    locationStore.allList({ 'used': null }).then(response => {
      if (response.result === true && response.data) {
        this.cityData = getLocationWithCity(locationStore.allLocation);
      }
    });
    this.refreshTable();
  }

  refreshTable = (params) => {
    const { boardStore } = this.props;
    boardStore.list(params);
  };

  handleTableChange = (pagination, filtersArg, sorter) => {
    const { searchFormValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = objectToCommaSeparatedString(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...searchFormValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    this.refreshTable(params);
  };

  handleSearchFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      searchFormValues: {},
    });

    this.refreshTable();
  };

  showDeleteConfirm = clickedId => {
    const parentMethods = { handleDelete: this.handleDelete };

    Confirm({
      title: 'Анхааруулга',
      content: 'Сонгосон хэрэглэгчийг устгах уу?',
      okText: 'Тийм',
      okType: 'danger',
      cancelText: 'Үгүй',
      onOk() {
        parentMethods.handleDelete(clickedId);
      },
    });
  };

  handleDelete = clickedId => {
    const { boardStore } = this.props;

    const payload = { id: clickedId };

    boardStore.delete(payload).then(response => {
      if (!response.result) {
        message.error(`Тойрог устгахад алдаа гарлаа: ${response.message}`);
      }
      this.refreshTable();
    })
      .catch(e => {
        message.error(`Тойрог устгахад алдаа гарлаа: ${e.message}`);
      });
  };

  handleSearch = e => {
    e.preventDefault();
    const { form } = this.props;
    console.log(form.getFieldsValue());
    this.setState({
      searchFormValues: form.getFieldsValue(),
    });

    this.refreshTable(form.getFieldsValue());
  };

  handleCreateModalVisible = flag => {
    this.setState({
      createModalVisible: !!flag,
    });
  };

  handleCreate = fields => {
    const { boardStore } = this.props;

    boardStore.create(fields).then(response => {
      if (response.result) {
        message.success('Тойрог амжилттай бүртгэлээ');
        this.setState({
          createModalVisible: false,
        });

        this.handleSearchFormReset();
      } else {
        message.error(`Тойрог бүртгэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Тойрог бүртгэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  showEditForm = record => {
    this.setState({
      editFormValues: record,
    });
    this.handleEditModalVisible(true);
  };

  handleEditModalVisible = flag => {
    this.setState({
      editModalVisible: !!flag,
    });
  };

  handleEdit = fields => {
    const { boardStore } = this.props;
    const { editFormValues } = this.state;

    const payload = Object.assign(editFormValues, fields);

    boardStore.update(payload).then(response => {
      if (response.result) {
        message.success('Тойрог амжилттай шинэчиллээ');
        this.setState({
          editModalVisible: false,
        });

        this.handleSearchFormReset();
      } else {
        message.error(`Тойрог шинэчлэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Тойрог  мэдээлэл шинэчлэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  renderFilterForm() {
    const { form, businessRoleStore } = this.props;
    const { getFieldDecorator } = form;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label='Нэр'>
              {getFieldDecorator('name')(<Input />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <FormItem label="Аймаг/ Хот">
              {getFieldDecorator('code')(
                <Cascader
                  style={{ width: '60%' }}
                  options={this.cityData}
                  placeholder='Аймаг/ Хот'
                />,
              )}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                &nbsp;<Trans>common.action.search</Trans>
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleSearchFormReset}>
                &nbsp;<Trans>common.action.clear</Trans>
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderCreateForm() {
    const { createModalVisible } = this.state;

    return (
      <BoardCreateModal
        handleCreate={this.handleCreate}
        handleModalVisible={this.handleCreateModalVisible}
        modalVisible={createModalVisible}
      />
    );
  }

  renderEditForm() {
    const { editModalVisible, editFormValues } = this.state;

    return (
      <BoardEditModal
        handleUpdate={this.handleEdit}
        handleModalVisible={this.handleEditModalVisible}
        modalVisible={editModalVisible}
        editFormValues={editFormValues}
      />
    );
  }

  renderLocation = (locations) => {
    let locationRender = [];
    locations.map(item => {
      locationRender.push(<Tag>{item}</Tag>);
    });
    return locationRender;
  };

  render() {
    const { boardStore, boardStore: { loading } } = this.props;

    const columns = [
      {
        title: 'Нэр',
        dataIndex: 'name',
      },
      {
        title: 'Хороо',
        dataIndex: 'locationNames',
        render: text => this.renderLocation(text)
      },
      // {
      //   title: 'Үйлдэл',
      //   render: (text, record) => (
      //     <Fragment>
      //       <a key="edit" onClick={() => this.showEditForm(record)}>
      //         <Icon type="edit" />
      //       </a>
      //       <Divider type="vertical" />
      //       <a key="delete" onClick={() => this.showDeleteConfirm(record.key)}>
      //         <Icon type="delete" />
      //       </a>
      //     </Fragment>
      //   ),
      // },
    ];

    const headerActions = (
      <Button icon='plus' type='primary' onClick={() => this.handleCreateModalVisible(true)}>
        Бүртгэх
      </Button>
    );

    return (
      <PageHeaderWrapper title="Тойрог" action={headerActions}>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderFilterForm()}</div>
            <Table
              rowKey="key"
              loading={loading}
              columns={columns}
              dataSource={boardStore && boardStore.data != null ? boardStore.data.list : []}
              pagination={boardStore && boardStore.data != null ? boardStore.data.pagination : []}
              onChange={this.handleTableChange}
            />
          </div>
        </Card>
        {this.renderCreateForm()}
        {this.renderEditForm()}
      </PageHeaderWrapper>
    );
  }
}

export default withStyles(styles)(BoardList);
