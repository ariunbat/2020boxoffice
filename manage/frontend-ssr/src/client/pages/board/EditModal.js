import React, { Component } from 'react';
import { Form, Input, Modal, Select, TreeSelect } from 'antd';
import { inject, observer } from 'mobx-react/index';
import { observable } from 'mobx';
import { getLocationByAll } from '../../../common/utils/location';

const FormItem = Form.Item;
const { Option } = Select;
const { confirm } = Modal;

@Form.create()
@inject('locationStore')
@observer
class BoardEditModal extends Component {

  @observable locationData = [];

  componentDidMount() {
    const { locationStore } = this.props;
    locationStore.allList({ 'used': null }).then(response => {
      if (response.result === true && response.data) {
        this.locationData = getLocationByAll(locationStore.allLocation);
      }
    });
  }

  submitHandle = () => {
    const { form, handleUpdate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  backHandle = () => {
    const { handleModalVisible, form } = this.props;
    form.resetFields();
    handleModalVisible(false);
  };

  showConfirm = () => {
    const { form } = this.props;
    const parentMethods = { backHandle: this.backHandle };
    if (form.isFieldsTouched()) {
      confirm({
        title: 'Та гарахдаа итгэлтэй байна уу?',
        content: '',
        okText: 'Тийм',
        okType: 'danger',
        cancelText: 'Үгүй',
        onOk() {
          parentMethods.backHandle()
        },
        onCancel() {
        },
      });
    } else {
      parentMethods.backHandle()
    }
  };

  render() {
    const { form, locationStore, modalVisible, editFormValues } = this.props;
    const { getFieldDecorator } = form;

    return (
      <Modal
        title="Тойрог засварлах"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={this.showConfirm}
      >
        <Form>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Нэр">
            {getFieldDecorator('name', {
              initialValue: editFormValues.name,
              rules: [{ required: true, message: 'Нэр бичнэ үү' }],
            })(
              <Input placeholder='Нэр' />
            )}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Засаг захиргаа">
            {getFieldDecorator('locationCodes', {
              initialValue: editFormValues.locationCodes,
            })(
              <TreeSelect
                treeData={this.locationData}
                placeholder="Засаг захиргаа"
                treeCheckable={true}
              />,
            )}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default BoardEditModal;
