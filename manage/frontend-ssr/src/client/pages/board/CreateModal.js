import React, { Component } from 'react';
import { Form, Input, Modal, Select, TreeSelect } from 'antd';
import { inject, observer } from 'mobx-react/index';
import { observable } from 'mobx';
import { getLocationByAll } from '../../../common/utils/location';

const FormItem = Form.Item;
const { Option } = Select;
const { confirm } = Modal;

@Form.create()
@inject('locationStore')
@observer
class BoardCreateModal extends Component {

  @observable locationData = [];

  componentDidMount() {
    const { locationStore } = this.props;
    locationStore.allList({ 'used': null }).then(response => {
      if (response.result === true && response.data) {
        this.locationData = getLocationByAll(locationStore.allLocation);
      }
    });
  }

  submitHandle = () => {
    const { form, handleCreate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      form.resetFields();
      handleCreate(fieldsValue);
    });
  };

  backHandle = () => {
    const { handleModalVisible, form } = this.props;
    form.resetFields();
    handleModalVisible(false);
  };

  showConfirm = () => {
    const { form } = this.props;
    const parentMethods = { backHandle: this.backHandle };
    if (form.isFieldsTouched()) {
      confirm({
        title: 'Та гарахдаа итгэлтэй байна уу?',
        content: '',
        okText: 'Тийм',
        okType: 'danger',
        cancelText: 'Үгүй',
        onOk() {
          parentMethods.backHandle()
        },
        onCancel() {
        },
      });
    } else {
      parentMethods.backHandle()
    }
  }

  render() {
    const { form, modalVisible, locationStore } = this.props;
    const { getFieldDecorator } = form;

    return (
      <Modal
        title="Тойрог бүртгэх"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={this.showConfirm}
        okText='Хадгалах'
        cancelText='Болих'
      >
        <Form>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Нэр">
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Нэр бичнэ үү' }],
            })(
              <Input placeholder='Нэр' />
            )}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Засаг захиргаа">
            {getFieldDecorator('locationCodes', {
              rules: [{ required: true, message: 'Засаг захиргаа сонгоно уу' }],
            })(
              <TreeSelect
                treeData={this.locationData}
                placeholder="Засаг захиргаа"
                treeCheckable={true}
                showCheckedStrategy='SHOW_CHILD'
              />,
            )}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default BoardCreateModal;