import React, { Component } from 'react';
import { Modal, Form, Input, Card, Button, DatePicker, message } from 'antd';
import { observer, inject } from 'mobx-react';
import PageHeaderWrapper from '../../../components/PageHeaderWrapper';
import moment from 'moment';
import { observable } from 'mobx';
import { apiFormat } from '../../../../common/utils/dateFormat';

const FormItem = Form.Item;

@Form.create()
@inject('ageCalcDate')
@observer
class AgeCalcDate extends Component {

  componentDidMount() {
    const { ageCalcDate } = this.props;
    ageCalcDate.get();
  }

  submitHandle = () => {
    const { form, ageCalcDate, history } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      ageCalcDate.update(fieldsValue).then(response => {
        if (response.result) {
          message.success('Амжилттай бүртгэлээ');
        } else {
          message.error(`Бүртгэхэд алдаа гарлаа: ${response.message}`);
        }
      })
        .catch(e => {
          console.log(e);
          // message.error(``);
        });
    });
  };

  render() {
    const { form, ageCalcDate } = this.props;
    console.log(ageCalcDate);
    const { getFieldDecorator } = form;

    return (
      <PageHeaderWrapper title="Нас тооцох огноо">
        <Card bordered={false}>
          <Form>
            <FormItem label="Нас тооцох огноо">
              {getFieldDecorator('checkDate', {
                initialValue: ageCalcDate.current && moment(ageCalcDate.current),
                rules: [
                  { required: true, message: 'Нас тооцох огноо оруулна уу' },
                ],
              })
              (<DatePicker />)
              }
            </FormItem>
            <FormItem>
              <Button type='primary' onClick={() => this.submitHandle()} style={{ float: 'right' }}>
                Засах
              </Button>
            </FormItem>
          </Form>
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default AgeCalcDate;
