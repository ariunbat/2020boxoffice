import React, { Component } from 'react';
import { Modal, Form, Input, Select, Upload, Icon, TreeSelect, AutoComplete } from 'antd';
import { inject, observer } from 'mobx-react/index';
import { getImageServerUrl } from '../../../common/services/cdn';
import { observable } from 'mobx';
import { getLocationByAll } from '../../../common/utils/location';
import { checkRegistry } from '../../../common/utils/utils';
import TagGroup from '../../components/TagGroup';

const FormItem = Form.Item;
const { Option } = Select;
const { confirm } = Modal;

@Form.create()
@inject('authStore', 'locationStore')
@observer
class UserCreateModal extends Component {

  @observable locationData = [];

  state = {
    phone: [],
  };

  normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  componentDidMount() {
    const { locationStore } = this.props;
    locationStore.tree();
  }

  setPhone = list => {
    const { form } = this.props;
    const { phone } = this.state;

    if (phone && list) {
      this.setState({
          phone: list,
          phoneValidate: null,
        },
        form.setFields({ 'phone': { value: list } })
      );

    }
  };

  checkPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (!value) {
      callback('Доод тал нь 6 тэмдэгч бичнэ үү');
    } else {
      form.validateFields(['confirmPassword'], { force: true });
    }
    callback();
  };

  checkConfirmPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value !== form.getFieldValue('password')) {
      callback('Нууц үгээ адилхан бичнэ үү');
    } else {
      callback();
    }
  };

  submitHandle = () => {
    const { form, handleCreate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { avatar, ...rest } = fieldsValue;
      let img;

      if (avatar && avatar.length !== 0) {
        avatar.map(item => {
          if (item.response && item.response.result) {
            img = {
              uid: item.uid,
              name: item.name,
              url: item.response.data.url
            }
          }
        })
      }

      form.resetFields();
      handleCreate({ ...rest, photo: img, businessRole: 'USER' });
    });
  };

  backHandle = () => {
    const { handleModalVisible, form } = this.props;
    form.resetFields();
    handleModalVisible(false);
  };

  showConfirm = () => {
    const { form } = this.props;
    const parentMethods = { backHandle: this.backHandle };
    if (form.isFieldsTouched()) {
      confirm({
        title: 'Та гарахдаа итгэлтэй байна уу?',
        content: '',
        okText: 'Тийм',
        okType: 'danger',
        cancelText: 'Үгүй',
        onOk() {
          parentMethods.backHandle()
        },
        onCancel() {
        },
      });
    } else {
      parentMethods.backHandle()
    }
  }

  render() {
    const { form, modalVisible, authStore, locationStore } = this.props;
    const { getFieldDecorator } = form;
    const uploadButton = (
      <Icon type="upload" />
    );

    return (
      <Modal
        title="Хэрэглэгч бүртгэх"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={this.showConfirm}
        okText='Хадгалах'
        cancelText='Болих'
        width={700}
      >
        <Form>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Засаг захиргаа">
            {getFieldDecorator('locationCodes', {
              rules: [{ required: true, message: 'Засаг захиргаа сонгоно уу' }],
            })(
              <TreeSelect
                treeData={locationStore && locationStore.treeData}
                placeholder="Засаг захиргаа"
                treeCheckable={true}
                showCheckedStrategy='SHOW_CHILD'
              />,
            )}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Нэвтрэх нэр">
            {getFieldDecorator('username', {
              rules: [
                { required: true, message: 'Нэвтрэх нэр бичнэ үү' },
              ],
            })(<Input placeholder="Нэвтрэх нэр" />)}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Нууц үг">
            {getFieldDecorator('password', {
              rules: [
                { required: true, message: 'Нууц үг бичнэ үү' },
                { validator: this.checkPassword },
              ],
            })(<Input placeholder="Password" />)}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Нууц үг дахин бичих">
            {getFieldDecorator('confirmPassword', {
              rules: [
                { required: true, message: 'Нууц үг дахин бичнэ үү' },
                { validator: this.checkConfirmPassword },
              ],
            })(<Input placeholder="Нууц үг дахин бичих" />)}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Овог">
            {getFieldDecorator('lastName', {})(<Input placeholder="Овог" />)}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Нэр">
            {getFieldDecorator('firstName', {
              rules: [{ required: true, message: 'Нэрээ бичнэ үү' }],
            })(<Input placeholder="Нэр" />)}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Регистрийн дугаар">
            {getFieldDecorator('registerNumber', {
              rules: [
                { validator: (rule, control, callback) => checkRegistry(rule, control, callback) },
              ],
            })(
              <Input placeholder="Регистрийн дугаар" />)
            }
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Имэйл">
            {getFieldDecorator('email', {
              rules: [{ type: 'email', message: 'Имэйл зөв үү' }],
            })(
              <Input placeholder="Имэйл" />)
            }
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Ургийн овог">
            {getFieldDecorator('familyName')(
              <Input placeholder="Ургийн овог" />)
            }
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Утас">
            {getFieldDecorator('phone', {
              valuePropName: 'initialTags',
            })(
              <TagGroup
                setTagList={this.setPhone}
                inputWith={200}
                tagColor="gold"
              />
            )}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Тайлбар">
            {getFieldDecorator('description')(
              <Input.TextArea placeholder="Тайлбар" rows={4} />)
            }
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Зураг">
            {getFieldDecorator('avatar', {
              valuePropName: 'fileList',
              getValueFromEvent: this.normFile,
            })(
              <Upload
                name="file"
                accept="image/*"
                listType="picture-card"
                headers={{ 'X-Auth-Token': authStore.values.token }}
                data={{ 'entity': 'userAvatar', 'entityId': Math.random().toString(36).substring(2) }}
                action={getImageServerUrl() + '/upload'}
                onPreview={this.handlePreview}
              >
                {form.getFieldValue('avatar') && form.getFieldValue('avatar').length !== 0 ? null : uploadButton}
              </Upload>)}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default UserCreateModal;