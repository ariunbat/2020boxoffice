import React, { Component } from 'react';
import { Form, Input, Card, Button, message } from 'antd';
import router from 'umi/router';
import { connect } from 'dva/index';
import PageHeaderWrapper from '../../components/PageHeaderWrapper';
import { inject, observer } from 'mobx-react';

const FormItem = Form.Item;

@inject('businessRoleStore')
@observer
@Form.create()
class ChangePassword extends Component {
  state = {
    confirmDirty: false,
  };

  handleConfirmBlur = (e) => {
    const { value } = e.target;
    const { confirmDirty } = this.state;
    this.setState({ confirmDirty: confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue('newPassword')) {
      callback('Нууц үгээ адилхан бичнэ үү');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    const { confirmDirty } = this.state;
    if (value && confirmDirty) {
      form.validateFields(['confirmPassword'], { force: true });
    }
    callback();
  };

  onValidateForm = () => {
    const { form, dispatch } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (!err) {
        const promise = dispatch({
          type: 'profile/changePassword',
          payload: {
            oldPassword: fieldsValue.oldPassword,
            newPassword: fieldsValue.newPassword,
          },
        });

        promise
          .then(response => {
            if (response.result) {
              dispatch({
                type: 'auth/clearAuthData',
              });
              router.push('/auth/login');
              message.success('Нууц үг амжилттай солигдлоо');
            } else {
              message.error(`Нууц үг солиход алдаа гарлаа: ${response.message}`);
            }
          })
          .catch(e => {
            message.error(`Нууц үг солиход алдаа гарлаа: ${e.message}`);
          });
      }
    });
  };

  render() {
    const { form } = this.props;
    const { getFieldDecorator } = form;

    return (
      <PageHeaderWrapper title="Нууц үг солих">
        <Card bordered={false}>
          <Form>
            <FormItem labelCol={{ span: 4 }} wrapperCol={{ span: 6 }} label="Хуучин нууц үг">
              {getFieldDecorator('oldPassword', {
                rules: [
                  { required: true, message: 'Нууц үг бичнэ үү' },
                ],
              })(<Input placeholder="Хуучин нууц үг" type="password" />)}
            </FormItem>
            <FormItem labelCol={{ span: 4 }} wrapperCol={{ span: 6 }} label="Шинэ нууц үг">
              {getFieldDecorator('newPassword', {
                rules: [
                  { required: true, message: ' Шинэ нууц үг бичнэ үү' },
                  { validator: this.validateToNextPassword },
                ],
              })(<Input placeholder="Шинэ нууц үг" type="password" />)}
            </FormItem>
            <FormItem
              labelCol={{ span: 4 }}
              wrapperCol={{ span: 6 }}
              label="Шинэ нууц  үг дахин бичих"
            >
              {getFieldDecorator('confirmPassword', {
                rules: [
                  { required: true, message: 'Шинэ нууц  үг дахин бичнэ үү' },
                  { validator: this.compareToFirstPassword, },
                ],
              })(<Input placeholder="Шинэ нууц  үг дахин бичих" type="password" onBlur={this.handleConfirmBlur} />)}
            </FormItem>
            <FormItem>
              <Button type="primary" onClick={this.onValidateForm} style={{ float: 'right' }}>
                Хадгалах
              </Button>
            </FormItem>
          </Form>
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default ChangePassword;
