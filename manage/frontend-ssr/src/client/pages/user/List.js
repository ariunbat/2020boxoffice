import React, { Component, Fragment } from 'react';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Icon,
  Button,
  Modal,
  Divider,
  message,
  Select,
  Table,
} from 'antd';
import { Trans } from 'react-i18next';
import PageHeaderWrapper from '../../components/PageHeaderWrapper';
import { objectToCommaSeparatedString } from '../../../common/utils/utils';
import UserCreateModal from './CreateModal';
import UserEditModal from './EditModal';

import { inject, observer } from 'mobx-react/index';
import withStyles from 'isomorphic-style-loader/withStyles';
import styles from './List.less';

const FormItem = Form.Item;
const { Option } = Select;
const Confirm = Modal.confirm;

@Form.create()
@inject('userStore', 'authStore')
@observer
class UserList extends Component {
  state = {
    createModalVisible: false,
    editModalVisible: false,
    editFormValues: [],
    searchFormValues: {},
  };

  componentDidMount() {
    this.refreshTable();
  }

  refreshTable = (params) => {
    const { userStore, authStore } = this.props;
    const payload = Object.assign({ active: true, businessRole: 'USER', boardId: authStore.values.boardId }, params);
    userStore.list(payload);
  };

  handleTableChange = (pagination, filtersArg, sorter) => {
    const { searchFormValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = objectToCommaSeparatedString(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...searchFormValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    this.refreshTable(params);
  };

  handleSearchFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      searchFormValues: {},
    });

    this.refreshTable();
  };

  showDeleteConfirm = clickedId => {
    const parentMethods = { handleDelete: this.handleDelete };

    Confirm({
      title: 'Анхааруулга',
      content: 'Сонгосон хэрэглэгчийг устгах уу?',
      okText: 'Тийм',
      okType: 'danger',
      cancelText: 'Үгүй',
      onOk() {
        parentMethods.handleDelete(clickedId);
      },
    });
  };

  handleDelete = clickedId => {
    const { userStore } = this.props;

    const payload = { id: clickedId };

    userStore.delete(payload).then(response => {
      if (!response.result) {
        message.error(`Хэрэглэгч устгахад алдаа гарлаа: ${response.message}`);
      }
      this.refreshTable();
    })
      .catch(e => {
        message.error(`Хэрэглэгч устгахад алдаа гарлаа: ${e.message}`);
      });
  };

  handleSearch = e => {
    e.preventDefault();
    const { form } = this.props;

    this.setState({
      searchFormValues: form.getFieldsValue(),
    });

    this.refreshTable(form.getFieldsValue());
  };

  handleCreateModalVisible = flag => {
    this.setState({
      createModalVisible: !!flag,
    });
  };

  handleCreate = fields => {
    const { userStore, authStore } = this.props;

    userStore.create(Object.assign(fields, {boardId: authStore.values.boardId})).then(response => {
      if (response.result) {
        message.success('Хэрэглэгч амжилттай бүртгэлээ');
        this.setState({
          createModalVisible: false,
        });

        this.handleSearchFormReset();
      } else {
        message.error(`Хэрэглэгч бүртгэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Хэрэглэгч бүртгэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  showEditForm = record => {
    this.setState({
      editFormValues: record,
    });
    this.handleEditModalVisible(true);
  };

  handleEditModalVisible = flag => {
    this.setState({
      editModalVisible: !!flag,
    });
  };

  handleEdit = fields => {
    const { userStore } = this.props;
    const { editFormValues } = this.state;

    const payload = Object.assign(editFormValues, fields);

    userStore.update(payload).then(response => {
      if (response.result) {
        message.success('Хэрэглэгчийн мэдээлэл амжилттай шинэчиллээ');
        this.setState({
          editModalVisible: false,
        });

        this.handleSearchFormReset();
      } else {
        message.error(`Хэрэглэгчийн мэдээлэл шинэчлэхэд алдаа гарлаа: ${response.message}`);
        2
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Хэрэглэгчийн мэдээлэл шинэчлэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  renderFilterForm() {
    const { form } = this.props;
    const { getFieldDecorator } = form;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label={<Trans>common.action.search</Trans>}>
              {getFieldDecorator('search')(<Input />)}
            </FormItem>
          </Col>
          <Col md={6} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                &nbsp;<Trans>common.action.search</Trans>
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleSearchFormReset}>
                &nbsp;<Trans>common.action.clear</Trans>
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderCreateForm() {
    const { createModalVisible } = this.state;

    return (
      <UserCreateModal
        handleCreate={this.handleCreate}
        handleModalVisible={this.handleCreateModalVisible}
        modalVisible={createModalVisible}
      />
    );
  }

  renderEditForm() {
    const { editModalVisible, editFormValues } = this.state;

    return (
      <UserEditModal
        handleUpdate={this.handleEdit}
        handleModalVisible={this.handleEditModalVisible}
        modalVisible={editModalVisible}
        editFormValues={editFormValues}
      />
    );
  }

  render() {
    const { userStore, userStore: { loading } } = this.props;

    const columns = [
      {
        title: 'Хэрэглэгчийн төрөл',
        dataIndex: 'businessRole',
      },
      {
        title: 'Нэвтрэх нэр',
        dataIndex: 'username',
      },
      {
        title: 'Овог',
        dataIndex: 'lastName',
      },
      {
        title: 'Нэр',
        dataIndex: 'firstName',
      },
      {
        title: 'Үйлдэл',
        render: (text, record) => (
          <Fragment>
            <a key="edit" onClick={() => this.showEditForm(record)}>
              <Icon type="edit" />
            </a>
            <Divider type="vertical" />
            <a key="delete" onClick={() => this.showDeleteConfirm(record.key)}>
              <Icon type="delete" />
            </a>
          </Fragment>
        ),
      },
    ];

    const headerActions = (
      <Button icon='plus' type='primary' onClick={() => this.handleCreateModalVisible(true)}>
        Бүртгэх
      </Button>
    );

    return (
      <PageHeaderWrapper title="Системийн хэрэглэгчид" action={headerActions}>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderFilterForm()}</div>
            <Table
              rowKey="key"
              loading={loading}
              columns={columns}
              dataSource={userStore && userStore.data != null ? userStore.data.list : []}
              pagination={userStore && userStore.data != null ? userStore.data.pagination : []}
              onChange={this.handleTableChange}
            />
          </div>
        </Card>
        {this.renderCreateForm()}
        {this.renderEditForm()}
      </PageHeaderWrapper>
    );
  }
}

export default withStyles(styles)(UserList);
