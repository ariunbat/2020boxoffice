import React, { Component } from 'react';
import { Form, Cascader } from 'antd';
import { observer, inject } from 'mobx-react';

@Form.create()
@inject('locationStore')
@observer
class LocationField extends Component {

  componentDidMount() {
    const { initial, loading } = this.props;
    this.load(initial);
  }

  load = (initial) => {
    const { locationStore } = this.props;
    locationStore.list({ parentId: 1 })
      .then((response) => {
        if (response && response.result) {
          if (initial && initial.length > 0) {
            const city = initial[0];
            const district = initial.length > 1 ? initial[1] : null;
            this.loadCity(city, district);
          }
        }
      });
  };

  loadCity = (city, district) => {
    const { locationStore: { data } } = this.props;
    const targetOption = data.find((r) => {
      return r.value === city
    });
    this.getLocation(targetOption, district);
  };

  loadDistrict = (district) => {
    const { locationStore: { data } } = this.props;
    let parentOption = data.find((r) => {
      return r.children && r.children.find(c => {
        return c.value === district;
      })
    });
    if (parentOption) {
      const targetOption = parentOption.children.find((r) => r.value === district);
      this.getLocation(targetOption);
    }
  };

  getLocation = (targetOption, district = null) => {
    const { locationStore } = this.props;
    const test = [];
    locationStore.list({ parentId: targetOption.value })
      .then(response => {
        response.data.map(option => {
          test.push({
            value: option.locationId,
            parent: option.parentId,
            label: option.name,
            isLeaf: option.leaf
          })
        })
        targetOption.loading = false;
        targetOption.children = test;
        this.setState({});
        if (district) {
          this.loadDistrict(district);
        }
      })
    targetOption.loading = true;
  };

  loadData = (selectedOptions) => {
    const targetOption = selectedOptions[selectedOptions.length - 1];
    this.getLocation(targetOption);
  };

  render() {
    const { locationStore: { data }, initial, onChange } = this.props;

    return (
      <Cascader
        placeholder={'Аймаг/Хот, Сум/Дүүрэг, Баг/Хороо'}
        options={data}
        loadData={this.loadData}
        onChange={onChange}
        changeOnSelect
        value={initial ? initial : []}
      />
    );
  }
}

export default LocationField;