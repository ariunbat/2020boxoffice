import React, { Component } from 'react';
import { Modal, Form, Input, Select, message, Radio, Upload, Icon } from 'antd';
import { inject, observer } from 'mobx-react/index';
import { checkRegistry } from '../../../common/utils/utils';
import moment from 'moment';
import { apiFormat } from '../../../common/utils/dateFormat';
import { observable } from 'mobx';
import { getImageServerUrl } from '../../../common/services/cdn';

const FormItem = Form.Item;
const { Option } = Select;
const { confirm } = Modal;

@Form.create()
@inject('ageCalcDate', 'partyStore', 'authStore')
@observer
class MemberEditModal extends Component {

  @observable age = null;

  componentDidMount() {
    const { ageCalcDate, partyStore } = this.props;
    ageCalcDate.get();
    partyStore.selectList();
  }

  submitHandle = () => {
    const { form, handleUpdate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  backHandle = () => {
    const { handleModalVisible, form } = this.props;
    form.resetFields();
    handleModalVisible(false);
  };

  showConfirm = () => {
    const { form } = this.props;
    const parentMethods = { backHandle: this.backHandle };
    if (form.isFieldsTouched()) {
      confirm({
        title: 'Та гарахдаа итгэлтэй байна уу?',
        content: '',
        okText: 'Тийм',
        okType: 'danger',
        cancelText: 'Үгүй',
        onOk() {
          parentMethods.backHandle();
        },
        onCancel() {
        },
      });
    } else {
      parentMethods.backHandle()
    }
  };

  getAge = (registerNumber) => {
    const { ageCalcDate } = this.props;
    let year;
    let month;
    let day;
    const regNum = registerNumber;
    let birthDate;

    if (regNum.length === 10) {
      year = regNum.substring(2, 4);
      month = regNum.substring(4, 6);
      day = regNum.substring(6, 8);
      if (year.substring(0, 1) === '1' || year.substring(0, 1) === '0') {
        year = '20'.concat(year);
        month = month - 20;
      } else {
        year = '19'.concat(year);
      }
      birthDate = year + '-' + month.toString().padStart(2, '0') + '-' + day;
      this.age = moment(ageCalcDate && ageCalcDate.current).diff(birthDate, 'years');
    }
  };

  normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  render() {
    const { form, modalVisible, ageCalcDate, partyStore, authStore, editFormValues } = this.props;
    const { getFieldDecorator } = form;
    const formItemLayout = { labelCol: { span: 8 }, wrapperCol: { span: 16 } };

    return (
      <Modal
        title="Гэр бүлийн гишүүн нэмэх"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={this.showConfirm}
        okText='Хадгалах'
        cancelText='Болих'
      >
        <Form>
          <FormItem {...formItemLayout} label="Зураг">
            {getFieldDecorator('file', {
              initialValue: editFormValues.photo && [editFormValues.photo] || [],
              valuePropName: 'fileList',
              getValueFromEvent: this.normFile,
            })(
              <Upload
                disabled
                name="file"
                accept="image/*"
                listType="picture-card"
                headers={{ 'X-Auth-Token': authStore.values.token }}
                data={{ 'entity': 'userAvatar', 'entityId': Math.random().toString(36).substring(2) }}
                action={getImageServerUrl() + '/upload'}
                onPreview={this.handlePreview}
              >
              </Upload>)}
          </FormItem>
          <FormItem {...formItemLayout} label="Регистрийн дугаар">
            {getFieldDecorator('registerNumber', {
              initialValue: editFormValues.registerNumber,
              rules: [
                { validator: (rule, control, callback) => checkRegistry(rule, control, callback) },
              ],
            })(
              <Input placeholder="Регистрийн дугаар" disabled />)
            }
          </FormItem>
          <FormItem {...formItemLayout} label="Ургийн овог">
            {getFieldDecorator('familyName', {
              initialValue: editFormValues.familyName,
            })(
              <Input placeholder="Ургийн овог" disabled />)
            }
          </FormItem>
          <FormItem {...formItemLayout} label="Нэр">
            {getFieldDecorator('firstName', {
              initialValue: editFormValues.firstName,
            })(<Input placeholder="Нэр" disabled />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Эцэг /эх/-ийн нэр">
            {getFieldDecorator('lastName', {
              initialValue: editFormValues.lastName,
            })(<Input placeholder="Эцэг /эх/-ийн нэр" disabled />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Хүйс">
            {getFieldDecorator('gender', {
              initialValue: editFormValues.gender,
            })
            (
              <Radio.Group disabled>
                <Radio key="MALE" value='MALE'>Эр</Radio>
                <Radio key="FEMALE" value='FEMALE'>Эм</Radio>
              </Radio.Group>
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Нас">
            <span>{this.age && `${this.age} (${moment(ageCalcDate.current).format(apiFormat)}-ны байдлаар)`}</span>
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Нам эвсэл">
            {getFieldDecorator('partyId', {
              initialValue: editFormValues.partyId,
              rules: [{ required: true, message: 'Нам эвсэл сонгоно уу' }],
            })(
              <Select placeholder="Нам эвсэл сонгоно уу" style={{ width: '100%' }}>
                {partyStore != null && partyStore.data != null
                  ? partyStore.data.list.map(party => <Option key={party.key} value={party.key}>{party.name}</Option>)
                  : ''}
              </Select>
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Албан тушаал">
            {getFieldDecorator('position', {
              initialValue: editFormValues.position,
            })(
              <Input placeholder="Албан тушаал" />)
            }
          </FormItem>
          <FormItem {...formItemLayout} label="Тайлбар">
            {getFieldDecorator('description', {
              initialValue: editFormValues.description,
            })(
              <Input.TextArea placeholder="Тайлбар" />)
            }
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default MemberEditModal;