import React, { Component, Fragment } from 'react';
import { Form, Table, Card, Row, Col, Icon, Button, Input, message, Divider, Modal, DatePicker, Tag, Select, Alert, Cascader } from 'antd';
import { observer, inject } from 'mobx-react';
import PageHeaderWrapper from '../../components/PageHeaderWrapper';
import { Trans } from 'react-i18next';
import { checkAuth } from '../../../common/utils/auth';
import { Link } from 'react-router-dom';
import LocationField from '../Location';
import moment from 'moment';
import { apiFormat, displayFormat } from '../../../common/utils/dateFormat';

import Description from '../../components/DescriptionList/Description';
import CitizenDefault from '../../assets/citizenMan.png';
import PartyDefaultLogo from '../../assets/party.png';
import styles from './List.less';
import withStyles from 'isomorphic-style-loader/withStyles';
import MemberCreateModal from './CreateModal';
import MemberEditModal from './EditModal';

const FormItem = Form.Item;
const Confirm = Modal.confirm;
const { RangePicker } = DatePicker;

@Form.create()
@inject('citizenStore', 'partyStore', 'authStore', 'locationStore')
@observer
class MemberList extends Component {
  state = {
    loading: false,
    searchFormValues: {},
    createModalVisible: false,
    editModalVisible: false,
    editFormValues: [],
  };

  componentDidMount() {
    const { citizenStore, partyStore, locationStore } = this.props;
    citizenStore.clearCurrent();
    partyStore.selectList();
    locationStore.tree();
    this.refreshList();
  }

  refreshList = (params) => {
    const { citizenStore, authStore } = this.props;
    const payload = Object.assign({ boardId: authStore.values.boardId }, params)
    citizenStore.memberList(payload);
  };

  handleTableChange = (pagination) => {
    const { searchFormValues } = this.state;

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...searchFormValues,
    };

    this.refreshList(params);
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      searchFormValues: {},
    });

    this.refreshList();
  };

  handleSearch = e => {
    e.preventDefault();
    const { form } = this.props;
    const { birthDate, locationCode, ...rest } = form.getFieldsValue();

    let birthStart;
    let birthEnd;
    if (birthDate) {
      birthStart = moment(birthDate[0]).format(apiFormat);
      birthEnd = moment(birthDate[1]).format(apiFormat);
    }
    const payload = Object.assign(rest, { birthStart, birthEnd, locationCode: locationCode.join() });

    this.setState({
      searchFormValues: payload,
    });

    this.refreshList(payload);
  };

  showDeleteConfirm = clickedId => {
    const parentMethods = { handleDelete: this.handleDelete };

    Confirm({
      title: 'Анхааруулга',
      content: 'Үйлчилгээ устгах уу?',
      okText: 'Тийм',
      okType: 'danger',
      cancelText: 'Үгүй',
      onOk() {
        parentMethods.handleDelete(clickedId);
      },
    });
  };

  handleDelete = clickedId => {
    const { citizenStore } = this.props;
    citizenStore.delete(Object.assign({ id: clickedId }))
      .then(response => {
        if (response.result === true && response.data) {
          message.success('Үйлчилгээ амжилттай устгалаа');
          this.handleFormReset();
        } else {
          message.error(`Үйлчилгээ устгахад алдаа гарлаа: ${response.message}`);
        }
      }).catch(e => {
      console.log(e);
      message.error(`Үйлчилгээ устгахад алдаа гарлаа: ${e.message}`);
    });
  };

  renderFilterForm = () => {
    const { form, partyStore, locationStore } = this.props;
    const { getFieldDecorator } = form;

    return (
      <Form onSubmit={this.handleSearch} layout='inline'>
        <Row>
          <Col span={20}>
            <FormItem>
              {getFieldDecorator('partyId')(
                <Select placeholder='Нам эвсэл' style={{ width: '200px' }}>
                  {partyStore != null && partyStore.data != null
                    ? partyStore.data.list.map(party => <Select.Option key={party.key} value={party.key}>{party.name}</Select.Option>)
                    : ''}
                </Select>
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('registerNumber')(<Input placeholder="РД" />)}
            </FormItem>
            <FormItem>
              {getFieldDecorator('firstName')(<Input placeholder="Нэр" />)}
            </FormItem>
            <FormItem>
              {getFieldDecorator('lastName')(<Input placeholder="Эцэг /эх/-ийн нэр" />)}
            </FormItem>
            <FormItem>
              {getFieldDecorator('familyName')(<Input placeholder="Ургийн овог" />)}
            </FormItem>
            <FormItem>
              {getFieldDecorator('locationCode', {
                initialValue: [],
              })(
                <Cascader
                  options={locationStore && locationStore.treeData}
                  placeholder="Засаг захиргаа"
                />,
              )}
            </FormItem>
            <FormItem label="Төрсөн огноо">
              {getFieldDecorator('birthDate')
              (<RangePicker
                format={displayFormat}
                placeholder={['эхний огноо', 'сүүлийн огноо']}
              />)}
            </FormItem>
          </Col>
          <Col span={4}>
            <FormItem>
              <Button type="primary" htmlType="submit">
                Хайх
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                Цэвэрлэх
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  };

  handleCreate = fields => {
    const { citizenStore } = this.props;

    const { file, ...rest } = fields;
    let fileData;

    if (file && file.length !== 0) {
      file.map(item => {
        if (item.response && item.response.result) {
          fileData = {
            uid: item.uid,
            name: item.name,
            url: item.response.data.url
          }
        }
      })
    }

    const payload = Object.assign(rest, { photo: fileData });

    citizenStore.createMember(payload).then(response => {
      if (response.result) {
        if (response.data) {
          message.success('Намын гишүүнчлэл амжилттай бүртгэлээ');
          this.setState({
            createModalVisible: false,
          });

          this.refreshList();
        } else {
          message.error(response.message);
        }
      } else {
        message.error(`Намын гишүүнчлэл бүртгэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Хэрэглэгч бүртгэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  handleCreateModalVisible = flag => {
    this.setState({
      createModalVisible: !!flag,
    });
  };

  showEditForm = record => {
    this.setState({
      editFormValues: record,
    });
    this.handleEditModalVisible(true);
  };

  handleEditModalVisible = flag => {
    this.setState({
      editModalVisible: !!flag,
    });
  };

  handleEdit = fields => {
    const { citizenStore } = this.props;
    const { editFormValues } = this.state;

    const payload = Object.assign(editFormValues, fields);

    citizenStore.updateMember(payload).then(response => {
      if (response.result) {
        message.success('Намын гишүүнчлэл амжилттай шинэчиллээ');
        this.setState({
          editModalVisible: false,
        });

        this.refreshList();
      } else {
        message.error(`Намын гишүүнчлэл шинэчлэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Хэрэглэгчийн мэдээлэл шинэчлэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  renderCreateForm() {
    const { createModalVisible } = this.state;

    return (
      <MemberCreateModal
        handleCreate={this.handleCreate}
        handleModalVisible={this.handleCreateModalVisible}
        modalVisible={createModalVisible}
      />
    );
  }

  renderEditForm() {
    const { editModalVisible, editFormValues } = this.state;

    return (
      <MemberEditModal
        handleUpdate={this.handleEdit}
        handleModalVisible={this.handleEditModalVisible}
        modalVisible={editModalVisible}
        editFormValues={editFormValues}
      />
    );
  }

  render() {
    const { citizenStore, form } = this.props;
    const { getFieldDecorator } = form;

    const columns = [
      {
        key: 'partyLogo',
        title: 'Зураг',
        dataIndex: 'partyLogo',
        render: text => <img alt='logo' src={text ? text.url : PartyDefaultLogo} width='40px' />,
      },
      {
        key: 'photo',
        title: 'Зураг',
        dataIndex: 'photo',
        render: text => <img alt='photo' src={text ? text.url : CitizenDefault} className={styles.citizenPhoto} />,
      },
      {
        key: 'registerNumber',
        title: 'Регистер',
        dataIndex: 'registerNumber',
      },
      {
        key: 'firstName',
        title: 'Нэр',
        dataIndex: 'firstName',
      },
      {
        key: 'lastName',
        title: 'Овог',
        dataIndex: 'lastName',
      },
      {
        key: 'genderName',
        title: 'Хүйс',
        dataIndex: 'genderName',
      },
      {
        key: 'age',
        title: 'Нас',
        dataIndex: 'age',
        render: text => Math.floor(text),
      },
      {
        key: 'address',
        title: 'Хаяг',
        dataIndex: 'locationCode',
        render: (text, record) => record.locationCode && <div>
          {record.cityName}, {record.districtName} {record.subDistrictName},
          {record.town} {record.apartment} {record.apartmentAddress} {record.street} {record.gerAddress}
          {record.countrySideAddress}
        </div>,
      },
      {
        key: 'actions',
        title: 'Үйлдэл',
        width: '170px',
        render: (text, record) => (
          <Fragment>
            <Button type="dashed" shape="circle">
              <a key="edit" onClick={() => this.showEditForm(record)}>
                <Icon type="edit" theme="twoTone" />
              </a>
            </Button>
          </Fragment>
        ),
      }];

    const headerActions = (
      <Button icon='plus' type='primary' onClick={() => this.handleCreateModalVisible(true)}>
        Бүртгэх
      </Button>
    );
    return (
      <PageHeaderWrapper
        title="Үндсэн жагсаалт"
        action={headerActions}
      >
        <Card>
          <Row gutter={16}>
            <Col span={24}>
              {this.renderFilterForm()}
              <Alert message={`ҮР ДҮН: Нийт бичлэг - ${
                citizenStore &&
                citizenStore.data != null &&
                citizenStore.data.pagination != null &&
                citizenStore.data.pagination.total != null
                  ? citizenStore.data.pagination.total
                  : '-'
              }`} />
              <br />
            </Col>
            <Col span={24}>
              <Table
                rowKey="id"
                loading={citizenStore && citizenStore.loading}
                columns={columns}
                dataSource={citizenStore && citizenStore.data && citizenStore.data.list ? citizenStore.data.list : []}
                pagination={citizenStore && citizenStore.data && citizenStore.data.pagination
                  ? citizenStore.data.pagination : []}
                onChange={this.handleTableChange}
                // scroll={{ x: 1300 }}
                scroll={{ x: 'max-content' }}
              />
            </Col>
          </Row>
        </Card>
        {this.renderCreateForm()}
        {this.renderEditForm()}
      </PageHeaderWrapper>
    );
  }
}

export default withStyles(styles)(MemberList);
