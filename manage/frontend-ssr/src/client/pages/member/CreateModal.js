import React, { Component } from 'react';
import { Modal, Form, Input, Select, message, Radio, Upload, Icon } from 'antd';
import { inject, observer } from 'mobx-react/index';
import { checkRegistry } from '../../../common/utils/utils';
import moment from 'moment';
import { apiFormat } from '../../../common/utils/dateFormat';
import { observable } from 'mobx';
import { getImageServerUrl } from '../../../common/services/cdn';

const FormItem = Form.Item;
const { Option } = Select;
const { confirm } = Modal;

@Form.create()
@inject('citizenStore', 'ageCalcDate', 'partyStore', 'authStore')
@observer
class MemberCreateModal extends Component {

  @observable age = null;

  state = {
    memberExists: false,
  };

  componentDidMount() {
    const { ageCalcDate, partyStore } = this.props;
    ageCalcDate.get();
    partyStore.selectList();
  }

  resetStates = () => {
    const { form } = this.props;
    this.setState({
      memberExists: false,
    });
    form.setFieldsValue({
      file: null,
      firstName: null,
      lastName: null,
      familyName: null,
      gender: 'MALE',
    });
    this.age = null;
  };

  submitHandle = () => {
    const { form, handleCreate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      form.resetFields();
      this.resetStates();
      handleCreate(fieldsValue);
    });
  };

  backHandle = () => {
    const { handleModalVisible, form } = this.props;
    form.resetFields();
    this.resetStates();
    handleModalVisible(false);
  };

  showConfirm = () => {
    const { form } = this.props;
    const parentMethods = { backHandle: this.backHandle };
    if (form.isFieldsTouched()) {
      confirm({
        title: 'Та гарахдаа итгэлтэй байна уу?',
        content: '',
        okText: 'Тийм',
        okType: 'danger',
        cancelText: 'Үгүй',
        onOk() {
          parentMethods.backHandle();
        },
        onCancel() {
        },
      });
    } else {
      parentMethods.backHandle()
    }
  };

  getAge = (registerNumber) => {
    const { ageCalcDate } = this.props;
    let year;
    let month;
    let day;
    const regNum = registerNumber;
    let birthDate;

    if (regNum.length === 10) {
      year = regNum.substring(2, 4);
      month = regNum.substring(4, 6);
      day = regNum.substring(6, 8);
      if (year.substring(0, 1) === '1' || year.substring(0, 1) === '0') {
        year = '20'.concat(year);
        month = month - 20;
      } else {
        year = '19'.concat(year);
      }
      birthDate = year + '-' + month.toString().padStart(2, '0') + '-' + day;
      this.age = moment(ageCalcDate && ageCalcDate.current).diff(birthDate, 'years');
    }
  };

  checkMemberExists = (e) => {
    const { citizenStore, form } = this.props;
    const registerNumber = e.target.value;
    if (registerNumber.length === 10) {
      citizenStore.checkCitizen({ registerNumber }).then(response => {
        if (response.result) {
          if (response.data) {
            form.setFieldsValue({
              file: response.data.photo ? [response.data.photo] : null,
              firstName: response.data.firstName,
              lastName: response.data.lastName,
              familyName: response.data.familyName,
              gender: response.data.gender,
            });
            this.getAge(registerNumber);
            this.setState({
              memberExists: true,
            })
          } else {
            this.resetStates();
            this.getAge(registerNumber);
          }
        } else {
          form.resetFields();
      this.resetStates();
    }
  })
        .catch(e => {
          console.log(e);
          // message.error(`алдаа гарлаа: ${response.message}`);
        });
    } else {
      form.resetFields();
      this.resetStates();
    }
  };

  normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  render() {
    const { form, modalVisible, ageCalcDate, partyStore, authStore } = this.props;
    const { getFieldDecorator } = form;
    const { memberExists } = this.state;

    const formItemLayout = { labelCol: { span: 8 }, wrapperCol: { span: 16 } };

    const uploadButton = (
      <Icon type="upload" />
    );

    return (
      <Modal
        title="Гишүүнчлэл нэмэх"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={this.showConfirm}
        okText='Хадгалах'
        cancelText='Болих'
      >
        <Form>
          <FormItem {...formItemLayout} label="Зураг">
            {getFieldDecorator('file', {
              valuePropName: 'fileList',
              getValueFromEvent: this.normFile,
            })(
              <Upload
                name="file"
                accept="image/*"
                listType="picture-card"
                headers={{ 'X-Auth-Token': authStore.values.token }}
                data={{ 'entity': 'userAvatar', 'entityId': Math.random().toString(36).substring(2) }}
                action={getImageServerUrl() + '/upload'}
                onPreview={this.handlePreview}
              >
                {form.getFieldValue('file') && form.getFieldValue('file').length !== 0 ? null : uploadButton}
              </Upload>)}
          </FormItem>
          <FormItem {...formItemLayout} label="Регистрийн дугаар">
            {getFieldDecorator('registerNumber', {
              rules: [
                { validator: (rule, control, callback) => checkRegistry(rule, control, callback) },
              ],
            })(
              <Input placeholder="Регистрийн дугаар" onChange={(value) => this.checkMemberExists(value)} />)
            }
          </FormItem>
          <FormItem {...formItemLayout} label="Ургийн овог">
            {getFieldDecorator('familyName')(
              <Input placeholder="Ургийн овог" disabled={memberExists} />)
            }
          </FormItem>
          <FormItem {...formItemLayout} label="Нэр">
            {getFieldDecorator('firstName', {
              rules: [{ required: true, message: 'Нэрээ бичнэ үү' }],
            })(<Input placeholder="Нэр" disabled={memberExists} />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Эцэг /эх/-ийн нэр">
            {getFieldDecorator('lastName')(<Input placeholder="Эцэг /эх/-ийн нэр" disabled={memberExists} />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Хүйс">
            {getFieldDecorator('gender', {
              initialValue: 'MALE',
            })
            (
              <Radio.Group disabled={memberExists}>
                <Radio key="MALE" value='MALE'>Эр</Radio>
                <Radio key="FEMALE" value='FEMALE'>Эм</Radio>
              </Radio.Group>
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Нас">
            <span>{this.age && `${this.age} (${moment(ageCalcDate.current).format(apiFormat)}-ны байдлаар)`}</span>
          </FormItem>
          <FormItem {...formItemLayout} label="Нам эвсэл">
            {getFieldDecorator('partyId', {
              rules: [{ required: true, message: 'Нам эвсэл сонгоно уу' }],
            })(
              <Select placeholder="Нам эвсэл сонгоно уу" style={{ width: '100%' }}>
                {partyStore != null && partyStore.data != null
                  ? partyStore.data.list.map(party => <Option key={party.key} value={party.key}>{party.name}</Option>)
                  : ''}
              </Select>
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Албан тушаал">
            {getFieldDecorator('position')(
              <Input placeholder="Албан тушаал" />)
            }
          </FormItem>
          <FormItem {...formItemLayout} label="Тайлбар">
            {getFieldDecorator('description')(
              <Input.TextArea placeholder="Тайлбар" />)
            }
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default MemberCreateModal;