import React, { Component } from 'react';
import { Modal, Form, Input, Select } from 'antd';
import { inject, observer } from 'mobx-react/index';

const FormItem = Form.Item;
const { Option } = Select;

@Form.create()
@inject('applicationRoleStore', 'businessRoleStore')
@observer
class BusinessRoleEditModal extends Component {
  componentDidMount() {
    // const { applicationRoleStore } = this.props;
    // applicationRoleStore.fetchRoles();
  }

  submitHandle = () => {
    const { form, handleUpdate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  render() {
    const { form, applicationRoleStore, handleModalVisible, modalVisible, editFormValues } = this.props;
    const { getFieldDecorator } = form;

    return (
      <Modal
        title="Хэрэглэгчийн төрөл засварлах"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={() => handleModalVisible(false)}
        okText='Хадгалах'
        cancelText='Болих'
      >
        <Form>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Хэрэглэгчийн төрөл">
            {getFieldDecorator('role', {
              initialValue: editFormValues.role,
              rules: [{ required: true, message: 'Хэрэглэгчийн төрөл бичнэ үү' }],
              // hidden: true,
            })(<Input placeholder="Хэрэглэгчийн төрөл" readOnly />)}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Хандах эрх">
            {getFieldDecorator('applicationRoles', {
              initialValue: editFormValues.applicationRoles || [],
            })(
              <Select
                mode="multiple"
                placeholder="Хандах эрхүүд сонгоно уу"
                style={{ width: '100%' }}
              >
                <Option key="" value="">
                  Сонгох
                </Option>
                {applicationRoleStore != null ?
                  applicationRoleStore.list.map(appRole => <Option key={appRole} value={appRole}>{appRole}</Option>) : ''}
              </Select>
            )}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}
export default (BusinessRoleEditModal);