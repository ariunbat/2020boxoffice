import React, { Component, Fragment } from 'react';
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Icon,
  Button,
  Dropdown,
  Menu,
  Modal,
  Divider,
  message,
  Tag,
  Select,
} from 'antd';
import StandardTable from '../../components/StandardTable';
import PageHeaderWrapper from '../../components/PageHeaderWrapper';
import { objectToCommaSeparatedString } from '../../../common/utils/utils';
import { Trans } from 'react-i18next';
import BusinessRoleCreateModal from './CreateModal';
import BusinessRoleEditModal from './EditModal';

import styles from './List.less';
import { inject, observer } from 'mobx-react/index';
import withStyles from 'isomorphic-style-loader/withStyles';

const FormItem = Form.Item;
const { Option } = Select;
const Confirm = Modal.confirm;

@Form.create()
@inject('applicationRoleStore', 'businessRoleStore')
@observer
class BusinessRoleList extends Component {
  state = {
    createModalVisible: false,
    editModalVisible: false,
    editFormValues: [],
    selectedRows: [],
    searchFormValues: {},
  };

  componentDidMount() {
    const { applicationRoleStore } = this.props;
    applicationRoleStore.fetchRoles();
    this.refreshTable();
  }

  refreshTable = (params) => {
    const { businessRoleStore } = this.props;
    businessRoleStore.list();
  };

  handleTableChange = (pagination, filtersArg, sorter) => {
    const { searchFormValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = objectToCommaSeparatedString(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...searchFormValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    this.refreshTable(params);
  };

  handleSearchFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      searchFormValues: {},
    });

    this.refreshTable();
  };

  showDeleteConfirm = clickedId => {
    const parentMethods = { handleDelete: this.handleDelete };

    Confirm({
      title: 'Анхааруулга',
      content: 'Хэрэглэгчийн төрлийг устгах уу? Тухайн төрөлд хамаарах хэрэглэгчид системд нэвтрэх боломжгүй болно',
      okText: 'Тийм',
      okType: 'danger',
      cancelText: 'Үгүй',
      onOk() {
        parentMethods.handleDelete(clickedId);
      },
    });
  };

  handleDelete = clickedId => {
    const { businessRoleStore } = this.props;

    const payload = { ids: [clickedId] };

    businessRoleStore.deleteMulti(payload).then(response => {
      if (!response.result) {
        message.error(`Хэрэглэгчийн төрөл устгахад алдаа гарлаа: ${response.message}`);
      }

      this.refreshTable();
      this.setState({
        selectedRows: [],
      });
    })
      .catch(e => {
        message.error(`Хэрэглэгчийн төрөл устгахад алдаа гарлаа: ${e.message}`);
      });
  };

  handleMenuClick = e => {
    const { selectedRows } = this.state;

    if (!selectedRows) return;

    switch (e.key) {
      case 'delete':
        this.showMultiDeleteConfirm();
        break;
      default:
        break;
    }
  };

  showMultiDeleteConfirm = () => {
    const parentMethods = { handleMultiDelete: this.handleMultiDelete };

    Confirm({
      title: 'Анхааруулга',
      content: 'Сонгосон хэрэглэгчийн төрлүүдийг устгах уу? Тухайн төрөлд хамаарах хэрэглэгчид системд нэвтрэх боломжгүй болно',
      okText: 'Тийм',
      okType: 'danger',
      cancelText: 'Үгүй',
      onOk() {
        parentMethods.handleMultiDelete();
      },
    });
  };

  handleMultiDelete = () => {
    const { businessRoleStore } = this.props;
    const { selectedRows } = this.state;

    const payload = { ids: [...selectedRows.map(row => row.role).values()] };

    businessRoleStore.delete(payload).then(response => {
      if (!response.result) {
        message.error(`Хэрэглэгчийн төрөл устгахад алдаа гарлаа: ${response.message}`);
      }

      this.handleSearchFormReset();
      this.setState({
        selectedRows: [],
      });
    })
      .catch(e => {
        message.error(`Хэрэглэгчийн төрөл устгахад алдаа гарлаа: ${e.message}`);
      });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleSearch = e => {
    e.preventDefault();
    const { form } = this.props;

    this.setState({
      searchFormValues: form.getFieldsValue(),
    });

    this.refreshTable(form.getFieldsValue());
  };

  handleCreateModalVisible = flag => {
    this.setState({
      createModalVisible: !!flag,
    });
  };

  handleCreate = fields => {
    const { businessRoleStore } = this.props;
    const payload = {
      role: fields.role,
      applicationRoles: fields.applicationRoles,
    };

    businessRoleStore.create(payload).then(response => {
      if (response.result) {
        message.success('Хэрэглэгчийн төрөл амжилттай бүртгэлээ');
        this.setState({
          createModalVisible: false,
        });

        this.handleSearchFormReset();
      } else {
        message.error(`Хэрэглэгчийн төрөл бүртгэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Хэрэглэгчийн төрөл бүртгэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  showEditForm = record => {
    this.setState({
      editFormValues: record,
    });
    this.handleEditModalVisible(true);
  };

  handleEditModalVisible = flag => {
    this.setState({
      editModalVisible: !!flag,
    });
  };

  handleEdit = fields => {
    const { businessRoleStore } = this.props;
    const { editFormValues } = this.state;

    const payload = {
      key: editFormValues.key,
      role: fields.role,
      applicationRoles: fields.applicationRoles,
    };

    businessRoleStore.update(payload).then(response => {
      if (response.result) {
        message.success('Хэрэглэгчийн төрлийг амжилттай шинэчиллээ');
        this.setState({
          editModalVisible: false,
        });

        this.handleSearchFormReset();
      } else {
        message.error(`Хэрэглэгчийн төрөл шинэчлэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Хэрэглэгчийн төрөл шинэчлэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  renderFilterForm() {
    const { form, applicationRoleStore } = this.props;
    const { getFieldDecorator } = form;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="Хэрэглэгчийн төрөл">
              {getFieldDecorator('role')(<Input placeholder="Хэрэглэгчийн төрөл" />)}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="Хандах эрхүүд">
              {getFieldDecorator('applicationRoles', { initialValue: '' })(
                <Select style={{ width: '100%' }}>
                  <Option key="" value="">
                    Бүгд
                  </Option>
                  {applicationRoleStore != null ?
                    applicationRoleStore.list.map(appRole => <Option key={appRole} value={appRole}>{appRole}</Option>) : ''}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit">
                &nbsp;<Trans>common.action.search</Trans>
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleSearchFormReset}>
                &nbsp;<Trans>common.action.clear</Trans>
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }

  renderCreateForm() {
    const { createModalVisible } = this.state;

    return (
      <BusinessRoleCreateModal
        handleCreate={this.handleCreate}
        handleModalVisible={this.handleCreateModalVisible}
        modalVisible={createModalVisible}
      />
    );
  }

  renderEditForm() {
    const { editModalVisible, editFormValues } = this.state;

    return (
      <BusinessRoleEditModal
        handleUpdate={this.handleEdit}
        handleModalVisible={this.handleEditModalVisible}
        modalVisible={editModalVisible}
        editFormValues={editFormValues}
      />
    );
  }

  render() {
    const { businessRoleStore, loading } = this.props;
    const { selectedRows } = this.state;

    const columns = [
      {
        title: 'Хэрэглэгчийн төрөл',
        dataIndex: 'role',
      },
      {
        title: 'Хандах эрхүүд',
        dataIndex: 'applicationRoles',
        render: (text, record) => (
          <span>
            {record.applicationRoles != null
              ? record.applicationRoles.map(appRole => <Tag color="blue" key={`${record.role}_${appRole}`}>{appRole}</Tag>)
              : ''}
          </span>
        ),
      },
      {
        title: 'Үйлдэл',
        render: (text, record) => (
          <Fragment>
            <a key="edit" onClick={() => this.showEditForm(record)}>
              <Icon type="edit" />
            </a>
            <Divider type="vertical" />
            <a key="delete" onClick={() => this.showDeleteConfirm(record.key)}>
              <Icon type="delete" />
            </a>
          </Fragment>
        ),
      },
    ];

    const headerActions = (
      <Button icon='plus' type='primary' onClick={() => this.handleCreateModalVisible(true)}>
        Бүртгэх
      </Button>
    );

    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="delete">Сонгосонг устгах</Menu.Item>
      </Menu>
    );

    return (
      <PageHeaderWrapper title="Хэрэглэгчийн төрлүүд" action={headerActions}>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderFilterForm()}</div>
            <StandardTable
              loading={loading}
              columns={columns}
              data={businessRoleStore != null ? businessRoleStore.data : []}
              selectedRows={selectedRows}
              onSelectRow={this.handleSelectRows}
              onChange={this.handleTableChange}
            />
            <div className={styles.tableListOperator}>
              {selectedRows.length > 0 && (
                <span>
                  <Dropdown overlay={menu}>
                    <Button>
                      Үйлдлүүд <Icon type="down" />
                    </Button>
                  </Dropdown>
                </span>
              )}
            </div>
          </div>
        </Card>
        {this.renderCreateForm()}
        {this.renderEditForm()}
      </PageHeaderWrapper>
    );
  }
}
export default withStyles(styles)(BusinessRoleList);