import React, { Component } from 'react';
import { Modal, Form, Input, Select } from 'antd';
import { inject, observer } from 'mobx-react/index';

const FormItem = Form.Item;
const { Option } = Select;


@Form.create()
@inject('applicationRoleStore', 'businessRoleStore')
@observer
class BusinessRoleCreateModal extends Component {
  componentDidMount() {
    // const { applicationRoleStore } = this.props;
    // applicationRoleStore.fetchRoles();
  }

  submitHandle = () => {
    const { form, handleCreate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleCreate(fieldsValue);
    });
  };

  render() {
    const { form, applicationRoleStore, handleModalVisible, modalVisible } = this.props;
    const { getFieldDecorator } = form;

    return (
      <Modal
        title="Хэрэглэгчийн төрөл бүртгэх"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={() => handleModalVisible(false)}
        okText='Хадгалах'
        cancelText='Болих'
      >
        <Form>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Хэрэглэгчийн төрөл">
            {getFieldDecorator('role', {
              rules: [{ required: true, message: 'Хэрэглэгчийн төрөл бичнэ үү' }],
            })(<Input placeholder="Хэрэглэгчийн төрөл" />)}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Хандах эрх">
            {getFieldDecorator('applicationRoles', {
              initialValue: [],
            })(
              <Select
                mode="multiple"
                placeholder="Хандах эрх сонгоно уу"
                style={{ width: '100%' }}
              >
                {/* <Option key="" value="">
                  Сонгох
                </Option> */}
                {applicationRoleStore != null ?
                  applicationRoleStore.list.map(appRole => <Option key={appRole} value={appRole}>{appRole}</Option>) : ''}
              </Select>
            )}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}
export default (BusinessRoleCreateModal);