import React, { Component } from 'react';
import { Row, Col, Button, Card, Steps, Icon, Form, Input, Checkbox, Radio, Select, Upload, DatePicker, TimePicker } from 'antd';
import { inject, observer } from 'mobx-react';
import withStyles from 'isomorphic-style-loader/withStyles';

import styles from './StepsPreview.less';

const { Step } = Steps;
const FormItem = Form.Item;
const { TextArea } = Input;
const Option = Select.Option;
const CheckboxGroup = Checkbox.Group;
const RadioGroup = Radio.Group;

@Form.create()
@inject('serviceFormStore', 'referenceStore', 'enumStore')
@observer
class StepsPreview extends Component {

  state = {
    currentStepKey: null,
  };

  componentDidMount() {
    const { serviceFormStore } = this.props;

    let currentStepKey = null;
    if (serviceFormStore.getStepArray().length > 0) {
      currentStepKey = serviceFormStore.getStepArray()[0].key;
    }
    this.setState({ currentStepKey });
  }

  onChangeStep = (currentStepKey) => {
    this.setState({ currentStepKey });
  };

  renderFormItem = (formItem) => {
    const { referenceStore, enumStore } = this.props;

    switch(formItem.itemType) {
      case 'TEXT':
        // TODO
        break;
      case 'INPUT':
        return <Input placeholder={ formItem.label } />;
      case 'TEXTAREA':
        return <TextArea placeholder={ formItem.label } rows={ formItem.rows } />;
      case 'SELECT':
        if (formItem.selectValueType) {
          switch(formItem.selectValueType) {
            case 'REFERENCE':
              return <Select mode={ formItem.multiple && 'multiple' }>
                {
                  referenceStore.referenceList.map((referenceData, i) => {
                    return <Option key={ `option-${referenceData.id}` } value={ referenceData.id }>{ referenceData.name }</Option>
                  })
                }
              </Select>;
            case 'ENUM':
              return <Select mode={ formItem.multiple && 'multiple' }>
                {
                  enumStore.enumList.map((enumData, i) => {
                    return <Option key={ `option-${enumData.value}` } value={ enumData.value }>{ enumData.name }</Option>
                  })
                }
              </Select>;
          }
        } else {
          return <Select mode={ formItem.multiple && 'multiple' } />
        }
        break;
      case 'CHECKBOX':
        if (formItem.selectValueType) {
          switch(formItem.selectValueType) {
            case 'REFERENCE':
              return <CheckboxGroup>
                {
                  referenceStore.referenceList.map((referenceData, i) => {
                    return <Checkbox key={ `option-${referenceData.id}` } value={ referenceData.id }>{ referenceData.name }</Checkbox>
                  })
                }
              </CheckboxGroup>;
            case 'ENUM':
              return <CheckboxGroup>
                {
                  enumStore.enumList.map((enumData, i) => {
                    return <Checkbox key={ `option-${enumData.value}` } value={ enumData.value }>{ enumData.name }</Checkbox>
                  })
                }
              </CheckboxGroup>;
          }
        } else {
          return <CheckboxGroup />
        }
        break;
      case 'RADIO_BUTTON':
        if (formItem.selectValueType) {
          switch(formItem.selectValueType) {
            case 'REFERENCE':
              return <RadioGroup>
                {
                  referenceStore.referenceList.map((referenceData, i) => {
                    return <Radio key={ `option-${referenceData.id}` } value={ referenceData.id }>{ referenceData.name }</Radio>
                  })
                }
              </RadioGroup>;
            case 'ENUM':
              return <RadioGroup>
                {
                  enumStore.enumList.map((enumData, i) => {
                    return <Radio key={ `option-${enumData.value}` } value={ enumData.value }>{ enumData.name }</Radio>
                  })
                }
              </RadioGroup>;
          }
        } else {
          return <RadioGroup />
        }
        break;
      case 'FILE_UPLOAD':
        return <Upload
          name={ formItem.field }
          //accept="image/*"
          //beforeUpload={beforeUpload}
          //onChange={this.onFileUploadChange}
          //headers={{'X-Auth-Token': authStore.values.token}}
          //data={{'entity': 'rekognition', 'entityId': Math.random().toString(36).substring(2)}}
          //action={getImageServerUrl() + '/upload'}
          //showUploadList={false}
        >
          <Button block>
            <Icon type='upload' /> Энд дарж оруулна уу
          </Button>
        </Upload>;
      case 'DATE_PICKER':
        return <DatePicker placeholder={ formItem.label } />;
      case 'TIME_PICKER':
        return <TimePicker placeholder={ formItem.label } />;
      case 'DATE_TIME_PICKER':
        return <DatePicker placeholder={ formItem.label } showTime={ true } />;
      case 'TABLE':
        // TODO
        break;
      default:
        return <p>Invalid form item</p>;
    }
  };

  renderFormItemContainer = (formItem) => {
    const { form } = this.props;
    const { getFieldDecorator } = form;

    if (formItem.field) {
      return <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 18 }} label={ formItem.label }>
        {getFieldDecorator(
          formItem.field,
          {rules: [{ required: formItem.required, message: `${formItem.label} бичнэ үү` }]}
        )(this.renderFormItem(formItem))}
      </FormItem>
    } else {
      return <p>Талбарын нэр бичнэ үү</p>
    }
  };

  render() {
    const { serviceFormStore } = this.props;
    const { currentStepKey } = this.state;

    let stepArray = serviceFormStore.getStepArray();
    // calc currentStep
    let currentStep = 0;
    if (currentStepKey) {
      for (let step of stepArray) {
        if (currentStepKey === step.key) {
          break;
        } else {
          currentStep++;
        }
      }
    }

    return <div>
      <Row gutter={16}>
        <Col span={6} />
        <Col span={12}>
          <Steps type='navigation' size='default' current={ currentStep } className={ styles.steps }>
            {
              stepArray.map((step, i) => {
                return <Step
                  title={ step.name }
                  onClick={ () => this.onChangeStep(step.key) }
                  className={ styles.step }
                  key={ step.key }
                />
              })
            }
          </Steps>
          {
            stepArray.map((step, i) => {
              return <Card
                key={ `card-${step.key}` }
                style={ { width: '100%', display: currentStepKey !== step.key && 'none' } }
              >
                <Form>
                  {
                    step.formItems.length > 0 &&
                    step.formItems.map((formItem, i) => {
                      return <div key={ `form-item-container-${i}` }>
                        {this.renderFormItemContainer(formItem)}
                      </div>
                    })
                  }
                </Form>
              </Card>
            })
          }
        </Col>
        <Col span={6} />
      </Row>
    </div>
  }
}

export default withStyles(styles)(StepsPreview);
