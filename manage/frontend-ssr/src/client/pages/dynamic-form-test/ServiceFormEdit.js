import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Tabs, Card, Icon } from 'antd';

import PageHeaderWrapper from '../../components/PageHeaderWrapper';
import StepsBuilder from './StepsBuilder';
import StepsPreview from './StepsPreview';

const { TabPane } = Tabs;

@inject('serviceFormStore')
@observer
class ServiceFormEdit extends Component {

    componentDidMount() {
        const { serviceFormStore } = this.props;

        // clear current value
        serviceFormStore.setStepCount(0);
        serviceFormStore.setSteps({});
    }

    render() {
        return <PageHeaderWrapper title={ 'Dynamic form builder' }>
            <Card>
                <Tabs defaultActiveKey='builder'>
                    <TabPane key='builder' tab={ <span><Icon type='form'/> Builder</span> }>
                        <StepsBuilder />
                    </TabPane>
                    <TabPane
                        key='preview'
                        tab={ <span><Icon type='eye'/> Preview</span> }
                        //forceRender={ true }
                    >
                        <StepsPreview />
                    </TabPane>
                </Tabs>
            </Card>
        </PageHeaderWrapper>
    }
}

export default ServiceFormEdit;
