import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { Row, Col, Button, Card, Steps } from 'antd';
import withStyles from 'isomorphic-style-loader/withStyles';
import HTML5Backend from 'react-dnd-html5-backend-cjs';
import { DndProvider } from 'react-dnd-cjs';
import update from 'immutability-helper';

import DndCard from '../../components/DragAndDrop/Card';
import DropTarget from '../../components/DragAndDrop/DropTarget';
import DynamicFormElements from '../../components/DynamicForm/DynamicFormElements'
import DynamicFormInput from '../../components/DynamicForm/DynamicFormInput';
import DynamicFormTextArea from '../../components/DynamicForm/DynamicFormTextArea';
import DynamicFormSelect from '../../components/DynamicForm/DynamicFormSelect';
import DynamicFormCheckBox from '../../components/DynamicForm/DynamicFormCheckBox';
import DynamicFormRadioButton from '../../components/DynamicForm/DynamicFormRadioButton';
import DynamicFormFileUpload from '../../components/DynamicForm/DynamicFormFileUpload';
import DynamicFormDatePicker from '../../components/DynamicForm/DynamicFormDatePicker';
import DynamicFormTimePicker from '../../components/DynamicForm/DynamicFormTimePicker';
import DynamicFormDateTimePicker from '../../components/DynamicForm/DynamicFormDateTimePicker';

import StepCreateModal from './StepCreateModal';
import styles from './StepsBuilder.less';

const { Step } = Steps;

@inject('serviceFormStore')
@observer
class StepsBuilder extends Component {

  state = {
    addStepModalVisible: false,
    currentStepKey: null,
  };

  handleAddStepModalVisible = (visible) => {
    this.setState({
      addStepModalVisible: !!visible,
    });
  };

  handleAddStep = (fields) => {
    const { serviceFormStore } = this.props;

    const stepKey = `step-${serviceFormStore.stepCount}`;

    serviceFormStore.addStep({
      key: stepKey,
      name: fields.stepName,
      formItems: [],
    });
    this.setState({ currentStepKey: stepKey });

    this.handleAddStepModalVisible(false);
  };

  onChangeStep = (currentStepKey) => {
    this.setState({ currentStepKey });
  };

  onDropFormItem = (itemType) => {
    const { serviceFormStore } = this.props;
    const { currentStepKey } = this.state;

    let currentStepData = serviceFormStore.steps[currentStepKey];
    let baseFormItem = {
      key: `form-item-${currentStepData.formItems.length + 1}`,
      itemType: itemType,
      label: '',
      field: '',
      required: false,
    };

    switch (itemType) {
      case 'TEXT':
        // TODO
        break;
      case 'INPUT':
      case 'DATE_PICKER':
      case 'TIME_PICKER':
      case 'DATE_TIME_PICKER':
        currentStepData.formItems.push(baseFormItem);
        break;
      case 'TEXTAREA':
        currentStepData.formItems.push(Object.assign(baseFormItem, {
          rows: 3
        }));
        break;
      case 'SELECT':
        currentStepData.formItems.push(Object.assign(baseFormItem, {
          multiple: false,
          selectValueType: null, // reference or enum
          selectValueName: '', // reference name or enum name
        }));
        break;
      case 'CHECKBOX':
      case 'RADIO_BUTTON':
        currentStepData.formItems.push(Object.assign(baseFormItem, {
          selectValueType: null, // reference or enum
          selectValueName: '', // reference name or enum name
        }));
        break;
      case 'FILE_UPLOAD':
        currentStepData.formItems.push(Object.assign(baseFormItem, {
          extensions: ['DOC', 'PDF', 'PIC'],
          maxSize: 10,
        }));
        break;
      case 'TABLE':
        // TODO
        break;
    }
  };

  handleFormItemMove = (dragIndex, hoverIndex) => {
    const { serviceFormStore } = this.props;
    const { currentStepKey } = this.state;

    let currentStepData = serviceFormStore.steps[currentStepKey];
    const dragFormItem = currentStepData.formItems[dragIndex];
    currentStepData.formItems = update(currentStepData.formItems, {
      $splice: [[dragIndex, 1], [hoverIndex, 0, dragFormItem]],
    });
  };

  handleFormItemRemove = (formItemKey) => {
    const { serviceFormStore } = this.props;
    const { currentStepKey } = this.state;

    let currentStepData = serviceFormStore.steps[currentStepKey];
    for (let i = 0; i < currentStepData.formItems.length; i++) {
      if (currentStepData.formItems[i].key === formItemKey) {
        currentStepData.formItems.splice(i, 1);
        break;
      }
    }
  };

  render() {
    const { serviceFormStore } = this.props;
    const { addStepModalVisible, currentStepKey } = this.state;

    let stepArray = serviceFormStore.getStepArray();

    // calc currentStep
    let currentStep = 0;
    for (let step of stepArray) {
      if (currentStepKey === step.key) {
        break;
      } else {
        currentStep++;
      }
    }

    return <Card>
      <Row gutter={16}>
        <Col span={21}>
          <Row gutter={16}>
            <Col span={2}>
              Алхам нэмэх:
              <Button
                type='primary'
                icon='plus'
                size='small'
                onClick={ () => this.handleAddStepModalVisible(true) }
              />
            </Col>
            <Col span={22}>
              <Steps type='navigation' size='default' current={ currentStep } className={ styles.steps }>
                {
                  stepArray.map((step, i) => {
                    return <Step
                      title={ step.name }
                      onClick={ () => this.onChangeStep(step.key) }
                      className={ styles.step }
                      key={ step.key }
                    />
                  })
                }
              </Steps>
            </Col>
          </Row>
          {
            stepArray.map((step, i) => {
              return <Card
                  key={ `card-${step.key}` }
                  style={ { width: '100%', display: currentStepKey !== step.key && 'none' } }
                >
                <DndProvider backend={HTML5Backend} key={`dnd-provider-${i}`}>
                  <DropTarget
                      accept={ ['TEXT', 'INPUT', 'SELECT', 'RADIO_BUTTON', 'CHECKBOX', 'TEXTAREA', 'FILE_UPLOAD',
                        'DATE_PICKER', 'TIME_PICKER', 'DATE_TIME_PICKER', 'TABLE'] }
                      onDrop={ (itemType) => this.onDropFormItem(itemType) }
                  >
                    {
                      step.formItems.length === 0 &&
                      <p>Форм элемэнт оруулна уу</p>
                    }
                    {
                      step.formItems.length > 0 &&
                      step.formItems.map((formItem, i) => {
                        const cardId = `form-item-container-${i}`;

                        let formElement = null;
                        switch(formItem.itemType) {
                          case 'INPUT':
                            formElement = <DynamicFormInput
                                key={ formItem.key }
                                formItem={ formItem }
                                handleRemove={ () => this.handleFormItemRemove(formItem.key) }
                                //onDragStart={ null }
                            />;
                            break;
                          case 'TEXTAREA':
                            formElement = <DynamicFormTextArea
                                key={ formItem.key }
                                formItem={ formItem }
                                handleRemove={ () => this.handleFormItemRemove(formItem.key) }
                                //onDragStart={ null }
                            />;
                            break;
                          case 'SELECT':
                            formElement = <DynamicFormSelect
                                key={ formItem.key }
                                formItem={ formItem }
                                handleRemove={ () => this.handleFormItemRemove(formItem.key) }
                                //onDragStart={ null }
                            />;
                            break;
                          case 'CHECKBOX':
                            formElement = <DynamicFormCheckBox
                                key={ formItem.key }
                                formItem={ formItem }
                                handleRemove={ () => this.handleFormItemRemove(formItem.key) }
                                //onDragStart={ null }
                            />;
                            break;
                          case 'RADIO_BUTTON':
                            formElement = <DynamicFormRadioButton
                                key={ formItem.key }
                                formItem={ formItem }
                                handleRemove={ () => this.handleFormItemRemove(formItem.key) }
                                //onDragStart={ null }
                            />;
                            break;
                          case 'FILE_UPLOAD':
                            formElement = <DynamicFormFileUpload
                                key={ formItem.key }
                                formItem={ formItem }
                                handleRemove={ () => this.handleFormItemRemove(formItem.key) }
                                //onDragStart={ null }
                            />;
                            break;
                          case 'DATE_PICKER':
                            formElement = <DynamicFormDatePicker
                                key={ formItem.key }
                                formItem={ formItem }
                                handleRemove={ () => this.handleFormItemRemove(formItem.key) }
                                //onDragStart={ null }
                            />;
                            break;
                          case 'TIME_PICKER':
                            formElement = <DynamicFormTimePicker
                                key={ formItem.key }
                                formItem={ formItem }
                                handleRemove={ () => this.handleFormItemRemove(formItem.key) }
                                //onDragStart={ null }
                            />;
                            break;
                          case 'DATE_TIME_PICKER':
                            formElement = <DynamicFormDateTimePicker
                                key={ formItem.key }
                                formItem={ formItem }
                                handleRemove={ () => this.handleFormItemRemove(formItem.key) }
                                //onDragStart={ null }
                            />;
                            break;
                        }

                        return <DndCard
                            key={cardId}
                            index={ i }
                            id={cardId}
                            element={formElement}
                            moveCard={this.handleFormItemMove}
                        />
                      })
                    }
                  </DropTarget>
                </DndProvider>
              </Card>
            })
          }
        </Col>
        <Col span={3}>
          <DynamicFormElements />
        </Col>
      </Row>
      <StepCreateModal
        modalVisible={ addStepModalVisible }
        handleModalVisible={ this.handleAddStepModalVisible }
        handleAdd={ this.handleAddStep }
      />
    </Card>
  }
}

export default withStyles(styles)(StepsBuilder);
