import React, { useState } from 'react';
import update from 'immutability-helper';
import { DndProvider } from 'react-dnd-cjs'
import HTML5Backend from 'react-dnd-html5-backend-cjs';
import { Input } from 'antd';

import Card from '../../components/DragAndDrop/Card';

const style = {
    width: 400,
};

const Dnd = () => {
    {
        const [cards, setCards] = useState([
            {
                id: 1,
                element: 'Write a cool JS library',
            },
            {
                id: 2,
                element: 'Make it generic enough',
            },
            {
                id: 3,
                element: 'Write README',
            },
            {
                id: 4,
                element: 'Create some examples',
            },
            {
                id: 5,
                element:
                    'Spam in Twitter and IRC to promote it (note that this element is taller than the others)',
            },
            {
                id: 6,
                element: <Input name='label' />,
            },
            {
                id: 7,
                element: 'PROFIT',
            },
        ]);
        const moveCard = (dragIndex, hoverIndex) => {
            const dragCard = cards[dragIndex];
            setCards(
                update(cards, {
                    $splice: [[dragIndex, 1], [hoverIndex, 0, dragCard]],
                }),
            )
        };
        return (
            <div style={style}>
                <DndProvider backend={HTML5Backend}>
                    {cards.map((card, i) => (
                        <Card
                            key={card.id}
                            index={i}
                            id={card.id}
                            element={card.element}
                            moveCard={moveCard}
                        />
                    ))}
                </DndProvider>
            </div>
        )
    }
};

export default Dnd;
