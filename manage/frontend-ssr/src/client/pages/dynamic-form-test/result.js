import React, { Component } from 'react';
import { Row, Col, Button, Skeleton } from 'antd';
import withStyles from 'isomorphic-style-loader/withStyles';
import DynamicFormTable from '../../components/DynamicForm/DynamicFormTable';

import styles from './result.less';

class DynamicFormResult extends Component {

    state = {
        currentDragItem: null,
        reportItems: [],
    };

    onDragStart(evt, blockType) {
        this.setState({ currentDragItem: blockType });
    };

    onDragOver(evt) {
        evt.preventDefault();
    };

    onDrop(evt) {
        const { currentDragItem, reportItems } = this.state;
        reportItems.push({
            blockType: currentDragItem
        });
        this.setState({ reportItems: reportItems });
    };

    render() {
        const {reportItems} = this.state;

        return (
            <div>
                <h2>Dynamic Form Result</h2>
                <Row gutter={16}>
                    <Col span={20}>
                        <div
                            onDragOver={ (event) => this.onDragOver(event) }
                            onDrop={ (event) => this.onDrop(event) }
                            style={ { width: '100%', height: 300 } }
                        >
                            {
                                reportItems.map((reportItem, i) => {
                                    return <div key={ `block-container-${i}` }>
                                        {reportItem.blockType === 'title' &&
                                            <input key={ `title-${i}` } style={ { width: '100%' } } placeholder='Гарчиг'/>
                                        }
                                        {reportItem.blockType === 'sentence' &&
                                            <textarea key={ `sentence-${i}` } style={ { width: '100%' } } placeholder='Өгүүлбэр' />
                                        }
                                        {reportItem.blockType === 'table' &&
                                            <DynamicFormTable key={ `table-${i}` } />
                                        }
                                        {reportItem.blockType === 'gap' &&
                                            <div key={ `gap-${i}` } style={ { height: 10, background: 'gray' } } />
                                        }
                                    </div>
                                })
                            }
                        </div>
                    </Col>
                    <Col span={4}>
                        <Button
                            type='default'
                            icon='download'
                            size='large'
                            draggable={ true }
                            onDragStart={ (event) => this.onDragStart(event, 'title') }
                        >
                            Гарчиг
                        </Button>
                        <Button
                            type='default'
                            icon='download'
                            size='large'
                            draggable={ true }
                            onDragStart={ (event) => this.onDragStart(event, 'sentence') }
                        >
                            Өгүүлбэр
                        </Button>
                        <Button
                            type='default'
                            icon='download'
                            size='large'
                            draggable={ true }
                            onDragStart={ (event) => this.onDragStart(event, 'table') }
                        >
                            Хүснэгт
                        </Button>
                        <Button
                            type='default'
                            icon='download'
                            size='large'
                            draggable={ true }
                            onDragStart={ (event) => this.onDragStart(event, 'gap') }
                        >
                            Зай
                        </Button>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default withStyles(styles)(DynamicFormResult);
