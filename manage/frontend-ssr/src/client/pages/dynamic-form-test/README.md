Form items:
- input
  - int (min, max)
  - string (mask)
- textarea (rows)
- select (multiple true|false)
  - static values
  - reference
  - enum
- radio
  - static values
  - reference
  - enum
- checkbox
  - static values
  - reference
  - enum
- file upload
- date picker (format)
- time picker (format)
- datetime picker (format)

Common attributes:
- required (true|false)
- name
