import React, { Component } from 'react';
import { Modal, Form, Input } from 'antd';
import withStyles from 'isomorphic-style-loader/withStyles';

import styles from './StepCreateModal.less';

const FormItem = Form.Item;

@Form.create()
class StepCreateModal extends Component {

    submitHandle = () => {
        const { form, handleAdd } = this.props;

        form.validateFields((err, fieldsValue) => {
            if (err) return;
            form.resetFields();
            handleAdd(fieldsValue);
        });
    };

    render() {
        const { form, modalVisible, handleModalVisible } = this.props;
        const { getFieldDecorator } = form;

        return <Modal
            title='Алхам нэмэх'
            visible={ modalVisible }
            onCancel={ () => handleModalVisible(false) }
            onOk={ () => this.submitHandle() }
        >
            <Form>
                <FormItem labelCol={{ span: 6 }} wrapperCol={{ span: 15 }} label='Алхамын нэр'>
                    {getFieldDecorator('stepName', {
                        rules: [{ required: true, message: 'Алхамын нэр бичнэ үү' }],
                    })(<Input placeholder='Алхамын нэр' />)}
                </FormItem>
            </Form>
        </Modal>
    }
}

export default withStyles(styles)(StepCreateModal);
