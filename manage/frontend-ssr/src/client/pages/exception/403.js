import React from 'react';
import { Link } from 'react-router-dom';
import Exception from '../../components/Exception';

const Exception403 = () => (
  <Exception
    type="403"
    desc='Хандах эрхгүй байна'
    linkElement={Link}
    backText='Буцах'
  />
);

export default Exception403;
