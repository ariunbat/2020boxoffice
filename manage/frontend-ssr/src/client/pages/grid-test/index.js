import React, { Component } from 'react';
import { Row, Col } from 'antd';
import withStyles from 'isomorphic-style-loader/withStyles';

import styles from './index.less';

class GridTest extends Component {

  render() {
    return (
      <div>
        <h2>Grid Test</h2>
          <Row gutter={16}>
              <Col span={6}>12</Col>
              <Col span={6}>12</Col>
          </Row>
      </div>
    )
  }
}

export default withStyles(styles)(GridTest);
