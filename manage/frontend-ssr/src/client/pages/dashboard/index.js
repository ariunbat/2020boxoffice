import React, { Component } from 'react';
import { ResponsiveBar } from '@nivo/bar'
import { Col, Row } from 'antd';

class Dashboard extends Component {
  componentDidMount() {
    // const { route } = this.props;
    // console.log(route.authority);
  }

  render() {

    const data = [
      {
        'country': 'AD',
        'hot dog': 148,
        'hot dogColor': 'hsl(359,100%,43%)',
        'burger': 4,
        'burgerColor': 'hsl(225,100%,43%)',
        'sandwich': 64,
        'sandwichColor': 'hsl(357,100%,21%)',
        'kebab': 35,
        'kebabColor': 'hsl(294, 70%, 50%)',
      },
      {
        'country': 'AE',
        'hot dog': 135,
        'hot dogColor': 'hsl(359,100%,43%)',
        'burger': 66,
        'burgerColor': 'hsl(225,100%,43%)',
        'sandwich': 88,
        'sandwichColor': 'hsl(357,100%,21%)',
        'kebab': 65,
        'kebabColor': 'hsl(296, 70%, 50%)',
      },
      {
        'country': 'AF',
        'hot dog': 0,
        'hot dogColor': 'hsl(359,100%,43%)',
        'burger': 53,
        'burgerColor': 'hsl(225,100%,43%)',
        'sandwich': 75,
        'sandwichColor': 'hsl(357,100%,21%)',
        'kebab': 51,
        'kebabColor': 'hsl(296, 70%, 50%)',
      },
      {
        'country': 'AG',
        'hot dog': 107,
        'hot dogColor': 'hsl(359,100%,43%)',
        'burger': 68,
        'burgerColor': 'hsl(225,100%,43%)',
        'sandwich': 70,
        'sandwichColor': 'hsl(357,100%,21%)',
        'kebab': 67,
        'kebabColor': 'hsl(296, 70%, 50%)',
      },
      {
        'country': 'AI',
        'hot dog': 63,
        'hot dogColor': 'hsl(359,100%,43%)',
        'burger': 184,
        'burgerColor': 'hsl(225,100%,43%)',
        'sandwich': 61,
        'sandwichColor': 'hsl(357,100%,21%)',
        'kebab': 24,
        'kebabColor': 'hsl(296, 70%, 50%)',
      },
      {
        'country': 'AL',
        'hot dog': 167,
        'hot dogColor': 'hsl(359,100%,43%)',
        'burger': 65,
        'burgerColor': 'hsl(225,100%,43%)',
        'sandwich': 39,
        'sandwichColor': 'hsl(357,100%,21%)',
        'kebab': 76,
        'kebabColor': 'hsl(296, 70%, 50%)',
      },
      {
        'country': 'AM',
        'hot dog': 133,
        'hot dogColor': 'hsl(359,100%,43%)',
        'burger': 117,
        'burgerColor': 'hsl(225,100%,43%)',
        'sandwich': 23,
        'sandwichColor': 'hsl(357,100%,21%)',
        'kebab': 112,
        'kebabColor': 'hsl(296, 70%, 50%)',
      }
    ];

    return (
          <ResponsiveBar
            stackOffsetDiverging
            data={data}
            height={500}
            keys={['hot dog', 'burger', 'sandwich', 'kebab']}
            indexBy="country"
            margin={{ top: 50, right: 130, bottom: 50, left: 60 }}
            padding={0.25}
            groupMode="grouped"
            enableLabel={false}
            axisTop={null}
            axisRight={null}
            axisBottom={{
              tickSize: 5,
              tickPadding: 5,
              tickRotation: 0,
              legendPosition: 'middle',
              legendOffset: 32
            }}
            colors={{ scheme: 'nivo' }}
            animate={true}
            motionStiffness={90}
            motionDamping={15}
          />
    );
  }
}

export default Dashboard;
