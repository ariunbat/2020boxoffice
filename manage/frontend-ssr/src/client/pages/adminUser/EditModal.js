import React, { Component } from 'react';
import { Modal, Form, Input, Select, Upload, Icon } from 'antd';
import { inject, observer } from 'mobx-react/index';
import { getImageServerUrl } from '../../../common/services/cdn';
import { checkRegistry } from '../../../common/utils/utils';
import TagGroup from '../../components/TagGroup';
import { getLocationByAll } from '../../../common/utils/location';

const FormItem = Form.Item;
const { Option } = Select;
const { confirm } = Modal;

@Form.create()
@inject('authStore', 'locationStore')
@observer
class UserEditModal extends Component {

  state = {
    phone: [],
  };

  componentDidMount() {
    const { locationStore } = this.props;
    locationStore.allList({'used': false}).then(response => {
      if (response.result === true && response.data) {
        this.locationData = getLocationByAll(locationStore.allLocation);
      }
    });
  }

  normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  setPhone = list => {
    const { form } = this.props;
    const { phone } = this.state;

    if (phone && list) {
      this.setState({
          phone: list,
          phoneValidate: null,
        },
        form.setFields({ 'phone': { value: list } })
      );

    }
  };

  checkPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value) {
      form.validateFields(['confirmPassword'], { force: true });
    }
    callback();
  };

  checkConfirmPassword = (rule, value, callback) => {
    const { form } = this.props;
    const password = form.getFieldValue('password');
    if (value !== password) {
      callback('Нууц үгээ адилхан бичнэ үү');
    } else {
      callback();
    }
  };

  submitHandle = () => {
    const { form, handleUpdate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { avatar, ...rest } = fieldsValue;
      let img;

      if (avatar && avatar.length !== 0) {
        avatar.map(item => {
          if (item.response && item.response.result) {
            img = {
              uid: item.uid,
              name: item.name,
              url: item.response.data.url
            };
          }
          else {
            img = item;
          }
        })
      }
      form.resetFields();
      handleUpdate({ ...rest, avatar: img, });
    });
  };

  backHandle = () => {
    const { handleModalVisible, form } = this.props;
    form.resetFields();
    handleModalVisible(false);
  };

  showConfirm = () => {
    const { form } = this.props;
    const parentMethods = { backHandle: this.backHandle };
    if (form.isFieldsTouched()) {
      confirm({
        title: 'Та гарахдаа итгэлтэй байна уу?',
        content: '',
        okText: 'Тийм',
        okType: 'danger',
        cancelText: 'Үгүй',
        onOk() {
          parentMethods.backHandle()
        },
        onCancel() {
        },
      });
    }
    else {
      parentMethods.backHandle()
    }
  };

  render() {
    const { form, modalVisible, editFormValues, authStore } = this.props;
    const { getFieldDecorator } = form;
    const uploadButton = (
      <Icon type="upload" />
    );

    return (
      <Modal
        title="Хэрэглэгчийн мэдээлэл засварлах"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={this.showConfirm}
      >
        <Form>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Нэвтрэх нэр">
            {getFieldDecorator('username', {
              initialValue: editFormValues && editFormValues.username,
              rules: [
                { required: true, message: 'Нэвтрэх нэр бичнэ үү' },
              ],
            })(<Input placeholder="Нэвтрэх нэр" />)}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Нууц үг">
            {getFieldDecorator('password', {
              rules: [
                { validator: this.checkPassword },
              ],
            })(<Input placeholder="Password" />)}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Нууц үг дахин бичих">
            {getFieldDecorator('confirmPassword', {
              rules: [
                { validator: this.checkConfirmPassword },
              ],
            })(<Input placeholder="Нууц үг дахин бичих" />)}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Овог">
            {getFieldDecorator('lastName', {
              initialValue: editFormValues && editFormValues.lastName,
            })(<Input placeholder="Овог" />)}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Нэр">
            {getFieldDecorator('firstName', {
              initialValue: editFormValues && editFormValues.firstName,
              rules: [{ required: true, message: 'Нэрээ бичнэ үү' }],
            })(<Input placeholder="Нэр" />)}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Регистрийн дугаар">
            {getFieldDecorator('registerNumber', {
              initialValue: editFormValues && editFormValues.registerNumber,
              rules: [
                { validator: (rule, control, callback) => checkRegistry(rule, control, callback) },
              ],
            })(
              <Input placeholder="Регистрийн дугаар" />)
            }
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Имэйл">
            {getFieldDecorator('email', {
              initialValue: editFormValues && editFormValues.email,
              rules: [{ type: 'email', message: 'Имэйл зөв үү' }],
            })(
              <Input placeholder="Имэйл" />)
            }
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Ургийн овог">
            {getFieldDecorator('familyName', {
              initialValue: editFormValues && editFormValues.familyName,
            })(
              <Input placeholder="Ургийн овог" />)
            }
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Утас">
            {getFieldDecorator('phone', {
              valuePropName: 'initialTags',
              initialValue: editFormValues && editFormValues.phone,
            })(
              <TagGroup
                setTagList={this.setPhone}
                inputWith={200}
                tagColor="gold"
              />
            )}
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Тайлбар">
            {getFieldDecorator('description', {
              initialValue: editFormValues && editFormValues.description,
            })(
              <Input.TextArea placeholder="Тайлбар" rows={4} />)
            }
          </FormItem>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Зураг">
            {getFieldDecorator('avatar', {
              valuePropName: 'fileList',
              getValueFromEvent: this.normFile,
              initialValue: editFormValues && editFormValues.photo && [editFormValues.photo]
            })(
              <Upload
                name="file"
                accept="image/*"
                listType="picture-card"
                headers={{ 'X-Auth-Token': authStore.values.token }}
                data={{ 'entity': 'userAvatar', 'entityId': Math.random().toString(36).substring(2) }}
                action={getImageServerUrl() + '/upload'}
                onPreview={this.handlePreview}
              >
                {form.getFieldValue('avatar') && form.getFieldValue('avatar').length !== 0 ? null : uploadButton}
              </Upload>)}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default UserEditModal;
