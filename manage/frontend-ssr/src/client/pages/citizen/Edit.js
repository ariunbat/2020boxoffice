import React, { Component } from 'react';
import { Modal, Form, Input, DatePicker, Select, Row, Col, Icon, Upload, Button, message, Card, Divider, Radio, AutoComplete, Cascader } from 'antd';
import { observer, inject } from 'mobx-react';
import { getImageServerUrl } from '../../../common/services/cdn';
import PageHeaderWrapper from '../../components/PageHeaderWrapper';
import { checkRegistry } from '../../../common/utils/utils';
import LocationField from '../Location';
import EsriMap from '../esri/MapLayerNew';
import moment from 'moment';
import { observable } from 'mobx';
import { apiFormat } from '../../../common/utils/dateFormat';
import Description from '../../components/DescriptionList/Description';

const { Option } = Select;
const FormItem = Form.Item;

@Form.create()
@inject('citizenStore', 'authStore', 'ageCalcDate', 'nameStore', 'locationStore')
@observer
class CitizenUpdate extends Component {

  @observable age = null;

  state = {
    previewVisible: false,
    preview: '',
    location: '',
    fullAddress: '',
    dataSource: [],
  };

  componentDidMount() {
    const { ageCalcDate, citizenStore, match: { params }, locationStore } = this.props;
    citizenStore.get(params.id);
    locationStore.tree();
    ageCalcDate.get();
  }

  normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  submitHandle = () => {
    const { form, citizenStore, history, match: { params } } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const { file, ...rest } = fieldsValue;
      let fileData;

      if (file && file.length !== 0) {
        file.map(item => {
          if (item.response && item.response.result) {
            fileData = {
              uid: item.uid,
              name: item.name,
              url: item.response.data.url
            }
          } else {
            fileData = item;
          }
        })
      }

      const payload = Object.assign(rest, { photo: fileData, id: params.id });

      citizenStore.update(payload).then(response => {
        if (response.result) {
          message.success('Амжилттай бүртгэлээ');
          history.push('/citizen');
        } else {
          message.error(`Бүртгэхэд алдаа гарлаа: ${response.message}`);
        }
      })
        .catch(e => {
          console.log(e);
          // message.error(`Хэрэглэгчийн төрөл бүртгэхэд алдаа гарлаа: ${e.message}`);
        });
    });
  };

  handlePreview = (file) => {
    this.setState({
      preview: file.response && file.response.data && file.response.data.url || file.url,
      previewVisible: true,
    });
  };

  handleCancelPreview = () => this.setState({ previewVisible: false });

  handleLocationChange = (value, selectedOptions) => {
    let location = '';
    selectedOptions.forEach(item => {
      location = location.concat(item.label + ', ')
    });
    this.setState({
      location,
    })
  };

  getAge = (e) => {
    e.preventDefault();
    const { ageCalcDate } = this.props;
    let year;
    let month;
    let day;
    const regNum = e.target.value;
    let birthDate;

    if (regNum.length === 10) {
      year = regNum.substring(2, 4);
      month = regNum.substring(4, 6);
      day = regNum.substring(6, 8);
      if (year.substring(0, 1) === '1' || year.substring(0, 1) === '0') {
        year = '20'.concat(year);
        month = month - 20;
      } else {
        year = '19'.concat(year);
      }
      birthDate = year + '-' + month.toString().padStart(2, '0') + '-' + day;
      this.age = moment(ageCalcDate && ageCalcDate.current).diff(birthDate, 'years');
    }
  };


  handleMapLoad(map, mapView) {
    this.setState({ map, mapView, mapLoadFinished: true }, () => {
      if (this.state.modulesFinished === true)
        this.initMap();
    });
    // this.initMap();
  }

  onChangeCoordinate = (x, y) => {
    console.log(x, y)
  };

  checkCitizenExists = (rule, value, callback) => {
    const { citizenStore } = this.props;
    if (value.length === 10 ) {
      citizenStore.checkExists({ registerNumber: value }).then(response => {
        if (response.result) {
          if (response.data) {
            callback('Регистрийн дугаар бүртгэлтэй байна');
          }
          return callback();
        }
        return callback();
      })
        .catch(e => {
          console.log(e);
          // message.error(`алдаа гарлаа: ${response.message}`);
        });
    } else {
      return callback();
    }
  };

  onSearch = (name, type) => {
    const { nameStore } = this.props;
    if(name.length > 0) {
      nameStore.list({ type, name }).then(response => {
        this.setState({
          dataSource: response.data && response.data.name || [],
        });
      })
    }
  };

  clearName = (value) => {
    this.setState({
      dataSource: [],
    })
  };

  render() {
    const { form, authStore, ageCalcDate, citizenStore, citizenStore: { loading }, locationStore } = this.props;
    const { getFieldDecorator } = form;
    const { previewVisible, preview, location, age, dataSource } = this.state;
    const formItemLayout = { labelCol: { span: 4 }, wrapperCol: { span: 6 } };

    const uploadButton = (
      <Icon type="upload" />
    );

    const currentCitizen = citizenStore.current;

    return (
      <PageHeaderWrapper title="Үндсэн жагсаалт руу нэмэх">
        <Card bordered={false} loading={loading}>
          <Form>
            <FormItem {...formItemLayout} label="Зураг">
              {getFieldDecorator('file', {
                valuePropName: 'fileList',
                getValueFromEvent: this.normFile,
                initialValue: currentCitizen.photo ? [currentCitizen.photo] : [],
              })(
                <Upload
                  name="file"
                  accept="image/*"
                  listType="picture-card"
                  headers={{ 'X-Auth-Token': authStore.values.token }}
                  data={{ 'entity': 'userAvatar', 'entityId': Math.random().toString(36).substring(2) }}
                  action={getImageServerUrl() + '/upload'}
                  onPreview={this.handlePreview}
                >
                  {form.getFieldValue('file') && form.getFieldValue('file').length !== 0 ? null : uploadButton}
                </Upload>)}
            </FormItem>
            <FormItem {...formItemLayout} label="Регистрийн дугаар">
              {getFieldDecorator('registerNumber', {
                initialValue: currentCitizen.registerNumber,
              })(
                <Input placeholder="Регистрийн дугаар" disabled />)
              }
            </FormItem>
            <FormItem {...formItemLayout} label="Ургийн овог">
              {getFieldDecorator('familyName', {
                initialValue: currentCitizen.familyName,
              })(
                <AutoComplete
                  dataSource={dataSource}
                  onSelect={this.clearName}
                  onSearch={(name) => this.onSearch(name, 'familyName')}
                  placeholder="Ургийн овог"
                  onBlur={this.clearName}
                />)
              }
            </FormItem>
            <FormItem {...formItemLayout} label="Эцэг /эх/-ийн нэр">
              {getFieldDecorator('lastName', {
                initialValue: currentCitizen.lastName,
                rules: [
                  { required: true, message: 'Эцэг /эх/-ийн нэр бичнэ үү' },
                ],
              })(
                <AutoComplete
                  dataSource={dataSource}
                  onSelect={this.clearName}
                  onSearch={(name) => this.onSearch(name, 'lastName')}
                  placeholder="Эцэг /эх/-ийн нэр"
                  onBlur={this.clearName}
                />)
              }
            </FormItem>
            <FormItem {...formItemLayout} label="Нэр">
              {getFieldDecorator('firstName', {
                initialValue: currentCitizen.firstName,
                rules: [
                  { required: true, message: 'Нэр бичнэ үү' },
                ],
              })(
                <AutoComplete
                  dataSource={dataSource}
                  onSelect={this.clearName}
                  onSearch={(name) => this.onSearch(name, 'firstName')}
                  placeholder="Нэр"
                  onBlur={this.clearName}
                />)
              }
            </FormItem>
            <FormItem {...formItemLayout} label="Хүйс">
              {getFieldDecorator('gender', {
                initialValue: currentCitizen.gender,
              })
              (
                <Radio.Group>
                  <Radio key="MALE" value='MALE'>Эр</Radio>
                  <Radio key="FEMALE" value='FEMALE'>Эм</Radio>
                </Radio.Group>
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="Нас">
              <span>{this.age ? this.age : Math.floor(currentCitizen.age)} ({(moment(ageCalcDate.current).format(apiFormat))}-ны байдлаар)</span>
            </FormItem>
            <Divider orientation='left'>Хаягын мэдээлэл</Divider>
            <FormItem {...formItemLayout} label="Хаягийн дэлгэрэнгүй">
              <span>
                {this.state.location || `${currentCitizen.cityName}, ${currentCitizen.districtName} ${currentCitizen.subDistrictName} `}

                {form.getFieldValue('addressType') === 'APARTMENT_DISTRICT' &&
                (form.getFieldValue('town') && `${form.getFieldValue('town')} `)}

                {form.getFieldValue('addressType') === 'APARTMENT_DISTRICT' &&
                (form.getFieldValue('apartment') && `${form.getFieldValue('apartment')} `)}

                {form.getFieldValue('addressType') === 'APARTMENT_DISTRICT' &&
                (form.getFieldValue('apartmentAddress') && `${form.getFieldValue('apartmentAddress')} `)}

                {form.getFieldValue('addressType') === 'GER_DISTRICT' &&
                (form.getFieldValue('street') && `${form.getFieldValue('street')} `)}

                {form.getFieldValue('addressType') === 'GER_DISTRICT' &&
                (form.getFieldValue('gerAddress') && `${form.getFieldValue('gerAddress')} `)}

                {form.getFieldValue('addressType') === 'COUNTRYSIDE' &&
                (form.getFieldValue('countrySideAddress') && `${form.getFieldValue('countrySideAddress')} `)}
              </span>
            </FormItem>
            <FormItem {...formItemLayout} label="Нутаг дэвсгэр">
              {getFieldDecorator('locationCode', {
                initialValue: currentCitizen.locationCode ? currentCitizen.locationCode : [],
                rules: [{ required: true, message: 'Нутаг дэвсгэр сонгоно уу' }],
              })(
                <Cascader
                  options={locationStore && locationStore.treeData}
                  onChange={this.handleLocationChange}
                  placeholder="Засаг захиргаа"
                />,
              )}
            </FormItem>
            <FormItem {...formItemLayout} label="Хаягын төрөл">
              {getFieldDecorator('addressType', {
                initialValue: currentCitizen.addressType,
              })
              (
                <Radio.Group>
                  <Radio key="APARTMENT_DISTRICT" value='APARTMENT_DISTRICT'>Байр хороолол</Radio>
                  <Radio key="GER_DISTRICT" value='GER_DISTRICT'>Гэр хороолол</Radio>
                  <Radio key="COUNTRYSIDE" value='COUNTRYSIDE'>Хөдөө</Radio>
                </Radio.Group>
              )}
            </FormItem>
            {
              form.getFieldValue('addressType') === 'APARTMENT_DISTRICT' &&
              <div>
                <FormItem {...formItemLayout} label="Хороолол/хотхон">
                  {getFieldDecorator('town', {
                    initialValue: currentCitizen.town,
                    rules: [
                      { required: true, message: 'Хороолол/хотхон оруулна уу' },
                    ],
                  })
                  (<AutoComplete
                    dataSource={dataSource}
                    onSelect={this.clearName}
                    onSearch={(name) => this.onSearch(name, 'town')}
                    placeholder="Хороолол/хотхон"
                    onBlur={this.clearName}
                  />)
                  }
                </FormItem>
                <FormItem {...formItemLayout} label="Байр">
                  {getFieldDecorator('apartment', {
                    initialValue: currentCitizen.apartment,
                    rules: [
                      { required: true, message: 'Байр оруулна уу' },
                    ],
                  })
                  (<Input placeholder="Байр" />)
                  }
                </FormItem>
                <FormItem {...formItemLayout} label="Хаягийн нэгж (орц, тоот)">
                  {getFieldDecorator('apartmentAddress', {
                    initialValue: currentCitizen.apartmentAddress,
                    rules: [
                      { required: true, message: 'Хаягийн нэгж (орц, тоот) оруулна уу' },
                    ],
                  })
                  (<Input placeholder="Хаягийн нэгж (орц, тоот)" />)
                  }
                </FormItem>
              </div>
            }
            {
              form.getFieldValue('addressType') === 'GER_DISTRICT' &&
              <div>
                <FormItem {...formItemLayout} label="Гудамж нэр">
                  {getFieldDecorator('street', {
                    initialValue: currentCitizen.street,
                    rules: [
                      { required: true, message: 'Гудамж нэр оруулна уу' },
                    ],
                  })
                  (<AutoComplete
                    dataSource={dataSource}
                    onSelect={this.clearName}
                    onSearch={(name) => this.onSearch(name, 'street')}
                    placeholder="Гудамж нэр"
                    onBlur={this.clearName}
                  />)
                  }
                </FormItem>
                <FormItem {...formItemLayout} label="Хаягийн нэгж (тоот)">
                  {getFieldDecorator('gerAddress', {
                    initialValue: currentCitizen.gerAddress,
                    rules: [
                      { required: true, message: 'Хаягийн нэгж (тоот) оруулна уу' },
                    ],
                  })
                  (<Input placeholder="Хаягийн нэгж (тоот)" />)
                  }
                </FormItem>
              </div>
            }
            {
              form.getFieldValue('addressType') === 'COUNTRYSIDE' &&
              <div>
                <FormItem {...formItemLayout} label="Хаягийн нэгж">
                  {getFieldDecorator('countrySideAddress', {
                    initialValue: currentCitizen.countrySideAddress,
                    rules: [
                      { required: true, message: 'Хаягийн нэгж оруулна уу' },
                    ],
                  })
                  (<AutoComplete
                    dataSource={dataSource}
                    onSelect={this.clearName}
                    onSearch={(name) => this.onSearch(name, 'countrySideAddress')}
                    placeholder="Гудамж нэр"
                    onBlur={this.clearName}
                  />)
                  }
                </FormItem>
              </div>
            }
            <Col span={24}>
              <FormItem label="Оршин суугаа албан ёсны хаяг">
                {getFieldDecorator('locationAttitude', {})(
                  <EsriMap
                    onChangeCoordinate={this.onChangeCoordinate}
                    initialCoordinate={currentCitizen.locationAttitude && {
                      x: currentCitizen.locationAttitude[0],
                      y: currentCitizen.locationAttitude[1],
                    }}
                  />
                )}
              </FormItem>
            </Col>
            <FormItem>
              <Button type='primary' onClick={() => this.submitHandle()} style={{ float: 'right' }}>
                Бүртгэх
              </Button>
            </FormItem>
          </Form>
          <Modal visible={previewVisible} footer={null} onCancel={this.handleCancelPreview}>
            <img src={preview} width='100%' />
          </Modal>
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default CitizenUpdate;
