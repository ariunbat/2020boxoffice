import React, { Component } from 'react';
import { Button, Cascader, Col, Form, Icon, Input, message, Modal, Row, Upload } from 'antd';
import { inject, observer } from 'mobx-react';
import { getImageServerUrl } from '../../../../common/services/cdn';
import { Link } from 'react-router-dom';
import TagGroup from '../../../components/TagGroup';
import FamilyMemberList from './familyMember/List';
import { getLocationByAll } from '../../../../common/utils/location';
import { observable } from 'mobx';

const FormItem = Form.Item;
const ButtonGroup = Button.Group;

@Form.create()
@inject('citizenStore', 'authStore', 'locationStore')
@observer
class MainInfo extends Component {

  @observable locationData= [];

  state = {
    phone: [],
    previewVisible: false,
    preview: '',
  };

  componentDidMount() {
    const { citizenStore, locationStore } = this.props;
    locationStore.allList().then(response => {
      if (response.result === true && response.data) {
        this.locationData = getLocationByAll(locationStore.allLocation);
      }
    });
  }

  setPhone = list => {
    const { form } = this.props;
    const { phone } = this.state;

    if (phone && list) {
      this.setState({
          phone: list,
          phoneValidate: null,
        },
        form.setFields({ 'phone': { value: list } })
      );

    }
  };

  normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  handleBack = () => {
    const { history } = this.props;
    history.push('/citizen');
  };

  submitHandle = () => {
    const { form, citizenStore, citizen } = this.props;

    form.validateFields((err, fieldsValue) => {

      const { file, ...rest } = fieldsValue;
      let fileData;

      if (file && file.length !== 0) {
        file.map(item => {
          if (item.response && item.response.result) {
            fileData = {
              uid: item.uid,
              name: item.name,
              url: item.response.data.url
            }
          } else {
            fileData = item;
          }
        })
      }

      const payload = Object.assign(rest, { photo: fileData, id: citizen.id });

      citizenStore.createMainInfo(payload).then(response => {
        if (response.result) {
          message.success('Амжилттай хадгаллаа');
        } else {
          message.error(`Хадгалахад алдаа гарлаа: ${response.message}`);
        }
      })
        .catch(e => {
          console.log(e);
          // message.error(`Хэрэглэгчийн төрөл бүртгэхэд алдаа гарлаа: ${e.message}`);
        });
    });
  };

  handlePreview = (file) => {
    this.setState({
      preview: file.response && file.response.data && file.response.data.url || file.url,
      previewVisible: true,
    });
  };

  handleCancelPreview = () => this.setState({ previewVisible: false });

  render() {
    const { citizen, form, authStore, locationStore, next } = this.props;
    const { previewVisible, preview } = this.state;
    const { getFieldDecorator } = form;

    const uploadButton = (
      <Icon type="upload" />
    );

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 6 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
      },
    };

    const tailFormItemLayout = {
      labelCol: { span: 6 },
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 10,
          offset: 14,
        },
      },
    };

    if (citizen) {
      return (
        <Row>
          <Form {...formItemLayout}>
            <Col span={4}>
              <FormItem label="Зураг">
                {getFieldDecorator('file', {
                  valuePropName: 'fileList',
                  getValueFromEvent: this.normFile,
                  initialValue: citizen.photo ? [citizen.photo] : []
                })(
                  <Upload
                    name="file"
                    accept="image/*"
                    listType="picture-card"
                    headers={{ 'X-Auth-Token': authStore.values.token }}
                    data={{ 'entity': 'userAvatar', 'entityId': Math.random().toString(36).substring(2) }}
                    action={getImageServerUrl() + '/upload'}
                    onPreview={this.handlePreview}
                  >
                    {form.getFieldValue('file') && form.getFieldValue('file').length !== 0 ? null : uploadButton}
                  </Upload>)}
              </FormItem>
            </Col>
            <Col span={14}>
              <FormItem label="Утас">
                {getFieldDecorator('phone', {
                  valuePropName: 'initialTags',
                  initialValue: citizen && citizen.phone,
                })(
                  <TagGroup
                    setTagList={this.setPhone}
                    inputWith={200}
                    tagColor="gold"
                  />
                )}
              </FormItem>
              <FormItem label="Имэйл">
                {getFieldDecorator('email', {
                  initialValue: citizen && citizen.email,
                  rules: [
                    { type: 'email', message: 'Имэйл зөв үү' },
                  ],
                })(
                  <Input placeholder="Имэйл" />)
                }
              </FormItem>
              <FormItem label="Сошиал хаяг">
                {getFieldDecorator('socialAccount', {
                  initialValue: citizen && citizen.socialAccount,
                })(
                  <Input placeholder="Сошиал хаяг" />)
                }
              </FormItem>
              <FormItem label="Мэргэжил">
                {getFieldDecorator('job', {
                  initialValue: citizen && citizen.job,
                })(
                  <Input placeholder="Мэргэжил" />)
                }
              </FormItem>
              <FormItem label="Төрсөн газар">
                <Input.Group compact>
                {getFieldDecorator('birthPlaceCode', {
                  initialValue: citizen && citizen.birthPlaceCode || [],
                })(
                  <Cascader
                    style={{ width: '60%'}}
                    options={this.locationData}
                    changeOnSelect
                    placeholder="Төрсөн газар"
                  />,
                )}
                {getFieldDecorator('birthPlaceAddress', {
                  initialValue: citizen && citizen.birthPlaceAddress,
                })(<Input style={{ width: '40%'}} />)
                }
                </Input.Group>
              </FormItem>
              <FormItem label="Оршин суугаа албан ёсны хаяг">
                {getFieldDecorator('officialAddressCode', {
                  initialValue: citizen && citizen.officialAddressCode  || [],
                })(
                  <Cascader
                    options={this.locationData}
                    placeholder="Оршин суугаа албан ёсны хаяг"
                  />,
                )}
              </FormItem>
            </Col>
            <Col span={24}>
              <FamilyMemberList citizenId={citizen.id} />
            </Col>
            <Col span={24}>
              <FormItem>
                <ButtonGroup style={{ float: 'right' }}>
                  <Button type='default' onClick={() => this.handleBack()}>
                    ЖАГСААЛТ РУУ БУЦАХ
                  </Button>
                  <Button type='primary' onClick={() => this.submitHandle()}>
                    ХАДГАЛАХ
                  </Button>
                  <Button type='primary' onClick={() => next('2')}>
                    ҮРГЭЖЛҮҮЛЭХ
                  </Button>
                </ButtonGroup>
              </FormItem>
            </Col>
          </Form>
          <Modal visible={previewVisible} footer={null} onCancel={this.handleCancelPreview}>
            <img src={preview} width='100%' />
          </Modal>
        </Row>
      );
    }
  }
}

export default MainInfo;