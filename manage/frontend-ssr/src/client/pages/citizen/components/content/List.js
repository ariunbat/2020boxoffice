import React, { Component, Fragment } from 'react';
import {
  Card,
  Form,
  Input,
  Icon,
  Button,
  Modal,
  Divider,
  message,
  Table, Tag,
} from 'antd';
import moment from 'moment';
import ContentCreateModal from './CreateModal';
import ContentEditModal from './EditModal';
import { inject, observer } from 'mobx-react/index';
import withStyles from 'isomorphic-style-loader/withStyles';
import styles from './List.less';
import { apiFormat } from '../../../../../common/utils/dateFormat';
import ReactPlayer from 'react-player'

const FormItem = Form.Item;
const Confirm = Modal.confirm;

function createMarkup(text) {
  return { __html: text };
}

@Form.create()
@inject('contentStore')
@observer
class ContentList extends Component {
  state = {
    previewVisible: false,
    preview: '',
    videoVisible: false,
    video: '',
    playing: false,
    createModalVisible: false,
    editModalVisible: false,
    editFormValues: [],
    searchFormValues: {},
  };

  componentDidMount() {
    this.refreshTable();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { citizenId, contentStore } = this.props;
    if (prevProps.citizenId !== citizenId) {
      contentStore.list({ citizenId });
    }
  }

  refreshTable = (params) => {
    const { contentStore, citizenId } = this.props;
    if (citizenId) {
      contentStore.list(Object.assign({ citizenId }, params));
    }
  };

  handleTableChange = (pagination) => {

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
    };

    this.refreshTable(params);
  };

  handleSearchFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      searchFormValues: {},
    });

    this.refreshTable();
  };

  showDeleteConfirm = clickedId => {
    const parentMethods = { handleDelete: this.handleDelete };

    Confirm({
      title: 'Анхааруулга',
      content: 'Сонгосон контент гишүүн устгах уу?',
      okText: 'Тийм',
      okType: 'danger',
      cancelText: 'Үгүй',
      onOk() {
        parentMethods.handleDelete(clickedId);
      },
    });
  };

  handleDelete = clickedId => {
    const { contentStore } = this.props;

    const payload = { id: clickedId };

    contentStore.delete(payload).then(response => {
      if (!response.result) {
        message.error(`Контент устгахад алдаа гарлаа: ${response.message}`);
      }
      this.refreshTable();
    })
      .catch(e => {
        message.error(`Контент устгахад алдаа гарлаа: ${e.message}`);
      });
  };

  handleSearch = e => {
    e.preventDefault();
    const { form } = this.props;

    this.setState({
      searchFormValues: form.getFieldsValue(),
    });

    this.refreshTable(form.getFieldsValue());
  };

  handleCreateModalVisible = flag => {
    this.setState({
      createModalVisible: !!flag,
    });
  };

  handleCreate = fields => {
    const { contentStore, citizenId } = this.props;

    const { images, video, ...rest } = fields;
    let imageData = [];
    let videoData = null;

    if (images && images.length !== 0) {
      images.map(item => {
        if (item.response && item.response.result) {
          imageData.push(
            {
              uid: item.uid,
              name: item.name,
              url: item.response.data.url
            }
          );
        }
      })
    }

    if (video && video.length !== 0) {
      video.map(item => {
        if (item.response && item.response.result) {
          videoData = {
            uid: item.uid,
            name: item.name,
            url: item.response.data.url
          }
        }
      })
    }

    const payload = Object.assign(fields, { citizenId, images: imageData, video: videoData });
    contentStore.create(payload).then(response => {
      if (response.result) {
        message.success('Контент амжилттай бүртгэлээ');
        this.setState({
          createModalVisible: false,
        });

        this.refreshTable();
      } else {
        message.error(`Контент бүртгэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Хэрэглэгч бүртгэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  showEditForm = record => {
    this.setState({
      editFormValues: record,
    });
    this.handleEditModalVisible(true);
  };

  handleEditModalVisible = flag => {
    this.setState({
      editModalVisible: !!flag,
    });
  };

  handleEdit = fields => {
    const { contentStore, citizenId } = this.props;
    const { editFormValues } = this.state;

    const { images, video, ...rest } = fields;
    let imageData = [];
    let videoData = null;

    if (images && images.length !== 0) {
      images.map(item => {
        if (item.response && item.response.result) {
          imageData.push(
            {
              uid: item.uid,
              name: item.name,
              url: item.response.data.url
            }
          );
        } else {
          imageData.push(item);
        }
      })
    }

    if (video && video.length !== 0) {
      video.map(item => {
        if (item.response && item.response.result) {
          videoData = {
            uid: item.uid,
            name: item.name,
            url: item.response.data.url
          }
        } else {
          videoData = item;
        }
      })
    }
    const editedValues = Object.assign(fields, { citizenId, images: imageData, video: videoData });
    const payload = Object.assign(editFormValues, editedValues);
    contentStore.update(payload).then(response => {
      if (response.result) {
        message.success('Контент амжилттай шинэчиллээ');
        this.setState({
          editModalVisible: false,
        });

        this.refreshTable();
      } else {
        message.error(`Контент шинэчлэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Контент шинэчлэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  renderCreateForm() {
    const { createModalVisible } = this.state;

    return (
      <ContentCreateModal
        handleCreate={this.handleCreate}
        handleModalVisible={this.handleCreateModalVisible}
        modalVisible={createModalVisible}
      />
    );
  }

  renderEditForm() {
    const { editModalVisible, editFormValues, } = this.state;

    return (
      <ContentEditModal
        handleUpdate={this.handleEdit}
        handleModalVisible={this.handleEditModalVisible}
        modalVisible={editModalVisible}
        editFormValues={editFormValues}
      />
    );
  }

  handlePreview = (file) => {
    this.setState({
      preview: file.url,
      previewVisible: true,
    });
  };

  handleCancelPreview = () => this.setState({ previewVisible: false, videoVisible: false, playing: false });

  renderContentType = (type) => {
    switch (type) {
      case 'TEXT':
        return 'Текст';
      case 'IMAGE':
        return 'Зураг';
      case 'VIDEO':
        return 'Бичлэг';
      case 'URL':
        return 'Холбоос';
      default:
        return '';
    }
  };

  renderImages = (images) => {
    return images.map(image => {
      return <span style={{ cursor: 'pointer' }}>
        <img src={image.url} onClick={() => this.handlePreview(image)} alt='image' width={50} />&nbsp;
      </span>
    })
  };
  renderVideo = (file) => {
    this.setState({
      video: file.url,
      videoVisible: true,
      playing: true,
    });
  };

  renderContent = (content) => {
    switch (content.type) {
      case 'TEXT':
        return <div dangerouslySetInnerHTML={createMarkup(content.text)} />
      case 'IMAGE':
        return this.renderImages(content.images);
      case 'VIDEO':
        return <Tag style={{ cursor: 'pointer' }} onClick={() => this.renderVideo(content.video)}>{content.video.name}</Tag>;
      case 'URL':
        return <a href={content.url} target="_blank" rel="noopener noreferrer">{content.url}</a>
      default:
        return '';
    }
  };

  calculateIndex = (index, company) => (
    <span>{index + 1 + company.data.pagination.currentPage * 10}</span>
  );

  render() {
    const { contentStore, contentStore: { loading }, citizenId } = this.props;
    const { previewVisible, preview, videoVisible, video, playing } = this.state;

    const columns = [
      {
        title: '#',
        dataIndex: 'key',
        render: (text, record, index) => this.calculateIndex(index, contentStore),
        width: 50,
      },
      {
        title: 'Огноо',
        dataIndex: 'createdDate',
        render: text => `${moment(text).format(apiFormat)}`,
        width: 150,
      },
      {
        title: 'Төрөл',
        dataIndex: 'type',
        render: text => this.renderContentType(text),
      },
      {
        title: 'Контент',
        dataIndex: 'description',
        render: (text, record) => this.renderContent(record),
      },
      {
        title: 'Үйлдэл',
        render: (text, record) => (
          <Fragment>
            <a key="edit" onClick={() => this.showEditForm(record)}>
              <Icon type="edit" />
            </a>
            <Divider type="vertical" />
            <a key="delete" onClick={() => this.showDeleteConfirm(record.key)}>
              <Icon type="delete" />
            </a>
          </Fragment>
        ),
      },
    ];

    return (
      <div>
        <Card bordered={false}>
          <Button icon='plus' type='primary' onClick={() => this.handleCreateModalVisible(true)} style={{ float: 'right' }}>
            Нэмэх
          </Button>
          <br />
          <br />
          <Table
            rowKey="id"
            loading={loading}
            columns={columns}
            dataSource={contentStore && contentStore.data && contentStore.data.list ? contentStore.data.list : []}
            pagination={contentStore && contentStore.data && contentStore.data.pagination
              ? contentStore.data.pagination : []}
            onChange={this.handleTableChange}
            scroll={{ x: 'max-content' }}
          />
        </Card>
        {this.renderCreateForm()}
        {this.renderEditForm()}
        <Modal visible={previewVisible} footer={null} onCancel={this.handleCancelPreview}>
          <img src={preview} width='100%' />
        </Modal>
        <Modal visible={videoVisible} footer={null} onCancel={this.handleCancelPreview}>
          <ReactPlayer url={video} controls width='100%' playing={playing} />
        </Modal>
      </div>
    );
  }
}

export default withStyles(styles)(ContentList);
