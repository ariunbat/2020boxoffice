import React, { Component } from 'react';
import { Modal, Form, Input, Select, Upload, Icon } from 'antd';
import { inject, observer } from 'mobx-react/index';
import { getImageServerUrl } from '../../../../../common/services/cdn';
import Editor from '../../../../components/RichTextEditor';

const FormItem = Form.Item;
const { confirm } = Modal;

@Form.create()
@inject('authStore')
@observer
class ContentCreateModal extends Component {

  submitHandle = () => {
    const { form, handleCreate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleCreate(fieldsValue);
    });
  };

  backHandle = () => {
    const { handleModalVisible, form } = this.props;
    form.resetFields();
    handleModalVisible(false);
  };

  showConfirm = () => {
    const { form } = this.props;
    const parentMethods = { backHandle: this.backHandle };
    if (form.isFieldsTouched()) {
      confirm({
        title: 'Та гарахдаа итгэлтэй байна уу?',
        content: '',
        okText: 'Тийм',
        okType: 'danger',
        cancelText: 'Үгүй',
        onOk() {
          parentMethods.backHandle();
        },
        onCancel() {
        },
      });
    } else {
      parentMethods.backHandle()
    }
  };

  normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  };

  handleEditorChange = (content, editor) => {
    const { form } = this.props;
    form.setFields({
      text: { value: content }
    })
  };

  render() {
    const { form, modalVisible, authStore } = this.props;
    const { getFieldDecorator } = form;

    const formItemLayout = { labelCol: { span: 8 }, wrapperCol: { span: 16 } };

    const uploadButton = (
      <Icon type="upload" />
    );

    return (
      <Modal
        title="Коллеги нэмэх"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={this.showConfirm}
        okText='Хадгалах'
        cancelText='Болих'
      >
        <Form>
          <FormItem {...formItemLayout} label="Төрөл">
            {getFieldDecorator('type', {
              initialValue: 'TEXT'
            })(
              <Select placeholder="Төрөл сонгоно уу" style={{ width: '100%' }}>
                <Select.Option value='TEXT' key='TEXT'>Текст</Select.Option>
                <Select.Option value='IMAGE' key='IMAGE'>Зураг</Select.Option>
                <Select.Option value='VIDEO' key='VIDEO'>Бичлэг</Select.Option>
                <Select.Option value='URL' key='TEXT'>Холбоос</Select.Option>
              </Select>
            )}
          </FormItem>
          {
            form.getFieldValue('type') === 'IMAGE' &&
            <FormItem {...formItemLayout} label="Зураг">
              {getFieldDecorator('images', {
                valuePropName: 'fileList',
                getValueFromEvent: this.normFile,
                rules: [{ required: true, message: 'Зураг оруулна уу' }],
              })(
                <Upload
                  name="file"
                  accept="image/*"
                  listType="picture-card"
                  headers={{ 'X-Auth-Token': authStore.values.token }}
                  data={{ 'entity': 'images', 'entityId': Math.random().toString(36).substring(2) }}
                  action={getImageServerUrl() + '/upload'}
                  onPreview={this.handlePreview}
                >
                  {uploadButton}
                </Upload>)}
            </FormItem>
          }
          {
            form.getFieldValue('type') === 'VIDEO' &&
            <FormItem {...formItemLayout} label="Бичлэг">
              {getFieldDecorator('video', {
                valuePropName: 'fileList',
                getValueFromEvent: this.normFile,
                rules: [{ required: true, message: 'Бичлэг оруулна уу' }],
              })(
                <Upload
                  name="file"
                  accept="video/*"
                  listType="picture-card"
                  headers={{ 'X-Auth-Token': authStore.values.token }}
                  data={{ 'entity': 'video', 'entityId': Math.random().toString(36).substring(2) }}
                  action={getImageServerUrl() + '/upload'}
                  onPreview={this.handlePreview}
                >
                  {form.getFieldValue('video') && form.getFieldValue('video').length !== 0 ? null : uploadButton}
                </Upload>)}
            </FormItem>
          }
          {
            form.getFieldValue('type') === 'URL' &&
            <FormItem {...formItemLayout} label="Холбоос">
              {getFieldDecorator('url', {
                rules: [
                  { required: true, message: 'Холбоос оруулна уу' },
                  { type: 'url', message: 'Холбоос зөв оруулна уу'}
                ],
              })(<Input />)}
            </FormItem>
          }
          {
            form.getFieldValue('type') === 'TEXT' &&
            <FormItem {...formItemLayout} label="Контент">
              {getFieldDecorator('text', {
                rules: [
                  { required: true, message: 'Контент оруулна уу' },
                ],
              })(
                <Editor onEditorChange={this.handleEditorChange} />
                )}
            </FormItem>
          }
          <FormItem {...formItemLayout} label="Taйлбар">
            {getFieldDecorator('description')(<Input.TextArea placeholder="Taйлбар" rows={4}/>)}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default ContentCreateModal;