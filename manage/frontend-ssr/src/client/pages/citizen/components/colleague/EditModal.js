import React, { Component } from 'react';
import { Modal, Form, Input, DatePicker, Select, AutoComplete } from 'antd';
import { inject, observer } from 'mobx-react/index';
import moment from 'moment';

const FormItem = Form.Item;
const { confirm } = Modal;

@Form.create()
@inject('colleagueTypeStore','colleagueStore')
@observer
class ColleagueEditModal extends Component {

  state = {
    dataSource: []
  };

  componentDidMount() {
    const { colleagueTypeStore } = this.props;
    colleagueTypeStore.list();
  }

  submitHandle = () => {
    const { form, handleUpdate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  backHandle = () => {
    const { handleModalVisible, form } = this.props;
    form.resetFields();
    handleModalVisible(false);
  };

  showConfirm = () => {
    const { form } = this.props;
    const parentMethods = { backHandle: this.backHandle };
    if (form.isFieldsTouched()) {
      confirm({
        title: 'Та гарахдаа итгэлтэй байна уу?',
        content: '',
        okText: 'Тийм',
        okType: 'danger',
        cancelText: 'Үгүй',
        onOk() {
          parentMethods.backHandle()
        },
        onCancel() {
        },
      });
    } else {
      parentMethods.backHandle()
    }
  };

  onSearch = (name, type) => {
    const { colleagueStore } = this.props;
    if (name.length > 0) {

      colleagueStore.selectList({ type, name }).then(response => {
        this.setState({
          dataSource: response.data && response.data.name || [],
        });
      })
    }
  };

  clearName = (value) => {
    this.setState({
      dataSource: [],
    })
  };

  render() {
    const { form, modalVisible, editFormValues, colleagueTypeStore } = this.props;
    const { dataSource } = this.state;
    const { getFieldDecorator } = form;

    const formItemLayout = { labelCol: { span: 8 }, wrapperCol: { span: 16 } };

    return (
      <Modal
        title="Коллеги засварлах"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={this.showConfirm}
        okText='Хадгалах'
        cancelText='Болих'
      >
        <Form>
          <FormItem labelCol={{ span: 8 }} wrapperCol={{ span: 15 }} label="Коллегийн төрөл">
            {getFieldDecorator('colleagueTypeId', {
              initialValue: editFormValues.colleagueTypeId,
              rules: [{ required: true, message: 'Коллегийн төрөл сонгоно уу' }],
            })(
              <Select placeholder="Коллегийн төрөл сонгоно уу" style={{ width: '100%' }}>
                {colleagueTypeStore != null && colleagueTypeStore.data != null
                  ? colleagueTypeStore.data.map(item => <Select.Option key={item.key} value={item.key}>{item.name}</Select.Option>)
                  : ''}
              </Select>
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Коллегийн нэр">
            {getFieldDecorator('name', {
              initialValue: editFormValues.name,
              rules: [{ required: true, message: 'Коллегийн нэр бичнэ үү' }],
            })(
              <AutoComplete
                dataSource={dataSource}
                onSelect={this.clearName}
                onSearch={(name) => this.onSearch(name, 'name')}
                placeholder="Коллегийн нэр"
                onBlur={this.clearName}
              />)
            }
          </FormItem>
          <FormItem {...formItemLayout} label="Taйлбар">
            {getFieldDecorator('description', {
              initialValue: editFormValues.description,
            })(<Input.TextArea placeholder="Taйлбар" rows={4}/>)}
          </FormItem>
          <FormItem {...formItemLayout} label="Элссэн огноо">
            {getFieldDecorator('startDate', {
              initialValue:  editFormValues.startDate && moment(editFormValues.startDate).parseZone(),
            })(<DatePicker placeholder='Элссэн огноо' />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Төгссөн огноо">
            {getFieldDecorator('endDate', {
              initialValue: editFormValues.endDate && moment(editFormValues.endDate).parseZone(),
            })(<DatePicker placeholder='Төгссөн огноо' />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Оролцоо">
            {getFieldDecorator('participation', {
              initialValue: editFormValues.participation,
            })(<Input.TextArea placeholder="Оролцоо" rows={4} />)}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default ColleagueEditModal;
