import React, { Component, Fragment } from 'react';
import {
  Card,
  Form,
  Input,
  Icon,
  Button,
  Modal,
  Divider,
  message,
  Table,
} from 'antd';
import moment from 'moment';
import ColleagueCreateModal from './CreateModal';
import ColleagueEditModal from './EditModal';
import { inject, observer } from 'mobx-react/index';
import withStyles from 'isomorphic-style-loader/withStyles';
import styles from './List.less';
import { apiFormat } from '../../../../../common/utils/dateFormat';

const FormItem = Form.Item;
const Confirm = Modal.confirm;

@Form.create()
@inject('colleagueStore')
@observer
class ColleagueList extends Component {
  state = {
    createModalVisible: false,
    editModalVisible: false,
    editFormValues: [],
    searchFormValues: {},
  };

  componentDidMount() {
    this.refreshTable();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { citizenId, colleagueStore } = this.props;
    if (prevProps.citizenId !== citizenId) {
      colleagueStore.list({ citizenId });
    }
  }

  refreshTable = () => {
    const { colleagueStore, citizenId } = this.props;
    if (citizenId) {
      colleagueStore.list({ citizenId });
    }
  };

  handleTableChange = () => {
    this.refreshTable();
  };

  handleSearchFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      searchFormValues: {},
    });

    this.refreshTable();
  };

  showDeleteConfirm = clickedId => {
    const parentMethods = { handleDelete: this.handleDelete };

    Confirm({
      title: 'Анхааруулга',
      content: 'Сонгосон коллеги гишүүн устгах уу?',
      okText: 'Тийм',
      okType: 'danger',
      cancelText: 'Үгүй',
      onOk() {
        parentMethods.handleDelete(clickedId);
      },
    });
  };

  handleDelete = clickedId => {
    const { colleagueStore } = this.props;

    const payload = { id: clickedId };

    colleagueStore.delete(payload).then(response => {
      if (!response.result) {
        message.error(`Коллеги устгахад алдаа гарлаа: ${response.message}`);
      }
      this.refreshTable();
    })
      .catch(e => {
        message.error(`Коллеги устгахад алдаа гарлаа: ${e.message}`);
      });
  };

  handleSearch = e => {
    e.preventDefault();
    const { form } = this.props;

    this.setState({
      searchFormValues: form.getFieldsValue(),
    });

    this.refreshTable(form.getFieldsValue());
  };

  handleCreateModalVisible = flag => {
    this.setState({
      createModalVisible: !!flag,
    });
  };

  handleCreate = fields => {
    const { colleagueStore, citizenId } = this.props;

    const payload = Object.assign(fields, { citizenId });
    colleagueStore.create(fields).then(response => {
      if (response.result) {
        message.success('Коллеги амжилттай бүртгэлээ');
        this.setState({
          createModalVisible: false,
        });

        this.refreshTable();
      } else {
        message.error(`Коллеги бүртгэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Хэрэглэгч бүртгэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  showEditForm = record => {
    this.setState({
      editFormValues: record,
    });
    this.handleEditModalVisible(true);
  };

  handleEditModalVisible = flag => {
    this.setState({
      editModalVisible: !!flag,
    });
  };

  handleEdit = fields => {
    const { colleagueStore } = this.props;
    const { editFormValues } = this.state;

    const payload = Object.assign(editFormValues, fields);

    colleagueStore.update(payload).then(response => {
      if (response.result) {
        message.success('Коллеги амжилттай шинэчиллээ');
        this.setState({
          editModalVisible: false,
        });

        this.refreshTable();
      } else {
        message.error(`Коллеги шинэчлэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Коллеги шинэчлэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  renderCreateForm() {
    const { createModalVisible } = this.state;

    return (
      <ColleagueCreateModal
        handleCreate={this.handleCreate}
        handleModalVisible={this.handleCreateModalVisible}
        modalVisible={createModalVisible}
      />
    );
  }

  renderEditForm() {
    const { editModalVisible, editFormValues, } = this.state;

    return (
      <ColleagueEditModal
        handleUpdate={this.handleEdit}
        handleModalVisible={this.handleEditModalVisible}
        modalVisible={editModalVisible}
        editFormValues={editFormValues}
      />
    );
  }

  render() {
    const { colleagueStore, colleagueStore: { loading }, citizenId, next } = this.props;

    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        key: 'id',
        render: (text, record, index) => index + 1,
        width: '30px',
      },
      {
        title: 'Огноо',
        dataIndex: 'startDate',
        render: (text, record) => `${moment(record.startDate).format('YYYY')}-${moment(record.endDate).format('YYYY')}`
      },
      {
        title: 'Төрөл',
        dataIndex: 'colleagueTypeName',
      },
      {
        title: 'Коллегийн нэр',
        dataIndex: 'name',
      },
      {
        title: 'Үйлдэл',
        render: (text, record) => (
          <Fragment>
            <a key="edit" onClick={() => this.showEditForm(record)}>
              <Icon type="edit" />
            </a>
            <Divider type="vertical" />
            <a key="delete" onClick={() => this.showDeleteConfirm(record.key)}>
              <Icon type="delete" />
            </a>
          </Fragment>
        ),
      },
    ];

    return (
      <div>
        <Card bordered={false}>
          <Button icon='plus' type='primary' onClick={() => this.handleCreateModalVisible(true)} style={{ float: 'right' }}>
            Нэмэх
          </Button>
          <br />
          <br />
          <Table
            rowKey="key"
            loading={loading}
            columns={columns}
            dataSource={colleagueStore && colleagueStore.data != null ? colleagueStore.data : []}
            onChange={this.handleTableChange}
          />
          <Button type='primary' onClick={() => next('3')}>
            ҮРГЭЖЛҮҮЛЭХ
          </Button>
        </Card>
        {this.renderCreateForm()}
        {this.renderEditForm()}
      </div>
    );
  }
}

export default withStyles(styles)(ColleagueList);
