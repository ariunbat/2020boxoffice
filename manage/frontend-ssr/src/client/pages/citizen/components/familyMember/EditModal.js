import React, { Component } from 'react';
import { Modal, Form, Input, Select } from 'antd';
import { inject, observer } from 'mobx-react/index';
import { checkRegistry } from '../../../../../common/utils/utils';
import moment from 'moment';
import { apiFormat } from '../../../../../common/utils/dateFormat';
import TagGroup from '../../../../components/TagGroup';
import { observable } from 'mobx';

const FormItem = Form.Item;
const { Option } = Select;
const { confirm } = Modal;

@Form.create()
@inject( 'familyMemberStore')
@observer
class FamilyMemberEditModal extends Component {

  @observable age = null;

  state = {
    phone: [],
  };

  submitHandle = () => {
    const { form, handleUpdate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleUpdate(fieldsValue);
    });
  };

  backHandle = () => {
    const { handleModalVisible, form } = this.props;
    form.resetFields();
    handleModalVisible(false);
  };

  showConfirm = () => {
    const { form } = this.props;
    const parentMethods = { backHandle: this.backHandle };
    if (form.isFieldsTouched()) {
      confirm({
        title: 'Та гарахдаа итгэлтэй байна уу?',
        content: '',
        okText: 'Тийм',
        okType: 'danger',
        cancelText: 'Үгүй',
        onOk() {
          parentMethods.backHandle()
        },
        onCancel() {
        },
      });
    } else {
      parentMethods.backHandle()
    }
  };

  getAge = (e) => {
    e.preventDefault();
    const { ageCalcDate } = this.props;
    let year;
    let month;
    let day;
    const regNum = e.target.value;
    let birthDate;

    if (regNum.length === 10) {
      year = regNum.substring(2, 4);
      month = regNum.substring(4, 6);
      day = regNum.substring(6, 8);
      if (year.substring(0, 1) === '1' || year.substring(0, 1) === '0') {
        year = '20'.concat(year);
        month = month - 20;
      } else {
        year = '19'.concat(year);
      }
      birthDate = year + '-' + month.toString().padStart(2, '0') + '-' + day;
      this.age = moment(ageCalcDate && ageCalcDate.current).diff(birthDate, 'years');
    }
  };

  setPhone = list => {
    const { form } = this.props;
    const { phone } = this.state;

    if (phone && list) {
      this.setState({
          phone: list,
          phoneValidate: null,
        },
        form.setFields({ 'phone': { value: list } })
      );

    }
  };

  render() {
    const { form, modalVisible, editFormValues, ageCalcDate } = this.props;
    const { getFieldDecorator } = form;

    const formItemLayout = { labelCol: { span: 8 }, wrapperCol: { span: 16 } };

    return (
      <Modal
        title="Хэрэглэгчийн мэдээлэл засварлах"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={this.showConfirm}
        okText='Хадгалах'
        cancelText='Болих'
      >
        <Form>
          <FormItem {...formItemLayout} label="Хамаарал">
            {getFieldDecorator('relationship', {
              initialValue: editFormValues.relationship,
              rules: [{ required: true, message: 'Хамаарал сонгоно уу' }],
            })(
              <Select placeholder="Хамаарал сонгоно уу" style={{ width: '100%' }}>
                <Option value='Эхнэр' key='Эхнэр'>
                  Эхнэр
                </Option>
                <Option value='Нөхөр' key='Нөхөр'>
                  Нөхөр
                </Option>
                <Option value='Хүү' key='Хүү'>
                  Хүү
                </Option>
                <Option value='Охин' key='Охин'>
                  Охин
                </Option>
                <Option value='Аав' key='Аав'>
                  Аав
                </Option>
                <Option value='Ээж' key='Ээж'>
                  Ээж
                </Option>
                <Option value='Бусад' key='Бусад'>
                  Бусад
                </Option>
              </Select>
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Регистрийн дугаар">
            {getFieldDecorator('registerNumber', {
              initialValue: editFormValues.registerNumber,
            })(
              <Input placeholder="Регистрийн дугаар" disabled />)
            }
          </FormItem>
          <FormItem {...formItemLayout} label="Нэр">
            {getFieldDecorator('firstName', {
              initialValue: editFormValues.firstName,
              rules: [{ required: true, message: 'Нэрээ бичнэ үү' }],
            })(<Input placeholder="Нэр" />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Эцэг /эх/-ийн нэр">
            {getFieldDecorator('lastName', {
              initialValue: editFormValues.lastName,
            })(<Input placeholder="Эцэг /эх/-ийн нэр" />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Ургийн овог">
            {getFieldDecorator('familyName', {
              initialValue: editFormValues.familyName,
            })(
              <Input placeholder="Ургийн овог" />)
            }
          </FormItem>
          <FormItem {...formItemLayout} label="Нас">
            {/* eslint-disable-next-line max-len */}
            <span>{this.age ? this.age : Math.floor(editFormValues.age)} ({(moment(ageCalcDate.current).format(apiFormat))}-ны байдлаар)</span>
          </FormItem>
          <FormItem {...formItemLayout} label="Утас">
            {getFieldDecorator('phone', {
              initialValue: editFormValues.phone || [],
              valuePropName: 'initialTags',
            })(
              <TagGroup
                setTagList={this.setPhone}
                inputWith={200}
                tagColor="gold"
              />
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Имэйл">
            {getFieldDecorator('email', {
              initialValue: editFormValues.email,
              rules: [
                { type: 'email', message: 'Мэйл хаягаа зөв бичнэ үү' },
              ],
            })(<Input placeholder="Имэйл" />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Эрхэлж буй ажил">
            {getFieldDecorator('job', {
              initialValue: editFormValues.job,
            })(<Input placeholder="Эрхэлж буй ажил" />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Сошиал хаяг">
            {getFieldDecorator('socialAccount', {
              initialValue: editFormValues.socialAccount,
            })(
              <Input placeholder="Сошиал хаяг" />)
            }
          </FormItem>
          <FormItem {...formItemLayout} label="Тайлбар">
            {getFieldDecorator('description', {
              initialValue: editFormValues.description,
            })(
              <Input.TextArea placeholder="Тайлбар" />)
            }
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default FamilyMemberEditModal;
