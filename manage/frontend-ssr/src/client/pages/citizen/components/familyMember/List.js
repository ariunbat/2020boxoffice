import React, { Component, Fragment } from 'react';
import {
  Card,
  Form,
  Input,
  Icon,
  Button,
  Modal,
  Divider,
  message,
  Table,
} from 'antd';
import FamilyMemberCreateModal from './CreateModal';
import FamilyMemberEditModal from './EditModal';

import { inject, observer } from 'mobx-react/index';
import withStyles from 'isomorphic-style-loader/withStyles';
import styles from './List.less';

const FormItem = Form.Item;
const Confirm = Modal.confirm;

@Form.create()
@inject('familyMemberStore', 'ageCalcDate')
@observer
class FamilyMemberList extends Component {
  state = {
    createModalVisible: false,
    editModalVisible: false,
    editFormValues: [],
    searchFormValues: {},
  };

  componentDidMount() {
    const { ageCalcDate } = this.props;
    ageCalcDate.get();
    this.refreshTable();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { citizenId, familyMemberStore } = this.props;
    if (prevProps.citizenId !== citizenId) {
      familyMemberStore.list({ citizenId });
    }
  }

  refreshTable = () => {
    const { familyMemberStore, citizenId } = this.props;
    if (citizenId) {
      familyMemberStore.list({ citizenId });
    }
  };

  handleTableChange = () => {
    this.refreshTable();
  };

  handleSearchFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      searchFormValues: {},
    });

    this.refreshTable();
  };

  showDeleteConfirm = clickedId => {
    const parentMethods = { handleDelete: this.handleDelete };

    Confirm({
      title: 'Анхааруулга',
      content: 'Сонгосон гэр бүлийн гишүүн устгах уу?',
      okText: 'Тийм',
      okType: 'danger',
      cancelText: 'Үгүй',
      onOk() {
        parentMethods.handleDelete(clickedId);
      },
    });
  };

  handleDelete = clickedId => {
    const { familyMemberStore } = this.props;

    const payload = { id: clickedId };

    familyMemberStore.delete(payload).then(response => {
      if (!response.result) {
        message.error(`Гэр бүлийн гишүүн устгахад алдаа гарлаа: ${response.message}`);
      }
      this.refreshTable();
    })
      .catch(e => {
        message.error(`Гэр бүлийн гишүүн устгахад алдаа гарлаа: ${e.message}`);
      });
  };

  handleSearch = e => {
    e.preventDefault();
    const { form } = this.props;

    this.setState({
      searchFormValues: form.getFieldsValue(),
    });

    this.refreshTable(form.getFieldsValue());
  };

  handleCreateModalVisible = flag => {
    this.setState({
      createModalVisible: !!flag,
    });
  };

  handleCreate = fields => {
    const { familyMemberStore, citizenId } = this.props;

    const payload = Object.assign(fields, { citizenId });
    familyMemberStore.create(payload).then(response => {
      if (response.result) {
        if(response.data) {
          message.success('Гэр бүлийн гишүүн амжилттай бүртгэлээ');
          this.setState({
            createModalVisible: false,
          });

          this.refreshTable();
        } else {
          message.error(response.message);
        }
      } else {
        message.error(`Гэр бүлийн гишүүн бүртгэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Хэрэглэгч бүртгэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  showEditForm = record => {
    this.setState({
      editFormValues: record,
    });
    this.handleEditModalVisible(true);
  };

  handleEditModalVisible = flag => {
    this.setState({
      editModalVisible: !!flag,
    });
  };

  handleEdit = fields => {
    const { familyMemberStore } = this.props;
    const { editFormValues } = this.state;

    const payload = Object.assign(editFormValues, fields);

    familyMemberStore.update(payload).then(response => {
      if (response.result) {
        message.success('Гэр бүлийн гишүүн амжилттай шинэчиллээ');
        this.setState({
          editModalVisible: false,
        });

        this.refreshTable();
      } else {
        message.error(`Гэр бүлийн гишүүн шинэчлэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Хэрэглэгчийн мэдээлэл шинэчлэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  renderCreateForm() {
    const { createModalVisible } = this.state;
    const { ageCalcDate, citizenId } = this.props;

    return (
      <FamilyMemberCreateModal
        ageCalcDate={ageCalcDate}
        citizenId={citizenId}
        handleCreate={this.handleCreate}
        handleModalVisible={this.handleCreateModalVisible}
        modalVisible={createModalVisible}
      />
    );
  }

  renderEditForm() {
    const { editModalVisible, editFormValues } = this.state;
    const { ageCalcDate } = this.props;

    return (
      <FamilyMemberEditModal
        ageCalcDate={ageCalcDate}
        handleUpdate={this.handleEdit}
        handleModalVisible={this.handleEditModalVisible}
        modalVisible={editModalVisible}
        editFormValues={editFormValues}
      />
    );
  }

  render() {
    const { familyMemberStore, familyMemberStore: { loading }, citizenId } = this.props;

    const columns = [
      {
        title: '#',
        dataIndex: 'id',
        key: 'id',
        render: (text, record, index) => index + 1,
        width: '30px',
      },
      {
        title: 'Хамаарал',
        dataIndex: 'relationship',
      },
      {
        title: 'Регистр',
        dataIndex: 'registerNumber',
      },
      {
        title: 'Овог',
        dataIndex: 'lastName',
      },
      {
        title: 'Нэр',
        dataIndex: 'firstName',
      },
      {
        title: 'Нас',
        dataIndex: 'age',
        render: text => text && Math.floor(text),
      },
      {
        title: 'Эрхэлж буй ажил',
        dataIndex: 'job',
        ellipsis: true
      },
      {
        title: 'Үйлдэл',
        render: (text, record) => (
          <Fragment>
            <a key="edit" onClick={() => this.showEditForm(record)}>
              <Icon type="edit" />
            </a>
            <Divider type="vertical" />
            <a key="delete" onClick={() => this.showDeleteConfirm(record.key)}>
              <Icon type="delete" />
            </a>
          </Fragment>
        ),
      },
    ];

    return (
      <div>
        <Card bordered={false}>
          <Button icon='plus' type='primary' onClick={() => this.handleCreateModalVisible(true)} style={{ float: 'right' }}>
            Нэмэх
          </Button>
          <br />
          <Table
            rowKey="key"
            loading={loading}
            columns={columns}
            dataSource={familyMemberStore && familyMemberStore.data != null ? familyMemberStore.data : []}
            onChange={this.handleTableChange}
          />
        </Card>
        {this.renderCreateForm()}
        {this.renderEditForm()}
      </div>
    );
  }
}

export default withStyles(styles)(FamilyMemberList);
