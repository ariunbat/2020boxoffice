import React, { Component } from 'react';
import { Modal, Form, Input, Select, message } from 'antd';
import { inject, observer } from 'mobx-react/index';
import { checkRegistry } from '../../../../../common/utils/utils';
import moment from 'moment';
import { apiFormat } from '../../../../../common/utils/dateFormat';
import { observable } from 'mobx';
import TagGroup from '../../../../components/TagGroup';

const FormItem = Form.Item;
const { Option } = Select;
const { confirm } = Modal;

@Form.create()
@inject('familyMemberStore')
@observer
class FamilyMemberCreateModal extends Component {

  @observable age = null;

  state = {
    phone: [],
    familyMemberExists: false,
  };

  resetStates = () => {
    const { form } = this.props;
    this.setState({
      familyMemberExists: false,
      phone: [],
    });
    form.setFieldsValue({
      firstName: null,
      lastName: null,
      familyName: null,
      email: null,
      job: null,
      socialAccount: null,
    });
    this.age = null;
  };

  submitHandle = () => {
    const { form, handleCreate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      form.resetFields();
      this.resetStates();
      handleCreate(fieldsValue);
    });
  };

  backHandle = () => {
    const { handleModalVisible, form } = this.props;
    form.resetFields();
    this.resetStates();
    handleModalVisible(false);
  };

  showConfirm = () => {
    const { form } = this.props;
    const parentMethods = { backHandle: this.backHandle };
    if (form.isFieldsTouched()) {
      confirm({
        title: 'Та гарахдаа итгэлтэй байна уу?',
        content: '',
        okText: 'Тийм',
        okType: 'danger',
        cancelText: 'Үгүй',
        onOk() {
          parentMethods.backHandle();
        },
        onCancel() {
        },
      });
    } else {
      parentMethods.backHandle()
    }
  };

  getAge = (registerNumber) => {
    const { ageCalcDate } = this.props;
    let year;
    let month;
    let day;
    const regNum = registerNumber;
    let birthDate;

    if (regNum.length === 10) {
      year = regNum.substring(2, 4);
      month = regNum.substring(4, 6);
      day = regNum.substring(6, 8);
      if (year.substring(0, 1) === '1' || year.substring(0, 1) === '0') {
        year = '20'.concat(year);
        month = month - 20;
      } else {
        year = '19'.concat(year);
      }
      birthDate = year + '-' + month.toString().padStart(2, '0') + '-' + day;
      this.age = moment(ageCalcDate && ageCalcDate.current).diff(birthDate, 'years');
    }
  };

  setPhone = list => {
    const { form } = this.props;
    const { phone } = this.state;

    if (phone && list) {
      this.setState({
          phone: list,
          phoneValidate: null,
        },
        form.setFields({ 'phone': { value: list } })
      );

    }
  };

  checkFamilyMemberExists = (e) => {
    const { familyMemberStore, form, citizenId } = this.props;
    const registerNumber = e.target.value;
    if (registerNumber.length === 10) {
      familyMemberStore.checkFamilyMember({ registerNumber }).then(response => {
        if (response.result) {
          if (response.data) {
            if (citizenId !== response.data.id) {
              form.setFieldsValue({
                firstName: response.data.firstName,
                lastName: response.data.lastName,
                familyName: response.data.familyName,
                email: response.data.email,
                job: response.data.job,
                socialAccount: response.data.socialAccount,
              });
              this.getAge(registerNumber);
              this.setState({
                familyMemberExists: true,
              })
            } else {
              form.setFieldsValue({
                registerNumber: null,
              });
              message.warning(`${registerNumber} регистерээр бүртгэх боломжгүй`)
            }
          } else {
            this.resetStates();
            this.getAge(registerNumber);
          }
        } else {
          this.resetStates();
        }
      })
        .catch(e => {
          console.log(e);
          // message.error(`алдаа гарлаа: ${response.message}`);
        });
    } else {
      form.resetFields();
      this.resetStates();
    }
  };

  render() {
    const { form, modalVisible, ageCalcDate } = this.props;
    const { getFieldDecorator } = form;
    const { familyMemberExists } = this.state;

    const formItemLayout = { labelCol: { span: 8 }, wrapperCol: { span: 16 } };

    return (
      <Modal
        title="Гэр бүлийн гишүүн нэмэх"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={this.showConfirm}
        okText='Хадгалах'
        cancelText='Болих'
      >
        <Form>
          <FormItem {...formItemLayout} label="Хамаарал">
            {getFieldDecorator('relationship', {
              rules: [{ required: true, message: 'Хамаарал сонгоно уу' }],
            })(
              <Select placeholder="Хамаарал сонгоно уу" style={{ width: '100%' }}>
                <Option value='Эхнэр' key='Эхнэр'>
                  Эхнэр
                </Option>
                <Option value='Нөхөр' key='Нөхөр'>
                  Нөхөр
                </Option>
                <Option value='Хүү' key='Хүү'>
                  Хүү
                </Option>
                <Option value='Охин' key='Охин'>
                  Охин
                </Option>
                <Option value='Аав' key='Аав'>
                  Аав
                </Option>
                <Option value='Ээж' key='Ээж'>
                  Ээж
                </Option>
                <Option value='Бусад' key='Бусад'>
                  Бусад
                </Option>
              </Select>
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Регистрийн дугаар">
            {getFieldDecorator('registerNumber', {
              rules: [
                { validator: (rule, control, callback) => checkRegistry(rule, control, callback) },
              ],
            })(
              <Input placeholder="Регистрийн дугаар" onChange={(value) => this.checkFamilyMemberExists(value)} />)
            }
          </FormItem>
          <FormItem {...formItemLayout} label="Нэр">
            {getFieldDecorator('firstName', {
              rules: [{ required: true, message: 'Нэрээ бичнэ үү' }],
            })(<Input placeholder="Нэр" disabled={familyMemberExists} />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Эцэг /эх/-ийн нэр">
            {getFieldDecorator('lastName')(<Input placeholder="Эцэг /эх/-ийн нэр" disabled={familyMemberExists} />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Ургийн овог">
            {getFieldDecorator('familyName')(
              <Input placeholder="Ургийн овог" disabled={familyMemberExists} />)
            }
          </FormItem>
          <FormItem {...formItemLayout} label="Нас">
            <span>{this.age && `${this.age} (${moment(ageCalcDate.current).format(apiFormat)}-ны байдлаар)`}</span>
          </FormItem>
          <FormItem {...formItemLayout} label="Утас">
            {getFieldDecorator('phone', {
              valuePropName: 'initialTags'
            })(
              <TagGroup
                setTagList={this.setPhone}
                inputWith={200}
                tagColor="gold"
              />
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Имэйл">
            {getFieldDecorator('email', {
              rules: [
                { type: 'email', message: 'Мэйл хаягаа зөв бичнэ үү' },
              ],
            })(<Input placeholder="Имэйл" />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Эрхэлж буй ажил">
            {getFieldDecorator('job')(<Input placeholder="Эрхэлж буй ажил" />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Сошиал хаяг">
            {getFieldDecorator('socialAccount')(
              <Input placeholder="Сошиал хаяг" />)
            }
          </FormItem>
          <FormItem {...formItemLayout} label="Тайлбар">
            {getFieldDecorator('description')(
              <Input.TextArea placeholder="Тайлбар" />)
            }
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default FamilyMemberCreateModal;