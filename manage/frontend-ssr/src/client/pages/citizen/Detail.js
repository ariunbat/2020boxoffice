import React, { Component } from 'react';
import { Card, Col, Divider, Form, Row, Spin, Tabs } from 'antd';
import { inject, observer } from 'mobx-react';
import PageHeaderWrapper from '../../components/PageHeaderWrapper';
import DescriptionList from '../../components/DescriptionList/DescriptionList';
import Description from '../../components/DescriptionList/Description';
import MainInfo from './components/MainInfo';
import Colleague from './components/colleague/List';
import Content from './components/content/List';
import moment from 'moment';
import { apiFormat } from '../../../common/utils/dateFormat';
import Exception from '../../components/Exception';

const { TabPane } = Tabs;

@Form.create()
@inject('citizenStore', 'ageCalcDate')
@observer
class Detail extends Component {

  state = {
    tabKey: '1'
  }

  componentDidMount() {
    const { citizenStore, match: { params }, ageCalcDate } = this.props;
    citizenStore.get(params.id);
    ageCalcDate.get();
  }

  next = (key) => {
    this.setState({
      tabKey: key
    })
  };

  render() {
    const { citizenStore, citizenStore: { loading, current: currentCitizen }, ageCalcDate } = this.props;
    return (
      <Spin tip="Түр хүлээнэ үү..." spinning={loading}>
        <PageHeaderWrapper title="Иргэний дэлгэрэнгүй мэдээлэл">
          <Card>
            <Row>
              <Col span={24}>
                <DescriptionList col={4}>
                  <Description term='РД'>{currentCitizen.registerNumber || '-'}</Description>
                  <Description term='Эцэг /эх/-ийн нэр'>{currentCitizen.lastName || '-'}</Description>
                  <Description term='Хүйс'>{currentCitizen.genderName || '-'}</Description>
                  <Description term='Нас'>
                    {currentCitizen.age ? `${Math.floor(currentCitizen.age)} (${moment(ageCalcDate.current).format(apiFormat)}-ны байдлаар)` : '-'}
                  </Description>
                  <Description term='Нэр'>{currentCitizen.firstName || '-'}</Description>
                  <Description term='Ургийн овог'>{currentCitizen.familyName || '-'}</Description>
                  <Description term='Статус'>-</Description>
                  <Description term='Хаяг'>
                    {currentCitizen.cityName}, {currentCitizen.districtName} {currentCitizen.subDistrictName},
                    {currentCitizen.town} {currentCitizen.apartment} {currentCitizen.apartmentAddress} {currentCitizen.street}
                    {currentCitizen.gerAddress} {currentCitizen.countrySideAddress}
                  </Description>
                </DescriptionList>
                <br />
              </Col>
              <Col span={24}>
                <Tabs activeKey={this.state.tabKey} onChange={(e) => this.next(e)}>
                  <TabPane tab="Ерөнхий мэдээлэл" key="1">
                    <MainInfo next={this.next} citizen={currentCitizen} {...this.props} />
                  </TabPane>
                  <TabPane tab="Коллеги" key="2">
                    <Colleague next={this.next} citizenId={currentCitizen.id} {...this.props} />
                  </TabPane>
                  <TabPane tab="Контент" key="3">
                    <Content next={this.next} citizenId={currentCitizen.id} {...this.props} />
                  </TabPane>
                </Tabs>
              </Col>
            </Row>
          </Card>
        </PageHeaderWrapper>
      </Spin>
    );
  }
}

export default Detail;