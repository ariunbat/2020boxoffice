import React, { Component } from 'react';
import { Modal, Form, Input, Select, InputNumber, Checkbox } from 'antd';
import { inject, observer } from 'mobx-react/index';
import moment from 'moment';

const FormItem = Form.Item;
const { confirm } = Modal;

@Form.create()
@inject('partyStore')
@observer
class ExpectationCreateModal extends Component {

  componentDidMount() {
    const { partyStore } = this.props;
    partyStore.selectList();
  }

  submitHandle = () => {
    const { form, handleExpectationCreate } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;
      form.resetFields();
      handleExpectationCreate(fieldsValue);
    });
  };

  backHandle = () => {
    const { handleModalVisible, form } = this.props;
    form.resetFields();
    handleModalVisible(false);
  };

  showConfirm = () => {
    const { form } = this.props;
    const parentMethods = { backHandle: this.backHandle };
    if (form.isFieldsTouched()) {
      confirm({
        title: 'Та гарахдаа итгэлтэй байна уу?',
        content: '',
        okText: 'Тийм',
        okType: 'danger',
        cancelText: 'Үгүй',
        onOk() {
          parentMethods.backHandle()
        },
        onCancel() {
        },
      });
    } else {
      parentMethods.backHandle()
    }
  };

  render() {
    const { form, modalVisible, expectationFormValues, partyStore } = this.props;
    const { getFieldDecorator } = form;

    const formItemLayout = { labelCol: { span: 8 }, wrapperCol: { span: 16 } };

    return (
      <Modal
        title="Магадлал нэмэх"
        visible={modalVisible}
        onOk={() => this.submitHandle()}
        onCancel={this.showConfirm}
        okText='Хадгалах'
        cancelText='Болих'
      >
        <Form>
          <FormItem {...formItemLayout} label="Зураг">
            <img src={expectationFormValues.photo && expectationFormValues.photo.url} height={70} />
          </FormItem>
          <FormItem {...formItemLayout} label="РД">
            <span>{expectationFormValues.registerNumber || '-'}</span>
          </FormItem>
          <FormItem {...formItemLayout} label="Овог нэр">
            <span>{expectationFormValues.lastName || '-'} {expectationFormValues.firstName ?
              expectationFormValues.firstName.toUpperCase() : '-'}</span>
          </FormItem>
          <FormItem {...formItemLayout} label="Хүйс">
            <span>{expectationFormValues.genderName || '-'}</span>
          </FormItem>
          <FormItem {...formItemLayout} label="Утас">
            <span>{expectationFormValues.phone || '-'}</span>
          </FormItem>
          <FormItem {...formItemLayout} label="Нам эвсэл">
            {getFieldDecorator('partyId', {
              rules: [{ required: true, message: 'Нам эвсэл сонгоно уу' }],
            })(
              <Select placeholder="Нам эвсэл сонгоно уу" style={{ width: '100%' }}>
                {partyStore != null && partyStore.data != null
                  ? partyStore.data.list.map(party => <Select.Option key={party.key} value={party.key}>{party.name}</Select.Option>)
                  : ''}
              </Select>
            )}
          </FormItem>
          <FormItem {...formItemLayout} label="Хувь">
            {getFieldDecorator('percent', {
            })(<InputNumber min={1} max={100} />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Шалтгаан">
            {getFieldDecorator('reason', {
            })(<Input.TextArea placeholder="Оролцоо" rows={4} />)}
          </FormItem>
          <FormItem {...formItemLayout} label="Системээр магадлалыг тооцоолуулах">
            {getFieldDecorator('needCalculation', {
              valuePropName: 'checked',
            })(<Checkbox />)}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}

export default ExpectationCreateModal;
