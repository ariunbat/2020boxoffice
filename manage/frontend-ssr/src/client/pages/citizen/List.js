import React, { Component, Fragment } from 'react';
import {
  Form,
  Table,
  Card,
  Row,
  Col,
  Icon,
  Button,
  Input,
  message,
  Divider,
  Modal,
  DatePicker,
  Tag,
  Tooltip,
  Alert,
  TreeSelect,
  Cascader
} from 'antd';
import { observer, inject } from 'mobx-react';
import PageHeaderWrapper from '../../components/PageHeaderWrapper';
import { Trans } from 'react-i18next';
import { checkAuth } from '../../../common/utils/auth';
import { Link } from 'react-router-dom';
import LocationField from '../Location';
import moment from 'moment';
import withStyles from 'isomorphic-style-loader/withStyles';
import { apiFormat, displayFormat } from '../../../common/utils/dateFormat';
import Description from '../../components/DescriptionList/Description';
import CitizenManDefault from '../../assets/citizenMan.png';
import CitizenWomanDefault from '../../assets/citizenWoman.png';
import ExpectationCreate from './ExpectationCreate';
import styles from './List.less';

const FormItem = Form.Item;
const Confirm = Modal.confirm;
const { RangePicker } = DatePicker;

@Form.create()
@inject('citizenStore', 'expectationStore', 'authStore', 'locationStore')
@observer
class CitizenList extends Component {
  state = {
    loading: false,
    searchFormValues: {},
    expectationFormValues: [],
    expectationModalVisible: false,
  };

  componentDidMount() {
    const { citizenStore, locationStore, authStore } = this.props;
    console.log(authStore);
    citizenStore.clearCurrent();
    locationStore.tree();
    this.refreshList();
  }

  refreshList = (params) => {
    const { citizenStore, authStore, locationStore } = this.props;
    const payload = Object.assign({ boardId: authStore.values.boardId }, params)
    citizenStore.fetchList(params);
  };

  handleTableChange = (pagination) => {
    const { searchFormValues } = this.state;

    const params = {
      currentPage: pagination.current,
      pageSize: pagination.pageSize,
      ...searchFormValues,
    };

    this.refreshList(params);
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      searchFormValues: {},
    });

    this.refreshList();
  };

  handleSearch = e => {
    e.preventDefault();
    const { form } = this.props;
    const { birthDate, locationCode, ...rest } = form.getFieldsValue();

    let birthStart;
    let birthEnd;
    if (birthDate) {
      birthStart = moment(birthDate[0]).format(apiFormat);
      birthEnd = moment(birthDate[1]).format(apiFormat);
    }
    const payload = Object.assign(rest, { birthStart, birthEnd, locationCode: locationCode.join() });

    this.setState({
      searchFormValues: payload,
    });

    this.refreshList(payload);
  };

  showDeleteConfirm = clickedId => {
    const parentMethods = { handleDelete: this.handleDelete };

    Confirm({
      title: 'Анхааруулга',
      content: 'Үйлчилгээ устгах уу?',
      okText: 'Тийм',
      okType: 'danger',
      cancelText: 'Үгүй',
      onOk() {
        parentMethods.handleDelete(clickedId);
      },
    });
  };

  handleDelete = clickedId => {
    const { citizenStore } = this.props;
    citizenStore.delete(Object.assign({ id: clickedId }))
      .then(response => {
        if (response.result === true && response.data) {
          message.success('Үйлчилгээ амжилттай устгалаа');
          this.handleFormReset();
        } else {
          message.error(`Үйлчилгээ устгахад алдаа гарлаа: ${response.message}`);
        }
      }).catch(e => {
      console.log(e);
      message.error(`Үйлчилгээ устгахад алдаа гарлаа: ${e.message}`);
    });
  };

  renderFilterForm = () => {
    const { form, locationStore } = this.props;
    const { getFieldDecorator } = form;

    return (
      <Form onSubmit={this.handleSearch} layout="inline">
        <Row>
          <Col span={20}>
            <FormItem>
              {getFieldDecorator('registerNumber')(<Input placeholder="РД" />)}
            </FormItem>
            <FormItem>
              {getFieldDecorator('firstName')(<Input placeholder="Нэр" />)}
            </FormItem>
            <FormItem>
              {getFieldDecorator('lastName')(<Input placeholder="Эцэг /эх/-ийн нэр" />)}
            </FormItem>
            <FormItem>
              {getFieldDecorator('familyName')(<Input placeholder="Ургийн овог" />)}
            </FormItem>
            <FormItem>
              {getFieldDecorator('locationCode', {
                initialValue: [],
              })(
                <Cascader
                  options={locationStore && locationStore.treeData}
                  placeholder="Засаг захиргаа"
                />,
              )}
            </FormItem>
            <FormItem>
              {getFieldDecorator('birthDate')
              (<RangePicker
                style={{ width: '80%' }}
                format={displayFormat}
                placeholder={['Төрсөн огноо', 'Төрсөн огноо']}
              />)}
            </FormItem>
          </Col>
          <Col span={4}>
            <FormItem>
              <Button type="primary" htmlType="submit">
                Хайх
              </Button>
              <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                Цэвэрлэх
              </Button>
            </FormItem>
          </Col>
        </Row>
      </Form>
    );
  };

  showExpectationForm = record => {
    console.log(record);
    this.setState({
      expectationFormValues: record,
    });
    this.handleExpectationModalVisible(true);
  };

  handleExpectationModalVisible = flag => {
    this.setState({
      expectationModalVisible: !!flag,
    });
  };

  handleExpectationCreate = fields => {
    const { expectationStore } = this.props;
    const { expectationFormValues } = this.state;

    const payload = Object.assign(fields, { citizenId: expectationFormValues.id });

    expectationStore.create(payload).then(response => {
      if (response.result) {
        message.success('Магадлал амжилттай бүртгэлээ');
        this.setState({
          expectationModalVisible: false,
        });

        this.refreshTable();
      } else {
        message.error(`Магадлал шинэчлэхэд алдаа гарлаа: ${response.message}`);
      }
    })
      .catch(e => {
        console.log(e);
        // message.error(`Хэрэглэгчийн мэдээлэл шинэчлэхэд алдаа гарлаа: ${e.message}`);
      });
  };

  renderExpectationCreateForm() {
    const { expectationModalVisible, expectationFormValues } = this.state;

    return (
      <ExpectationCreate
        handleExpectationCreate={this.handleExpectationCreate}
        handleModalVisible={this.handleExpectationModalVisible}
        modalVisible={expectationModalVisible}
        expectationFormValues={expectationFormValues}
      />
    );
  }

  render() {
    const { citizenStore, form } = this.props;
    const { getFieldDecorator } = form;

    const columns = [
      {
        key: 'photo',
        title: 'Зураг',
        dataIndex: 'photo',
        render: (text, record) => <img alt='photo' src={text ? text.url : record.genderName && record.genderName === 'Эр' ?
          CitizenManDefault : CitizenWomanDefault} className={styles.citizenPhoto} />,
      },
      {
        key: 'registerNumber',
        title: 'Регистер',
        dataIndex: 'registerNumber',
      },
      {
        key: 'firstName',
        title: 'Нэр',
        dataIndex: 'firstName',
      },
      {
        key: 'lastName',
        title: 'Овог',
        dataIndex: 'lastName',
      },
      {
        key: 'genderName',
        title: 'Хүйс',
        dataIndex: 'genderName',
      },
      {
        key: 'age',
        title: 'Нас',
        dataIndex: 'age',
        render: text => Math.floor(text),
      },
      {
        key: 'address',
        title: 'Хаяг',
        dataIndex: 'locationCode',
        render: (text, record) => record.locationCode && <div>
          {record.cityName}, {record.districtName} {record.subDistrictName},
          {record.town} {record.apartment} {record.apartmentAddress} {record.street} {record.gerAddress}
          {record.countrySideAddress}
        </div>,
      },
      {
        key: 'actions',
        title: 'Үйлдэл',
        width: '170px',
        render: (text, record) => (
          <Fragment>
            <Tooltip title='Дэлгэрэнгүй'>
              <Button type="dashed" shape="circle">
                <Link to={`citizen/${record.id}`}><Icon type="eye" theme="twoTone" /></Link>
              </Button>
            </Tooltip>
            <Divider type='vertical' />
            <Tooltip title='Засах'>
              <Button type="dashed" shape="circle">
                <Link to={`citizen/edit/${record.id}`}><Icon type="edit" theme="twoTone" /></Link>
              </Button>
            </Tooltip>
            <Divider type='vertical' />
            <Tooltip title='Магадлал бүртгэх'>
              <Button type="dashed" shape="circle" onClick={() => this.showExpectationForm(record)}>
                <Icon type="snippets" theme="twoTone" />
              </Button>
            </Tooltip>
          </Fragment>
        ),
      }];

    const headerActions = (
      checkAuth('ROLE_MANAGE_CITIZEN') ? (
        <Link to='/citizen/create'>
          <Button icon='plus' type='primary'>
            Бүртгэх
          </Button>
        </Link>
      ) : '');

    return (
      <PageHeaderWrapper
        title="Үндсэн жагсаалт"
        action={headerActions}
      >
        <Card>
          <Row gutter={16}>
            <Col span={24}>
              {this.renderFilterForm()}
              <Alert message={`ҮР ДҮН: Нийт бичлэг - ${
                citizenStore &&
                citizenStore.data != null &&
                citizenStore.data.pagination != null &&
                citizenStore.data.pagination.total != null
                  ? citizenStore.data.pagination.total
                  : '-'
              }`} />
              <br />
            </Col>
            <Col span={24}>
              <Table
                rowKey="id"
                loading={citizenStore && citizenStore.loading}
                columns={columns}
                dataSource={citizenStore && citizenStore.data && citizenStore.data.list ? citizenStore.data.list : []}
                pagination={citizenStore && citizenStore.data && citizenStore.data.pagination
                  ? citizenStore.data.pagination : []}
                onChange={this.handleTableChange}
                // scroll={{ x: 1300 }}
                scroll={{ x: 'max-content' }}
              />
            </Col>
          </Row>
        </Card>
        {this.renderExpectationCreateForm()}
      </PageHeaderWrapper>
    );
  }
}

export default withStyles(styles)(CitizenList);
