const Html = (title, initialStates, initialI18nStore, initialLanguage, component) => `
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <title>${title}</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="keyword" content="astvision,astvision starter">

    <!-- OG tags -->
    <meta property="og:title" content="astvision.mn - Astvision Starter">
    <meta property="og:description" name="description" content="Astvision Starter">
    <meta property="og:image" content="http://astvision.mn/static/assets/og_image.jpg">
    <meta property="og:url" content="http://astvision.mn">

<!--    <script src="/static/register-service-worker.js"></script>-->
    <link rel="shortcut icon" href="/static/assets/favicon.png">
    <link rel="manifest" href="/static/assets/manifest.json">
    <link rel="stylesheet" href="/static/assets/vendors~index.js.css">
    <link rel="stylesheet" href="/static/assets/index.js.css">

    <script>
      window.__INITIAL_STATE__ = ${JSON.stringify(initialStates)};
      window.initialI18nStore = ${JSON.stringify(initialI18nStore)};
      window.initialLanguage = '${initialLanguage}';
    </script>
  </head>
  <body>
    <noscript>You need to enable JavaScript to run this app.</noscript>
    <div id="root"><div style="display: none;">${component}</div></div>
    <script src="/static/vendors~index.js"></script>
    <script src="/static/index.js"></script>
    <link rel="stylesheet" href="/static/tinymce/skin.min.css">
    <!--<script src="/static/vendors~multipleRoutes.js"></script>
    <script src="/static/multipleRoutes.js"></script>-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-128745132-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      //gtag('js', new Date());
      //gtag('config', 'UA-128745132-1'); // TODO replace
    </script>
  </body>
  </html>
`;

export default Html;
