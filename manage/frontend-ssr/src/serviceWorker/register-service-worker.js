// console.log('registering service worker');
let newWorker;

if ('serviceWorker' in navigator) {
  let refreshing;

  navigator.serviceWorker.addEventListener('controllerchange', function (event) {
    console.log(navigator.serviceWorker.controller, 'controllerchange');
    //this.controller = navigator.serviceWorker.controller;
    if (refreshing) {
      return
    }
    window.location.reload();
    refreshing = true;
  });
  // navigator.serviceWorker.addEventListener('message', function (event) {
  //   console.log(event, 'message');
  // });

  // navigator.serviceWorker.register('/static/service-worker.js', {scope: '/'})
  navigator.serviceWorker.register('/service-worker.js')
    .then(function (registration) {
      //console.log('Registration successful, scope is:', registration.scope);
      registration.addEventListener('updatefound', event => {
        //console.log(event, 'updatefound');
        newWorker = registration.installing;
        newWorker.addEventListener('statechange', event => {
          //console.log(event, 'statechange');
          if (newWorker.state === 'installed') {
            //console.log(navigator.serviceWorker.controller, 'statechange -> installed');
            if (navigator.serviceWorker.controller) {
              //console.log('request update');
              // new update found -> request update
              newWorker.postMessage({ action: 'skipWaiting' });
            }
          }
        });
      });
    })
    .catch(function (error) {
      //console.log('Service worker registration failed, error:', error);
    });
}
