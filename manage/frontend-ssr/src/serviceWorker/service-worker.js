const CACHE = 'ast-starter-2019-10-16';

self.addEventListener('install', event => {
  // console.log(event, 'sw install');
  event.waitUntil(
    caches.open(CACHE)
      .then(cache => {
        [
          '/static/vendors~index.js',
          '/static/index.js',
          '/static/assets/vendors~index.js.css',
          '/static/assets/index.js.css',
          '/static/assets/favicon.png',
          '/static/assets/manifest.json',
          '/static/assets/og_image.jpg',
          '/locales/en/translations.json',
          '/locales/mn/translations.json',
        ].map(url => {
          return fetch(`${url}?${Math.random()}`).then(response => {
            // fail on 404, 500 etc
            if (!response.ok) throw Error('Not ok');
            return cache.put(url, response);
          })
        });
        }
      )
  );
});

self.addEventListener('activate', event => {
  //console.log(event, 'sw activate');
  // remove outdated cache
  event.waitUntil(
    caches.keys().then(cacheNames => {
      return Promise.all(
        cacheNames.map(cacheName => {
          //console.log('checking cache name: ' + cacheName);
          if (CACHE !== cacheName) {
            //console.log('removing cache: ' + cacheName);
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});

self.addEventListener('fetch', function (event) {
  //console.log(event, 'sw fetch');
  if (event.request.method !== 'GET') {
    return;
  }

  // local request
  // else {
  //   let localUrl = event.request.url.replace(self.location.origin, '');
  //   // TODO skip all API requests
  //   if (localUrl.startsWith('/api/')) {
  //     return;
  //   }
  // }

  event.respondWith(
    caches.match(event.request)
      .then(function (response) {
        //console.log('intercepting: ' + event.request.url + ' -> ' + event.request.url.startsWith(self.location.origin));
        if (response) {
          //console.log('serving from cache -> ' + event.request.url);
          return response;
        } else {
          //console.log('serving from network -> ' + event.request.url);
          return fetch(event.request);
        }
      })
  );
});

self.addEventListener('message', event => {
  // console.log(event, 'sw message');
  if (event.data.action === 'skipWaiting') {
    self.skipWaiting();
  }
});

