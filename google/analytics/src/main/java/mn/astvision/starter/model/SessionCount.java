package mn.astvision.starter.model;

import java.time.LocalDate;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.Indexed;

/**
 *
 * @author digz6
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SessionCount extends BaseEntity {

    @Indexed(unique = true)
    private LocalDate date;

    private int count;
}
