package mn.astvision.starter.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 *
 * @author MethoD
 */
@Configuration
@EnableAsync
@EnableScheduling
public class CronConfig {
}
