package mn.astvision.starter.api.response;

import lombok.Data;

/**
 *
 * @author digz6
 */
@Data
public class SessionCountData {

    private long today;
    private long thisWeek;
    private long thisMonth;
    private long total;
}
