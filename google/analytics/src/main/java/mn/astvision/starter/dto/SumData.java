package mn.astvision.starter.dto;

import lombok.Data;

/**
 *
 * @author digz6
 */
@Data
public class SumData {

    private long total;
}
