package mn.astvision.starter.api;

import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.cron.SessionCountCron;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author digz6
 */
@RestController
@RequestMapping("/api/session-count")
public class SessionCountApi {

    @Autowired
    private SessionCountCron sessionCountCron;

    @GetMapping
    public BaseResponse get() {
        BaseResponse response = new BaseResponse();

        response.setData(sessionCountCron.getSessionCountData());
        response.setResult(true);

        return response;
    }
}
