package mn.astvision.starter.cron;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Optional;
import mn.astvision.starter.api.response.SessionCountData;
import mn.astvision.starter.dao.SessionCountDAO;
import mn.astvision.starter.ga.AnalyticsReportingService;
import mn.astvision.starter.model.SessionCount;
import mn.astvision.starter.repository.SessionCountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author digz6
 */
@Component
public class SessionCountCron {

    private static final Logger LOGGER = LoggerFactory.getLogger(SessionCountCron.class);

    @Autowired
    private SessionCountRepository sessionCountRepository;

    @Autowired
    private SessionCountDAO sessionCountDAO;

    @Autowired
    private AnalyticsReportingService analyticsReportingService;

    private final SessionCountData sessionCountData = new SessionCountData();

    // update every hour
    @Scheduled(initialDelay = 10000, fixedRate = 3600000)
    public void update() {
        LOGGER.info("Updating google analytics counts");
        try {
            SessionCount lastData = sessionCountRepository.findTop1ByOrderByDateDesc();

            LocalDate start;
            if (lastData != null) {
                start = lastData.getDate();
            } else {
                start = AnalyticsReportingService.FIRST_DATE;
            }
            LocalDate end = LocalDate.now();

            for (LocalDate date = start; date.equals(end) || date.isBefore(end); date = date.plusDays(1)) {
                SessionCount sessionCount = sessionCountRepository.findTop1ByDate(date);
                if (sessionCount == null) {
                    sessionCount = new SessionCount();
                    sessionCount.setDate(date);
                }

                Integer count = analyticsReportingService.getSessionCount(date, date);
                if (count != null) {
                    sessionCount.setCount(count);
                }

                sessionCountRepository.save(sessionCount);
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        LOGGER.info("Updated google analytics counts");

        LOGGER.info("Caching google analytics counts");
        try {
            // cache data
            SessionCount lastData = sessionCountRepository.findTop1ByOrderByDateDesc();
            if (lastData != null) {
                sessionCountData.setToday(lastData.getCount());
            }

            LocalDate now = LocalDate.now();

            long thisWeekCount = sessionCountDAO.getSessionCount(now.with(DayOfWeek.MONDAY).minusDays(1), now.with(DayOfWeek.SUNDAY).minusDays(1));
            if (thisWeekCount == 0) {
                thisWeekCount = sessionCountData.getToday();
            }
            sessionCountData.setThisWeek(thisWeekCount);

            long thisMonthCount = sessionCountDAO.getSessionCount(now.withDayOfMonth(1).minusDays(1), now.withDayOfMonth(now.lengthOfMonth()).minusDays(1));
            if (thisMonthCount == 0) {
                thisMonthCount = sessionCountData.getToday();
            }
            sessionCountData.setThisMonth(thisMonthCount);

            sessionCountData.setTotal(sessionCountDAO.getTotalSessionCount());
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        LOGGER.info("Cached google analytics counts");
    }

    public SessionCountData getSessionCountData() {
        return sessionCountData;
    }
}
