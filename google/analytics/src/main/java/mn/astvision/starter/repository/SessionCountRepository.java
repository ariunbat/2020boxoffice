package mn.astvision.starter.repository;

import java.time.LocalDate;
import mn.astvision.starter.model.SessionCount;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author digz6
 */
@Repository
public interface SessionCountRepository extends MongoRepository<SessionCount, LocalDate> {

    public SessionCount findTop1ByOrderByDateDesc();

    public SessionCount findTop1ByDate(LocalDate date);

    //public LocalDate findTop1DateByOrderByDateDesc();

    //public long findTop1CountByOrderByDateDesc();

    //public long countByDateBetween(LocalDate startDate, LocalDate endDate);
}
