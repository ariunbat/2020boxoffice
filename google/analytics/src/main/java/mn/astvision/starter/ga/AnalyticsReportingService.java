package mn.astvision.starter.ga;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.analyticsreporting.v4.AnalyticsReporting;
import com.google.api.services.analyticsreporting.v4.AnalyticsReportingScopes;
//import com.google.api.services.analyticsreporting.v4.model.ColumnHeader;
import com.google.api.services.analyticsreporting.v4.model.DateRange;
import com.google.api.services.analyticsreporting.v4.model.DateRangeValues;
import com.google.api.services.analyticsreporting.v4.model.GetReportsRequest;
import com.google.api.services.analyticsreporting.v4.model.GetReportsResponse;
import com.google.api.services.analyticsreporting.v4.model.Metric;
//import com.google.api.services.analyticsreporting.v4.model.MetricHeaderEntry;
import com.google.api.services.analyticsreporting.v4.model.Report;
import com.google.api.services.analyticsreporting.v4.model.ReportRequest;
import com.google.api.services.analyticsreporting.v4.model.ReportRow;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author digz6
 */
@Service
public class AnalyticsReportingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnalyticsReportingService.class);

    public static final LocalDate FIRST_DATE = LocalDate.of(2019, 3, 1);

    private static final String KEY_FILE_LOCATION = "/ga.json";

    @Value("${analytics.viewId}")
    private String viewId;

    @Value("${analytics.applicationName}")
    private String applicationName;

    private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();

    private AnalyticsReporting analyticsReporting;

    @PostConstruct
    public void init() {
        LOGGER.info("Preparing analytics reporting service");
        try {
            HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            GoogleCredential credential = GoogleCredential
                    .fromStream(AnalyticsReportingService.class.getResourceAsStream(KEY_FILE_LOCATION))
                    .createScoped(Collections.singleton(AnalyticsReportingScopes.ANALYTICS_READONLY));

            // Construct the Analytics Reporting service object
            analyticsReporting = new AnalyticsReporting.Builder(httpTransport, JSON_FACTORY, credential)
                    .setApplicationName(applicationName).build();
        } catch (GeneralSecurityException | IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    private GetReportsResponse getReport(LocalDate start, LocalDate end) throws IOException {
        DateRange dateRange = new DateRange();
        dateRange.setStartDate(start.toString());
        dateRange.setEndDate(end.toString());
        //dateRange.setStartDate("7DaysAgo");
        //dateRange.setEndDate("today");

        Metric sessions = new Metric().setExpression("ga:sessions"); //.setAlias("sessions");

        //Dimension pageTitle = new Dimension().setName("ga:pageTitle");
        // Create the ReportRequest object.
        ReportRequest request = new ReportRequest()
                .setViewId(viewId)
                .setDateRanges(Arrays.asList(dateRange))
                .setMetrics(Arrays.asList(sessions));
                //.setDimensions(Arrays.asList(pageTitle))

        List<ReportRequest> requests = new ArrayList<>();
        requests.add(request);
        GetReportsRequest getReport = new GetReportsRequest().setReportRequests(requests);
        return analyticsReporting.reports().batchGet(getReport).execute();
    }

    public Integer getSessionCount(LocalDate start, LocalDate end) {
        Integer sessions = null;

        try {
            GetReportsResponse response = getReport(start, end);
            Report report = response.getReports().get(0);
            ReportRow row = report.getData().getRows().get(0);
            DateRangeValues metric = row.getMetrics().get(0);

            sessions = Integer.parseInt(metric.getValues().get(0));
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException | IOException | NullPointerException e) {
            LOGGER.error(e.getMessage(), e);
        }

        return sessions;
    }

    /*public void printResponse(GetReportsResponse response) {
        for (Report report : response.getReports()) {
            ColumnHeader header = report.getColumnHeader();
            //List<String> dimensionHeaders = header.getDimensions();
            List<MetricHeaderEntry> metricHeaders = header.getMetricHeader().getMetricHeaderEntries();
            List<ReportRow> rows = report.getData().getRows();

            if (rows == null) {
                LOGGER.info("No data found for " + VIEW_ID);
                return;
            }

            for (ReportRow row : rows) {
                //List<String> dimensions = row.getDimensions();
                List<DateRangeValues> metrics = row.getMetrics();

                //for (int i = 0; i < dimensionHeaders.size() && i < dimensions.size(); i++) {
                //    LOGGER.info(dimensionHeaders.get(i) + ": " + dimensions.get(i));
                //}

                for (int j = 0; j < metrics.size(); j++) {
                    System.out.print("Date Range (" + j + "): ");
                    DateRangeValues values = metrics.get(j);
                    for (int k = 0; k < values.getValues().size() && k < metricHeaders.size(); k++) {
                        LOGGER.info(metricHeaders.get(k).getName() + ": " + values.getValues().get(k));
                    }
                }
            }
        }
    }*/
}
