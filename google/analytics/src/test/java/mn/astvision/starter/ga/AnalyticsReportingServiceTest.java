package mn.astvision.starter.ga;

import java.io.IOException;
import java.time.LocalDate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author digz6
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AnalyticsReportingServiceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnalyticsReportingServiceTest.class);

    @Autowired
    private AnalyticsReportingService analyticsReportingService;

    @Test
    public void testCount() throws IOException {
        LocalDate start = LocalDate.of(2019, 3, 24);
        LocalDate end = LocalDate.of(2019, 3, 24);
        for (LocalDate date = start; date.equals(end) || date.isBefore(end); date = date.plusDays(1)) {
            LOGGER.info("Sessions: " + date + " -> " + analyticsReportingService.getSessionCount(date, date));
        }
    }

    /*@Test
    public void testPrint() throws IOException {
        try {
            GetReportsResponse report = analyticsReportingService.getReport(LocalDate.of(2019, 3, 20), LocalDate.of(2019, 3, 24));
            analyticsReportingService.printResponse(report);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }*/
}
