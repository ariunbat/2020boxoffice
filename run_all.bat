@echo off
REM mvn clean install
SET curpath=%CD%
cd %curpath%\manage\backend & start mvn spring-boot:run
REM cd %curpath%\cdn & start mvn spring-boot:run
REM cd %curpath%\apis\email-api & start mvn spring-boot:run
cd %curpath%\apis\notification-api & start mvn spring-boot:run
REM cd %curpath%\apis\sms-api & start mvn spring-boot:run
REM cd %curpath%\google\analytics & start mvn spring-boot:run
cd %curpath%
