package mn.astvision.starter.api.response.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import mn.astvision.starter.api.response.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import java.util.Locale;

public class BaseController {

  @Autowired
  private MessageSource messageSource;

  @Autowired
  protected ObjectMapper objectMapper;

  public BaseResponse responseError(BaseResponse response, Locale locale, String code){
    response.setResult(false);
    response.setMessage(messageSource.getMessage(code, null, locale));
    return response;
  }

  public BaseResponse responseError(Locale locale, String code){
    BaseResponse response = new BaseResponse();
    response.setResult(false);
    response.setMessage(messageSource.getMessage(code, null, locale));
    return response;
  }

  public BaseResponse responseError(Locale locale, String code, String param){
    BaseResponse response = new BaseResponse();
    response.setResult(false);
    response.setMessage(messageSource.getMessage(code, null, locale) + " : " + param);
    return response;
  }


  public BaseResponse responseData(BaseResponse response, Object data){
    response.setResult(true);
    response.setData(data);
    return response;
  }

  public BaseResponse responseData(Object data){
    BaseResponse response = new BaseResponse();
    response.setResult(true);
    response.setData(data);
    return response;
  }

  public String getMessage(Locale locale, String code) {
    return messageSource.getMessage(code, null, locale);
  }

  public String getMessage(Locale locale, String code, String param) {
    return messageSource.getMessage(code, null, locale) + " : " + param;
  }

}
