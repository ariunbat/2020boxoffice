package mn.astvision.starter.api.response.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mn.astvision.starter.model.auth.BusinessRole;

/**
 *
 * @author MethoD
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthResponse {

    private String token;
    private String userId;
    private BusinessRole businessRole;
    private String boardId;
}
