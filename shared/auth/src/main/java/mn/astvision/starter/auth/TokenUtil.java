package mn.astvision.starter.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.model.enums.DeviceType;
import mn.astvision.starter.repository.auth.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

/**
 *
 * @author MethoD
 */
public class TokenUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(TokenUtil.class);

    @Value("${token.secret}")
    private String secret;

    @Value("${token.expiration}")
    private Long expiration;

    @Autowired
    private UserRepository userRepository;

    public String getUsernameFromToken(String token) {
        String username = null;
        Claims claims = getClaimsFromToken(token);
        //LOGGER.debug("Got claims from token: " + claims);
        if (claims != null) {
            username = claims.getSubject();
        }
        return username;
    }

    public Date getCreatedDateFromToken(String token) {
        Date created = null;
        Claims claims = getClaimsFromToken(token);
        if (claims != null) {
            created = new Date((Long) claims.get("created"));
        }
        return created;
    }

    public Date getExpirationDateFromToken(String token) {
        Date expirationDate = null;
        Claims claims = getClaimsFromToken(token);
        if (claims != null) {
            expirationDate = claims.getExpiration();
        }
        return expirationDate;
    }

    public DeviceType getAudienceFromToken(String token) {
        DeviceType deviceType = DeviceType.UNKNOWN;
        Claims claims = getClaimsFromToken(token);
        if (claims != null && claims.get("audience") != null) {
            try {
                deviceType = DeviceType.valueOf((String) claims.get("audience"));
            } catch (IllegalArgumentException e) {
            }
        }
        return deviceType;
    }

    public Boolean canTokenBeRefreshed(String token, Date lastPasswordReset) {
        final Date created = getCreatedDateFromToken(token);
        return !(isCreatedBeforeLastPasswordReset(created, lastPasswordReset)) && (!(isTokenExpired(token)) || ignoreTokenExpiration(token));
    }

    public String refreshToken(String token) {
        String refreshedToken = null;
        Claims claims = getClaimsFromToken(token);
        if (claims != null) {
            claims.put("created", new Date());
            refreshedToken = generateToken(claims);
        }
        return refreshedToken;
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        String username = getUsernameFromToken(token);
        Date created = getCreatedDateFromToken(token);

        User user = userRepository.findByUsernameAndDeletedFalse(username);
        return user != null && username.equals(user.getUsername()) && !(isTokenExpired(token)) && !(isCreatedBeforeLastPasswordReset(created, user.getPasswordChangeDate()));
    }

    private Claims getClaimsFromToken(String token) {
        Claims claims = null;
        if (!StringUtils.isEmpty(token)) {
            try {
                claims = Jwts.parser()
                        .setSigningKey(secret)
                        .parseClaimsJws(token)
                        .getBody();
            } catch (ExpiredJwtException | MalformedJwtException | SignatureException | UnsupportedJwtException | IllegalArgumentException e) {
                LOGGER.error(e.getMessage());
            }
        }
        return claims;
    }

    private Date generateExpirationDate() {
        return new Date(System.currentTimeMillis() + expiration * 1000);
    }

    private Boolean isTokenExpired(String token) {
        Date expirationdDate = getExpirationDateFromToken(token);
        return expirationdDate.before(new Date());
    }

    private Boolean isCreatedBeforeLastPasswordReset(Date created, Date lastPasswordReset) {
        return lastPasswordReset != null && created.before(lastPasswordReset);
    }

    private Boolean ignoreTokenExpiration(String token) {
        DeviceType deviceType = getAudienceFromToken(token);
        return deviceType == DeviceType.MOBILE || deviceType == DeviceType.TABLET;
    }

    public String generateToken(String username, DeviceType deviceType) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("sub", username);
        claims.put("audience", deviceType.name());
        claims.put("created", new Date());
        return generateToken(claims);
    }

    private String generateToken(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(generateExpirationDate())
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
}
