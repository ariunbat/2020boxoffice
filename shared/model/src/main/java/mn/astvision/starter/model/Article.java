package mn.astvision.starter.model;

import com.querydsl.core.annotations.QueryEntity;
import mn.astvision.starter.model.enums.ArticleType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MethoD
 */
@QueryEntity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class Article extends BaseEntityWithUser {

    private ArticleType articleType;
    private String title;
    private String content;
    private String coverImgUrl;
}
