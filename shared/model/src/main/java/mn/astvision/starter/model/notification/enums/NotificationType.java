package mn.astvision.starter.model.notification.enums;

/**
 *
 * @author MethoD
 */
public enum NotificationType {

    GENERAL,
    //SYSTEM,
    DATA_UPDATE;
}
