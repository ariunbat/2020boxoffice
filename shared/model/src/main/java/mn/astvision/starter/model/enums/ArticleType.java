package mn.astvision.starter.model.enums;

/**
 *
 * @author MethoD
 */
public enum ArticleType {

    NEWS,
    PROJECT,
    KNOWLEDGE_BASE;

    public static ArticleType fromString(String input) {
        ArticleType articleType = null;
        try {
            articleType = ArticleType.valueOf(input);
        } catch (NullPointerException | IllegalArgumentException e) {
        }
        return articleType;
    }
}
