package mn.astvision.starter.repository.party;

import mn.astvision.starter.model.party.Party;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.lang.Nullable;

import java.util.List;

/**
 * @author Ariuka
 */
public interface PartyRepository extends MongoRepository<Party, String>, QuerydslPredicateExecutor<Party> {
    @Nullable
    public Party findByOrderAndDeletedFalse(Integer order);
    @Nullable
    public List<Party> findAllByDeletedFalseOrderByOrder(Sort sort);
}
