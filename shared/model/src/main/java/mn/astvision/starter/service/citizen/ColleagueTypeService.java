package mn.astvision.starter.service.citizen;

import mn.astvision.starter.model.citizen.ColleagueType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Ariuka
 */
@Service
public class ColleagueTypeService {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<ColleagueType> list(Locale locale) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(false)));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<ColleagueType> aggregationResults = mongoTemplate.aggregate(aggregation, "colleagueType", ColleagueType.class);
        return aggregationResults.getMappedResults();
    }
}
