package mn.astvision.starter.repository.citizen;

import mn.astvision.starter.model.citizen.ColleagueType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author Ariuka
 */
@Repository
public interface ColleagueTypeRepository extends MongoRepository<ColleagueType, String>, QuerydslPredicateExecutor<ColleagueType> {
}
