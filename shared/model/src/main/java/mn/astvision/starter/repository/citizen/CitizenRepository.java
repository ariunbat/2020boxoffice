package mn.astvision.starter.repository.citizen;

import mn.astvision.starter.model.citizen.Citizen;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author Ariuka
 */
@Repository
public interface CitizenRepository extends MongoRepository<Citizen, String>, QuerydslPredicateExecutor<Citizen> {
    public Boolean existsByRegisterNumberAndDeletedFalse(String registerNumber);
    public Citizen findByRegisterNumber(String registerNumber);
}
