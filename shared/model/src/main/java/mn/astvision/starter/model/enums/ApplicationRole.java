package mn.astvision.starter.model.enums;

/**
 *
 *
 * @author MethoD
 */
public enum ApplicationRole {

    ROLE_DEFAULT, // view and update profile, upload a file
    ROLE_SEND_EMAIL,

    ROLE_MANAGE_BUSINESS_ROLE,
    ROLE_MANAGE_USER,
    ROLE_MANAGE_ADMIN,
    ROLE_MANAGE_CITIZEN,
    ROLE_MANAGE_MEMBER,
    ROLE_MANAGE_EXPECTATION,

    ROLE_MANAGE_DATA,
    ROLE_MANAGE_ARTICLE;

    public static ApplicationRole[] getDefaultRoles() {
        return new ApplicationRole[]{ROLE_DEFAULT, ROLE_SEND_EMAIL};
    }

    public static ApplicationRole fromString(String input) {
        ApplicationRole applicationRole = null;
        try {
            applicationRole = ApplicationRole.valueOf(input);
        } catch (NullPointerException | IllegalArgumentException e) {
        }
        return applicationRole;
    }
}
