package mn.astvision.starter.repository.citizen;

import mn.astvision.starter.model.citizen.Content;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * @author Ariuka
 */
public interface ContentRepository extends MongoRepository<Content, String>, QuerydslPredicateExecutor<Content> {
}
