package mn.astvision.starter.repository.auth;

import mn.astvision.starter.model.auth.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

/**
 *
 * @author digz6
 */
@Repository
public interface UserRepository extends MongoRepository<User, String>, QuerydslPredicateExecutor<User> {

    public boolean existsByUsernameAndDeletedFalse(String username);

    public boolean existsByUsernameAndIdNotAndDeletedFalse(String username, String id);

    public long deleteByUsername(String username); // for testing

    @Nullable
    public User findByUsernameAndDeletedFalse(String username);
}
