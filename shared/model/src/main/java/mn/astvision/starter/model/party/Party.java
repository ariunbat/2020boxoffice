package mn.astvision.starter.model.party;

import com.querydsl.core.annotations.QueryEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mn.astvision.starter.model.BaseEntityWithUser;
import mn.astvision.starter.model.FileData;
import org.hibernate.validator.constraints.UniqueElements;

/**
 * @author Ariuka
 */
@EqualsAndHashCode(callSuper = true)
@QueryEntity
@Data
public class Party extends BaseEntityWithUser {
    private String name;
    private String description;
    private Integer order;
    private FileData icon;
    private String color;
    private boolean visible;
}
