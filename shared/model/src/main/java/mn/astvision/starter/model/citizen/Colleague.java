package mn.astvision.starter.model.citizen;

import com.querydsl.core.annotations.QueryEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mn.astvision.starter.model.BaseEntityWithUser;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;

/**
 * @author Ariuka
 */
@QueryEntity
@Data
@EqualsAndHashCode(callSuper = true)
public class Colleague extends BaseEntityWithUser {
    @Indexed
    private String citizenId;
    private String colleagueTypeId;
    private String name;
    private String description;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private String participation;
    @Field("colleagueType.name")
    private String colleagueTypeName;
}
