package mn.astvision.starter.repository.citizen;

import mn.astvision.starter.model.citizen.FamilyMember;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author Ariuka
 */
@Repository
public interface FamilyMemberRepository extends MongoRepository<FamilyMember, String>, QuerydslPredicateExecutor<FamilyMember> {
    public Boolean existsByRegisterNumberAndCitizenIdAndDeletedFalse(String registerNumber, String citizenId);
}
