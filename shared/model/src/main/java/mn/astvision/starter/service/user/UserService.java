package mn.astvision.starter.service.user;

import mn.astvision.starter.dao.UserDAO;
import mn.astvision.starter.model.Board;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.repository.board.BoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Ariuka
 */
@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private BoardRepository boardRepository;

    public Iterable<User> list(String businessRole, String search, String boardId, Boolean deleted, Pageable pageable) {
        Iterable<User> users = userDAO.find(businessRole, search, boardId, deleted, pageable);
        for (User user : users) {
            fillRelatedData(user);
        }
        return users;
    }

    private void fillRelatedData(User user) {
        if (user.getBoardId() != null) {
            Optional<Board> boardOptional = boardRepository.findById(user.getBoardId());
            boardOptional.ifPresent(user::setBoard);
        }
    }
}
