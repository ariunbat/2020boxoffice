package mn.astvision.starter.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

/**
 * @author Ariuka
 */
@Data
public class LocationDto {
    @Field("locationId")
    private int value;
    private int parentId;
    private int type;
    @Field("nameMn")
    private String label;
    private boolean isLeaf;
//    private boolean used;

    private String getKey() {
        return value + "";
    };

    private List<LocationDto> children;
}
