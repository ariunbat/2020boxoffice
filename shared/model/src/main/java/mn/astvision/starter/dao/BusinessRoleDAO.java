package mn.astvision.starter.dao;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import mn.astvision.starter.model.auth.BusinessRole;
import mn.astvision.starter.model.auth.QBusinessRole;
import mn.astvision.starter.model.enums.ApplicationRole;
import mn.astvision.starter.repository.auth.BusinessRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

/**
 *
 * @author MethoD
 */
@Repository
public class BusinessRoleDAO {

    @Autowired
    private BusinessRoleRepository businessRoleRepository;

    public long count(String role, ApplicationRole applicationRole) {
        return businessRoleRepository.count(buildPredicate(role, applicationRole));
    }

    public Iterable<BusinessRole> find(String role, ApplicationRole applicationRole, Pageable pageable) {
        return businessRoleRepository.findAll(buildPredicate(role, applicationRole), pageable).getContent();
    }

    private QBusinessRole getQ() {
        return new QBusinessRole("businessRole");
    }

    private Predicate buildPredicate(String role, ApplicationRole applicationRole) {
        QBusinessRole qBusinessRole = getQ();
        BooleanBuilder where = new BooleanBuilder();

        if (!StringUtils.isEmpty(role)) {
            where.and(qBusinessRole.role.containsIgnoreCase(role));
        }
        if (applicationRole != null) {
            where.and(qBusinessRole.applicationRoles.contains(applicationRole));
        }

        return where;
    }
}
