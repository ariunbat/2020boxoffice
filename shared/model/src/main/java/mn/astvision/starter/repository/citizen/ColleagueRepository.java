package mn.astvision.starter.repository.citizen;

import mn.astvision.starter.model.citizen.Colleague;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * @author Ariuka
 */
public interface ColleagueRepository extends MongoRepository<Colleague, String>, QuerydslPredicateExecutor<Colleague> {
}
