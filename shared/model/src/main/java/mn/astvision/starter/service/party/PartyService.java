package mn.astvision.starter.service.party;

import mn.astvision.starter.model.party.Party;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

/**
 * @author Ariuka
 */
@Service
public class PartyService {

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<Party> list(String name, Boolean deleted, Pageable pageable, Locale locale) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(deleted)));

        if (!StringUtils.isEmpty(name)) {
            aggOperations.add(Aggregation.match(Criteria.where("name").regex(name, "i")));
        }

        int skip = pageable.getPageNumber() * pageable.getPageSize();
        aggOperations.add(Aggregation.sort(Sort.Direction.DESC, "createdDate"));
        aggOperations.add(Aggregation.skip(skip));
        aggOperations.add(Aggregation.limit(pageable.getPageSize()));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<Party> aggregationResults = mongoTemplate.aggregate(aggregation, "party", Party.class);
        return aggregationResults.getMappedResults();
    }

    public List<Party> selectList() {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(false)));
        aggOperations.add(Aggregation.match(Criteria.where("visible").is(true)));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<Party> aggregationResults = mongoTemplate.aggregate(aggregation, "party", Party.class);
        return aggregationResults.getMappedResults();
    }

    public Integer count(String name, Boolean deleted, Locale locale) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(deleted)));

        if (!StringUtils.isEmpty(name)) {
            aggOperations.add(Aggregation.match(Criteria.where("name").regex(name, "i")));
        }

        aggOperations.add(group().count().as("count"));
        aggOperations.add(project().andExclude("_id"));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<Party> aggregationResults = mongoTemplate.aggregate(aggregation, "party", Party.class);

        List<Document> totalResult = (List<Document>) aggregationResults.getRawResults().get("results");
        return totalResult.isEmpty() ? 0 : totalResult.get(0).getInteger("count");
    }
}
