package mn.astvision.starter.service.citizen;

import mn.astvision.starter.model.citizen.NameDto;
import mn.astvision.starter.util.GenericAggregationOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ariuka
 */
@Service
public class NameService {

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<NameDto> list(String field, String name) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        if (!StringUtils.isEmpty(name)) {
            aggOperations.add(Aggregation.match(Criteria.where(field).regex("^" + name, "i")));
        }
        aggOperations.add(new GenericAggregationOperation("$group", "{\n" +
                "            \"_id\": \"$" + field + "\",\n" +
                "        }"));

        aggOperations.add(new GenericAggregationOperation("$group", "{\n" +
                "            \"_id\": \"0\",\n" +
                "            \"name\": {\"$push\": \"$_id\"}\n" +
                "        }"));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<NameDto> aggregationResults = mongoTemplate.aggregate(aggregation, "citizen", NameDto.class);

        return aggregationResults.getMappedResults();
    }
}
