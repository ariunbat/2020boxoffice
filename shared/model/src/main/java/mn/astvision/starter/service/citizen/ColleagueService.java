package mn.astvision.starter.service.citizen;

import mn.astvision.starter.model.citizen.Colleague;
import mn.astvision.starter.model.citizen.NameDto;
import mn.astvision.starter.util.GenericAggregationOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Ariuka
 */
@Service
public class ColleagueService {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<Colleague> list(String citizenId, Boolean deleted, Locale locale) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(deleted)));
        aggOperations.add(Aggregation.match(Criteria.where("citizenId").is(citizenId)));

        aggOperations.add(new GenericAggregationOperation("$addFields", "{ \"colleagueType_Id\": { \"$toObjectId\": \"$colleagueTypeId\" }}"));
        aggOperations.add(Aggregation.lookup("colleagueType", "colleagueType_Id", "_id", "colleagueType"));
        aggOperations.add(Aggregation.unwind("colleagueType", true));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<Colleague> aggregationResults = mongoTemplate.aggregate(aggregation, "colleague", Colleague.class);
        return aggregationResults.getMappedResults();
    }

    public List<NameDto> selectList(String field, String name) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        if (!StringUtils.isEmpty(name)) {
            aggOperations.add(Aggregation.match(Criteria.where(field).regex("^" + name, "i")));
        }
        aggOperations.add(new GenericAggregationOperation("$group", "{\n" +
                "            \"_id\": \"$" + field + "\",\n" +
                "        }"));

        aggOperations.add(new GenericAggregationOperation("$group", "{\n" +
                "            \"_id\": \"0\",\n" +
                "            \"name\": {\"$push\": \"$_id\"}\n" +
                "        }"));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<NameDto> aggregationResults = mongoTemplate.aggregate(aggregation, "colleague", NameDto.class);

        return aggregationResults.getMappedResults();
    }
}
