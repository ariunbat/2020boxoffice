package mn.astvision.starter.service.board;

import mn.astvision.starter.model.Board;
import mn.astvision.starter.repository.board.BoardRepository;
import mn.astvision.starter.util.GenericAggregationOperation;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

/**
 * @author Ariuka
 */
@Service
public class BoardService {

    @Autowired
    private BoardRepository boardRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<Board> list(String name, Integer code, Pageable pageable, Locale locale) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(false)));

        if (!StringUtils.isEmpty(name)) {
            aggOperations.add(Aggregation.match(Criteria.where("name").regex(name, "i")));
        }

        if (!StringUtils.isEmpty(code)) {
            aggOperations.add(Aggregation.match(Criteria.where("locationCodes").all(code)));
        }

        aggOperations.add(Aggregation.lookup("location", "locationCodes", "locationId", "locations"));
        aggOperations.add(Aggregation.unwind("locations"));

        aggOperations.add(new GenericAggregationOperation("$group", "{\n" +
                "            _id: \"$_id\",\n" +
                "            \"doc\": { \"$first\": \"$$ROOT\" },\n" +
                "            \"names\": { \"$push\": \"$locations.nameMn\" }\n" +
                "        }"));

        aggOperations.add(new GenericAggregationOperation("$project", "{\n" +
                "            \"name\": \"$doc.name\",\n" +
                "            \"locationCodes\": \"$doc.locationCodes\",\n" +
                "            \"locationNames\": \"$names\",\n" +
                "            \"_id\": \"$doc._id\"\n" +
                "        }"));

        int skip = pageable.getPageNumber() * pageable.getPageSize();
        aggOperations.add(Aggregation.sort(Sort.Direction.ASC, "order"));
        aggOperations.add(Aggregation.skip(skip));
        aggOperations.add(Aggregation.limit(pageable.getPageSize()));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<Board> aggregationResults = mongoTemplate.aggregate(aggregation, "board", Board.class);
        return aggregationResults.getMappedResults();
    }

    public int count(String name, Integer code, Locale locale) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(false)));

        if (!StringUtils.isEmpty(name)) {
            aggOperations.add(Aggregation.match(Criteria.where("name").regex(name, "i")));
        }

        if (!StringUtils.isEmpty(code)) {
            aggOperations.add(Aggregation.match(Criteria.where("locationCodes").all(code)));
        }

        aggOperations.add(group().count().as("count"));
        aggOperations.add(project().andExclude("_id"));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<Board> aggregationResults = mongoTemplate.aggregate(aggregation, "board", Board.class);

        List<Document> totalResult = (List<Document>) aggregationResults.getRawResults().get("results");
        return totalResult.isEmpty() ? 0 : totalResult.get(0).getInteger("count");
    }

}
