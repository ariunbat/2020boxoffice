package mn.astvision.starter.model.citizen.enums;

/**
 * @author Ariuka
 */
public enum ContentType {
    TEXT,
    IMAGE,
    VIDEO,
    URL;
}
