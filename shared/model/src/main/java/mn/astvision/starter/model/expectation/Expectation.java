package mn.astvision.starter.model.expectation;

import com.querydsl.core.annotations.QueryEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mn.astvision.starter.model.BaseEntityWithUser;
import mn.astvision.starter.model.citizen.enums.Gender;

import java.util.List;

/**
 * @author Ariuka
 */
@EqualsAndHashCode(callSuper = true)
@QueryEntity
@Data
public class Expectation extends BaseEntityWithUser {
    private String citizenId;
    private String partyId;
    private Integer percent;
    private String reason;
    private boolean needCalculation;
    private Integer age;
    private String color;
    private String partyName;
    private Gender gender;
    private List<Integer> locationCode;
}
