package mn.astvision.starter.repository;

import mn.astvision.starter.model.AgeCalcDate;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Ariuka
 */
public interface AgeCalcDateRepository extends MongoRepository<AgeCalcDate, String> {
    public AgeCalcDate findFirstByDeletedFalse();
}
