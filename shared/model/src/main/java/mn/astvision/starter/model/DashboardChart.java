package mn.astvision.starter.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
public class DashboardChart {

    @Field(value = "data")
    private String x;
    @Field(value = "count")
    private Integer y;
}

