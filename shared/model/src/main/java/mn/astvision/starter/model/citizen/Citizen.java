package mn.astvision.starter.model.citizen;

import com.querydsl.core.annotations.QueryEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mn.astvision.starter.model.BaseEntityWithUser;
import mn.astvision.starter.model.FileData;
import mn.astvision.starter.model.citizen.enums.AddressType;
import mn.astvision.starter.model.citizen.enums.Gender;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Ariuka
 */
@Data
@QueryEntity
@EqualsAndHashCode(callSuper = true)
public class Citizen extends BaseEntityWithUser {
    private String boardId;
    private String registerNumber;
    private LocalDate birthDate;
    private String familyName;
    private String lastName;
    private String firstName;
    private Gender gender;
    private FileData photo;
    private List<Integer> phone;
    private String email;
    private String socialAccount;
    private String job;
    private List<Integer> birthPlaceCode;
    private String birthPlaceAddress;
    private List<Integer> officialAddressCode;
//    private List<Double> officialAddressAttitude;
    private List<Double> locationAttitude;

    // Хаяг
    @Field("city.nameMn")
    private String cityName;
    @Field("district.nameMn")
    private String districtName;
    @Field("subDistrict.nameMn")
    private String subDistrictName;
    private List<Integer> locationCode;
    private Integer zipCode;
    private AddressType addressType;
    // Байр хороолол
    private String town; // Хороолол/хотхон
    private String apartment; // Байр
    private String apartmentAddress; // Хаягийн нэгж (орц, тоот)

    // Гэр хороолол
    private String street;
    private String gerAddress; // Хаягийн нэгж (тоот)

    // Хөдөө
    private String countrySideAddress; // Хаягын нэгж

    private String age;
    private String genderName;

    // Гишүүнчлэл
    private String partyId;
    private String position;
    private String description;
}
