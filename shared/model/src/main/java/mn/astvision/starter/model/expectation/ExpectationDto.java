package mn.astvision.starter.model.expectation;

import lombok.Data;
import mn.astvision.starter.model.citizen.Citizen;
import mn.astvision.starter.model.party.Party;

/**
 * @author Ariuka
 */
@Data
public class ExpectationDto {
    private Citizen citizen;
    private Party party;
    private String partyId;
    private Integer percent;
    private String reason;
    private boolean needCalculation;
}
