package mn.astvision.starter.repository.notification;

import java.util.List;
import mn.astvision.starter.model.notification.Notification;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

/**
 *
 * @author digz6
 */
@Repository
public interface NotificationRepository extends MongoRepository<Notification, String> {

    public long countByUser(String user);
    public List<Notification> findByUser(String user, Pageable pageable);

    @Query(value = "{'read': false, 'user': ?0}", count = true)
    public long countUnreadByUser(String user);
    @Query("{'read': false, 'user': ?0}")
    public List<Notification> findUnreadByUser(String user, Pageable pageable);

    @Nullable
    public Notification findByIdAndUser(String id, String user);
}
