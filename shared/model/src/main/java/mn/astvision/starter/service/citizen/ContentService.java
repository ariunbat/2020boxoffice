package mn.astvision.starter.service.citizen;

import mn.astvision.starter.model.citizen.Content;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

/**
 * @author Ariuka
 */
@Service
public class ContentService {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<Content> list(String citizenId, Boolean deleted, Pageable pageable, Locale locale) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(deleted)));
        aggOperations.add(Aggregation.match(Criteria.where("citizenId").is(citizenId)));

        int skip = pageable.getPageNumber() * pageable.getPageSize();
        aggOperations.add(Aggregation.sort(Sort.Direction.DESC, "createdDate"));
        aggOperations.add(Aggregation.skip(skip));
        aggOperations.add(Aggregation.limit(pageable.getPageSize()));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<Content> aggregationResults = mongoTemplate.aggregate(aggregation, "content", Content.class);
        return aggregationResults.getMappedResults();
    }

    public Integer count(String citizenId, Boolean deleted, Locale locale) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(deleted)));
        aggOperations.add(Aggregation.match(Criteria.where("citizenId").is(citizenId)));

        aggOperations.add(group().count().as("count"));
        aggOperations.add(project().andExclude("_id"));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<Content> aggregationResults = mongoTemplate.aggregate(aggregation, "content", Content.class);

        List<Document> totalResult = (List<Document>) aggregationResults.getRawResults().get("results");
        return totalResult.isEmpty() ? 0 : totalResult.get(0).getInteger("count");
    }
}
