package mn.astvision.starter.repository.party;

import mn.astvision.starter.model.party.Color;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Ariuka
 */
@Repository
public interface ColorRepository extends MongoRepository<Color, String> {
    public List<Color> findByUsedFalse();
    public Color findByCode(String code);
}
