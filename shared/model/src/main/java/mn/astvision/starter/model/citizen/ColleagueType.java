package mn.astvision.starter.model.citizen;

import com.querydsl.core.annotations.QueryEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mn.astvision.starter.model.BaseEntityWithUser;

/**
 * @author Ariuka
 */
@EqualsAndHashCode(callSuper = true)
@QueryEntity
@Data
public class ColleagueType extends BaseEntityWithUser {
    private String name;
}
