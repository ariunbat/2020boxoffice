package mn.astvision.starter.model.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.querydsl.core.annotations.QueryEntity;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import mn.astvision.starter.model.*;
import org.springframework.data.annotation.Transient;
import org.springframework.lang.Nullable;
//import org.springframework.data.mongodb.core.index.Indexed;

/**
 *
 * @author MethoD
 */
@QueryEntity
@Data
@EqualsAndHashCode(callSuper = true)
@JsonInclude(Include.NON_NULL)
public class User extends BaseEntityWithUser {

    //@Indexed(unique = true)
    @NonNull
    private String username;

    @NonNull
    @JsonIgnore
    private String password;

    // TODO replace with LocalDateTime
    @JsonIgnore
    private Date passwordChangeDate;

    private boolean active;

    private String lastName;
    private String firstName;
    private String familyName;
    private String registerNumber;
    private String email;
    private List<Integer> phone;
    private String description;
    private FileData photo;
    @Nullable
    private String boardId;
    private List<Integer> locationCodes;
    private LocationDto locationData;

    private String businessRole; // DEVELOPER, ADMIN, DATA_SPECIALIST

    @Transient
    public String getFullName() {
        return firstName + " " + lastName;
    }

    @Transient
    public Board board;
}
