package mn.astvision.starter.util;

import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * @author Ariuka
 */
@Component
public class BirthDateGenerator {

    public LocalDate getBirthDateFromRegNum(String registerNumber) {
        String year = registerNumber.substring(2, 4);
        String month = registerNumber.substring(4, 6);
        String day = registerNumber.substring(6, 8);
        if (year.substring(0, 1).equals("1") || year.substring(0, 1).equals("0")) {
            year = "20" + year;
            month = String.format("%02d", Integer.parseInt(month) - 20);
        } else {
            year = "19" + year;
        }
        return LocalDate.parse(year + "-" + month + "-" + day);

    }
}
