package mn.astvision.starter.model.citizen.enums;

/**
 * @author Ariuka
 */
public enum  Gender {
    MALE,
    FEMALE;
}
