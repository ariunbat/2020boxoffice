package mn.astvision.starter.model.citizen;

import lombok.Data;
import mn.astvision.starter.model.FileData;
import mn.astvision.starter.model.citizen.enums.AddressType;
import mn.astvision.starter.model.citizen.enums.Gender;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

/**
 * @author Ariuka
 */
@Data
public class CitizenDto {
    private String id;
    private String registerNumber;
    private String lastName;
    private String firstName;
    private List<Integer> locationCode;
    @Field("city.nameMn")
    private String cityName;
    @Field("district.nameMn")
    private String districtName;
    @Field("subDistrict.nameMn")
    private String subDistrictName;
    private FileData photo;

    // Байр хороолол
    private String town; // Хороолол/хотхон
    private String apartment; // Байр
    private String apartmentAddress; // Хаягийн нэгж (орц, тоот)

    // Гэр хороолол
    private String street;
    private String gerAddress; // Хаягийн нэгж (тоот)

    // Хөдөө
    private String countrySideAddress; // Хаягын нэгж

    private String age;
    private String genderName;

    // Гишүүнчлэл
    private String partyId;
    @Field("party.icon")
    private FileData partyLogo;
    private String position;
    private String description;
}
