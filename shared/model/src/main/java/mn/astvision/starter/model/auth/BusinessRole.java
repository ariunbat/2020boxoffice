package mn.astvision.starter.model.auth;

import com.querydsl.core.annotations.QueryEntity;
import mn.astvision.starter.model.enums.ApplicationRole;
import java.io.Serializable;
import java.util.List;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author MethoD
 */
@QueryEntity
@Document
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class BusinessRole implements Serializable {

    @Id
    @NonNull
    private String role;

    private List<ApplicationRole> applicationRoles;

    @Transient
    public String getKey() {
        return this.role;
    }
}
