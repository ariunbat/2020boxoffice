package mn.astvision.starter.repository;

import mn.astvision.starter.model.Location;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Tergel
 */
@Repository
public interface LocationRepository extends MongoRepository<Location, String> {

    @Nullable
    Location findByLocationId(Integer locationId);

    @Nullable
    List<Location> findByLocationIdIsIn(List<Integer> locationIds);

    @Nullable
    List<Location> findByParentId(Integer parentId);

    @Nullable
    List<Location> findByUsed(Boolean used);
}
