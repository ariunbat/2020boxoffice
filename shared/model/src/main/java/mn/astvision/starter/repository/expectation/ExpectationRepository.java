package mn.astvision.starter.repository.expectation;

import mn.astvision.starter.model.expectation.Expectation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

/**
 * @author Ariuka
 */
@Repository
public interface ExpectationRepository extends MongoRepository<Expectation, String>, QuerydslPredicateExecutor<Expectation> {
}
