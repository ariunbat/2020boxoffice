package mn.astvision.starter.service.citizen;

import mn.astvision.starter.model.AgeCalcDate;
import mn.astvision.starter.model.citizen.Citizen;
import mn.astvision.starter.model.citizen.CitizenDto;
import mn.astvision.starter.repository.AgeCalcDateRepository;
import mn.astvision.starter.util.GenericAggregationOperation;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.query.Criteria.where;

/**
 * @author Ariuka
 */
@Service
public class CitizenService {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private AgeCalcDateRepository ageCalcDateRepository;

    public List<CitizenDto> list(String registerNumber, String firstName, String lastName, String familyName, List<Integer> locationCode,
                                 String boardId, LocalDate birthStart, LocalDate birthEnd, Boolean deleted, Pageable pageable, Locale locale) {
        List<AggregationOperation> aggOperations = new ArrayList<>();
        AgeCalcDate ageCalcDate = ageCalcDateRepository.findFirstByDeletedFalse();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(deleted)));

        if (!StringUtils.isEmpty(registerNumber)) {
            aggOperations.add(Aggregation.match(Criteria.where("registerNumber").regex(registerNumber, "i")));
        }

        if (!StringUtils.isEmpty(firstName)) {
            aggOperations.add(Aggregation.match(Criteria.where("firstName").regex(firstName, "i")));
        }

        if (!StringUtils.isEmpty(lastName)) {
            aggOperations.add(Aggregation.match(Criteria.where("lastName").regex(lastName, "i")));
        }

        if (!StringUtils.isEmpty(familyName)) {
            aggOperations.add(Aggregation.match(Criteria.where("familyName").regex(familyName, "i")));
        }

        if (!locationCode.isEmpty()) {
            aggOperations.add(Aggregation.match(Criteria.where("locationCode").in(locationCode)));
        }

//        if(boardId != null) {
//            aggOperations.add(Aggregation.match(Criteria.where("boardId").is(boardId)));
//        }

        if (birthStart != null && birthEnd != null) {
            aggOperations.add(Aggregation.match(where("birthDate").gte(birthStart.atTime(LocalTime.MIN))));
            aggOperations.add(Aggregation.match(where("birthDate").lte(birthEnd.atTime(LocalTime.MAX))));
        }

        int skip = pageable.getPageNumber() * pageable.getPageSize();
        aggOperations.add(Aggregation.sort(Sort.Direction.DESC, "createdDate"));
        aggOperations.add(Aggregation.skip(skip));
        aggOperations.add(Aggregation.limit(pageable.getPageSize()));

        aggOperations.add(new GenericAggregationOperation("$addFields", "{  \"genderName\": {\n" +
                "                 $cond: { if: { $eq: [ \"$gender\", \"MALE\" ] }, then: '" +
                messageSource.getMessage("GENDER.MALE", null, locale) + "', else: '" +
                messageSource.getMessage("GENDER.FEMALE", null, locale) + "' }\n" +
                "}}"));

        aggOperations.add(new GenericAggregationOperation("$addFields", "{\n" +
                "            checkDate: { $dateFromString: { dateString: \"" + ageCalcDate.getCheckDate() + "\" } }\n" +
                "        }"));

        aggOperations.add(new GenericAggregationOperation("$addFields", "{\n" +
                "            age: {\n" +
                "                $divide: \n" +
                "                [\n" +
                "                    {\n" +
                "                        $subtract: [\"$checkDate\",  \"$birthDate\"]\n" +
                "                    },\n" +
                "                    31536000000\n" +
                "                ]\n" +
                "            }\n" +
                "\n" +
                "        }"));

        aggOperations.add(Aggregation.lookup("location", "zipCode", "locationId", "subDistrict"));
        aggOperations.add(Aggregation.unwind("subDistrict", true));
        aggOperations.add(Aggregation.lookup("location", "subDistrict.parentId", "locationId", "district"));
        aggOperations.add(Aggregation.unwind("district", true));
        aggOperations.add(Aggregation.lookup("location", "district.parentId", "locationId", "city"));
        aggOperations.add(Aggregation.unwind("city", true));


        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<CitizenDto> aggregationResults = mongoTemplate.aggregate(aggregation, "citizen", CitizenDto.class);
        return aggregationResults.getMappedResults();
    }

    public int count(String registerNumber, String firstName, String lastName, String familyName, List<Integer> locationCode,
                     String boardId, LocalDate birthStart, LocalDate birthEnd, Boolean deleted) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(deleted)));

        if (!StringUtils.isEmpty(registerNumber)) {
            aggOperations.add(Aggregation.match(Criteria.where("registerNumber").regex(registerNumber, "i")));
        }

        if (!StringUtils.isEmpty(firstName)) {
            aggOperations.add(Aggregation.match(Criteria.where("firstName").regex(firstName, "i")));
        }

        if (!StringUtils.isEmpty(lastName)) {
            aggOperations.add(Aggregation.match(Criteria.where("lastName").regex(lastName, "i")));
        }

        if (!StringUtils.isEmpty(familyName)) {
            aggOperations.add(Aggregation.match(Criteria.where("familyName").regex(familyName, "i")));
        }

        if (!locationCode.isEmpty()) {
            aggOperations.add(Aggregation.match(Criteria.where("locationCode").in(locationCode)));
        }

        if (birthStart != null && birthEnd != null) {
            aggOperations.add(Aggregation.match(where("birthDate").gte(birthStart.atTime(LocalTime.MIN))));
            aggOperations.add(Aggregation.match(where("birthDate").lte(birthEnd.atTime(LocalTime.MAX))));
        }

//        if(boardId != null) {
//            aggOperations.add(Aggregation.match(Criteria.where("boardId").is(boardId)));
//        }

        aggOperations.add(group().count().as("count"));
        aggOperations.add(project().andExclude("_id"));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<CitizenDto> aggregationResults = mongoTemplate.aggregate(aggregation, "citizen", CitizenDto.class);

        List<Document> totalResult = (List<Document>) aggregationResults.getRawResults().get("results");
        return totalResult.isEmpty() ? 0 : totalResult.get(0).getInteger("count");
    }

    public Citizen get(String id, Locale locale) {
        if (ObjectId.isValid(id)) {
            List<AggregationOperation> aggOperations = new ArrayList<>();

            aggOperations.add(Aggregation.match(Criteria.where("deleted").is(false)));
            aggOperations.add(Aggregation.match(Criteria.where("_id").is(new ObjectId(id))));
            AgeCalcDate ageCalcDate = ageCalcDateRepository.findFirstByDeletedFalse();

            aggOperations.add(new GenericAggregationOperation("$addFields", "{  \"genderName\": {\n" +
                    "                 $cond: { if: { $eq: [ \"$gender\", \"MALE\" ] }, then: '" +
                    messageSource.getMessage("GENDER.MALE", null, locale) + "', else: '" +
                    messageSource.getMessage("GENDER.FEMALE", null, locale) + "' }\n" +
                    "}}"));

            aggOperations.add(new GenericAggregationOperation("$addFields", "{\n" +
                    "            checkDate: { $dateFromString: { dateString: \"" + ageCalcDate.getCheckDate() + "\" } }\n" +
                    "        }"));

            aggOperations.add(new GenericAggregationOperation("$addFields", "{\n" +
                    "            age: {\n" +
                    "                $divide: \n" +
                    "                [\n" +
                    "                    {\n" +
                    "                        $subtract: [\"$checkDate\",  \"$birthDate\"]\n" +
                    "                    },\n" +
                    "                    31536000000\n" +
                    "                ]\n" +
                    "            }\n" +
                    "\n" +
                    "        }"));

            aggOperations.add(Aggregation.lookup("location", "zipCode", "locationId", "subDistrict"));
            aggOperations.add(Aggregation.unwind("subDistrict", true));
            aggOperations.add(Aggregation.lookup("location", "subDistrict.parentId", "locationId", "district"));
            aggOperations.add(Aggregation.unwind("district", true));
            aggOperations.add(Aggregation.lookup("location", "district.parentId", "locationId", "city"));
            aggOperations.add(Aggregation.unwind("city", true));

            Aggregation aggregation = Aggregation.newAggregation(aggOperations);

            AggregationResults<Citizen> aggregationResults = mongoTemplate.aggregate(aggregation, "citizen", Citizen.class);
            return aggregationResults.getMappedResults().size() == 1
                    ? aggregationResults.getMappedResults().get(0) : null;
        } else {
            return null;
        }
    }

    public List<CitizenDto> memberList(String registerNumber, String firstName, String lastName, String familyName, List<Integer> locationCode, String partyId,
                                       String boardId, LocalDate birthStart, LocalDate birthEnd, Boolean deleted, Pageable pageable, Locale locale) {
        List<AggregationOperation> aggOperations = new ArrayList<>();
        AgeCalcDate ageCalcDate = ageCalcDateRepository.findFirstByDeletedFalse();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(deleted)));
        aggOperations.add(Aggregation.match(Criteria.where("partyId").ne(null)));

        if (partyId != null) {
            aggOperations.add(Aggregation.match(Criteria.where("partyId").is(partyId)));
        }

        if (!StringUtils.isEmpty(registerNumber)) {
            aggOperations.add(Aggregation.match(Criteria.where("registerNumber").regex(registerNumber, "i")));
        }

        if (!StringUtils.isEmpty(firstName)) {
            aggOperations.add(Aggregation.match(Criteria.where("firstName").regex(firstName, "i")));
        }

        if (!StringUtils.isEmpty(lastName)) {
            aggOperations.add(Aggregation.match(Criteria.where("lastName").regex(lastName, "i")));
        }

        if (!StringUtils.isEmpty(familyName)) {
            aggOperations.add(Aggregation.match(Criteria.where("familyName").regex(familyName, "i")));
        }

        if (!locationCode.isEmpty()) {
            aggOperations.add(Aggregation.match(Criteria.where("locationCode").in(locationCode)));
        }

        if (birthStart != null && birthEnd != null) {
            aggOperations.add(Aggregation.match(where("birthDate").gte(birthStart.atTime(LocalTime.MIN))));
            aggOperations.add(Aggregation.match(where("birthDate").lte(birthEnd.atTime(LocalTime.MAX))));
        }

//        if (boardId != null) {
//            aggOperations.add(Aggregation.match(Criteria.where("boardId").is(boardId)));
//        }

        int skip = pageable.getPageNumber() * pageable.getPageSize();
        aggOperations.add(Aggregation.sort(Sort.Direction.DESC, "createdDate"));
        aggOperations.add(Aggregation.skip(skip));
        aggOperations.add(Aggregation.limit(pageable.getPageSize()));

        aggOperations.add(new GenericAggregationOperation("$addFields", "{ \"party_id\": { \"$toObjectId\": \"$partyId\" }}"));
        aggOperations.add(Aggregation.lookup("party", "party_id", "_id", "party"));
        aggOperations.add(Aggregation.unwind("party", true));

        aggOperations.add(new GenericAggregationOperation("$addFields", "{  \"genderName\": {\n" +
                "                 $cond: { if: { $eq: [ \"$gender\", \"MALE\" ] }, then: '" +
                messageSource.getMessage("GENDER.MALE", null, locale) + "', else: '" +
                messageSource.getMessage("GENDER.FEMALE", null, locale) + "' }\n" +
                "}}"));

        aggOperations.add(new GenericAggregationOperation("$addFields", "{\n" +
                "            checkDate: { $dateFromString: { dateString: \"" + ageCalcDate.getCheckDate() + "\" } }\n" +
                "        }"));

        aggOperations.add(new GenericAggregationOperation("$addFields", "{\n" +
                "            age: {\n" +
                "                $divide: \n" +
                "                [\n" +
                "                    {\n" +
                "                        $subtract: [\"$checkDate\",  \"$birthDate\"]\n" +
                "                    },\n" +
                "                    31536000000\n" +
                "                ]\n" +
                "            }\n" +
                "\n" +
                "        }"));

        aggOperations.add(Aggregation.lookup("location", "zipCode", "locationId", "subDistrict"));
        aggOperations.add(Aggregation.unwind("subDistrict", true));
        aggOperations.add(Aggregation.lookup("location", "subDistrict.parentId", "locationId", "district"));
        aggOperations.add(Aggregation.unwind("district", true));
        aggOperations.add(Aggregation.lookup("location", "district.parentId", "locationId", "city"));
        aggOperations.add(Aggregation.unwind("city", true));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<CitizenDto> aggregationResults = mongoTemplate.aggregate(aggregation, "citizen", CitizenDto.class);
        return aggregationResults.getMappedResults();
    }

    public int memberCount(String registerNumber, String firstName, String lastName, String familyName, List<Integer> locationCode,
                           String boardId, String partyId, LocalDate birthStart, LocalDate birthEnd, Boolean deleted) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(deleted)));
        aggOperations.add(Aggregation.match(Criteria.where("partyId").ne(null)));

        if (partyId != null) {
            aggOperations.add(Aggregation.match(Criteria.where("partyId").is(partyId)));
        }

        if (!StringUtils.isEmpty(registerNumber)) {
            aggOperations.add(Aggregation.match(Criteria.where("registerNumber").regex(registerNumber, "i")));
        }

        if (!StringUtils.isEmpty(firstName)) {
            aggOperations.add(Aggregation.match(Criteria.where("firstName").regex(firstName, "i")));
        }

        if (!StringUtils.isEmpty(lastName)) {
            aggOperations.add(Aggregation.match(Criteria.where("lastName").regex(lastName, "i")));
        }

        if (!StringUtils.isEmpty(familyName)) {
            aggOperations.add(Aggregation.match(Criteria.where("familyName").regex(familyName, "i")));
        }

        if (!locationCode.isEmpty()) {
            aggOperations.add(Aggregation.match(Criteria.where("locationCode").in(locationCode)));
        }

        if (birthStart != null && birthEnd != null) {
            aggOperations.add(Aggregation.match(where("birthDate").gte(birthStart.atTime(LocalTime.MIN))));
            aggOperations.add(Aggregation.match(where("birthDate").lte(birthEnd.atTime(LocalTime.MAX))));
        }

//        if (boardId != null) {
//            aggOperations.add(Aggregation.match(Criteria.where("boardId").is(boardId)));
//        }

        aggOperations.add(group().count().as("count"));
        aggOperations.add(project().andExclude("_id"));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<CitizenDto> aggregationResults = mongoTemplate.aggregate(aggregation, "citizen", CitizenDto.class);

        List<Document> totalResult = (List<Document>) aggregationResults.getRawResults().get("results");
        return totalResult.isEmpty() ? 0 : totalResult.get(0).getInteger("count");
    }
}
