package mn.astvision.starter.repository;

import java.util.List;
import mn.astvision.starter.model.Article;
import mn.astvision.starter.model.enums.ArticleType;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author digz6
 */
@Repository
public interface ArticleRepository extends MongoRepository<Article, String> {

    public long countByArticleType(ArticleType articleType);
    public List<Article> findByArticleType(ArticleType articleType, Pageable pageable);
}
