package mn.astvision.starter.model.citizen;

import com.querydsl.core.annotations.QueryEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mn.astvision.starter.model.BaseEntityWithUser;
import org.springframework.data.mongodb.core.index.Indexed;

import java.time.LocalDate;
import java.util.List;

/**
 * @author Ariuka
 */
@QueryEntity
@Data
@EqualsAndHashCode(callSuper = true)
public class FamilyMember extends BaseEntityWithUser {
    @Indexed
    private String citizenId;
    private String relationship;
    private String registerNumber;
    private LocalDate birthDate;
    private String age;
    private String familyName;
    private String lastName;
    private String firstName;
    private List<Integer> phone;
    private String email;
    private String job;
    private String socialAccount;
    private String description;
}
