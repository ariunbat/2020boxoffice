package mn.astvision.starter.model.notification;

import com.querydsl.core.annotations.QueryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mn.astvision.starter.model.BaseEntity;
import mn.astvision.starter.model.notification.enums.NotificationType;

/**
 *
 * @author MethoD
 */
@QueryEntity
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class Notification extends BaseEntity {

    private String user; // for user
    private boolean sent;
    private boolean read;

    private NotificationType type;
    private String content;

    // used if type == DATA_UPDATE
    private String entity;
    private String entityId;
    //private String link; // open on click?
}
