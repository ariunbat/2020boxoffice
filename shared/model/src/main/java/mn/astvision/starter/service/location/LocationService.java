package mn.astvision.starter.service.location;

import mn.astvision.starter.model.LocationDto;
import mn.astvision.starter.util.GenericAggregationOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ariuka
 */
@Service
public class LocationService {

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<LocationDto> treeSelectData(List<Integer> locationCodes) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        aggOperations.add(Aggregation.match(Criteria.where("locationId").in(locationCodes)));

        aggOperations.add(Aggregation.lookup("location", "parentId", "locationId", "subDistrict"));
        aggOperations.add(Aggregation.unwind("subDistrict", true));

        aggOperations.add(new GenericAggregationOperation("$group", "{\n" +
                "            _id: \"$subDistrict.locationId\",\n" +
                "            children: { $addToSet: \"$$ROOT\" },\n" +
                "            \"locationId\": { \"$first\": \"$subDistrict.locationId\" },\n" +
                "            \"parentId\": { \"$first\": \"$subDistrict.parentId\" },\n" +
                "            \"nameMn\": { \"$first\": \"$subDistrict.nameMn\" }\n" +
                "        }"));

        aggOperations.add(Aggregation.lookup("location", "parentId", "locationId", "district"));
        aggOperations.add(Aggregation.unwind("district", true));

        aggOperations.add(new GenericAggregationOperation("$group", "{\n" +
                "            _id: \"$district.locationId\",\n" +
                "            children: { $addToSet: \"$$ROOT\" },\n" +
                "            \"locationId\": { \"$first\": \"$district.locationId\" },\n" +
                "            \"parentId\": { \"$first\": \"$district.parentId\" },\n" +
                "            \"nameMn\": { \"$first\": \"$district.nameMn\" }\n" +
                "        }"));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<LocationDto> aggregationResults = mongoTemplate.aggregate(aggregation, "location", LocationDto.class);
        return aggregationResults.getMappedResults();
    }
}
