package mn.astvision.starter.model.enums;

/**
 *
 * @author digz6
 */
public enum DeviceType {

    MOBILE("Mobile Phone"),
    DESKTOP("Desktop"),
    TABLET("Tablet"),
    CONSOLE("Console"),
    TV("TV Device"),
    UNKNOWN("Unknown");

    String value;

    DeviceType(String value) {
        this.value = value;
    }

    public static DeviceType fromString(String input) {
        DeviceType deviceType = DeviceType.UNKNOWN;
        try {
            deviceType = DeviceType.valueOf(input);
        } catch (NullPointerException | IllegalArgumentException e) {
        }
        return deviceType;
    }
}
