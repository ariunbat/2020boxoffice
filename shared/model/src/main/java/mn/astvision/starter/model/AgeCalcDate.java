package mn.astvision.starter.model;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Ariuka
 */
@Data
public class AgeCalcDate extends BaseEntityWithUser {
    private LocalDateTime checkDate;
}
