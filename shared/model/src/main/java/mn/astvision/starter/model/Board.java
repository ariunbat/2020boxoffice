package mn.astvision.starter.model;

import com.querydsl.core.annotations.QueryEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author Ariuka
 */
@EqualsAndHashCode(callSuper = true)
@QueryEntity
@Data
public class Board extends BaseEntityWithUser{
    private String name;
    private List<Integer> locationCodes;
    private List<String> locationNames;
}
