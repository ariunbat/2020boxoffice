package mn.astvision.starter.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Ariuka
 */
@Data
public class FileData implements Serializable {
    private String uid;
    private String name;
    private String url;
    private String status = "done";
}
