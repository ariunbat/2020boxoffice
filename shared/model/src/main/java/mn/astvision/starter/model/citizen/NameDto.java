package mn.astvision.starter.model.citizen;

import lombok.Data;

import java.util.List;

/**
 * @author Ariuka
 */
@Data
public class NameDto {
    private String _id;
    private List<String> name;
}
