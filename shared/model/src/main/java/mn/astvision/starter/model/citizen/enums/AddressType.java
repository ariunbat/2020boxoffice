package mn.astvision.starter.model.citizen.enums;

/**
 * @author Ariuka
 */
public enum  AddressType {
    APARTMENT_DISTRICT,
    GER_DISTRICT,
    COUNTRYSIDE;
}
