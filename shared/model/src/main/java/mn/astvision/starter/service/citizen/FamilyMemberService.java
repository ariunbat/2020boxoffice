package mn.astvision.starter.service.citizen;

import mn.astvision.starter.model.AgeCalcDate;
import mn.astvision.starter.model.citizen.FamilyMember;
import mn.astvision.starter.repository.AgeCalcDateRepository;
import mn.astvision.starter.util.GenericAggregationOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author Ariuka
 */
@Service
public class FamilyMemberService {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private AgeCalcDateRepository ageCalcDateRepository;

    public List<FamilyMember> list(String citizenId, Boolean deleted, Locale locale) {
        List<AggregationOperation> aggOperations = new ArrayList<>();
        AgeCalcDate ageCalcDate = ageCalcDateRepository.findFirstByDeletedFalse();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(deleted)));
        aggOperations.add(Aggregation.match(Criteria.where("citizenId").is(citizenId)));

        aggOperations.add(new GenericAggregationOperation("$addFields", "{\n" +
                "            checkDate: { $dateFromString: { dateString: \"" + ageCalcDate.getCheckDate() + "\" } }\n" +
                "        }"));

        aggOperations.add(new GenericAggregationOperation("$addFields", "{\n" +
                "            age: {\n" +
                "                $divide: \n" +
                "                [\n" +
                "                    {\n" +
                "                        $subtract: [\"$checkDate\",  \"$birthDate\"]\n" +
                "                    },\n" +
                "                    31536000000\n" +
                "                ]\n" +
                "            }\n" +
                "\n" +
                "        }"));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<FamilyMember> aggregationResults = mongoTemplate.aggregate(aggregation, "familyMember", FamilyMember.class);
        return aggregationResults.getMappedResults();
    }
}
