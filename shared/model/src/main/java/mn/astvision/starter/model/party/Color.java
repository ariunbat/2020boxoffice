package mn.astvision.starter.model.party;

import com.querydsl.core.annotations.QueryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mn.astvision.starter.model.BaseEntity;

/**
 * @author Ariuka
 */
@EqualsAndHashCode(callSuper = true)
@QueryEntity
@Data
@AllArgsConstructor
public class Color extends BaseEntity {
    private String code;
    private boolean used;
}
