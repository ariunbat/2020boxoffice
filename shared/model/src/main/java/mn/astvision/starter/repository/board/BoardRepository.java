package mn.astvision.starter.repository.board;

import mn.astvision.starter.model.Board;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

/**
 * @author Ariuka
 */
public interface BoardRepository extends MongoRepository<Board, String>, QuerydslPredicateExecutor<Board> {
}
