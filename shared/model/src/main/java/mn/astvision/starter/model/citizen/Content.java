package mn.astvision.starter.model.citizen;

import com.querydsl.core.annotations.QueryEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import mn.astvision.starter.model.BaseEntityWithUser;
import mn.astvision.starter.model.FileData;
import mn.astvision.starter.model.citizen.enums.ContentType;

import java.util.List;

/**
 * @author Ariuka
 */
@QueryEntity
@Data
@EqualsAndHashCode(callSuper = true)
public class Content extends BaseEntityWithUser {
    private String citizenId;
    private ContentType type;
    private String text;
    private List<FileData> images;
    private FileData video;
    private String url;
    private String description;
}
