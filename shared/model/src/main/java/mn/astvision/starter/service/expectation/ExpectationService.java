package mn.astvision.starter.service.expectation;

import mn.astvision.starter.model.expectation.Expectation;
import mn.astvision.starter.model.expectation.ExpectationDto;
import mn.astvision.starter.util.GenericAggregationOperation;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

/**
 * @author Ariuka
 */
@Service
public class ExpectationService {

    @Autowired
    private MongoTemplate mongoTemplate;

    public List<ExpectationDto> list(Boolean deleted, Pageable pageable, Locale locale) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        aggOperations.add(new GenericAggregationOperation("$addFields", "{ \"citizen_Id\": { \"$toObjectId\": \"$citizenId\" }}"));
        aggOperations.add(Aggregation.lookup("citizen", "citizen_Id", "_id", "citizen"));
        aggOperations.add(Aggregation.unwind("citizen", true));

        int skip = pageable.getPageNumber() * pageable.getPageSize();
        aggOperations.add(Aggregation.sort(Sort.Direction.DESC, "createdDate"));
        aggOperations.add(Aggregation.skip(skip));
        aggOperations.add(Aggregation.limit(pageable.getPageSize()));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<ExpectationDto> aggregationResults = mongoTemplate.aggregate(aggregation, "expectation", ExpectationDto.class);
        return aggregationResults.getMappedResults();
    }

    public Integer count(Boolean deleted, Locale locale) {
        List<AggregationOperation> aggOperations = new ArrayList<>();

        aggOperations.add(Aggregation.match(Criteria.where("deleted").is(deleted)));

        aggOperations.add(group().count().as("count"));
        aggOperations.add(project().andExclude("_id"));

        Aggregation aggregation = Aggregation.newAggregation(aggOperations);

        AggregationResults<Expectation> aggregationResults = mongoTemplate.aggregate(aggregation, "expectation", Expectation.class);

        List<Document> totalResult = (List<Document>) aggregationResults.getRawResults().get("results");
        return totalResult.isEmpty() ? 0 : totalResult.get(0).getInteger("count");
    }
}
