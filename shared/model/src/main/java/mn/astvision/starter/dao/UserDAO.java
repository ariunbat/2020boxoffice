package mn.astvision.starter.dao;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import mn.astvision.starter.model.auth.QUser;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.repository.auth.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

/**
 *
 * @author MethoD
 */
@Repository
public class UserDAO {

    @Autowired
    private UserRepository userRepository;

    public long count(String businessRole, String search, String boardId, Boolean deleted) {
        return userRepository.count(buildPredicate(businessRole, search, boardId, deleted));
    }

    public Iterable<User> find(String businessRole, String search, String boardId, Boolean deleted, Pageable pageable) {
        return userRepository.findAll(buildPredicate(businessRole, search, boardId, deleted), pageable).getContent();
    }

    private QUser getQ() {
        return new QUser("user");
    }

    private Predicate buildPredicate(String businessRole, String search, String boardId, Boolean deleted) {
        QUser qUser = getQ();
        BooleanBuilder where = new BooleanBuilder();

        if (!StringUtils.isEmpty(businessRole)) {
            where.and(qUser.businessRole.eq(businessRole));
        }

        if (!StringUtils.isEmpty(boardId)) {
            where.and(qUser.boardId.eq(boardId));
        }
        if (!StringUtils.isEmpty(search)) {
            where.and(qUser.username.containsIgnoreCase(search))
                    .or(qUser.lastName.containsIgnoreCase(search))
                    .or(qUser.firstName.containsIgnoreCase(search));
        }
        if (deleted != null) {
            where.and(qUser.deleted.eq(deleted));
        }

        return where;
    }
}
