package mn.astvision.starter.api.response;

import lombok.Data;

/**
 *
 * @author MethoD
 * @param <T>
 */
@Data
public class PageableListData<T> {

    private long count;
    private Iterable<T> list;
}
