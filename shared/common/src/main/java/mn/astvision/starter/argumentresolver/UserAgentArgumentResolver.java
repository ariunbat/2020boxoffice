package mn.astvision.starter.argumentresolver;

import com.blueconic.browscap.BrowsCapField;
import com.blueconic.browscap.Capabilities;
import com.blueconic.browscap.ParseException;
import com.blueconic.browscap.UserAgentParser;
import com.blueconic.browscap.UserAgentService;
import java.io.IOException;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 *
 * @author MethoD
 */
public class UserAgentArgumentResolver implements HandlerMethodArgumentResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserAgentArgumentResolver.class);

    private UserAgentParser parser;

    public UserAgentArgumentResolver() {
        try {
            parser = new UserAgentService().loadParser(Arrays.asList(
                    BrowsCapField.BROWSER, BrowsCapField.BROWSER_TYPE, BrowsCapField.BROWSER_MAJOR_VERSION,
                    BrowsCapField.DEVICE_TYPE, BrowsCapField.PLATFORM, BrowsCapField.PLATFORM_VERSION/*,
                    BrowsCapField.RENDERING_ENGINE_VERSION, BrowsCapField.RENDERING_ENGINE_NAME,
                    BrowsCapField.PLATFORM_MAKER, BrowsCapField.RENDERING_ENGINE_MAKER*/));
        } catch (IOException | ParseException e) {
            LOGGER.error("Failed to initialize UserAgentArgumentResolver");
            LOGGER.error(e.getMessage(), e);
        }
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().equals(Capabilities.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter,
            ModelAndViewContainer mavContainer,
            NativeWebRequest webRequest,
            WebDataBinderFactory binderFactory) throws Exception {

        Capabilities caps = null;
        try {
            caps = parser.parse(webRequest.getParameter(parameter.getParameterName()));
        } catch (Exception e) {
        }
        return caps;
    }
}
