package mn.astvision.starter.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 *
 * @author MethoD
 */
public class DateUtil {

    public static LocalDateTime toLocalDate(Date in) {
        return LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
    }
}
