package mn.astvision.starter.util.jaxb;

import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.bind.JAXB;

/**
 *
 * @author MethoD.
 */
public class JaxbXmlParser {

    public static <T> T unmarshal(String xml, Class<T> entityClass) {
        StringReader reader = new StringReader(xml);
        return JAXB.unmarshal(reader, entityClass);
    }

    public static String marshal(Object entity) {
        StringWriter sw = new StringWriter();
        JAXB.marshal(entity, sw);
        return sw.toString();
    }
}
