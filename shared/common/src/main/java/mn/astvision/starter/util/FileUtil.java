package mn.astvision.starter.util;

import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileUtil.class);

    public static boolean createDirectory(String filePath) {
        boolean result = false;

        File file = new File(filePath);
        if (!file.exists()) {
            result = file.mkdirs();
        } else if (file.isDirectory()) {
            result = true;
        }

        if (!result) {
            LOGGER.debug("Create directory failed: " + filePath);
        }
        return result;
    }
}
