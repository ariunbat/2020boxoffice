package mn.astvision.starter.repository;

import mn.astvision.starter.model.Sms;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author digz6
 */
@Repository
public interface SmsRepository extends MongoRepository<Sms, String> {
}
