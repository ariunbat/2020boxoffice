package mn.astvision.starter;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author MethoD
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SmsResponse {

    private String from;
    private String gateway;
    private String response;
    private String errorMessage;
}
