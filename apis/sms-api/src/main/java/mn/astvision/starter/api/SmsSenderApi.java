package mn.astvision.starter.api;

import mn.astvision.starter.SmsResponse;
import mn.astvision.starter.service.MobicomSmsService;
import mn.astvision.starter.service.SkytelSmsService;
import mn.astvision.starter.service.UnitelSmsService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author naba
 */


@RestController
@RequestMapping("/api/sms")
public class SmsSenderApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(SmsSenderApi.class);

    @Value("${sms.unitelFrom}")
    private String unitelFrom;

    @Autowired
    private MobicomSmsService mobicomSmsService;

    @Autowired
    private SkytelSmsService skytelSmsService;

    @Autowired
    private UnitelSmsService unitelSmsService;


    //    @Secured("ROLE_MANAGE_DEFAULT")
    @GetMapping("send-sms")
    public SmsResponse sendSms(String number,String text) {
        SmsResponse smsResponse = new SmsResponse();

        System.out.println("number::::" + number);
        System.out.println("text::::" + text);
        // check number and select gateway
        if (number != null && number.length() == 8) {
            try {
                switch (number.substring(0, 2)) {
                    case "94":
                    case "95":
                    case "99":
                        // mobicom
                        smsResponse.setGateway("mobicom");
                        smsResponse.setFrom("");
                        //smsResponse.setResponse(mobicomSmsApi.send(number, text));
                        smsResponse.setErrorMessage("not implemented");
                        break;
                    case "90":
                    case "91":
                    case "96":
                        // skytel
                        smsResponse.setGateway("skytel");
                        smsResponse.setFrom("");
                        smsResponse.setResponse(skytelSmsService.send(number, text));
                        break;
                    case "80":
                    case "86":
                    case "88":
                    case "89":
                        // unitel
                        smsResponse.setGateway("unitel");
                        smsResponse.setFrom(unitelFrom);
                        smsResponse.setResponse(unitelSmsService.send(number, text));
                        break;
                    case "93":
                    case "97":
                    case "98":
                        // g-mobile
                        smsResponse.setGateway("gmobile");
                        smsResponse.setFrom("");
                        smsResponse.setErrorMessage("not implemented");
                        break;
                }
            } catch (Exception e) {
                smsResponse.setErrorMessage(e.getMessage());
            }
        }

        if (StringUtils.isEmpty(smsResponse.getGateway())) {
            smsResponse.setResponse("wrong number");
        }

        return smsResponse;
    }
}
