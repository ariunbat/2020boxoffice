package mn.astvision.starter.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author MethoD
 */

@Service
public class MobicomSmsService {

    @Value("${sms.mobicomUrl}")
    private String url;

    @Value("${sms.mobicomUsername}")
    private String username;

    @Value("${sms.mobicomPassword}")
    private String password;

    @Value("${sms.mobicomFrom}")
    private String from;

    private final RestTemplate restTemplate;

    public MobicomSmsService() {
        restTemplate = new RestTemplate();
        //List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        //messageConverters.add(new MappingJackson2HttpMessageConverter());
        //restTemplate.setMessageConverters(messageConverters);
    }

    public String send(String number, String text) throws RestClientException {
        String requestUrl = String.format(url,
                username,
                password,
                from,
                number,
                text);
        return restTemplate.getForObject(requestUrl, String.class);
    }
}
