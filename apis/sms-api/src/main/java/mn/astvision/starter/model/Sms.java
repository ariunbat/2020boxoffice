package mn.astvision.starter.model;

import com.querydsl.core.annotations.QueryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 *  naba
 */
@QueryEntity
@Data
@AllArgsConstructor
public class Sms implements java.io.Serializable {

    private Integer id;
    private String fromNumber;
    private String toNumber;
    private String text;
    private Date createdDate;
    private Date scheduledDate;
    private Date sentDate;
    private String gw;
    private int tries;
    private String response;
    private String errorMessage;

}
