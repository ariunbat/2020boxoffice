package mn.astvision.starter.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author naba
 */
@Service
public class UnitelSmsService {

    @Value("${sms.unitelUrl}")
    private String url;

    @Value("${sms.unitelUsername}")
    private String username;

    @Value("${sms.unitelPassword}")
    private String password;

    @Value("${sms.unitelFrom}")
    private String from;

    private final RestTemplate restTemplate;

    public UnitelSmsService() {
        restTemplate = new RestTemplate();
        //List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        //messageConverters.add(new MappingJackson2HttpMessageConverter());
        //restTemplate.setMessageConverters(messageConverters);
    }

    public String send(String number, String text) throws RestClientException {
        String requestUrl = String.format(url,
                username,
                password,
                from,
                number,
                text);
        return restTemplate.getForObject(requestUrl, String.class);
//        System.out.println("requestUrl::::" + requestUrl);
//
//        return "ok";
    }
}
