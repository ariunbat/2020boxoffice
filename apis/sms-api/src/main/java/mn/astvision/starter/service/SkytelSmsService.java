package mn.astvision.starter.service;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 * @author MethoD
 */
@Service
public class SkytelSmsService {

    @Value("${sms.skytelUrl}")
    private String url;

    @Value("${sms.skytelToken}")
    private String token;

    private final RestTemplate restTemplate;

    public SkytelSmsService() {
        restTemplate = new RestTemplate();
        //List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        //messageConverters.add(new MappingJackson2HttpMessageConverter());
        //restTemplate.setMessageConverters(messageConverters);
    }

    public String send(String number, String text) throws RestClientException {
        String requestUrl = String.format(url,
                token,
                number,
                text);
        return restTemplate.getForObject(requestUrl, String.class);
    }
}
