package mn.astvision.starter.api.response;

import java.util.List;
import lombok.Data;
import mn.astvision.starter.model.notification.Notification;

/**
 *
 * @author MethoD
 */
@Data
public class NotificationListData {

    private long unreadCount;
    private long totalCount;

    private List<Notification> list;
}
