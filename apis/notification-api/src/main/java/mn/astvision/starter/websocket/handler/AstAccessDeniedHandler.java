package mn.astvision.starter.websocket.handler;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
//import org.springframework.stereotype.Component;

/**
 *
 * @author MethoD
 */
//@Component
public class AstAccessDeniedHandler implements AccessDeniedHandler {

    @Autowired
    @Qualifier("clientOutboundChannel")
    private MessageChannel clientOutboundChannel;

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException ex) throws IOException, ServletException {
        Message<String> message = new Message<String>() {
            @Override
            public String getPayload() {
                return "Access denied.";
            }

            @Override
            public MessageHeaders getHeaders() {
                return null;
            }
        };

        clientOutboundChannel.send(message);
    }
}
