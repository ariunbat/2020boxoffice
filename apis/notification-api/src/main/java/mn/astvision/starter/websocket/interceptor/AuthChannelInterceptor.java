package mn.astvision.starter.websocket.interceptor;

import mn.astvision.starter.auth.TokenUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 *
 * @author MethoD
 */
@Component
public class AuthChannelInterceptor implements ChannelInterceptor {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthChannelInterceptor.class);

    @Value("${token.header}")
    private String tokenHeader;

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private UserDetailsService userDetailsService;

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

        LOGGER.info("accessor: " + accessor);
        if (accessor != null && StompCommand.CONNECT == accessor.getCommand()) {
            String authToken = accessor.getFirstNativeHeader(tokenHeader);
            LOGGER.info("authToken: " + authToken);

            if (!StringUtils.isEmpty(authToken)) {
                String username = tokenUtil.getUsernameFromToken(authToken);
                LOGGER.info("username: " + username);
                if (username != null) {
                    UserDetails userDetails = userDetailsService.loadUserByUsername(username);

                    if (tokenUtil.validateToken(authToken, userDetails)) {
                        LOGGER.info("token valid!");
                        accessor.setUser(new UsernamePasswordAuthenticationToken(userDetails.getUsername(), null, userDetails.getAuthorities()));
                    }
                }
            }
        }

        return message;
    }
}
