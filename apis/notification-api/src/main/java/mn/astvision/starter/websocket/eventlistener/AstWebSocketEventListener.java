package mn.astvision.starter.websocket.eventlistener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
//import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;

/**
 *
 * @author MethoD
 */
//@Component
public class AstWebSocketEventListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(AstWebSocketEventListener.class);

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        //StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
        LOGGER.info("Received a new web socket connection");
    }
}
