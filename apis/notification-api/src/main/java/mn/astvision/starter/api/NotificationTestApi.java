package mn.astvision.starter.api;

import java.time.LocalDateTime;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.model.notification.Notification;
import mn.astvision.starter.model.notification.enums.NotificationType;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.repository.notification.NotificationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MethoD
 */
@RestController
@RequestMapping("/api/notification-test")
public class NotificationTestApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationTestApi.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    /*@EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
        LOGGER.info("Received a new web socket connection: " + headerAccessor.getLogin());
        LOGGER.info("event: " + event);
    }*/

    @GetMapping("create")
    public BaseResponse create(String username, String content, NotificationType type) {
        BaseResponse response = new BaseResponse();

        User user = userRepository.findByUsernameAndDeletedFalse(username);
        if (user != null) {
            Notification notification = new Notification(user.getId(), true, false, type, content, null, null);
            notification.setCreatedDate(LocalDateTime.now());
            notificationRepository.save(notification);
            response.setResult(true);

            try {
                //messagingTemplate.convertAndSendToUser(username, "/topic/notification", notification);
                messagingTemplate.convertAndSend("/topic/notification/" + user.getId(), notification);
            } catch (MessagingException e) {
                LOGGER.error(e.getMessage(), e);
            }
        }

        return response;
    }
}
