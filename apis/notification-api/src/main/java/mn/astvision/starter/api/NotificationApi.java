package mn.astvision.starter.api;

import java.security.Principal;
import mn.astvision.starter.api.request.Pagination;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.api.response.NotificationListData;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.model.notification.Notification;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.repository.notification.NotificationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MethoD
 */
@Secured("ROLE_DEFAULT")
@RestController
@RequestMapping("/api/notification")
public class NotificationApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationApi.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private NotificationRepository notificationRepository;

    @GetMapping
    public BaseResponse getLatest(Pagination pagination, Principal principal) {
        BaseResponse response = new BaseResponse();

        User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
        if (user != null) {
            NotificationListData notificationListData = new NotificationListData();

            notificationListData.setUnreadCount(notificationRepository.countUnreadByUser(user.getId()));
            notificationListData.setTotalCount(notificationRepository.countByUser(user.getId()));
            notificationListData.setList(notificationRepository.findByUser(
                    user.getId(),
                    PageRequest.of(pagination.getCurrentPage(), pagination.getPageSize(), Sort.Direction.DESC, "createdDate")));

            response.setData(notificationListData);
            response.setResult(true);
        }

        return response;
    }

    @GetMapping("mark-read/{id}")
    public BaseResponse markRead(@PathVariable String id, Principal principal) {
        BaseResponse response = new BaseResponse();

        User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
        if (user != null) {
            Notification notification = notificationRepository.findByIdAndUser(id, user.getId());
            if (notification != null) {
                notification.setRead(true);
                notificationRepository.save(notification);

                response.setResult(true);
            }
        }

        return response;
    }

    @GetMapping("mark-read-all")
    public BaseResponse markReadAll(Principal principal) {
        BaseResponse response = new BaseResponse();

        User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
        if (user != null) {
            // TODO impl
        }

        return response;
    }
}
