package mn.astvision.starter.service;

import java.io.File;
import java.util.List;
import mn.astvision.starter.util.EmailAddressUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

/**
 *
 * @author MethoD
 */
@Service
public class EmailSenderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailSenderService.class);

    @Autowired
    public JavaMailSender mailSender;

    public boolean send(String from, String fromName, String to, String subject, String content, List<File> attachments) {
        LOGGER.debug("Sending email: {from: " + from + ", to: " + to
                + ", subject: " + subject + ", content: " + content + ", attachments: " + attachments + " }");
        boolean result = false;

        if (EmailAddressUtil.isValid(to)) {
            try {
                MimeMessagePreparator messagePreparator = mimeMessage -> {
                    MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
                    messageHelper.setFrom(from, fromName);
                    messageHelper.setTo(to);
                    messageHelper.setSubject(subject);
                    messageHelper.setText(content, true); // html
                    if (attachments != null) {
                        for (File file : attachments) {
                            messageHelper.addAttachment(file.getName(), file);
                        }
                    }
                };
                mailSender.send(messagePreparator);
                result = true;
            } catch (MailException e) {
                LOGGER.error(e.getMessage(), e);
            }
        } else {
            LOGGER.error("Invalid email address: " + to);
        }

        return result;
    }
}
