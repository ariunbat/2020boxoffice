package mn.astvision.starter.api;

import java.io.File;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import mn.astvision.starter.api.response.BaseResponse;
import mn.astvision.starter.model.Email;
import mn.astvision.starter.model.auth.User;
import mn.astvision.starter.repository.EmailRepository;
import mn.astvision.starter.repository.auth.UserRepository;
import mn.astvision.starter.service.EmailSenderService;
import mn.astvision.starter.util.EmailAddressUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author MethoD
 */
@Secured("ROLE_SEND_EMAIL")
@RestController
@RequestMapping("/api/email")
public class EmailApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailApi.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmailRepository emailRepository;

    @Autowired
    private EmailSenderService emailSenderService;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "send", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public BaseResponse uploadImage(
            @RequestParam("from") String from,
            @RequestParam("name") String name,
            @RequestParam("to") String to,
            @RequestParam("subject") String subject,
            @RequestParam("content") String content,
            //@RequestPart("file") MultipartFile file,
            @RequestParam("files") List<MultipartFile> files,
            Principal principal, Locale locale,
            HttpServletRequest servletRequest) {

        BaseResponse baseResponse = new BaseResponse();

        try {
            User user = userRepository.findByUsernameAndDeletedFalse(principal.getName());
            if (user != null) {
                LOGGER.debug("Email send request: {from: " + from + ", to: " + to + ", subject: " + subject + ", content: " + content + "}");

                if (EmailAddressUtil.isValid(from) && EmailAddressUtil.isValid(to)) {
                    Email email = new Email();
                    email.setFrom(from);
                    email.setName(name);
                    email.setTo(to);
                    email.setSubject(subject);
                    email.setContent(content);

                    List<File> attachments = new ArrayList<>();
                    for (MultipartFile multipartFile : files) {
                        // TODO
                        //multipartFile.transferTo(file);
                        //attachments.add(e);
                    }

                    boolean result = emailSenderService.send(from, name, to, subject, content, attachments);
                    email.setResult(result);
                    if (!result) {
                        email.setQueueSend(true); // to queue
                    }

                    email.setResult(result);
                    if (!result) {
                        email.setQueueSend(true); // to queue
                    }

                    email.setCreatedBy(user.getId());
                    email.setCreatedDate(LocalDateTime.now());
                    emailRepository.save(email);

                    baseResponse.setResult(true);
                } else {
                    baseResponse.setMessage("Email address invalid");
                }
            } else {
                baseResponse.setMessage(messageSource.getMessage("error.permission", null, locale));
            }
        } catch (NoSuchMessageException e) {
            LOGGER.error(e.getMessage(), e);
            baseResponse.setMessage(e.getMessage());
        }

        return baseResponse;
    }
}
